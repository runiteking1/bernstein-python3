from scipy.sparse.linalg import LinearOperator, spsolve
from scipy.sparse import csr_matrix, block_diag, lil_matrix
import numpy as np
from scipy.special import binom
from BernsteinFEM import BernsteinMatrix
from combined_preconditioner import Preconditioner

class ThinPlatePreconditioner(LinearOperator):
    def __init__(self, matrix: np.ndarray, fem: BernsteinMatrix, m: int):
        """

        :param matrix: Matrix of the whole thing, sparse
        :param fem:
        :param m:
        """
        super(ThinPlatePreconditioner, self).__init__(dtype=matrix.dtype, shape=matrix.shape)

        self.m = m
        # Need to figure out dimensions of each block
        self.n = int(matrix.shape[0]/m)

        # Create a list of preconditioners
        self.preconds = list()
        counter = 0
        for i in range(m):
            # Extract diagonals from matrix
            self.preconds.append(Preconditioner(matrix[counter:counter + self.n, counter:counter + self.n], fem))

            counter += self.n

    def _matvec(self, x: np.ndarray):
        counter = 0
        out = np.zeros_like(x)
        for i in range(self.m):
            out[counter:counter + self.n] = self.preconds[i].solve(x[counter:counter + self.n])

            counter += self.n

        return out

if __name__ == '__main__':
    pass
