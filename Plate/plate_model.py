import numpy as np
from scipy.sparse.linalg import spsolve, LinearOperator
from scipy.sparse import kron
from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
import CythonizedKernels.nonlinear_c as nonlinear_c
import pcg
from mass_solver import MassSolver
from plate_precond import ThinPlatePreconditioner
import scipy

class Stochastic(BernsteinMatrix):
    def __init__(self, mesh: BernsteinMesh) -> None:
        """
        Creates a
        :param mesh:
        """
        super(Stochastic, self).__init__(mesh)

    def nonlinear(self, u: np.ndarray) -> np.ndarray:
        """
        Nonlinear generator for sin(u)

        :param u: vector to find sin(u) in Bernstein form
        :return: sin(u) in Bernstein form
        """
        out = np.zeros((self.eldof.max() + 1,))

        c = np.zeros((self.p + 1,) * 2)
        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Put u and v into a form that I can evaluate function
            c[np.triu_indices(self.p + 1)] = u[self.eldof[k]][self.mesh.c]
            c = np.fliplr(c)
            c = np.ascontiguousarray(c)

            U = nonlinear_c.evaluate(c, self.q, self.p, 2, self.xis)

            F = np.sin(U)

            outk = 2 * area * nonlinear_c.moment(F, self.q, self.p, 2, self.xis, self.omegas)

            for i in range(self.mesh.dofs_per_element):
                out[self.eldof[k, i]] += outk[self.mesh.lookup_dof(i)[:-1]]

        return out

    def generate_coefficients(self, m: int):
        """

        :param m: Number of coefficients for stochastic portion
        :return:
        """
        # TODO: generate correct coefficients for the plate model

        import scipy.integrate as integrate
        import scipy.special as special
        import scipy.linalg as linalg

        # Use Legendre polynomials
        mass = np.zeros((m, m))
        stiff = np.zeros((m, m))
        for i in range(m):
            for j in range(i, m):
                mass[i, j] = integrate.quad(lambda x: special.eval_legendre(i, x) * special.eval_legendre(j, x), -1, 1)[0]
                mass[j, i] = mass[i, j]

                stiff[i, j] = integrate.quad(lambda x: (i+1)*special.eval_jacobi(i-1, 1, 1, x)/2 * (j+1)*special.eval_jacobi(j-1, 1, 1, x)/2, -1, 1)[0]
                stiff[j, i] = stiff[i, j]
        # print(mass)
        # print(stiff)
        # Now do the general eigenvalue problem
        vals, vectors = linalg.eigh(stiff, mass)

        # for i in range(m):
        #     print(vectors[i].T @ mass @ vectors[i])

        return np.eye(m), np.diag(vals)

        # return np.diag(np.arange(1, m+1)**2), np.eye(m)

    @staticmethod
    def initial(x):
        """
        Initial condition from paper for u(x, y)

        :param x: a point in x, y plane
        :return: initial value as specified in paper
        """
        return 4 * np.arctan(np.exp(x[0] + 1 - 2 / np.cosh(x[1] + 7) - 2 / np.cosh(x[1] - 7)))


def wrapper(p, num_expansions = 5, verbose=False, subdivide = 0):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = Stochastic(mesh)

    S, M, f = fem.assemble_matrix()

    if verbose:
        print("Generated Matrix")

    # These are the eigenvalues of the
    coefs_mass, coefs_stiff = fem.generate_coefficients(num_expansions)
    # print(coefs_stiff)
    # print(coefs_mass)
    A = kron(coefs_mass, S) + kron(coefs_stiff, M)
    # A = S
    # print()

    # Setup preconditioner
    Solver = ThinPlatePreconditioner(A, fem, num_expansions)
    f = A @ np.ones(A.shape[0])

    # Solver.dot(f)
    print(p, num_expansions, end=' ')
    # print(pcg.pcg(A, f)[1], end = ' ' )
    # print(pcg.pcg(A, f, P=Solver)[1])

    def mult_precond(v: np.ndarray) -> np.ndarray:
        return Solver.dot(A @ v)

    PinvS = LinearOperator(dtype=A.dtype, shape=A.shape, matvec=mult_precond)

    w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
                             maxiter=A.shape[0] * 50, ncv=A.shape[0] * 5)
    large = max(np.real(w))

    w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=A.shape[0] * 100,
                             tol=1E-9, ncv=A.shape[0] * 30)
    small = min(np.real(w))

    print("{:1.2f}".format((large / small)), end='\n')


if __name__ == '__main__':
    # Coordinates for a large plate
    coords = np.array([[-100, -100], [0, 0], [100, 100], [100, -100], [-100, 100]])
    elnode = np.array([[1, 2, 5], [1, 4, 2], [2, 4, 3], [3, 5, 2]])
    bc = np.array([[0], [2], [3], [4]])
    elnode -= 1

    np.set_printoptions(precision=3, linewidth=160)
    # Generate
    # wrapper(4, verbose=True, subdivide=0)
    # wrapper(8, verbose=True, subdivide=0)
    # wrapper(12, verbose=True, subdivide=0)
    # wrapper(16, verbose=True, subdivide=0)
    # wrapper(20, verbose=True, subdivide=0)
    #
    # wrapper(4, verbose=True, num_expansions=10, subdivide=0)
    # wrapper(8, verbose=True, num_expansions=10, subdivide=0)
    # wrapper(12, verbose=True, num_expansions=10, subdivide=0)
    # wrapper(16, verbose=True, num_expansions=10, subdivide=0)
    # wrapper(20, verbose=True, num_expansions=10, subdivide=0)
    #
    wrapper( 4, verbose=True, num_expansions=15, subdivide=0)
    wrapper( 8, verbose=True, num_expansions=15, subdivide=0)
    wrapper(12, verbose=True, num_expansions=15, subdivide=0)
    wrapper(16, verbose=True, num_expansions=15, subdivide=0)
    wrapper(20, verbose=True, num_expansions=15, subdivide=0)
