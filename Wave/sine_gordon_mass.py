import numpy as np
from scipy.sparse.linalg import spsolve
from tqdm import tqdm
import matplotlib.pyplot as plt

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
import CythonizedKernels.nonlinear_c as nonlinear_c
import pcg
from mass_preconditioner import MassPreconditioner
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D


class SineGordon(BernsteinMatrix):
    def __init__(self, mesh: BernsteinMesh) -> None:
        """
        Creates a
        :param mesh:
        """
        super(SineGordon, self).__init__(mesh)

    def nonlinear(self, u: np.ndarray) -> np.ndarray:
        """
        Nonlinear generator for sin(u)

        :param u: vector to find sin(u) in Bernstein form
        :return: sin(u) in Bernstein form
        """
        out = np.zeros((self.eldof.max() + 1,))

        c = np.zeros((self.p + 1,) * 2)
        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Put u and v into a form that I can evaluate function
            c[np.triu_indices(self.p + 1)] = u[self.eldof[k]][self.mesh.c]
            c = np.fliplr(c)
            c = np.ascontiguousarray(c)

            U = nonlinear_c.evaluate(c, self.q, self.p, 2, self.xis)

            F = np.sin(U)

            outk = 2 * area * nonlinear_c.moment(F, self.q, self.p, 2, self.xis, self.omegas)

            for i in range(self.mesh.dofs_per_element):
                out[self.eldof[k, i]] += outk[self.mesh.lookup_dof(i)[:-1]]

        return out

    def contour_plot(self, values, save: bool = False, index: int = 0) -> None:
        """
        Simple plotting with inefficient methods

        :param values: vector to plot
        :param save: boolean value to indicate what to save; requires index to be filled if true
        :param index: number to save as name
        """
        sns.set()
        points = self.output_points(values)
        fig = plt.figure()
        ax = Axes3D(fig)

        ax.plot_trisurf(points[:, 0], points[:, 1], np.sin(points[:, 2] / 2), cmap=plt.cm.hot, linewidth=0,
                        edgecolor='none', antialiased=False)
        ax.set_zlim3d(0, 1)
        ax.view_init(40, 125)

        if save:
            plt.savefig('pictures/plot_%04d.png' % (index,), bbox_inches='tight')
            plt.close()
        else:
            plt.show()

    @staticmethod
    def initial(x):
        """
        Initial condition from paper for u(x, y)

        :param x: a point in x, y plane
        :return: initial value as specified in paper
        """
        return 4 * np.arctan(np.exp(x[0] + 1 - 2 / np.cosh(x[1] + 7) - 2 / np.cosh(x[1] - 7)))


def wrapper(p, subdivide, delta_t=0.02, verbose=False, name=None, plot=False, animate=False):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """

    # Setup on output file
    if name is None:
        name = "data/data_%d_%d_%.1e.dat" % (p, subdivide, delta_t)

    if verbose:
        print("p = {}, time step of {}, subdivide of {}".format(p, delta_t, subdivide))

    output = open(name, 'w')

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = SineGordon(mesh)

    S, M, _ = fem.assemble_matrix()

    if verbose:
        print("Generated Matrix")

    # Setup preconditioner
    Solver = MassPreconditioner(M, fem)

    # Set initial conditions
    u_initial_rhs = fem.l2_projection(fem.initial)
    u = spsolve(M, u_initial_rhs)

    if verbose:
        print("Initial Conditions set")

    up = np.zeros(u.shape)  # in our case, u_t is 0 at time 0
    all_its = list()
    print("pre time-step", u.dot(M.dot(u)))
    # for step in tqdm(range(int(np.rint(10 / delta_t)))):  # A nystrom order 4 method, pg 285 from Hairer
    for step in range(100):  # A nystrom order 4 method, pg 285 from Hairer
        # First do nonlinear
        sinu = fem.nonlinear(u)

        k1, it, resid = pcg.pcg(M, -S.dot(u) - sinu, P=Solver, x0=u)
        all_its.append(it)

        u_temp = u + .5 * delta_t * up + .125 * delta_t * delta_t * k1
        sinu = fem.nonlinear(u_temp)
        k2, it, _ = pcg.pcg(M, -S.dot(u_temp) - sinu, P=Solver, x0=k1)
        all_its.append(it)

        u_temp = u + delta_t * up + .5 * delta_t * delta_t * k2
        sinu = fem.nonlinear(u_temp)
        k3, it, _ = pcg.pcg(M, -S.dot(u_temp) - sinu, P=Solver, x0=k2)
        all_its.append(it)

        u = u + delta_t * up + delta_t * delta_t * (k1 / 6.0 + k2 / 3.0)
        up = up + delta_t * (k1 / 6.0 + 2.0 * k2 / 3.0 + k3 / 6.0)

        if verbose:
            print(step, it, resid, file=output)

        if animate:
            fem.contour_plot(u, save=True, index=step)
    print("post time-step", u.dot(M.dot(u)))
    if verbose:
        import statistics
        print("Min, Median, Max: {} {} {}".format(min(all_its), int(statistics.median(all_its)), max(all_its)), end='\n\n')

    if plot:
        fem.plot(u)


def residual_wrapper(p, subdivide, delta_t=0.02, verbose=False, name=None, plot=False, animate=False):
    """
    Wrapper function to plot the residual

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """

    # Setup on output file
    if name is None:
        name = "data/resid_%d.dat" % (p)

    if verbose:
        print("p = {}, time step of {}, subdivide of {}".format(p, delta_t, subdivide))

    output = open(name, 'w')

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = SineGordon(mesh)

    S, M, _ = fem.assemble_matrix()

    if verbose:
        print("Generated Matrix")

    # Setup preconditioner
    Solver = MassPreconditioner(M, fem)
    print(M.shape)
    # Set initial conditions
    u_initial_rhs = fem.l2_projection(fem.initial)
    u = spsolve(M, u_initial_rhs)

    if verbose:
        print("Initial Conditions set")

    up = np.zeros(u.shape)  # in our case, u_t is 0 at time 0

    sinu = fem.nonlinear(u)
    k1, it, resid = pcg.pcg(M, -S.dot(u) - sinu, P=Solver, x0=u)

    for i in resid:
        print(i/np.linalg.norm(-S.dot(u) - sinu), file=output)

    output.close()

    # all_its = list()
    # for step in tqdm(range(int(np.rint(10 / delta_t)))):  # A nystrom order 4 method, pg 285 from Hairer
    #     # First do nonlinear
    #     sinu = fem.nonlinear(u)
    #
    #     k1, it, resid = pcg.pcg(M, -S.dot(u) - sinu, P=Solver, x0=u)
    #     all_its.append(it)
    #
    #     u_temp = u + .5 * delta_t * up + .125 * delta_t * delta_t * k1
    #     sinu = fem.nonlinear(u_temp)
    #     k2, it, _ = pcg.pcg(M, -S.dot(u_temp) - sinu, P=Solver, x0=k1)
    #     all_its.append(it)
    #
    #     u_temp = u + delta_t * up + .5 * delta_t * delta_t * k2
    #     sinu = fem.nonlinear(u_temp)
    #     k3, it, _ = pcg.pcg(M, -S.dot(u_temp) - sinu, P=Solver, x0=k2)
    #     all_its.append(it)
    #
    #     u = u + delta_t * up + delta_t * delta_t * (k1 / 6.0 + k2 / 3.0)
    #     up = up + delta_t * (k1 / 6.0 + 2.0 * k2 / 3.0 + k3 / 6.0)
    #
    #     if verbose:
    #         print(step, it, resid, file=output)
    #
    #     if animate:
    #         fem.contour_plot(u, save=True, index=step)
    #
    # if verbose:
    #     import statistics
    #     print("Min, Median, Max: {} {} {}".format(min(all_its), int(statistics.median(all_its)), max(all_its)), end='\n\n')
    #
    # if plot:
    #     fem.plot(u)


if __name__ == '__main__':
    # Old coordinates
    coords = np.array([[-7, -7], [0, 0], [7, 7], [7, -7], [-7, 7]])
    elnode = np.array([[1, 2, 5], [1, 4, 2], [2, 4, 3], [3, 5, 2]])
    bc = np.array([])
    elnode -= 1

    print("Mass preconditioner! Not the most efficient")
    # residual_wrapper(4, 1, verbose=True, animate=False, plot=False, delta_t = 0.01)
    # residual_wrapper(8, 1, verbose=True, animate=False, plot=False, delta_t = 0.01)
    # residual_wrapper(12, 1, verbose=True, animate=False, plot=False, delta_t = 0.01)
    # residual_wrapper(16, 1, verbose=True, animate=False, plot=False, delta_t = 0.01)
    # residual_wrapper(20, 1, verbose=True, animate=False, plot=False, delta_t = 0.01)

    # Generate
    wrapper(6, 0, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(4, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(8, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(8, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(12, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(12, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(16, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(16, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(20, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(20, 2, verbose=True, animate=False, plot=False, delta_t=0.01)

    # wrapper(4, 3, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(8, 3, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(12, 3, verbose=True, animate=False, plot=False, delta_t=0.01)
    # wrapper(16, 3, verbose=True, animate=False, plot=False, delta_t=0.01)