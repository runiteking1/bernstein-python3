import numpy as np
from scipy.sparse.linalg import spsolve
from tqdm import tqdm
import matplotlib.pyplot as plt

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
import CythonizedKernels.nonlinear_c as nonlinear_c
import pcg
from mass_preconditioner import MassPreconditioner
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D


class LinearWave(BernsteinMatrix):
    def __init__(self, mesh: BernsteinMesh) -> None:
        """
        Creates a
        :param mesh:
        """
        super(LinearWave, self).__init__(mesh)

    def contour_plot(self, values, save: bool = False, index: int = 0) -> None:
        """
        Simple plotting with inefficient methods

        :param values: vector to plot
        :param save: boolean value to indicate what to save; requires index to be filled if true
        :param index: number to save as name
        """
        sns.set()
        points = self.output_points(values)
        fig = plt.figure()
        ax = Axes3D(fig)

        ax.plot_trisurf(points[:, 0], points[:, 1], np.sin(points[:, 2] / 2), cmap=plt.cm.hot, linewidth=0,
                        edgecolor='none', antialiased=False)
        ax.set_zlim3d(0, 1)
        ax.view_init(40, 125)

        if save:
            plt.savefig('pictures/plot_%04d.png' % (index,), bbox_inches='tight')
            plt.close()
        else:
            plt.show()

    @staticmethod
    def initial(x):
        """
        Initial condition from paper for u(x, y)

        :param x: a point in x, y plane
        :return: initial value as specified in paper
        """
        return 4 * np.arctan(np.exp(x[0] + 1 - 2 / np.cosh(x[1] + 7) - 2 / np.cosh(x[1] - 7)))


def wrapper(p, subdivide, delta_t=0.02, verbose=False, name=None, plot=False, animate=False):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """

    # Setup on output file
    if name is None:
        name = "data/data_%d_%d_%.1e.dat" % (p, subdivide, delta_t)

    if verbose:
        print("p = {}, time step of {}, subdivide of {}".format(p, delta_t, subdivide))

    output = open(name, 'w')

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = LinearWave(mesh)

    S, M, _ = fem.assemble_matrix()

    if verbose:
        print("Generated Matrix")

    # Setup preconditioner
    Solver = MassPreconditioner(M, fem)

    # Set initial conditions
    u_initial_rhs = fem.l2_projection(fem.initial)
    u = spsolve(M, u_initial_rhs)
    print(M.shape)

    if verbose:
        print("Initial Conditions set")

    up = np.zeros(u.shape)  # in our case, u_t is 0 at time 0
    # average_it = 0
    all_its = list()
    for step in tqdm(range(int(np.rint(10 / delta_t)))):  # A nystrom order 4 method, pg 285 from Hairer
        k1, it, resid = pcg.pcg(M, -S.dot(u), P=Solver, x0=u)
        all_its.append(it)

        u_temp = u + .5 * delta_t * up + .125 * delta_t * delta_t * k1
        k2, it, _ = pcg.pcg(M, -S.dot(u_temp), P=Solver, x0=k1)
        all_its.append(it)

        u_temp = u + delta_t * up + .5 * delta_t * delta_t * k2
        k3, it, _ = pcg.pcg(M, -S.dot(u_temp), P=Solver, x0=k2)
        all_its.append(it)

        u = u + delta_t * up + delta_t * delta_t * (k1 / 6.0 + k2 / 3.0)
        up = up + delta_t * (k1 / 6.0 + 2.0 * k2 / 3.0 + k3 / 6.0)

        if verbose:
            print(step, it, resid, file=output)
            # average_it += it

        if animate:
            fem.plot(u, save=True, index=step)

    if verbose:
        import statistics
        print("Min, Median, Max: {} {} {}".format(min(all_its), statistics.median(all_its), max(all_its)))
        # print("Average iteration count: {}".format(average_it / (10 / delta_t)))

    if plot:
        fem.plot(u)


if __name__ == '__main__':
    # Old coordinates
    coords = np.array([[-7, -7], [0, 0], [7, 7], [7, -7], [-7, 7]])
    elnode = np.array([[1, 5, 2], [1, 4, 2], [3, 4, 2], [3, 5, 2]])
    bc = np.array([])
    elnode -= 1

    print("Mass preconditioner! Not the most efficient")

    # Generate
    wrapper(4, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(4, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(8, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(8, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(12, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(12, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(16, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(16, 2, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(20, 1, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(20, 2, verbose=True, animate=False, plot=False, delta_t=0.01)

    wrapper(4, 3, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(8, 3, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(12, 3, verbose=True, animate=False, plot=False, delta_t=0.01)
    wrapper(16, 3, verbose=True, animate=True, plot=False, delta_t=0.01)
    # wrapper(20, 3, verbose=True, animate=True, plot=False, delta_t=0.005)
