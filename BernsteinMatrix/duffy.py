def duffy(t, x):
    """Duffy transformation for 2D"""
    return x[:, [0]] * t[0] + x[:, [1]] * (t[1] * (1 - t[0])) + x[:, [2]] * (1 - t[0] - (t[1] * (1 - t[0])))


def duffy_1d(t, x):
    """Duffy transformation for 1D"""
    return x[:, [0]] * t + x[:, [1]] * (1 - t)


if __name__ == '__main__':
    import numpy as np

    xs = np.zeros((2, 3))

    xs[:, 0] = [0, 0.75]
    xs[:, 1] = [-.75, -.5]
    xs[:, 2] = [0.75, -.455]

    ts = np.array([[.4], [.4]])

    print(duffy(ts, xs))  # For this, expect .09, .0162
