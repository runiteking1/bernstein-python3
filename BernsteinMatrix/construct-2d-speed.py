import numpy as np


def binomial(m: int, n: int) -> np.ndarray:
    c = np.zeros(m + 1, n + 1)
    for p in range(m + 1):
        c[p, 0] = 1
    for q in range(n + 1):
        c[0, q] = 1
    for p in range(m + 1):
        for q in range(n + 1):
            c[p, q] += c[p, q - 1] + c[p - 1, q]

    return c
