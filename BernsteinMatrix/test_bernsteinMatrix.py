from unittest import TestCase
import numpy as np
from BernsteinFEM import BernsteinMatrix, BernsteinMesh


class TestBernsteinMatrix(TestCase):
    def test_assemble_matrix(self):
        # Johnson mesh
        coords_john = np.array(
            [[1 / 4, 1 / 4], [0, 5 / 4], [1, 0], [1, 3 / 4], [1, 3 / 2], [3, 0], [5 / 2, 1 / 2], [3 / 2, 1], [2, 3 / 2],
             [3, 1 / 2]])
        elnode_john = np.array(
            [[1, 3, 4], [1, 4, 2], [4, 5, 2], [3, 8, 4], [4, 8, 5], [8, 9, 5], [6, 7, 3], [3, 7, 8], [8, 7, 9],
             [7, 10, 9], [6, 10, 7]])
        bc_john = np.array([[1], [2], [3], [5], [6], [9], [10]])
        elnode_john -= 1
        bc_john[:, 0] -= 1

        mesh = BernsteinMesh(coords_john, elnode_john, bc_john, p=8, subdiv=1)
        b = BernsteinMatrix(mesh)

        s, m, f = b.assemble_matrix()

        alphas = np.linalg.solve(s.todense(), f)
        np.testing.assert_almost_equal(np.dot(f.T, alphas), 0.3943, decimal=3)
