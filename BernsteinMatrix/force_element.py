import numpy as np
import itertools
from scipy import special
from duffy import duffy
from index_set import index_set


def moment(f0, q, n, d, xis, omegas):
    """
    Evaluates a polynomial in Bernstein form at the Stroud nodes (see algorithm 3 in optimal assembly paper)

    :type f0: np.ndarray
    :type q: int
    :type n: int
    :type d: int
    :type xis: np.ndarray
    :type omegas: np.ndarray
    :param f0: values of function f at Stroud nodes
    :param q: positive integer
    :param n: order
    :param d: dimension
    :return: q by q matrix
    """
    for l in range(1, d + 1):
        f0 = moment_step(f0, l, q, n, d, xis, omegas)
        print(f0)

    return f0


def moment_step(fin, l, q, n, d, xis, omegas):
    """
    Algorithm 4 of AAD paper

    :param fin:
    :param l:
    :param q:
    :param n:
    :param d:
    :param xis:
    :param omegas:
    :return:
    """
    f_out = np.zeros((n + 1,) * l + (q,) * (d - l))  # Create the output array
    for i_l in range(q):
        xi = xis[d - l, i_l]
        omega = omegas[d - l, i_l]

        s = 1 - xi
        r = xi / s
        for alphas in index_set(n, l):
            alphas = alphas[:-1]  # exclude last element
            w = omega * (s ** (n - sum(alphas)))
            for alpha_l in range(n - sum(alphas) + 1):
                for i_list in itertools.product(range(q), repeat=d - l):
                    f_out[alphas + (alpha_l,) + i_list] += w * fin[alphas + (i_l,) + i_list]

                w *= r * (n - sum(alphas) - alpha_l) / (1.0 + alpha_l)
    return f_out


def f(x):
    """
    A bernstein function on
    x[:,0] = [0,0]
    x[:,1] = [1,0]
    x[:,2] = [0,1]

     6 *x[0] * x[1] * (1 - x[0] - x[1]) +
    """
    return 3 * x[0] * x[0] * x[1]

def duffy3d(t: np.ndarray, x: np.ndarray) -> np.ndarray:
    """Duffy transformation for 3D"""
    l1 = t[0]
    l2 = t[1] * (1 - l1)
    l3 = t[2] * (1 - l1 - l2)
    l4 = 1 - l1 - l2 - l3
    return x[:, [0]] * l1 + x[:, [1]] * l2 + x[:, [2]] * l3 + x[:, [3]] * l4


if __name__ == "__main__":
    vertices = np.zeros((2, 3))
    vertices[:, 0] = [0, 0]
    vertices[:, 1] = [1, 0]
    vertices[:, 2] = [0, 1]

    # First, generate the quadrature points
    my_q = 5
    my_xis = np.zeros((2, my_q))
    my_omegas = np.zeros((2, my_q))
    for i in range(2):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    fa = np.zeros((my_q, my_q))
    for i in range(my_q):
        for j in range(my_q):
            fa[i, j] = f(duffy([my_xis[1, i], my_xis[0, j]], vertices))

    # Return moment (need to scale correctly with area)
    # print((.75 * .75 / 2) * 2 * moment(fa, my_q, 2, 2, my_xis, my_omegas))
    # Expected Result: [[ 0.00401786  0.00803571  0.00803571]
    #                   [ 0.00267857  0.00401786  0.        ]
    #                   [ 0.00133929  0.          0.        ]]

    vertices = np.zeros((3, 4))
    vertices[:, 0] = [0, 0, 0]
    vertices[:, 1] = [1, 0, 0]
    vertices[:, 2] = [0, 1, 0]
    vertices[:, 3] = [0, 0, 1]

    # First, generate the quadrature points
    my_q = 5
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    fa = np.zeros((my_q, my_q, my_q))
    for i in range(my_q):
        for j in range(my_q):
            for k in range(my_q):
                fa[i, j, k] = f(duffy3d([my_xis[2, i], my_xis[1, j], my_xis[0, k]], vertices))

    # Return moment (need to scale correctly with area)
    print(moment(fa, my_q, 2, 3, my_xis, my_omegas))
    # Expected Result: [[ 0.00401786  0.00803571  0.00803571]
    #                   [ 0.00267857  0.00401786  0.        ]
    #                   [ 0.00133929  0.          0.        ]]
