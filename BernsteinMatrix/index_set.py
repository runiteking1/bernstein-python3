def index_set(n: int, d: int):
    """
    Generates I^n_{d-1}

    :param n: sum of tuple
    :param d: dimension of the set
    :return: a generator of tuples
    """
    if d == 1:
        yield (n,)
    elif d == 0:
        yield ()
    else:
        for i in range(n + 1):  # Loop through all possible in the sum
            for j in index_set(n - i, d - 1):  # Recursively call
                yield (i,) + j
