from multinomial import Multinomial


class Mass(Multinomial):
    """
    Mass matrix calculation helper based on multinomials (see algorithm 6 in optimal assembly paper)
    """

    def __init__(self, n: int, d: int):
        super(Mass, self).__init__(n, n, d)
        self.mu = None

    def increment(self, alphas, alpha, sum_alpha, betas, beta, sum_beta, w):
        wd1 = w * self.c[self.m - sum_alpha - alpha + self.n - sum_beta - beta, self.m - sum_alpha - alpha] * \
              self.c[alpha + beta, alpha]

        self.A[alphas + (alpha, self.m - sum_alpha - alpha) + betas + (beta, self.n - sum_beta - beta)] = wd1 * self.mu

    def loop(self, sum_alpha, sum_beta, w, alphas, betas):
        """
        Recursive defintion of the for loops;

        Specifically for 2D case
        """
        for alpha_1 in range(self.m + 1):
            for beta_1 in range(self.n + 1):
                for alpha_2 in range(self.m - alpha_1 + 1):
                    for beta_2 in range(self.n - beta_1 + 1):
                        self.increment((alpha_1,), alpha_2, alpha_1, (beta_1,), beta_2, beta_1,
                                       self.c[alpha_1 + beta_1, alpha_1] / self.c[self.m + self.n, self.m])

    def mass_struct(self, mu):
        self.mu = mu
        self.loop(0, 0, 1, tuple(), tuple())
        return self.A


def mass_mat(n, area):
    # Calculate mu^(2n)(1)
    mu = area / ((n + 1) * (2 * n + 1))
    test = Mass(n, 2)
    test.mass_struct(mu)
    return test.A


if __name__ == "__main__":
    print(mass_mat(2, .5)[(1, 0, 1, 0, 1, 1)])  # Should be .0111111111
