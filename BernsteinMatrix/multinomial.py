import numpy as np
from scipy import special
from sparray import sparray

class Multinomial(object):
    """
    Class to generate multinomial coefficients; (too many global variables and lookup tables to not have in a class)

    Algorithm 5 in optimal assembly paper
    """
    def __init__(self, m: int, n: int, d: int):
        """
        :type m: int
        :type n: int
        :type d: int
        :param d: dimension
        """
        self.m = m
        self.n = n
        self.d = d

        # Generate table of combinatorial values (precomputed binomial coefficients)
        self.c = np.zeros((self.m + 1 + self.n, self.m + 1))
        for i in range(m + 1):
            for j in range(n + 1):
                self.c[i + j, i] = special.binom(i + j, i)

        # Output
        # print((self.m + 1,) * (self.d + 1) + (self.n + 1,) * (self.d + 1))
        # self.A = np.zeros((self.m + 1,) * (self.d + 1) + (self.n + 1,) * (self.d + 1))
        self.A = sparray((self.m + 1,) * (self.d + 1) + (self.n + 1,) * (self.d + 1),default=0)

    def multinomial(self):
        self.loop(0, 0, 1, tuple(), tuple())

    def loop(self, sum_alpha, sum_beta, w, alphas, betas):
        """
        Recursive defintion of the for loops; DO NOT CHANGE.

        For General case
        """
        count_d = len(alphas) + 1
        for alpha in range(self.m - sum_alpha + 1):
            for beta in range(self.n - sum_beta + 1):
                if count_d == self.d:
                    self.increment(alphas, alpha, sum_alpha, betas, beta, sum_beta, w)
                else:
                    if count_d == 1:
                        self.loop(sum_alpha + alpha, sum_beta + beta,
                                  w * self.c[alpha + beta, alpha] / self.c[self.m + self.n, self.m],
                                  alphas + (alpha,), betas + (beta,))
                    else:
                        self.loop(sum_alpha + alpha, sum_beta + beta,
                                  w * self.c[alpha + beta, alpha],
                                  alphas + (alpha,), betas + (beta,))

    def increment(self, alphas, alpha, sum_alpha, betas, beta, sum_beta, w):
        self.A[alphas + (alpha, self.m - sum_alpha - alpha) + betas + (beta, self.n - sum_beta - beta)] = \
            w * self.c[self.m - sum_alpha - alpha + self.n - sum_beta - beta, self.m - sum_alpha - alpha] * \
            self.c[alpha + beta, alpha]


if __name__ == "__main__":
    test = Multinomial(3, 3, 2)
    test.multinomial()
    print(test.A[2, 0, 1, 1, 2, 0])  # Should be .15
