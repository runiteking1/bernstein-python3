from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D
from scipy import special
from scipy.sparse import lil_matrix, csr_matrix, dok_matrix, dia_matrix

from bernstein_mesh import BernsteinMesh
from duffy import duffy
from mass_element import Mass
from stiffness_element import Stiffness
import CythonizedKernels.nonlinear_c as nonlinear_c
from pyevtk.hl import unstructuredGridToVTK
from pyevtk.vtk import VtkTriangle


class BernsteinMatrix(object):
    """
    Creates a Bernstein FEM object which deals with the matrix creations for 2D, H1 conforming
    """

    def __init__(self, mymesh: BernsteinMesh):
        """
        :param mymesh:
        :type mymesh: BernsteinMesh
        """
        self.mesh = mymesh
        self.p = self.mesh.p
        self.eldof = self.mesh.eldof
        self.elnode = self.mesh.elnode
        self.elements = self.mesh.elements
        self.coordinates = self.mesh.coordinates

        # Set up quadrature points
        self.q = mymesh.p + 2
        self.xis = np.zeros((2, self.q))
        self.omegas = np.zeros((2, self.q))
        for i in range(2):
            nodes, weights = special.roots_sh_jacobi(self.q, i + 1, 1)
            self.xis[i, :] = nodes
            self.omegas[i, :] = weights

        # Create personal multinomial subclasses so we don't create a lot of classes
        self.mass_multinomial = Mass(self.p, 2)
        self.stiff_multinomial = Stiffness(self.p, 2)

        # For plotting purposes
        self.bary1, self.bary2, self.bary3 = self.generate_bary()
        self.triangulation = np.array(self.return_triangulation())
        self.mplot = False

    def assemble_matrix(self) -> (csr_matrix, csr_matrix, np.ndarray):
        """
        Generates the mass and stiffness matrix

        :return Stiffness, Mass, Force
        """
        stiffness_raw = lil_matrix((self.eldof.max() + 1, self.eldof.max() + 1), dtype=np.double)
        mass_raw = lil_matrix((self.eldof.max() + 1, self.eldof.max() + 1), dtype=np.double)
        force_raw = np.zeros(self.eldof.max() + 1, )

        # from tqdm import tqdm
        # ref_mk = self.mass_multinomial.mass_struct(1 / ((self.p + 1) * (2 * self.p + 1)))

        # Populate the matrices
        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Calculate element matrices
            mk = self.mass_multinomial.mass_struct(area / ((self.p + 1) * (2 * self.p + 1)))
            sk = self.stiff_multinomial.stiff_struct(np.array([t1, t2, t0]).T, area)
            fk = self.element_load(self.coordinates[self.elnode[k, 0]], self.coordinates[self.elnode[k, 1]],
                                   self.coordinates[self.elnode[k, 2]], area, self.f)

            for i in range(self.mesh.dofs_per_element):
                iglobal = self.eldof[k, i]
                i_dof = self.mesh.lookup_dof(i)
                force_raw[iglobal] += fk[i_dof[:-1]]
                for j in range(self.mesh.dofs_per_element):
                    jglobal = self.eldof[k, j]
                    j_dof = self.mesh.lookup_dof(j)
                    mass_raw[iglobal, jglobal] += mk[i_dof + j_dof]
                    stiffness_raw[iglobal, jglobal] += sk[i_dof + j_dof]

        # Depopoulate the dirchlet boundary conditions
        for i in range(len(self.mesh.dirchlet_bc)):
            stiffness_raw[:, self.mesh.dirchlet_bc[i][0]] = 0
            stiffness_raw[self.mesh.dirchlet_bc[i][0], :] = 0
            stiffness_raw[self.mesh.dirchlet_bc[i][0], self.mesh.dirchlet_bc[i][0]] = 1

            mass_raw[:, self.mesh.dirchlet_bc[i][0]] = 0
            mass_raw[self.mesh.dirchlet_bc[i][0], :] = 0
            mass_raw[self.mesh.dirchlet_bc[i][0], self.mesh.dirchlet_bc[i][0]] = 1

            force_raw[self.mesh.dirchlet_bc[i][0]] = 0

        return csr_matrix(stiffness_raw), csr_matrix(mass_raw), force_raw

    def element_load(self, t0, t1, t2, area: float, func: Callable) -> np.ndarray:
        """
        Uses moments calculations for load functions

        :type t0: numpy.ndarray
        :type t1: numpy.ndarray
        :type t2: numpy.ndarray
        :type func: Callable
        :param t0: side one (watch for order)
        :param t1: side two
        :param t2: side three
        :param area: area of triangle
        :param func: function call to set forcing term
        :return:
        """
        vertices = np.zeros((2, 3))
        vertices[:, 0] = t0
        vertices[:, 1] = t1
        vertices[:, 2] = t2

        fa = np.zeros((self.q, self.q))
        for i in range(self.q):
            for j in range(self.q):
                fa[i, j] = func(duffy([self.xis[1, i], self.xis[0, j]], vertices))
        # print(2 * area * nonlinear_c.moment(fa, self.q, self.p, 2, self.xis, self.omegas))
        return 2 * area * nonlinear_c.moment(fa, self.q, self.p, 2, self.xis, self.omegas)

    def l2_projection(self, func: Callable) -> np.ndarray:
        """
        Given a function, performs a L2 projection to find initial condition.

        Very similar to matrix assembly

        :param func: function to perform projection on
        :return:
        """
        u = np.zeros((self.eldof.max() + 1,))

        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            uk = self.element_load(self.coordinates[self.elnode[k, 0]], self.coordinates[self.elnode[k, 1]],
                                   self.coordinates[self.elnode[k, 2]], area, func)

            for i in range(self.mesh.dofs_per_element):
                u[self.eldof[k, i]] += uk[self.mesh.lookup_dof(i)[:-1]]

        return u

    def output_points(self, vec) -> np.ndarray:
        """
        Method which outputs a whole bunch of points based on each triangle (i.e. no need to search points)

        :param vec: vector to evaluate
        :return:
        """
        # Create some barycentric coordinates
        if self.elements > 16:
            num = 8
            x = np.linspace(0, 1, num=num)
        else:
            num = 16
            x = np.linspace(0, 1, num=num)

        xx, yy = np.meshgrid(x, x)
        xx = np.fliplr(xx)
        u = np.append(xx[np.triu_indices(num, 1)], [0, 1])
        v = np.append(yy[np.triu_indices(num, 1)], [1, 0])
        w = 1 - u - v
        lambdas = np.vstack((u, v, w)).T
        num_points = len(lambdas)

        blocks = list()
        for elem in range(self.elements):
            block = np.zeros((num_points, 3))
            for point in range(num_points):
                block[point, 2] = self.decasteljau(lambdas[point], vec[self.eldof[elem][self.mesh.c]])

            block[:, 0:2] = np.outer(w, self.coordinates[self.elnode[elem][0]]) + np.outer(u, self.coordinates[
                self.elnode[elem][1]]) + np.outer(v, self.coordinates[self.elnode[elem][2]])

            blocks.append(block)

        return np.vstack(blocks)

    @staticmethod
    def f(x: np.ndarray) -> float:
        return x[0] - x[0] + 1
        # return x[0] * x[0] + x[1]
        # return ((-x[0] - x[1])/2)**3
        # return ((1+x[0])/2)**3
        # return ((1 + x[1])/2)**3
        # return 6*((1+x[0])/2)*((1 + x[1])/2) * (-x[0] - x[1])/2

    @staticmethod
    def area(t0, t1, t2) -> float:
        """
        Calculates the area of a triangle usign Heron's formula

        :type t0: np.ndarray
        :type t1: np.ndarray
        :type t2: np.ndarray
        :param t0: side 0 (numpy vector)
        :param t1: side 1 (numpy vector)
        :param t2: side 2 (numpy vector)
        :return: area of triangle
        """
        t0 = np.linalg.norm(t0)
        t1 = np.linalg.norm(t1)
        t2 = np.linalg.norm(t2)
        s = (t0 + t1 + t2) / 2
        return np.sqrt(s * (s - t0) * (s - t1) * (s - t2))

    def decasteljau(self, lambdas: np.ndarray, cp: np.ndarray) -> float:
        """
        Applies de Casteljau algorithm to the ith element using the provided barycentric coordinates
        :param lambdas: barycentric coordinates (MATCHES "in_triangle" OUTPUT, USE IN CONJUNCTION)
        :param cp: control point
        :return:
        """
        C = np.zeros((self.p + 1,) * 2)
        C[np.triu_indices(self.p + 1)] = cp
        C = np.fliplr(C)

        # Iterate p times
        for r in range(self.p):
            for order in range(self.p - r):
                for i in range(order + 1):
                    C[i, order - i] = lambdas[1] * C[i, order - i] + lambdas[2] * C[i + 1, order - i] + lambdas[0] * C[
                        i, order - i + 1]

        return C[0, 0]

    def contour_plot(self, values, save: bool = False, index: int = 0) -> None:
        """
        Simple plotting with inefficient methods

        :param values: vector to plot
        :param save: boolean value to indicate what to save; requires index to be filled if true
        :param index: number to save as name
        """
        sns.set()
        points = self.output_points(values)
        fig = plt.figure()
        ax = Axes3D(fig)

        ax.plot_trisurf(points[:, 0], points[:, 1], points[:, 2] / np.max(points[:, 2]), cmap=plt.cm.hot, linewidth=0,
                        edgecolor='none', antialiased=False)
        ax.set_zlim3d(0, 1)
        ax.set_xlabel('x axis')
        ax.set_ylabel('y axis')
        ax.view_init(40, 145)

        if save:
            plt.savefig('pictures/plot_%04d.png' % (index,), bbox_inches='tight')
            plt.close()
        else:
            plt.show()

    def plot(self, values: np.ndarray, subdivision: int = 1, grad: bool = False, save: bool = False, index: int = 0):
        """
        Plots the Bernstein solution corresponding to this finite element method

        Subdivides twice; should be enough for most applications

        :param values:
        :param subdivision: Subdivisions to use, default of 1
        :param save: save the image or not
        :param index: name of save file
        :return: None
        """
        # First subdivide, need to aggregate information from mesh
        triangles = list()

        if grad:    # If we want to plot the gradients, we need to keep track of all the gradient triangles
            grad_triangles = list()

        for elem in range(self.elements):
            D = np.zeros((self.p + 1,) * 2, dtype=np.float)

            my_element = self.eldof[elem][self.mesh.c]

            D[np.triu_indices(self.p + 1)] = values[my_element]
            D = np.fliplr(D)

            if grad:
                # Need to find the gradients of barycentric
                t0 = -(self.coordinates[int(self.elnode[elem, 1])] - self.coordinates[int(self.elnode[elem, 2])])
                t1 = -(self.coordinates[int(self.elnode[elem, 2])] - self.coordinates[int(self.elnode[elem, 0])])
                t2 = -(self.coordinates[int(self.elnode[elem, 0])] - self.coordinates[int(self.elnode[elem, 1])])
                area = self.area(t0, t1, t2)

                # Calculate unit normals
                n0 = [t0[1], -t0[0]] / np.linalg.norm(t0)
                n1 = [t1[1], -t1[0]] / np.linalg.norm(t1)
                n2 = [t2[1], -t2[0]] / np.linalg.norm(t2)

                gl0 = -np.linalg.norm(t0) * n0 / (2 * area)
                gl1 = -np.linalg.norm(t1) * n1 / (2 * area)
                gl2 = -np.linalg.norm(t2) * n2 / (2 * area)

                out = self._generate_cvec(D, [gl0, gl1, gl2])

                grad_triangles.append(tuple((out, [self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
                     self.coordinates[my_element[-1]]])))
                # grad_triangles.extend(self.subdivide_2d_gallier_gradient(out, np.vstack(
                #     (self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
                #      self.coordinates[my_element[-1]]))))

            # print(D, np.vstack(
            #     (self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
            #      self.coordinates[my_element[-1]])))
            triangles.append(tuple((D,
                [self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
                 self.coordinates[my_element[-1]]])))
            # triangles.extend(self.subdivide_2d_gallier(D, np.vstack(
            #     (self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
            #      self.coordinates[my_element[-1]]))))
        # print(triangles)
        for s in range(subdivision):
            holder = list()
            for t in triangles:
                holder.extend(self.subdivide_2d_gallier(t[0], t[1]))
            triangles = holder

            # Now do gradients
            if grad:
                holder_grad = list()

                for t in grad_triangles:
                    holder_grad.extend(self.subdivide_2d_gallier_gradient(t[0], t[1]))

                grad_triangles = holder_grad

        # Apparently it is faster to aggregate into one giant list instead of having multiple plot commands
        all_xs = list()
        all_ys = list()
        all_zs = list()
        all_triangulations = list()
        scale = 0
        for t in triangles:
            xs1, ys1, zs1 = self.plot_triangle(t[0], t[1], self.bary1, self.bary2, self.bary3)

            all_triangulations.extend(self.triangulation + len(all_xs))
            all_xs.extend(xs1)
            all_ys.extend(ys1)
            all_zs.extend(zs1)
            scale = max(scale, (np.max(t[0])))

        if grad:
            grad_zs = list()
            for t in grad_triangles:
                _, _, zs1 = self.plot_triangle(self._degree_raise(t[0]), t[1], self.bary1, self.bary2, self.bary3)
                grad_zs.extend(zs1)

        if self.mplot:
            sns.set()
            fig = plt.figure()
            ax = Axes3D(fig)

            ax.plot_trisurf(all_xs, all_ys, all_triangulations, all_zs / np.max(all_zs), linewidth=0, edgecolor='none',
                            antialiased=False, cmap=plt.cm.hot, vmin=0, vmax=1)

            # ax.set_zlim3d(0., 1)
            ax.set_xlabel("x axis")
            ax.set_ylabel("y axis")
            ax.view_init(45, 125)

            if save:
                plt.savefig('pictures/plot_%04d.png' % (index,), bbox_inches='tight')
                plt.close()
            else:
                plt.show()
        else:
            offset = np.arange(1, len(all_triangulations) + 1, dtype=int) * 3
            ctype = np.ones(len(all_triangulations)) * VtkTriangle.tid

            if not grad:
                unstructuredGridToVTK('pictures/plot_%04d' % (index,), np.array(all_xs), np.array(all_ys),
                                  np.array(all_zs),
                                  connectivity=np.concatenate(all_triangulations), offsets=offset, cell_types=ctype,
                                  cellData=None, pointData=None)

            if grad:
                gradients = np.zeros((len(grad_zs), 2))
                gradients[:, 0:2] = np.array(grad_zs)

                # offset = np.arange(1, len(grad_triangulations) + 1, dtype=int) * 3
                # ctype = np.ones(len(grad_triangulations)) * VtkTriangle.tid

                unstructuredGridToVTK('pictures/grad_%04d' % (index,), np.array(all_xs), np.array(all_ys),
                                  np.array(all_zs),
                                  connectivity=np.concatenate(all_triangulations), offsets=offset, cell_types=ctype,
                                  cellData=None, pointData={'grad_x': np.array(gradients[:, 0]), 'grad_y': np.array(gradients[:, 1])})


            # from mayavi import mlab
            # mlab.options.offscreen = True
            # f = mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1))
            #
            # mlab.triangular_mesh(all_xs, all_ys, all_zs/np.max(all_zs), all_triangulations, colormap='hot',
            #                      vmin=0, vmax=1)
            #
            # # f.scene.save("test.png")
            # # mlab.savefig("test.png")
            # mlab.show()

    def decast(self, lambdas: list, ctrl: np.ndarray, degree: int = None) -> (np.ndarray, np.ndarray, np.ndarray):
        """
        de Castlejau algorithm returns control nets to the three subdivided triangles.

        Triangle ABC is oriented such that A is [0,0], B, is [1, 0], C is [0, 1].
        Call the barycentric coordinate L.

        They way we store the indices allow us to have triangle LBC easily; just need to store ALC, ALB.

        :param ctrl: control point in 2d array form (NOTE: THIS IS OVERWRITTEN)
        :param lambdas: barycnetric coordinates
        :return: 4 Numpy arrays
        """
        if degree is None:
            degree = self.p

        # Storage for two other triangles
        alc = np.zeros_like(ctrl)
        abl = np.zeros_like(ctrl)

        alc[:, 0] = ctrl[:, 0]
        abl[0, :] = ctrl[0, :]

        # Iterate p times
        for r in range(degree):
            for order in range(degree - r):
                for i in range(order + 1):
                    ctrl[i, order - i] = lambdas[2] * ctrl[i, order - i] + lambdas[0] * ctrl[i + 1, order - i] + \
                                         lambdas[1] * ctrl[i, order - i + 1]
            alc[0:degree - r, r + 1] = ctrl[0:degree - r, 0]
            abl[r + 1, 0:degree - r] = ctrl[0, 0:degree - r]

        return ctrl, abl, alc

    def subdivide_2d_gallier_gradient(self, ctrl: np.ndarray, abscissa: np.ndarray):
        """
        Subdivides a triangle in an efficient manner into 4 triangle; follows the algorithm in Jean Gallier's book

        :param ctrl: control points
        :param abscissa: coordiantes corresponding to the current control points
        :return: a tuple of 4 triangles
        """

        # Step 1: Initial decast algorithm
        _, abr, arc = self.decast([.5, .5, 0], ctrl, self.p - 1)

        # Step 2: calculate TBR
        tbr, _, _ = self.decast([0, .5, .5], abr, self.p - 1)

        # Step 3: Calculate SRC:
        src, ars, _ = self.decast([.5, 0, .5], arc, self.p - 1)

        # Step 4: Calculate the two remaining triangles
        trs, _, ats = self.decast([-1, 1, 1], ars, self.p - 1)

        # Step 5: Give correct vertices data
        a_v = abscissa[0]
        b_v = abscissa[1]
        c_v = abscissa[2]
        t_v = (a_v + b_v) / 2
        r_v = (b_v + c_v) / 2
        s_v = (a_v + c_v) / 2

        return (ats, [a_v, t_v, s_v]), (trs, [t_v, r_v, s_v]), (tbr, [t_v, b_v, r_v]), (src, [s_v, r_v, c_v])

    def subdivide_2d_gallier(self, ctrl: np.ndarray, abscissa: np.ndarray):
        """
        Subdivides a triangle in an efficient manner into 4 triangle; follows the algorithm in Jean Gallier's book

        :param ctrl: control points
        :param abscissa: coordiantes corresponding to the current control points
        :return: a tuple of 4 triangles
        """

        # Step 1: Initial decast algorithm
        _, abr, arc = self.decast([.5, .5, 0], ctrl)

        # Step 2: calculate TBR
        tbr, _, _ = self.decast([0, .5, .5], abr)

        # Step 3: Calculate SRC:
        src, ars, _ = self.decast([.5, 0, .5], arc)

        # Step 4: Calculate the two remaining triangles
        trs, _, ats = self.decast([-1, 1, 1], ars)

        # Step 5: Give correct vertices data
        a_v = abscissa[0]
        b_v = abscissa[1]
        c_v = abscissa[2]
        t_v = (a_v + b_v) / 2
        r_v = (b_v + c_v) / 2
        s_v = (a_v + c_v) / 2

        return (ats, [a_v, t_v, s_v]), (trs, [t_v, r_v, s_v]), (tbr, [t_v, b_v, r_v]), (src, [s_v, r_v, c_v])

    def subdivide_2d_personal(self, ctrl: np.ndarray, abscissa: np.ndarray):
        """
        Subdivides a triangle in an efficient manner into 4 triangle; non-uniform, but does not rely on unstable
        de Casteljau coefficients

        :param ctrl: control points
        :param abscissa: coordiantes corresponding to the current control points
        :return: a tuple of 4 triangles
        """

        # Step 1: Initial decast algorithm
        rbc, abr, arc = self.decast([1 / 3, 1 / 3, 1 / 3], ctrl)

        # Step 2: calculate TBR
        tbr, _, atr = self.decast([0, .5, .5], abr)

        # Step 3: Calculate SRC:
        src, ars, _ = self.decast([.5, 0, .5], arc)

        # Step 4:
        _, rqb, rcq = self.decast([.5, .5, 0], rbc)

        # Step 5: Give correct vertices data
        a_v = abscissa[0]
        b_v = abscissa[1]
        c_v = abscissa[2]
        t_v = (a_v + b_v) / 2
        r_v = (a_v + b_v + c_v) / 3
        s_v = (a_v + c_v) / 2
        q_v = (b_v + c_v) / 2

        return (atr, [a_v, t_v, r_v]), (tbr, [t_v, b_v, r_v]), (ars, [a_v, r_v, s_v]), (src, [s_v, r_v, c_v]), (
        rqb, [r_v, b_v, q_v]), (rcq, [r_v, q_v, c_v])

    def _degree_raise(self, ctrl: np.ndarray):
        """
        Degree raises the control points; infers the degree from the control point array.

        :param ctrl: control points
        :return:
        """
        p = ctrl.shape[0] # The degree we want to raise to

        if len(ctrl.shape) == 2:       # We want to be able to accept vectors too
            out = np.zeros((p+1, p+1))
        else:
            out = np.zeros((p+1, p+1, ctrl.shape[2]))

        for i in range(p):
            for j in range(p - i):
                k = p - 1 - i - j

                out[i, j] += (k + 1)/p * ctrl[i, j]
                out[i+1, j] += (i + 1)/p * ctrl[i, j]
                out[i, j+1] += (j + 1)/p * ctrl[i, j]

        return out

    def _generate_cvec(self, ctrl: np.ndarray, gls: np.ndarray) -> np.ndarray:
        """
        Given a set of control points, we give the correct vectors for the gradients

        This is an element-wise operation

        :param ctrl: control points of p-degree approximation
        :param gls: the gradient of the barycentric coordinates TODO: Need to give better ordering for this
        :return: An array which contains the correct gradient values
        """

        # Need to mask the diagonal
        shift_none = np.copy(ctrl[0:self.p, 0:self.p]) # np.copy to not overwrite ctrl
        indices = (range(self.p - 1, 0, -1))
        shift_none[indices, indices[::-1]] = 0  # We're working with a rotated matrix, so no anti-diagonal command

        # The other two are easy to create
        shift_left = np.copy(ctrl[0:self.p, 1:])
        shift_down = np.copy(ctrl[1:, 0:self.p])

        out = np.outer(shift_none, gls[2]) + np.outer(shift_down, gls[0]) + np.outer(shift_left, gls[1])
        out = out.reshape((self.p, self.p, 2))

        return self.p * out

    def generate_bary(self) -> (np.ndarray, np.ndarray, np.ndarray):
        """
        Generates the 3 barycentric matrices

        :return: three matrices corresponding to the barycentric coordinates in subdivide_2d
        """
        # Generate some barycentric coordinates
        lambda_1 = np.zeros((self.p + 1, self.p + 1))
        lambda_2 = np.zeros((self.p + 1, self.p + 1))
        lambda_3 = np.zeros((self.p + 1, self.p + 1))

        for i in range(self.p + 1):
            # Because I'm lazy, and numpy is actually stupid for not having off diagonals
            for j in range(self.p - i + 1):
                lambda_1[j, self.p - i - j] = i / self.p
            lambda_2[0:self.p - i + 1, i] = i / self.p
            lambda_3[i, 0: self.p - i + 1] = i / self.p

        return lambda_1, lambda_2, lambda_3

    def return_triangulation(self):
        out = list()

        # Create a 2D matrix for helping (only need to run this once, so it shouldn't be bad)
        helper = np.zeros((self.p + 1, self.p + 1), dtype=int)
        helper[np.triu_indices(self.p + 1)] = range(int((self.p + 1) * (self.p + 2) / 2))

        # Now create top triangles
        for i in range(self.p):
            for j in range(self.p - i):
                out.append((helper[i, j + i], helper[i, j + i + 1], helper[i + 1, j + i + 1]))

        # Now the more awkward bottom triangles (Can combine with above but for readability)
        for i in range(self.p - 1):
            for j in range(self.p - 1 - i):
                out.append((helper[i, j + i + 1], helper[i + 1, j + i + 1], helper[i + 1, j + i + 2]))

        return out

    @staticmethod
    def plot_triangle(ctrl: np.ndarray, vertices: np.ndarray, bary1, bary2, bary3):
        """
        Returns 1d arrays of x, y, z for trisurf plotting

        :param bary3: Third barycentric coordinates
        :param bary2: Second barycentric coordinates
        :param bary1: First barycentric coordinates
        :param ctrl: control points
        :param vertices: self-explanatory
        :return:
        """
        p = ctrl.shape[0] - 1

        xs = vertices[0][0] * bary1 + vertices[1][0] * bary2 + vertices[2][0] * bary3
        ys = vertices[0][1] * bary1 + vertices[1][1] * bary2 + vertices[2][1] * bary3

        return np.rot90(xs, -1)[np.triu_indices(p + 1)], np.rot90(ys, -1)[np.triu_indices(p + 1)], np.rot90(ctrl, -1)[
            np.triu_indices(p + 1)]

    def massmult(self, u: np.ndarray) -> np.ndarray:
        """
        Nonlinear uv^2 generator
        :param u: u
        :return: uv^2 in Bernstein coordinates
        """
        out = np.zeros((self.eldof.max() + 1,))

        c = np.zeros((self.p + 1,) * 2)
        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Put u and v into a form that I can evaluate function
            c[np.triu_indices(self.p + 1)] = u[self.eldof[k]][self.mesh.c]
            c = np.fliplr(c)
            c = np.ascontiguousarray(c)
            print("c", c)
            U = nonlinear_c.evaluate(c, self.q, self.p, 2, self.xis)
            print("U", U)
            outk = 2 * area * nonlinear_c.moment(U, self.q, self.p, 2, self.xis, self.omegas)

            for i in range(self.mesh.dofs_per_element):
                out[self.eldof[k, i]] += outk[self.mesh.lookup_dof(i)[:-1]]

        return out

    def stiffmult(self, u: np.ndarray) -> np.ndarray:
        """
        Nonlinear uv^2 generator
        :param u: u
        :return: uv^2 in Bernstein coordinates
        """
        out = np.zeros((self.eldof.max() + 1,))
        import math

        c = np.zeros((self.p + 1,) * 2)
        for k in range(1):
            v0 = self.coordinates[int(self.elnode[k, 0])]
            v1 = self.coordinates[int(self.elnode[k, 1])]
            v2 = self.coordinates[int(self.elnode[k, 2])]

            # Find side lengths for later
            t0 = -(v0 - v1)
            t1 = -(v1 - v2)
            t2 = -(v2 - v0)
            area = self.area(t0, t1, t2)

            # Calculate unit normals TODO: make sure this is correct for arbitrary orientation
            n0 = [t0[1], -t0[0]] / np.linalg.norm(t0)
            if np.dot(n0, v0 - v2) < 0:
                n0 = -n0

            n1 = [t1[1], -t1[0]] / np.linalg.norm(t1)
            if np.dot(n1, v2 - v0) < 0:
                n1 = -n1

            n2 = [t2[1], -t2[0]] / np.linalg.norm(t2)
            if np.dot(n2, v2 - v1) < 0:
                n2 = -n2

            # print(n0, n1, n2, v0, v1, v2)

            # Put u and v into a form that I can evaluate function
            c[np.triu_indices(self.p + 1)] = u[self.eldof[k]][self.mesh.c]
            c = np.fliplr(c)
            c = np.ascontiguousarray(c)
            # print(c)
            # After obtaining the Bernstein coefficients, now I can calculate grad u
            # First obtain

            # First do the x
            grad_cx = np.zeros((self.p, self.p))
            grad_cy = np.zeros((self.p, self.p))
            for alpha1 in range(self.p - 1 + 1):
                for alpha2 in range(self.p - 1 + 1 - alpha1):
                    grad_cx[alpha1, alpha2] = - self.p * (c[alpha1, alpha2] * n0[0] * np.linalg.norm(t0)+
                                                        c[alpha1 + 1, alpha2] * n1[0] * np.linalg.norm(t1)+
                                                        c[alpha1, alpha2 + 1] * n2[0] * np.linalg.norm(t2))/(2*area)
                    grad_cy[alpha1, alpha2] = - self.p * (c[alpha1, alpha2] * n0[1] * np.linalg.norm(t0)+
                                                        c[alpha1 + 1, alpha2] * n1[1] * np.linalg.norm(t1)+
                                                        c[alpha1, alpha2 + 1] * n2[1] * np.linalg.norm(t2))/(2*area)
            print(grad_cx)
            # print(grad_cy)
            # Test this

            gradbary0 = - np.linalg.norm(t0) * n0 / (2 * area)
            gradbary1 = - np.linalg.norm(t1) * n1 / (2 * area)
            gradbary2 = - np.linalg.norm(t2) * n2 / (2 * area)
            # print("gradient barycentric", gradbary0, gradbary1, gradbary2)

            U_x = nonlinear_c.evaluate(grad_cx, self.q, self.p - 1, 2, self.xis)
            U_y = nonlinear_c.evaluate(grad_cy, self.q, self.p - 1, 2, self.xis)

            print("U_x", U_x)
            # print("U_y", U_y)

            moment_x = 2 * area * nonlinear_c.moment(U_x, self.q, self.p - 1, 2, self.xis, self.omegas)

            # print(moment_x)
            moment_y = 2 * area * nonlinear_c.moment(U_y, self.q, self.p - 1, 2, self.xis, self.omegas)
            # print(moment_y)
            outk = np.zeros((self.p + 1, self.p + 1))
            # for alpha1 in range(self.p + 1):
            #     for alpha2 in range(self.p - alpha1 + 1):
            #         if alpha1 > 0:
            #             outk[alpha1, alpha2] += self.p * (moment_x[alpha1 - 1, alpha2] * gradbary1[0] + moment_y[alpha1 - 1, alpha2] * gradbary1[1])
            #         if alpha2 > 0:
            #             outk[alpha1, alpha2] += self.p * (moment_x[alpha1, alpha2 - 1] * gradbary2[0] + moment_y[alpha1, alpha2 - 1] * gradbary2[1])
            #
            #         if alpha1 < self.p and alpha2 < self.p:
            #             outk[alpha1, alpha2] += self.p * (moment_x[alpha1, alpha2] * gradbary0[0] + moment_y[alpha1, alpha2] * gradbary0[1])

            for alpha1 in range(self.p - 1 + 1):
                for alpha2 in range(self.p - 1 - alpha1 + 1):
                    outk[alpha1 + 1, alpha2] += self.p * (moment_x[alpha1, alpha2] * gradbary1[0] + moment_y[alpha1, alpha2] * gradbary1[1])
                    outk[alpha1, alpha2 + 1] += self.p * (moment_x[alpha1, alpha2] * gradbary2[0] + moment_y[alpha1, alpha2] * gradbary2[1])
                    outk[alpha1, alpha2] +=     self.p * (moment_x[alpha1, alpha2] * gradbary0[0] + moment_y[alpha1, alpha2] * gradbary0[1])
                    # outk[alpha1, alpha2] += outk[alpha1 + 1, alpha2]

            print(outk)
            # outk = 2 * area * nonlinear_c.moment(U, self.q, self.p, 2, self.xis, self.omegas)
            #
            for i in range(self.mesh.dofs_per_element):
                out[self.eldof[k, i]] += outk[self.mesh.lookup_dof(i)[:-1]]

        return out

if __name__ == '__main__':
    # Simple square
    coords_square = np.array([[0, 0], [0.5, 0.5], [1, 1], [1, 0], [0, 1]])
    elnode_square = np.array([[0, 3, 1], [0, 4, 1], [2, 3, 1], [2, 4, 1]])
    bc_square = np.array([[0], [2], [3], [4]])

    # 2 Triangles
    # coords_tri = np.array([[-1, -1], [1, -1], [-1, 1], [1, 1]])
    # elnode_tri = np.array([[0, 1, 2], [1,2,3]])
    # bc_tri = np.array([])

    # 1 Triangles
    coords_tri = np.array([[-1, -1], [1, -1], [-1, 1]])
    elnode_tri = np.array([[0, 1, 2]])
    bc_tri = np.array([])

    # Johnson mesh
    coords_john = np.array(
        [[1 / 4, 1 / 4], [0, 5 / 4], [1, 0], [1, 3 / 4], [1, 3 / 2], [3, 0], [5 / 2, 1 / 2], [3 / 2, 1], [2, 3 / 2],
         [3, 1 / 2]])
    elnode_john = np.array(
        [[1, 3, 4], [1, 4, 2], [4, 5, 2], [3, 8, 4], [4, 8, 5], [8, 9, 5], [6, 7, 3], [3, 7, 8], [8, 7, 9], [7, 10, 9],
         [6, 10, 7]])
    bc_john = np.array([[1], [2], [3], [5], [6], [9], [10]])
    elnode_john -= 1
    bc_john[:, 0] -= 1

    coords_3d = np.array([[0,0,0], [1, 0, 0], [0, 1, 1], [1, 1, 1]])
    elnode_3d = np.array([[0, 1, 2], [1, 2, 3]])
    bc_3d = np.array([])

    # mesh = BernsteinMesh(coords_3d, elnode_3d, bc_3d, p=4, subdiv=0)
    # b = BernsteinMatrix(mesh)

    # mesh = BernsteinMesh(coords_square, elnode_square, bc_square, p=10, subdiv=0)
    mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=3, subdiv=0)
    # mesh = BernsteinMesh(coords_john, elnode_john, bc_john, p=8, subdiv=0)
    b = BernsteinMatrix(mesh)

    np.set_printoptions(precision=4, linewidth=500, threshold=np.inf)
    S, M, f = b.assemble_matrix()
    u = np.linalg.solve(M.todense(), f)
    print(u)
    print(b.massmult(u))
    print(M.dot(u))
    # print(b.massmult(f))
    # B = M[0:M.shape[0] - int(b.elements * (b.p-2) * (b.p - 1)/2), M.shape[0] - int(b.elements * (b.p-2) * (b.p - 1)/2):]
    # print(M.dot(f))
    # print(B.dot(f[M.shape[0] - int(b.elements * (b.p-2) * (b.p - 1)/2):]))
    # print(f)
    # u = np.linalg.solve(M.todense(), f)
    # # print((b.stiffmult(f)))
    # print(S.dot(f))
    # print(max(np.real(np.linalg.eigvals(M.todense())))/min(np.real(np.linalg.eigvals(M.todense()))))
    # alphas = np.linalg.solve(S.todense(), f)
    # print(alphas)
    # print("Energy:", np.dot(f.T, alphas))
    # b.plot(alphas, subdivision=3, grad=True)
