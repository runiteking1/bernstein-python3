import itertools
import numpy as np
from scipy import special

from index_set import index_set


def evaluate(c0: np.ndarray, q: int, n: int, d: int, xis: np.ndarray) -> np.ndarray:
    """
    Evaluates a Bernstein polynomial vector at the values at the Stroud node

    :param n: order
    :param c0: BB-vector of a polynomial u
    :param q: index for number of points
    :param d: dimension
    :param xis: nodes of jacobi polynomial roots (i.e.
    :return: array C^d equal to value of u at Stroud node
    """

    for l in range(d, 0, -1):
        c0 = _eval_step(c0, l, q, n, d, xis)

    return c0


def _eval_step(cIn: np.ndarray, l: int, q: int, n: int, d: int, xis: np.ndarray):
    """
    See algorithm 2
    """
    cOut = np.zeros((n + 1,) * (l - 1) + (q,) * (d - l + 1))  # Create the output array

    for i_l in range(q):
        xi = xis[d - l, i_l]
        s = 1 - xi
        r = xi / s

        for alphas in index_set(n, l):
            alphas = alphas[:-1]  # exclude last element
            w = s ** (n - sum(alphas))
            for alpha_l in range(n - sum(alphas) + 1):
                for i_list in itertools.product(range(q), repeat=d - l):
                    cOut[alphas + (i_l,) + i_list] += w * cIn[alphas + (alpha_l,) + i_list]

                w *= r * (n - sum(alphas) - alpha_l) / (1.0 + alpha_l)

    return cOut

if __name__ == '__main__':
    c0 = np.zeros((4, 4, 4))

    c0[3, 0, 0] = 1 # Corresponds to (3, 0, 0, 0)

    # First, generate the quadrature points
    my_q = 5
    my_xis = np.zeros((3, my_q))
    my_omegas = np.zeros((3, my_q))
    for i in range(3):
        nodes, weights = special.roots_sh_jacobi(my_q, i + 1, 1)
        my_xis[i, :] = nodes
        my_omegas[i, :] = weights

    print(evaluate(c0, my_q, 3, 3, my_xis))
