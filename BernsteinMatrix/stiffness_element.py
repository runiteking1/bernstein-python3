import numpy as np
from multinomial import Multinomial


class Stiffness(Multinomial):
    """
    Stiffness matrix calculation helper based on multinomials (see algorithm 7 in optimal assembly paper)
    """

    def __init__(self, n, d):
        self.mu = None
        super(Stiffness, self).__init__(n - 1, n - 1, d)

        self.A = np.zeros((self.n + 2,) * (self.d + 1) + (self.n + 2,) * (self.d + 1))  # Note: n+2 rather n+1

    def increment(self, alphas, alpha, sum_alpha, betas, beta, sum_beta, w):
        wd1 = ((self.n + 1) ** 2) * w * \
              self.c[self.m - sum_alpha - alpha + self.n - sum_beta - beta, self.m - sum_alpha - alpha] * \
              self.c[alpha + beta, alpha]

        for k in range(3):
            for l in range(3):
                alpha_index = list(alphas + (alpha, self.m - sum_alpha - alpha))
                alpha_index[k] += 1
                beta_index = list(betas + (beta, self.n - sum_beta - beta))
                beta_index[l] += 1
                self.A[tuple(alpha_index + beta_index)] += wd1 * self.mu[k, l]

    def stiff_struct(self, tangents, area):
        self.mu = np.zeros((3, 3))
        for k in range(3):
            for l in range(k, 3):
                # self.n + 1 as we intialize Multinomial(S, n-1, n-1) so we need to add one to retrieve self.p
                self.mu[k, l] = np.dot(tangents[:, l], tangents[:, k]) / (4 * area * ((self.n + 1) * (2 * self.n + 1)))
                self.mu[l, k] = self.mu[k, l]

        self.A = np.zeros((self.n + 2,) * (self.d + 1) + (self.n + 2,) * (self.d + 1))  # Note: n+2 rather n+1
        self.loop(0, 0, 1, tuple(), tuple())

        return self.A


def stiff_mat(n, ts, area):
    test = Stiffness(n, 2)
    return test.stiff_struct(ts, area)


if __name__ == "__main__":
    t = np.array([[0, 1, -1], [-1, 0, 1]])
    print(stiff_mat(2, t, .5)[(1, 1, 0, 0, 1, 1)])  # Should be -0.3333333
