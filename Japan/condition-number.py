import numpy as np
# import scipy
from scipy.sparse.linalg import LinearOperator
import gc

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from mass_preconditioner import MassPreconditioner


def extractmesh():
    from xml.dom import minidom
    xmldoc = minidom.parse("/home/marshall/Documents/bernstein-v3/Japan/japan-msh.xml")

    vertexlist = xmldoc.getElementsByTagName("vertex")
    trianglelist = xmldoc.getElementsByTagName("triangle")

    coords = np.zeros((len(vertexlist), 2))

    for i in range(len(vertexlist)):
        coords[i, :] = [float(vertexlist[i].attributes['x'].value), float(vertexlist[i].attributes['y'].value)]

    elnode = np.zeros((len(trianglelist), 3), dtype=np.int)

    for i in range(len(trianglelist)):
        elnode[i, :] = [int(trianglelist[i].attributes['v0'].value), int(trianglelist[i].attributes['v1'].value),
                        int(trianglelist[i].attributes['v2'].value)]

    return coords, elnode


# def power_iteration(A, tol=1e-6):
#     b_k = np.random.rand(A.shape[0])
#     old = np.linalg.norm(b_k)
#     new = 0
#     while np.abs(old - new) > tol:
#         # calculate the matrix-by-vector product Ab
#         b_k1 = A.dot(b_k)
#
#         # calculate the norm
#         b_k1_norm = np.linalg.norm(b_k1)
#         old = new
#         new = b_k1_norm
#
#         # re normalize the vector
#         b_k = b_k1 / b_k1_norm
#     return b_k1_norm
def power_iteration(A, num_simulations):
    # Ideally choose a random vector
    # To decrease the chance that our vector
    # Is orthogonal to the eigenvector
    b_k = np.random.rand(A.shape[0])

    for _ in range(num_simulations):
        # calculate the matrix-by-vector product Ab
        b_k1 = np.dot(A, b_k)

        # calculate the norm
        b_k1_norm = np.linalg.norm(b_k1)

        # re normalize the vector
        b_k = b_k1 / b_k1_norm

    return b_k

if __name__ == '__main__':
    coords, elnode = extractmesh()
    bc = np.array([])

    print("Mesh Read")
    mesh = BernsteinMesh(coords, elnode, bc, 3)
    fem = BernsteinMatrix(mesh)
    gc.collect()
    _, mass, _ = fem.assemble_matrix()
    print(mass.shape)
    gc.collect()
    precond = MassPreconditioner(mass, fem)
    gc.collect()


    def mult(v: np.ndarray) -> np.ndarray:
        return precond.solve(mass.dot(v))
    gc.collect()

    PinvS = LinearOperator(dtype=mass.dtype, shape=mass.shape, matvec=mult)

    # PinvS.dot(np.ones((PinvS.shape[0],)))
    # gc.collect()
    #
    import scipy
    # w = scipy.sparse.linalg.eigs(PinvS, which='LM', sigma=2.6548438547, return_eigenvectors=False, k=1, tol=1E-4)
    w = scipy.sparse.linalg.eigs(mass, which='LM', return_eigenvectors=False, k=1, tol=1E-4)

    #
    gc.collect()
    large = max(np.real(w))
    print(large)
    #
    # w = scipy.sparse.linalg.eigs(PinvS, which='SM', sigma=0.100248795255, return_eigenvectors=False, k=1, tol = 1E-4)
    w = scipy.sparse.linalg.eigs(mass, which='SM', return_eigenvectors=False, k=1, tol = 1E-4)

    # gc.collect()
    #
    small = min(np.real(w))
    print(small)
    print(large / small)
