from xml.dom import minidom
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import numpy as np

xmldoc = minidom.parse("/home/marshall/Documents/bernstein-v3/Japan/japan-msh.xml")
vertexlist = xmldoc.getElementsByTagName("vertex")
trianglelist = xmldoc.getElementsByTagName("triangle")
coords = np.zeros((len(vertexlist), 2))
for i in range(len(vertexlist)):
    coords[i, :] = [float(vertexlist[i].attributes['x'].value), float(vertexlist[i].attributes['y'].value)]
elnode = np.zeros((len(trianglelist), 3), dtype=np.int)
for i in range(len(trianglelist)):
    elnode[i, :] = [int(trianglelist[i].attributes['v0'].value), int(trianglelist[i].attributes['v1'].value),
                    int(trianglelist[i].attributes['v2'].value)]

fig, ax = plt.subplots()

ax.triplot(coords[:, 0], coords[:, 1], triangles=elnode, linewidth=.05)
plt.xticks([])
plt.yticks([])

axins = zoomed_inset_axes(ax, 12, loc='lower right') # zoom = 6
axins.triplot(coords[:, 0], coords[:, 1], triangles=elnode, linewidth=.5)
x1, x2, y1, y2 = 1020000, 1100000, 1500000, 1580000
axins.set_xlim(x1, x2)
axins.set_ylim(y1, y2)
plt.xticks(visible=False)
plt.yticks(visible=False)
mark_inset(ax, axins, loc1=2, loc2=1, fc="none", ec="0.5")

axins2 = zoomed_inset_axes(ax, 12, loc='upper left') # zoom = 6
axins2.triplot(coords[:, 0], coords[:, 1], triangles=elnode, linewidth=.5)
x1, x2, y1, y2 = 200000, 280000, 900000, 980000
axins2.set_xlim(x1, x2)
axins2.set_ylim(y1, y2)
plt.xticks(visible=False)
plt.yticks(visible=False)
mark_inset(ax, axins2, loc1=3, loc2=4, fc="none", ec="0.5")
plt.draw()
plt.show()

