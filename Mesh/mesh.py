import numpy as np


class Mesh(object):
    """
    General finite element mesh object for H1-conforming elements in 2D
    """
    def __init__(self, coordinates, elnode, dirchlet_bc, *, subdiv: int = 0, neumann_bc=np.array([[]])):
        """
        :type coordinates: np.ndarray
        :type elnode: np.ndarray
        :type dirchlet_bc: np.ndarray
        :type subdiv: int
        """
        self.neumann_bc = neumann_bc.tolist()
        self.dirchlet_bc = dirchlet_bc.tolist()
        self.coordinates = coordinates
        self.elnode = np.array(elnode, dtype=np.int)
        self.subdiv = subdiv

        self.elements = len(self.elnode)

    def subdivide(self):
        """
        Refines the mesh by taking the midpoints and giving 4 triangles per single triangle

        :return: None
        """
        # Need to first all all the points into a set, to weed out duplicates
        new_points = set()
        for element in range(self.elements):
            # Find midpoint for each element
            mdpt0 = (self.coordinates[int(self.elnode[element, 0])] + self.coordinates[
                int(self.elnode[element, 1])]) / 2
            mdpt1 = (self.coordinates[int(self.elnode[element, 1])] + self.coordinates[
                int(self.elnode[element, 2])]) / 2
            mdpt2 = (self.coordinates[int(self.elnode[element, 2])] + self.coordinates[
                int(self.elnode[element, 0])]) / 2

            # Keep track of all the new points
            new_points.add(tuple(mdpt0))
            new_points.add(tuple(mdpt1))
            new_points.add(tuple(mdpt2))

        # Now we have a list of new coordinates
        self.coordinates = np.array(self.coordinates.tolist() + list(new_points))

        elnode_new = np.zeros((self.elements * 4, 3))  # There will be 4 times with the midpoint method
        dirchlet_bc_new = self.dirchlet_bc
        neumann_bc_new = self.neumann_bc

        # Reloop to process adding the elements in
        for element in range(self.elements):
            # Find midpoint for each element
            mdpt0 = (self.coordinates[int(self.elnode[element, 0])] + self.coordinates[
                int(self.elnode[element, 1])]) / 2
            mdpt1 = (self.coordinates[int(self.elnode[element, 1])] + self.coordinates[
                int(self.elnode[element, 2])]) / 2
            mdpt2 = (self.coordinates[int(self.elnode[element, 2])] + self.coordinates[
                int(self.elnode[element, 0])]) / 2

            # Find the index of the coordinates to make new element
            point0_index = np.where((self.coordinates == mdpt0).all(axis=1))[0][0]
            point1_index = np.where((self.coordinates == mdpt1).all(axis=1))[0][0]
            point2_index = np.where((self.coordinates == mdpt2).all(axis=1))[0][0]

            # Add in the four elements
            elnode_new[element, :] = [point1_index, point2_index, point0_index]
            elnode_new[self.elements + element, :] = [self.elnode[element, 0], point0_index, point2_index]
            elnode_new[2 * self.elements + element, :] = [point0_index, self.elnode[element, 1], point1_index]
            elnode_new[3 * self.elements + element, :] = [point2_index, point1_index, self.elnode[element, 2]]

            # Add new Dirchlet BCs; if two points are on a side, then BC (not the most efficient)
            if self.elnode[element, 0] in self.dirchlet_bc and self.elnode[element, 1] in self.dirchlet_bc:
                dirchlet_bc_new.append([point0_index])
            if self.elnode[element, 1] in self.dirchlet_bc and self.elnode[element, 2] in self.dirchlet_bc:
                dirchlet_bc_new.append([point1_index])
            if self.elnode[element, 0] in self.dirchlet_bc and self.elnode[element, 2] in self.dirchlet_bc:
                dirchlet_bc_new.append([point2_index])

            # Add new Neumann BCs
            if self.elnode[element, 0] in self.neumann_bc and self.elnode[element, 1] in self.neumann_bc:
                neumann_bc_new.append([point0_index])
            if self.elnode[element, 1] in self.neumann_bc and self.elnode[element, 2] in self.neumann_bc:
                neumann_bc_new.append([point1_index])
            if self.elnode[element, 0] in self.neumann_bc and self.elnode[element, 2] in self.neumann_bc:
                neumann_bc_new.append([point2_index])

        # Now set new stuff
        self.elnode = np.array(elnode_new, dtype=np.int)
        self.dirchlet_bc = dirchlet_bc_new
        self.neumann_bc = neumann_bc_new
        self.elements *= 4


if __name__ == '__main__':
    # Simple square
    coords_square = np.array([[0, 0], [0.5, 0.5], [1, 1], [1, 0], [0, 1]])
    elnode_square = np.array([[0, 3, 1], [0, 4, 1], [2, 3, 1], [2, 4, 1]])
    bc_square = np.array([[0], [2], [3], [4]])

    m = Mesh(coords_square, elnode_square, bc_square)
    m.subdivide()
