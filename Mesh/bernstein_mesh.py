from mesh import Mesh
import numpy as np
from index_set import index_set


class BernsteinMesh(Mesh):
    """
    Extends Mesh object to better suit a Bernstein mesh.

    Takes care of degrees of freedom/multi-indices.

    TODO: Implement plotting, and evluation
    """

    def __init__(self, coordinates, elnode, dirchlet_bc, p: int, *, subdiv: int = 0, neumann_bc=np.array([[]])) -> None:
        """
        :rtype: object
        :type coordinates: np.ndarray
        :type elnode: np.ndarray
        :type dirchlet_bc: np.ndarray
        :type p: int
        :type subdiv: int
        """
        # Initialize variables
        super(BernsteinMesh, self).__init__(coordinates, elnode, dirchlet_bc, subdiv=subdiv, neumann_bc=neumann_bc)
        self.dofs_per_element = int((p + 1) * (p + 2) / 2)
        if p < 3:
            raise NotImplementedError("It is not safe to use this code without interior DOFs, proceed at your own risk")
        else:
            self.p = p

        # Now we first subdivide what we want
        for i in range(subdiv):
            self.subdivide()

        # pre-processing for dofs_to_multi function; creates good dofs for interior points
        self.multi = list(index_set(p - 3, 3))

        # Create the inverse to lookup_dof
        self.lookup_multi = dict()
        for i in range(self.dofs_per_element):
            self.lookup_multi[self.lookup_dof(i)] = i

        # Create a set of edges
        edges = set()
        for element in range(self.elements):
            edges.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 1]))))
            edges.add(tuple(sorted((self.elnode[element, 1], self.elnode[element, 2]))))
            edges.add(tuple(sorted((self.elnode[element, 0], self.elnode[element, 2]))))
        edges = sorted(list(edges))

        # Assign DoF numbers to those edges
        dof = int(self.elnode.max() + 1)
        edge_dofs = dict()
        for edge in range(len(edges)):
            edge_dofs[edges[edge]] = range(dof, dof + p - 1)

            # Check if edge is on boundary, then add to dirchlet_dof and similar with Neumann
            if edges[edge][0] in self.dirchlet_bc and edges[edge][1] in self.dirchlet_bc:
                self.dirchlet_bc += [[i] for i in range(dof, dof + p - 1)]

            if edges[edge][0] in self.neumann_bc and edges[edge][1] in self.neumann_bc:
                self.neumann_bc += [[i] for i in range(dof, dof + p - 1)]

            # Increment dof count
            dof += p - 1
        # Create a helper function
        def add_edge(n1, n2):
            if n1 < n2:
                return edge_dofs[(n1, n2)]
            else:
                return edge_dofs[(n2, n1)][::-1]

        # Now we can finally aggregate into a eldof list
        total = list()
        for element in range(self.elements):
            temp_list = self.elnode[element].tolist()  # first add vertices
            temp_list += add_edge(temp_list[1], temp_list[2])
            temp_list += add_edge(temp_list[2], temp_list[0])  # Order is important here
            temp_list += add_edge(temp_list[0], temp_list[1])

            # Insert interior DOFs
            interior = self.dofs_per_element - 3 * self.p
            temp_list += range(dof, dof + interior)
            dof += interior

            # Finally, lump all the elements together
            total.append(temp_list)

        # element degree of freedom thing
        self.eldof = np.array(total, dtype=np.int)

        # Create list such that we can output an element to the appropriate DOFs in matrix form once reshaped
        # eg given an element, we can find the (p+1, p+1) matrix such that (0,0,0), (1, 0, 0) is laid out in matrix form
        c = np.zeros((self.p + 1,) * 2, dtype=np.int)
        for k1 in range(0, self.p + 1):
            for k2 in range(0, self.p + 1 - k1):
                c[k1, k2] = self.lookup_multi[(k1, k2, self.p - k1 - k2)]

        self.c = np.fliplr(c)[np.triu_indices(self.p + 1)].astype(np.int)

    def lookup_dof(self, i) -> tuple:
        """
        Creates a 1-1 correspondence between the number of DOF and the multinomial.

        :param i: degree of freedom counter
        :type i: int
        :return: Corresponds to the 3-tuple monomial
        """
        if i == 0:
            return self.p, 0, 0
        elif i == 1:
            return 0, self.p, 0
        elif i == 2:
            return 0, 0, self.p
        elif i <= self.p + 1:
            return 0, self.p - (i - 2), (i - 2)
        elif i <= 2 * self.p:
            return i - (self.p + 1), 0, 2 * self.p + 1 - i
        elif i <= 3 * self.p - 1:
            return 3 * self.p - i, i - 2 * self.p, 0
        else:
            return tuple(np.array((self.multi[i - (3 * self.p)])) + np.array([1, 1, 1]))


if __name__ == '__main__':
    # Simple square
    coords_square = np.array([[0, 0], [0.5, 0.5], [1, 1], [1, 0], [0, 1]])
    elnode_square = np.array([[0, 3, 1], [0, 4, 1], [2, 3, 1], [2, 4, 1]])
    bc_square = np.array([[0], [2], [3], [4]])

    # 2 Triangles
    coords_tri = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
    elnode_tri = np.array([[0, 1, 2], [1, 2, 3]])
    bc_tri = np.array([])

    # Test if 3D coordinates work
    coords_3d = np.array([[0,0,0], [1, 0, 0], [0, 1, 1], [1, 1, 1]])
    elnode_3d = np.array([[0, 1, 2], [1, 2, 3]])
    bc_3d = np.array([])

    # b = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=3, subdiv=0)
    b = BernsteinMesh(coords_3d, elnode_3d, bc_3d, p=4, subdiv=0)

    print(b.eldof)
