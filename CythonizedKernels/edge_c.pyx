#cython: boundscheck=False, wraparound=False, cdivision=True

cdef cinv(double[::1] f, double[::1] work, double[:, ::1] coefs, double[::1] output, int len_f):
    """
    Algorithm to convert to Jacobi basis inner-product

    :param: f
    :return:
    """
    cdef int i
    cdef int n = len_f + 1
    for i in range(n - 1):
        work[i] = f[i]
        output[i] = 0 # Reset output to 0; might not need this later

    # A bit non-Cythonic, but clearer
    cdef int outer_counter
    cdef int inner_counter
    cdef int local_n

    # Note that unravelling the loop doesn't speed up the process by significant amounts
    for outer_counter in range(n - 1):
        i = n - 2 - outer_counter

        if i != n - 2:
            local_n = i + 3
            for inner_counter in range((i+3) - 2):
                work[inner_counter] = work[inner_counter] * (local_n-1 - inner_counter) / local_n + work[inner_counter + 1] * (inner_counter + 2) / local_n

        for j in range(i + 1):
            output[i] += 4*coefs[i, j] * work[j]


cdef algo(double[::1] w, double[::1] work, double[:, ::1] coefs, double[::1] output, int len_f):
    """
    :param w:
    :param work: Worker array; needs to be length len_f + 1; needs to be zeroed
    :param coefs: algorithm coefficients precomputed
    :param output: zeros out naturally
    :param len_f:
    :return:
    """
    cdef int n, inner_loop, i, p
    cdef float holder, holder_1

    p = len_f + 1
    for n in range(len_f):
        if n != 0:
            holder = 0
            for inner_loop in range(n + 2):
                holder_1 = work[inner_loop]
                work[inner_loop] = ((n - inner_loop) * work[inner_loop])/n + inner_loop * holder /n
                holder = holder_1

        for i in range(n + 1):
            work[i] += coefs[n, i] * w[n]

    for n in range(p - 1):
        output[n] = (n + 1)* work[n]/(p - 1)
        output[n] *= 4.0 * (p - 1 - n)/p

def process(int edges, int edge_dofs, double[::1] diags, double[::1] x,  double[::1] subworker,
            double[::1] worker,
             double[:, ::1] cinv_coefs,  double[:, ::1] algo_coefs, double[::1] output):
    cdef int counter = 0

    cdef int e, interior
    for e in range(edges):
        # Compute Cinverse, and store in worker
        cinv(x[counter:counter + edge_dofs], subworker, cinv_coefs, worker, edge_dofs)

        # Product with diagonals
        for interior in range(edge_dofs):
            worker[interior] = worker[interior] * diags[counter + interior]

        # Reset worker to 0 before algo algorithm
        for interior in range(edge_dofs + 1):
            subworker[interior] = 0

        algo(worker, subworker, algo_coefs, output[counter:counter + edge_dofs], edge_dofs)

        counter = counter + edge_dofs

