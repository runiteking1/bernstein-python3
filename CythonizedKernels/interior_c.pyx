#cython: boundscheck=False, wraparound=False, cdivision=True

from libc.stdlib cimport calloc, malloc, free

def process(const double[::1] x, int internal_dofs, int num_elements, int p, double[:, ::1] j2b, double[:, :, ::1] nu,
            double[:, ::1] q, const double[::1] scaling, double[::1] out):
    cdef int i,j,k, counter = 0
    cdef double *b = <double*> malloc((p-2) * (p-2) * sizeof(double))
    cdef double *worker = <double*> malloc((p-1) * (p-1) * sizeof(double))


    for i in range(num_elements):
        # Zero out q
        for j in range(p - 2):
            for k in range(p - 2):
                q[j, k] = 0

        bernstein_mass_solver(x[counter:counter+internal_dofs], p, j2b, nu, q, out[counter:counter + internal_dofs], b, worker)

        # Scale each term
        for j in range(internal_dofs):
            out[counter + j] = out[counter + j] / scaling[i]

        counter += internal_dofs

    free(b)
    free(worker)

cdef bernstein_mass_solver(const double[::1] f, const int n, const double[:, ::1] j22bc, const double[:, :, ::1] nu,
    double[:, ::1] q, double[::1] output, double *b, double *worker):

    cdef int ni = n - 3
    cdef int i, j, k, counter = 0

    # Should pass in sooner or later
    cdef double *ftilde = <double*> malloc((ni + 1) * (ni + 1) * sizeof(double))

    for i in range(ni + 1):
        for j in range(ni - i + 1):
            k = ni - i - j
            ftilde[i + (ni + 1)*j] =  (f[counter] * (i + 1) * (j + 1) * (k + 1)) / (n * (n - 1) * (n - 2))
            counter = counter + 1

    # Computation of BB-form of polynomial ptilde
    j22b2d(ftilde, ni, j22bc, nu, q, b, worker)

    counter = 0
    for i in range(ni + 1):
        for j in range(ni - i + 1):
            k = ni - i - j
            output[counter] = ((i + 1) * (j + 1) * (k + 1)) * q[i, j]/(n * (n-1) * (n-2))
            counter += 1

    # Free memory
    free(ftilde)

cdef void j22b2d(const double* f, const int n, const double[:, ::1] c, const double[:, :, ::1] nu,
    double[:, ::1] q, double *b, double *worker):
    cdef int r, k, i, j
    cdef double holder, holder_1

    # cdef double *b = <double*> malloc((n+1) * (n+1) * sizeof(double))
    cdef double *qn = <double*>calloc((n+1) * (n+1) , sizeof(double))
    cdef double *sn = <double*>calloc((n+1) ,sizeof(double))
    # cdef double *worker = <double*> malloc((n+2) * (n+2) * sizeof(double))
    # Step 1 (verified)
    for r in range(n + 1):

        # Equivalent to "gamma = c[r, range(0, r + 1)]"
        for k in range(r+1):
            worker[k] = c[r, k]

        for k in range(n-r, -1, -1):
            for i in range(0, n-k+1):
                j = n - k - i
                qn[k + (n+1) * r] = qn[k + (n+1) * r] + worker[i] * f[i + (n + 1)*j]

            sn[r] = sn[r] + qn[k + (n+1)*r] * nu[k, n, r]

            degree_raising_1d(worker, n - k + 1) # worker array is correct

        b[n + (n+1) * r] = sn[r]/norm_pnr222_square(n, r)

    # Step 2 (verified)
    for i in range((n-1), -1, -1):

        # Reuse sn from above as Sm; need to reset to zero; only need (i+1) elements
        for r in range(i+1):
            sn[r] = 0

        for r in range(i+1):
            for k in range(i-r+1):
                qn[k + (n+1)*r] = qn[k+1 + (n+1)*r] * (k+1)/(i+1) + qn[k + (n+1)*r] * (i + 1 - k)/(i+1)
                sn[r] = sn[r] + qn[k + (n+1)*r] * nu[k, i, r]

            b[i + (n+1)*r] = sn[r]/norm_pnr222_square(i,r)

    # Second stage
    for k in range(n+1):
        # Reuse sn from above (previously Sm) as Tk now; reset k + 1 elements
        for r in range(k+1):
            sn[r] = 0

        # worker is standin for gamma_m (now needs two dimensional)
        for r in range(k+1):
            worker[r + (n + 2)*k] = c[k, r]

        # Step 1 (verified)
        for r in range(k+1):
            # Tk = Tk + nu22(0, m, r) * gamma_r * b[m, r]
            # TODO: Try using daxpy eventually
            for i in range(k+1):
                sn[i] = sn[i] + nu[0, k, r] * b[k + (n+1)*r] * worker[i + (n+2) * r]

            # Degree raise
            degree_raising_1d(&worker[(n+2)*r], k + 1)


        for i in range(k+1):
            j = k - i

            # Reusing qn from above (previously Qm, Qmp1); standin for cp
            qn[i + (n+1) * j] = sn[i]

        # Step 2; compute T_{ijk}^m recursively
        if k > 0:
            for r in range(k):
                for i in range(k - (r + 1) + 1):
                    j = k - (r + 1) - i
                    sn[i] = -((2 + i + 1)* sn[i+1] / (2 + r + 1)  + (2 + j + 1) * sn[i] / (2 + r + 1))

                    qn[i + (n+1) * j] = sn[i]

        # Perform q = q + cp (note it's upper triangular matrix of a certain size)
        for i in range(k + 1):
            for j in range(i+1):
                q[j, i - j] =  q[j, i - j] + qn[j + (n+1) * (i - j)]

        if k < n:
            degree_raising_2d(q, k)

    # free(b)
    free(qn)
    free(sn)
    # free(worker)

cdef void degree_raising_1d(double* c, int len_c):
    """
    Degree raising algorithm for 1D; overwrites input

    :param c: overwritten with degree raising
    :param len_c: should be length of c
    :return:
    """
    cdef int j
    cdef int n = len_c - 1
    cdef double holder, holder_1

    holder = c[0]
    for j in range(1, n+1):
        holder_1 = c[j]
        c[j] = (n + 1 - j) * holder_1/(n + 1)  + j * holder/(n + 1)
        holder = holder_1

    c[n+1] = holder

cdef void degree_raising_2d(double[:, ::1] c, int m):
    cdef double holder
    cdef int i, k
    for k in range(m+1, -1, -1):
        for i in range(m + 1 - k, -1, -1):
            holder = 0
            if i > 0:
                holder = holder + i  * c[i - 1, k]/ (m + 1.0)
            if k > 0:
                holder = holder + k * c[i, k - 1] / (m + 1.0)
            if m + 1 - k - i > 0:
                holder = holder + (m + 1 - k - i)* c[i, k] / (m + 1.0)

            c[i, k] = holder

cdef double norm_pnr222_square(int n, int r):
    h1 = 1.0 / (2.0 * r + 5.0) * ((r + 1.0) * (r + 2.0) / ((r + 3.0) * (r + 4.0)))
    h2 = 1.0 / (2.0 * (n + 4.0)) * ((n - r + 1.0) * (n - r + 2.0) / ((n + r + 6.0) * (n + r + 7.0)))
    return h1 * h2
