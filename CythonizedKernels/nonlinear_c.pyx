#cython: boundscheck=False, cdivision=True, wraparound=False

# Code which contains "evaluate" and "moment" functions from evaluate_bernstein.py and force_element.py respectively in
# Cython format which allows for faster evaluation in time stepping

import numpy as np

def evaluate(double[:, ::1] c0, int q, int n, int d, double[:, ::1] xis):
    """
    Evaluates a Bernstein polynomial vector at the values at the Stroud node
    :param c0: BB-vector of a polynomial u
    :param q: index for number of points
    :param d: dimension
    :param xis: nodes of jacobi polynomial roots (i.e.
    :param n: order of polynomials
    :return:
    """
    step1 = np.zeros((n+1, q))
    step2 = np.zeros((q,q))

    _eval_step_1(c0, 2, q, n, d, xis, step1)
    _eval_step_2(step1, 1, q, n, d, xis, step2)

    return step2


cdef _eval_step_1(double[:, ::1] cIn, int l, int q, int n, int d, double[:, ::1]  xis, double[:, ::1] cOut):
    """
    See algorithm 2 in paper; adapted for 2D case
    """
    cdef int i_l, alphas, alpha_l
    cdef double xi, s, r, w

    for i_l in range(q):
        xi = xis[d-l, i_l]
        s = 1 - xi
        r = xi/s

        for alphas in range(n + 1):
            w = s**(n - alphas)
            for alpha_l in range(n - alphas + 1):
                cOut[alphas,i_l] += w*cIn[alphas,alpha_l]

                w *= r * (n - alphas - alpha_l)/(1.0 + alpha_l)


cdef _eval_step_2(double[:, ::1] cIn, int l, int q, int n, int d, double[:, ::1]  xis, double[:, ::1] cOut):
    cdef int i_l, alpha_l, i_list
    cdef double xi, s, r, w

    for i_l in range(q):
        xi = xis[d-l, i_l]
        s = 1 - xi
        r = xi/s

        w = s**n
        for alpha_l in range(n + 1):
            for i_list in range(q):
                cOut[i_l,i_list] += w*cIn[alpha_l,i_list]

            w *= r * (n  - alpha_l)/(1.0 + alpha_l)


def moment(double[:, ::1] f0, int q, int n, int d,double[:, ::1] xis, double[:, ::1] omegas):
    """
    Evaluates a polynomial in Bernstein form at the Stroud nodes (see algorithm 3 in optimal assembly paper)
    note that d = 2
    """
    step0 = np.zeros((n+1, q))
    step1 = np.zeros((n+1, n+1))

    _moment_step_1(f0, 1, q, n, xis, omegas, step0)
    _moment_step_2(step0, 2, q, n, xis, omegas, step1)

    return step1


cdef _moment_step_2(double[:, ::1] fin, int l, int q, int n, double[:, ::1] xis, double[:, ::1] omegas, double[:, ::1] f_out):
    """
    Algorithm 4 of AAD paper; adapted for 2D case
    """
    cdef int i_l, alpha_l
    cdef double xi, s, r, w
    for i_l in range(q):
        xi = xis[2 - l, i_l]
        omega = omegas[2 - l, i_l]

        s = 1 - xi
        r = xi / s
        for alphas in range(n+1):
            w = omega * (s ** (n - alphas))
            for alpha_l in range(n - alphas + 1):
                f_out[alphas,alpha_l] += w * fin[alphas,i_l]

                w *= r * (n - alphas - alpha_l) / (1.0 + alpha_l)


cdef _moment_step_1(double[:, ::1] fin, int l, int q, int n, double[:, ::1] xis, double[:, ::1] omegas, double[:, ::1] f_out):
    """
    Algorithm 4 of AAD paper; adapted for 2D case
    """
    cdef int i_l, i_list
    cdef double xi, s, r, w

    for i_l in range(q):
        xi = xis[2 - l, i_l]
        omega = omegas[2 - l, i_l]

        s = 1 - xi
        r = xi / s

        w = omega * (s ** n)
        for alpha_l in range(n + 1):
            for i_list in range(q):
                f_out[alpha_l,i_list] += w * fin[i_l,i_list]

            w *= r * (n - alpha_l) / (1.0 + alpha_l)

