from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

import numpy

# Code to run is "python setup.py build_ext --inplace"

##
# Line profiler (https://stackoverflow.com/questions/28301931/how-to-profile-cython-functions-line-by-line)
# from Cython.Compiler.Options import directive_defaults
# import numpy
# directive_defaults['linetrace'] = True
# directive_defaults['binding'] = True
# extensions = [Extension("transforms", ["transforms.pyx"], include_dirs=[numpy.get_include()],
#                         define_macros=[('CYTHON_TRACE', '1')]), Extension("transforms_2", ["transforms_2.pyx"])]
##

extensions = [Extension("interior_c", ["interior_c.pyx"], extra_compile_args=["-O3", '-march=native'],
                        extra_link_args=['-O3', '-march=native']),
              Extension("edge_c", ["edge_c.pyx"], extra_compile_args=["-O3", '-march=native'],
                        extra_link_args=['-O3', '-march=native']),
              Extension("nonlinear_c", ["nonlinear_c.pyx"], include_dirs=[numpy.get_include()],
                        extra_compile_args=["-O3", '-march=native'], extra_link_args=['-O3', '-march=native']),
              Extension("basis_helper", ["basis_helper.pyx"], include_dirs=[numpy.get_include()],
                        extra_compile_args=["-O3", '-march=native'], extra_link_args=['-O3', '-march=native']),
              ]


setup(
    ext_modules=cythonize(extensions), requires=['seaborn']
)
