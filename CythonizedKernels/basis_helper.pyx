#cython: boundscheck=False, wraparound=False, cdivision=True

from scipy.special import eval_jacobi as jac
import numpy as np
cimport numpy as cnp
from scipy.linalg.cython_blas cimport daxpy
from cpython cimport array

def quadtriangle(f, double[::1] w00, double[::1]w10, double[::1] x00, double[::1] x10):
    total = 0
    for i in range(len(w00)):
        sum = 0
        for j in range(len(w10)):
            sum += w10[j] / 2 * f([(1 + x00[i]) * (1 - x10[j]) / 2 - 1, x10[j]])
        #
        total += w00[i] * sum
        # total += w00[i] * np.dot(w10/2, f([(1+x00[i])*(1 - x10)/2 - 1 ,x10]))

    return total

def test():
    cdef array.array[double] anorms2_arr = array.array('d', np.array([1,2,3]))
    jacobi(5, 1, 1, anorms2_arr.data.as_doubles, 3)
    return(np.frombuffer(anorms2_arr)  )

cdef jacobi(int n, int alpha, int beta, double * x, int m):
    # v = np.zeros((m, n+1))

    # v[:, 0] = 1
    # v[:, 1] = ( 1.0 + 0.5 * ( alpha + beta ) ) * x  + 0.5 * ( alpha - beta )
    #
    cdef double c1, c2, c3, c4
    for i in range(2, n + 1):
        c1 = 2 * i * (i + alpha + beta) * (2 * i - 2 + alpha + beta)
        c2 = (2 * i - 1 + alpha + beta) * (2 * i + alpha + beta) *(2 * i - 2 + alpha + beta)
        c3 = (2 * i - 1 + alpha + beta) * (alpha + beta) * (alpha - beta)
        c4 = - 2 * (i - 1 + alpha) * (i - 1 + beta) * (2 * i + alpha + beta)
    #     v[:, i] = ((c3 + c2 * x) * v[:, i - 1] + c4 * v[:, i - 2]) / c1
    # return v

# def phi1(double[::1] x, p: int):
#     return (-1) ** (p + 1) / p * l1(x) * jac(p - 1, 1, 1, 1 - 2 * l1(x))
#
#
# def phi2(double[::1] x, p: int):
#     return (-1) ** (p + 1) / p * l2(x) * jac(p - 1, 1, 1, 1 - 2 * l2(x))
#
#
# def phi3(double[::1] x, p: int):
#     return (-1) ** (p + 1) / p * l3(x) * jac(p - 1, 1, 1, 1 - 2 * l3(x))
#
#
# cdef l1(double[::1] x):
#     return (-x[0] - x[1]) / 2
#
#
# cdef l2(double[::1] x):
#     return (1 + x[0]) / 2
#
#
# cdef l3(double[::1] x):
#     return (1 + x[1]) / 2
