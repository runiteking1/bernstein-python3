from typing import Callable

import BernsteinFEM
from bernstein_mesh import BernsteinMesh
import numpy as np
from BernsteinFEM import BernsteinMatrix
from scipy.sparse import lil_matrix, csr_matrix, dok_matrix, dia_matrix
import CythonizedKernels.nonlinear_c as nonlinear_c
from pyevtk.hl import unstructuredGridToVTK
from pyevtk.vtk import VtkTriangle
from duffy import duffy
from scipy.sparse.linalg import spsolve, LinearOperator
from pcg import pcg
from combined_preconditioner import Preconditioner
from tqdm import tqdm
from mass_preconditioner import MassPreconditioner

class BernsteinMatrix3D(BernsteinMatrix):
    def __init__(self, myMesh: BernsteinMesh):
        super(BernsteinMatrix3D, self).__init__(myMesh)

    def assemble_matrix(self) -> (csr_matrix, csr_matrix, np.ndarray):
        """
        Generates the mass and stiffness matrix

        :return Stiffness, Mass, Force
        """
        stiffness_raw = lil_matrix((self.eldof.max() + 1, self.eldof.max() + 1), dtype=np.double)
        mass_raw = lil_matrix((self.eldof.max() + 1, self.eldof.max() + 1), dtype=np.double)
        force_raw = np.zeros(self.eldof.max() + 1, )

        # from tqdm import tqdm
        # ref_mk = self.mass_multinomial.mass_struct(1 / ((self.p + 1) * (2 * self.p + 1)))

        # Populate the matrices
        for k in tqdm(range(self.elements)):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Calculate element matrices
            mk = self.mass_multinomial.mass_struct(area / ((self.p + 1) * (2 * self.p + 1)))
            sk = self.stiff_multinomial.stiff_struct(np.array([t1, t2, t0]).T, area)
            fk = self.element_load(self.coordinates[self.elnode[k, 0]], self.coordinates[self.elnode[k, 1]],
                                   self.coordinates[self.elnode[k, 2]], area, self.f)

            for i in range(self.mesh.dofs_per_element):
                iglobal = self.eldof[k, i]
                i_dof = self.mesh.lookup_dof(i)
                force_raw[iglobal] += fk[i_dof[:-1]]
                for j in range(self.mesh.dofs_per_element):
                    jglobal = self.eldof[k, j]
                    j_dof = self.mesh.lookup_dof(j)
                    mass_raw[iglobal, jglobal] += mk[i_dof + j_dof]
                    stiffness_raw[iglobal, jglobal] += sk[i_dof + j_dof]

        return csr_matrix(stiffness_raw), csr_matrix(mass_raw), force_raw

    @staticmethod
    def f(x: np.ndarray) -> float:
        return 1

    def nonlinear(self, u: np.ndarray, v: np.ndarray) -> np.ndarray:
        """
        Nonlinear uv^2 generator
        :param u: u
        :param v: v
        :return: uv^2 in Bernstein coordinates
        """
        out = np.zeros((self.eldof.max() + 1,))

        c = np.zeros((self.p + 1,) * 2)
        d = np.zeros((self.p + 1,) * 2)
        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Put u and v into a form that I can evaluate function
            c[np.triu_indices(self.p + 1)] = u[self.eldof[k]][self.mesh.c]
            c = np.fliplr(c)
            d[np.triu_indices(self.p + 1)] = v[self.eldof[k]][self.mesh.c]
            d = np.fliplr(d)
            c = np.ascontiguousarray(c)
            d = np.ascontiguousarray(d)

            U = nonlinear_c.evaluate(c, self.q, self.p, 2, self.xis)
            V = nonlinear_c.evaluate(d, self.q, self.p, 2, self.xis)

            F = (U ) * V**2

            outk = 2 * area * nonlinear_c.moment(F, self.q, self.p, 2, self.xis, self.omegas)

            for i in range(self.mesh.dofs_per_element):
                out[self.eldof[k, i]] += outk[self.mesh.lookup_dof(i)[:-1]]

        return out

    def element_load(self, t0, t1, t2, area: float, func: Callable) -> np.ndarray:
        """
        Uses moments calculations for load functions

        :type t0: numpy.ndarray
        :type t1: numpy.ndarray
        :type t2: numpy.ndarray
        :type func: Callable
        :param t0: side one (watch for order)
        :param t1: side two
        :param t2: side three
        :param area: area of triangle
        :param func: function call to set forcing term
        :return:
        """
        fa = np.ones((self.q, self.q))
        vertices = np.zeros((3, 3))
        vertices[:, 0] = t0
        vertices[:, 1] = t1
        vertices[:, 2] = t2

        for i in range(self.q):
            for j in range(self.q):
                fa[i, j] = func(duffy([self.xis[1, i], self.xis[0, j]], vertices))

        return 2 * area * nonlinear_c.moment(fa, self.q, self.p, 2, self.xis, self.omegas)

    def plot(self, values: np.ndarray, subdivision: int = 1, grad: bool = False, save: bool = False, index: int = 0):
        """
        Plots the Bernstein solution corresponding to this finite element method

        Subdivides twice; should be enough for most applications

        :param values:
        :param subdivision: Subdivisions to use, default of 1
        :param save: save the image or not
        :param index: name of save file
        :return: None
        """
        # First subdivide, need to aggregate information from mesh
        triangles = list()

        for elem in range(self.elements):
            D = np.zeros((self.p + 1,) * 2, dtype=np.float)

            my_element = self.eldof[elem][self.mesh.c]

            D[np.triu_indices(self.p + 1)] = values[my_element]
            D = np.fliplr(D)

            triangles.append(tuple((D,
                [self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
                 self.coordinates[my_element[-1]]])))

        for s in range(subdivision):
            holder = list()
            for t in triangles:
                holder.extend(self.subdivide_2d_gallier(t[0], t[1]))
            triangles = holder

        # Apparently it is faster to aggregate into one giant list instead of having multiple plot commands
        all_xs = list()
        all_ys = list()
        all_zs = list()
        all_us = list()
        all_triangulations = list()
        scale = 0
        for t in triangles:
            xs1, ys1, zs1, us1 = self.plot_triangle(t[0], t[1], self.bary1, self.bary2, self.bary3)

            all_triangulations.extend(self.triangulation + len(all_xs))
            all_xs.extend(xs1)
            all_ys.extend(ys1)
            all_zs.extend(zs1)
            all_us.extend(us1)
            scale = max(scale, (np.max(t[0])))

        if self.mplot:
            pass
        else:
            offset = np.arange(1, len(all_triangulations) + 1, dtype=int) * 3
            ctype = np.ones(len(all_triangulations)) * VtkTriangle.tid

            unstructuredGridToVTK('data/plot_torus%04d' % (index,), np.array(all_xs), np.array(all_ys),
                                  np.array(all_zs),
                                  connectivity=np.concatenate(all_triangulations), offsets=offset, cell_types=ctype,
                                  cellData=None, pointData={'values' : np.array(all_us)})

    @staticmethod
    def init_u(x: np.ndarray) -> float:
       # if x[0] > 5.1:
       #     return .5
       # elif x[0] > 5:
       #     return .5*jump(x[0], 5, .1) + .5
       # else:
       #     return 1
       if x[0] > 2.6:
           return .5
       elif x[0] > 2.5:
           return .5*jump(x[0], 2.5, .1) + .5
       else:
           return 1
        # if x[0] > 1.1:
        #     return .5
        # elif x[0] > 1:
        #     return .5*jump(x[0], 1, .1) + .5
        # else:
        #     return 1

    @staticmethod
    def init_v(x: np.ndarray) -> float:
        # if x[0] > 5.1:
        #     return .25
        # elif x[0] > 5:
        #     return .25*jump(x[0], 5, .1)
        # else:
        #     return 0
        if x[0] > 2.6:
            return .25
        elif x[0] > 2.5:
            return .25*jump(x[0], 2.5, .1)
        else:
            return 0

        # if x[0] > 1.1:
        #     return .25
        # elif x[0] > 1:
        #     return .25*jump(x[0], 1, .1)
        # else:
        #     return 0

    @staticmethod
    def plot_triangle(ctrl: np.ndarray, vertices: np.ndarray, bary1, bary2, bary3):
        """
        Returns 1d arrays of x, y, z for trisurf plotting

        :param bary3: Third barycentric coordinates
        :param bary2: Second barycentric coordinates
        :param bary1: First barycentric coordinates
        :param ctrl: control points
        :param vertices: self-explanatory
        :return:
        """
        p = ctrl.shape[0] - 1

        xs = vertices[0][0] * bary1 + vertices[1][0] * bary2 + vertices[2][0] * bary3
        ys = vertices[0][1] * bary1 + vertices[1][1] * bary2 + vertices[2][1] * bary3
        zs = vertices[0][2] * bary1 + vertices[1][2] * bary2 + vertices[2][2] * bary3

        return np.rot90(xs, -1)[np.triu_indices(p + 1)], np.rot90(ys, -1)[np.triu_indices(p + 1)], \
               np.rot90(zs, -1)[np.triu_indices(p + 1)], np.rot90(ctrl, -1)[np.triu_indices(p + 1)]

def jump(x, a, r):
    return np.exp(1/((x-a)**2 - r**2) + 1/r**2)

def load_file(name: str):
    file_in = open(name, mode='r')

    for line in file_in:
        if "$Nodes" in line:
            num_nodes = int(file_in.readline())
            vertices = np.zeros((num_nodes, 3))
            for j in range(num_nodes):
                vertices[j, :] = np.array([float(x) for x in file_in.readline().strip().split(' ')[1:]])

        if "$Elements" in line:
            total_elements = int(file_in.readline())
            elements = list()
            boundary = list()
            for j in range(total_elements):
                # First classify between boundary and real element
                data = [int(x)-1 for x in file_in.readline().strip().split(' ')]

                # First case is triangle
                if data[1] == 1:
                    elements.append(tuple(sorted(data[5:])))

    file_in.close()
    return vertices, elements, np.array([])

def wrapper(p: int, subdivide: int, delta_t=0.01, verbose=True, name='2.msh', plot=False, animate=False):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """

    # Setup on output file
    if name is None:
        name = "data/data_%d_%d_%.1e.dat" % (p, subdivide, delta_t)

    if verbose:
        print("p = {}, time step of {}, subdivide of {}".format(p, delta_t, subdivide))

    vertices, elements, boundaries = load_file(name)
    if verbose:
        print(len(elements))
    mesh = BernsteinMesh(vertices, elements, boundaries, p=p)
    fem = BernsteinMatrix3D(mesh)
    fem.mplot = False

    S, M, f = fem.assemble_matrix()
    if verbose:
        print(M.shape)
        print("Generated Matrix")

    # Setup preconditioner
    # Solver_u = Preconditioner(M * (1 + delta_t * (F)) + delta_t * d_u / 2 * S, fem)
    # Solver_v = Preconditioner(M * (1 + delta_t * (F + k)) + delta_t * d_v / 2 * S, fem)
    Solver_mass = MassPreconditioner(M, fem)
    if verbose:
        print("Generated preconditioner; setting initial conditions")

    # Set initial conditions
    u_initial_rhs = fem.l2_projection(fem.init_u)
    v_initial_rhs = fem.l2_projection(fem.init_v)

    import time
    start = time.time()
    u, iter, _ = pcg(M, u_initial_rhs, P=Solver_mass)
    stop = time.time()
    # print("Precond:", iter, stop - start)

    # start = time.time()
    # u, iter, _ = pcg(M, u_initial_rhs)
    # stop = time.time()
    # print("No precond:", iter, stop - start)

    v, iter, _ = pcg(M, v_initial_rhs, P=Solver_mass)
    Fw = F * f  # Vector for (b, w) in variational form (constant)

    if verbose:
        print("Initial Conditions set")

    # Eigenvalues
    # def mult_precond(v: np.ndarray) -> np.ndarray:
    # #     return Solver_mass.dot((M)  @ v)
    #     return Solver_u.dot((M * (1 + delta_t * (F)) + delta_t * d_u / 2 * S) @ v)
    #
    # PinvS = LinearOperator(dtype=M.dtype, shape=M.shape, matvec=mult_precond)
    # import scipy
    # w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8)
    # large = max(np.real(w))
    #
    # w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, tol=1E-9)
    # small = min(np.real(w))
    #
    # print(p, name, delta_t, "{:1.2f} {:1.2f} {:1.2f}".format((large / small), large, small), end='\n')

    all_u_its = list()
    all_v_its = list()
    for step in tqdm(range(int(np.rint(50 / delta_t)))):
        # First do nonlinear
        uv2 = fem.nonlinear(u, v)

        # First solve u
        u, it, resid = pcg(M * (1 + delta_t * (F)) + delta_t * d_u / 2 * S,
                           M.dot(u) + delta_t * (Fw - uv2 - d_u / 2 * S.dot(u)), P=Solver_mass, x0=u)
        all_u_its.append(it)
        # print(it)

        # Now solve v
        v, it, resid = pcg(M * (1 + delta_t * (F + k)) + delta_t * d_v / 2 * S,
                           M.dot(v) + delta_t * (uv2 - d_v / 2 * S.dot(u)), P=Solver_mass, x0=v)
        all_v_its.append(it)
        # print(it)

        if animate and step % 10 ==0:  # and step in [833, 1666]:
            fem.plot(u, save=True, index=step, grad=False, subdivision=1)

    if verbose:
        import statistics
        print("Min, Median, Max: [{}, {}, {}]".format(min(all_u_its), int(statistics.median(all_u_its)), max(all_u_its)))
        print("Min, Median, Max: [{}, {}, {}]".format(min(all_v_its), int(statistics.median(all_v_its)), max(all_v_its)))

    if plot:
        # fem.contour_plot(u)
        # fem.contour_plot(v)
        # fem.plot(u, subdivision=2)
        fem.plot(u, subdivision=2)

    import gc
    gc.collect()

if __name__ == '__main__':
    # coords_3d = np.array([[0,0,0], [1, 0, 0], [0, 1, 0], [1, 1, 5*np.sqrt(2)]])
    # # coords_3d = np.array([[0,0,0], [1, 0, 0], [0, 1, 0]])
    # # coords_3d = np.array([[0,0,0], [np.sqrt(2)/2, 0, np.sqrt(2)/2], [0, 1, 0]])
    # elnode_3d = np.array([[0, 1, 2], [1, 2, 3]])
    # # elnode_3d = np.array([[0, 1, 2]])
    # bc_3d = np.array([])
    # #
    # b = BernsteinMesh(coords_3d, elnode_3d, bc_3d, p=4, subdiv=0)
    # matrixmaker = BernsteinMatrix3D(b)
    # stiff, M, force = matrixmaker.assemble_matrix()
    # Solver_mass = MassPreconditioner(M, matrixmaker)
    #
    # def mult_precond(v: np.ndarray) -> np.ndarray:
    #     return Solver_mass.dot((M)  @ v)
    # #     # return Solver_u.dot((M * (1 + delta_t * (F)) + delta_t * d_u / 2 * S) @ v)
    #
    # PinvS = LinearOperator(dtype=M.dtype, shape=M.shape, matvec=mult_precond)
    # import scipy
    # w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8)
    # large = max(np.real(w))
    #
    # w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, tol=1E-9)
    # small = min(np.real(w))
    #
    # print("{:1.2f}".format((large / small)), end='\n')

    # print(Solver_mass.solve(np.ones(M.shape[0])))

    # vertices, elements, boundaries = load_file('0.msh')
    # torus = BernsteinMesh(vertices, elements, boundaries, p=4)
    # matrixmaker = BernsteinMatrix3D(torus)

    # print(matrixmaker.init_u([1.1, 1, 1]))
    # stiff, mass, force = matrixmaker.assemble_matrix()
    # u_initial_rhs = matrixmaker.l2_projection(matrixmaker.init_u)
    # v_initial_rhs = matrixmaker.l2_projection(matrixmaker.init_v)

    # alphas_u = np.linalg.solve(mass.todense(), u_initial_rhs)
    # alphas_v = np.linalg.solve(mass.todense(), v_initial_rhs)
    # matrixmaker.plot(alphas_v)

    d_u = 2*10**(-5)
    d_v = 10**(-5)

    # Theta region
    # k = 0.058
    # F = 0.04

    # alpha region0
    k = .05
    F =  .01 # .02 in picture of donut # (outputed with subdivision = 1)

    # kappa region
    # k = .062
    # F = 0.04

    F = .04 # Constant for the tables in paper

    # wrapper(16, 0, delta_t=1, animate=False, plot=True, name='1_less.msh')
    # wrapper(3, 0, delta_t=1, animate=True, plot=False, name='monkey_sub.msh')
    # wrapper(4, 0, delta_t=1, animate=True, plot=False, name='monkey_sub.msh')
    # wrapper(5, 0, delta_t=1, animate=True, plot=False, name='monkey_sub.msh')
    wrapper(3, 0, delta_t=1, animate=False, plot=False, name='monkey_sub.msh')
    # wrapper(4, 0, delta_t=1, animate=False, plot=False, name='monkey_sub.msh')
    # wrapper(5, 0, delta_t=1, animate=False, plot=False, name='monkey_sub.msh')
    # wrapper(6, 0, delta_t=1, animate=False, plot=False, name='monkey_sub.msh')
    # wrapper(7, 0, delta_t=1, animate=True, plot=False, name='monkey_sub.msh')
    # wrapper(8, 0, delta_t=1, animate=True, plot=False, name='monkey_sub.msh')
    # wrapper(8, 0, delta_t=1, animate=False, plot=False, name='1_less.msh')
    # wrapper(8, 0, delta_t=1, animate=False, plot=False, name='2_less.msh')
    # wrapper(12, 0, delta_t=1, animate=False, plot=False, name='2_less.msh')
    # wrapper(16, 0, delta_t=1, animate=False, plot=False, name='2_less.msh')
    # wrapper(20, 0, delta_t=1, animate=False, plot=False, name='2_less.msh')

    # wrapper(4, 0, delta_t=5, animate=False, plot=False, name='2_less.msh')
    # wrapper(8, 0, delta_t=5, animate=False, plot=False, name='2_less.msh')
    # wrapper(12, 0, delta_t=5, animate=False, plot=False, name='2_less.msh')
    # wrapper(16, 0, delta_t=5, animate=False, plot=False, name='2_less.msh')
    # wrapper(20, 0, delta_t=5, animate=False, plot=False, name='2_less.msh')
    # wrapper(4, 0, delta_t=1, animate=False, plot=False, name='1_less.msh')
    # wrapper(8, 0, delta_t=1, animate=False, plot=False, name='1_less.msh')
    # wrapper(12, 0, delta_t=1, animate=False, plot=False, name='1_less.msh')
    # wrapper(16, 0, delta_t=1, animate=False, plot=False, name='1_less.msh')
    # wrapper(20, 0, delta_t=1, animate=False, plot=False, name='1_less.msh')

    # Find eigenvalues of differing element size, delta t and p

    # from multiprocessing import Pool
    # import functools
    # pool = Pool()
    # mapped_func = functools.partial(wrapper, subdivide=0, delta_t=100, animate=False, plot=False, verbose=False, name='0_less.msh')
    # pool.map(mapped_func, [4,8,12,16,20])
    # mapped_func = functools.partial(wrapper, subdivide=0, delta_t=100, animate=False, plot=False, verbose=False, name='1_less.msh')
    # pool.map(mapped_func, [4,8,12,16,20])
    # mapped_func = functools.partial(wrapper, subdivide=0, delta_t=100, animate=False, plot=False, verbose=False, name='2_less.msh')
    # pool.map(mapped_func, [4,8,12,16,20])
    # print("Delta")
    # mapped_func = functools.partial(wrapper, subdivide=0, delta_t=10000, animate=False, plot=False, verbose=False, name='0_less.msh')
    # pool.map(mapped_func, [4,8,12,16,20])
    # mapped_func = functools.partial(wrapper, subdivide=0, delta_t=10000, animate=False, plot=False, verbose=False, name='1_less.msh')
    # pool.map(mapped_func, [4,8,12,16,20])
    # mapped_func = functools.partial(wrapper, subdivide=0, delta_t=10000, animate=False, plot=False, verbose=False, name='2_less.msh')
    # pool.map(mapped_func, [4,8,12,16,20])
    #
    # pool.terminate()

    # wrapper(4, 0, delta_t=1, animate=False, plot=False, verbose=False, name='0_less.msh')
    # wrapper(8, 0, delta_t=1000, animate=False, plot=False, verbose=False, name='0_less.msh')
    # wrapper(12, 0, delta_t=1000, animate=False, plot=False, verbose=False, name='0_less.msh')
    # wrapper(16, 0, delta_t=1000, animate=False, plot=False, verbose=False, name='0_less.msh')
    # wrapper(20, 0, delta_t=1000, animate=False, plot=False, verbose=False, name='0_less.msh')
