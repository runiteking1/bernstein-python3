import numpy as np
from pyevtk.hl import unstructuredGridToVTK
from pyevtk.vtk import VtkTriangle


def index_set(n: int, d: int):
    """
    Generates I^n_{d-1}

    :param n: sum of tuple
    :param d: dimension of the set
    :return: a generator of tuples
    """
    if d == 1:
        yield (n,)
    elif d == 0:
        yield ()
    else:
        for i in range(n + 1):  # Loop through all possible in the sum
            for j in index_set(n - i, d - 1):  # Recursively call
                yield (i,) + j


class Plotting:
    def __init__(self, p, coords, elnode, eldof):
        self.p = p
        self.coordinates = coords
        self.elnode = elnode
        self.eldof = eldof
        self.elements = len(self.elnode)

        self.dofs_per_element = int((p + 1) * (p + 2) / 2)

        # pre-processing for dofs_to_multi function; creates good dofs for interior points
        self.multi = list(index_set(p - 3, 3))

        # Create the inverse to lookup_dof
        self.lookup_multi = dict()
        for i in range(self.dofs_per_element):
            self.lookup_multi[self.lookup_dof(i)] = i

        # Create list such that we can output an element to the appropriate DOFs in matrix form once reshaped
        # eg given an element, we can find the (p+1, p+1) matrix such that (0,0,0), (1, 0, 0) is laid out in matrix form
        c = np.zeros((self.p + 1,) * 2, dtype=np.int)
        for k1 in range(0, self.p + 1):
            for k2 in range(0, self.p + 1 - k1):
                c[k1, k2] = self.lookup_multi[(k1, k2, self.p - k1 - k2)]

        self.c = np.fliplr(c)[np.triu_indices(self.p + 1)].astype(np.int)

        # For plotting purposes
        self.bary1, self.bary2, self.bary3 = self.generate_bary()
        self.triangulation = np.array(self.return_triangulation())

    def generate_bary(self) -> (np.ndarray, np.ndarray, np.ndarray):
        """
        Generates the 3 barycentric matrices

        :return: three matrices corresponding to the barycentric coordinates in subdivide_2d
        """
        # Generate some barycentric coordinates
        lambda_1 = np.zeros((self.p + 1, self.p + 1))
        lambda_2 = np.zeros((self.p + 1, self.p + 1))
        lambda_3 = np.zeros((self.p + 1, self.p + 1))

        for i in range(self.p + 1):
            # Because I'm lazy, and numpy is actually stupid for not having off diagonals
            for j in range(self.p - i + 1):
                lambda_1[j, self.p - i - j] = i / self.p
            lambda_2[0:self.p - i + 1, i] = i / self.p
            lambda_3[i, 0: self.p - i + 1] = i / self.p

        return lambda_1, lambda_2, lambda_3

    def lookup_dof(self, i) -> tuple:
        """
        Creates a 1-1 correspondence between the number of DOF and the multinomial.

        :param i: degree of freedom counter
        :type i: int
        :return: Corresponds to the 3-tuple monomial
        """
        if i == 0:
            return self.p, 0, 0
        elif i == 1:
            return 0, self.p, 0
        elif i == 2:
            return 0, 0, self.p
        elif i <= self.p + 1:
            return 0, self.p - (i - 2), (i - 2)
        elif i <= 2 * self.p:
            return i - (self.p + 1), 0, 2 * self.p + 1 - i
        elif i <= 3 * self.p - 1:
            return 3 * self.p - i, i - 2 * self.p, 0
        else:
            return tuple(np.array((self.multi[i - (3 * self.p)])) + np.array([1, 1, 1]))

    def return_triangulation(self):
        out = list()

        # Create a 2D matrix for helping (only need to run this once, so it shouldn't be bad)
        helper = np.zeros((self.p + 1, self.p + 1), dtype=int)
        helper[np.triu_indices(self.p + 1)] = range(int((self.p + 1) * (self.p + 2) / 2))

        # Now create top triangles
        for i in range(self.p):
            for j in range(self.p - i):
                out.append((helper[i, j + i], helper[i, j + i + 1], helper[i + 1, j + i + 1]))

        # Now the more awkward bottom triangles (Can combine with above but for readability)
        for i in range(self.p - 1):
            for j in range(self.p - 1 - i):
                out.append((helper[i, j + i + 1], helper[i + 1, j + i + 1], helper[i + 1, j + i + 2]))

        return out

    def plot(self, values: np.ndarray, subdivision: int = 1, grad: bool = False, save: bool = False, index: int = 0):
        """
        Plots the Bernstein solution corresponding to this finite element method

        Subdivides twice; should be enough for most applications

        :param values:
        :param subdivision: Subdivisions to use, default of 1
        :param save: save the image or not
        :param index: name of save file
        :return: None
        """
        # First subdivide, need to aggregate information from mesh
        triangles = list()

        for elem in range(self.elements):
            D = np.zeros((self.p + 1,) * 2, dtype=np.float)

            my_element = self.eldof[elem][self.c]

            D[np.triu_indices(self.p + 1)] = values[my_element]
            D = np.fliplr(D)

            triangles.append(tuple((D,
                                    [self.coordinates[my_element[self.p]], self.coordinates[my_element[0]],
                                     self.coordinates[my_element[-1]]])))

        for s in range(subdivision):
            holder = list()
            for t in triangles:
                holder.extend(self.subdivide_2d_gallier(t[0], t[1]))
            triangles = holder

        # Apparently it is faster to aggregate into one giant list instead of having multiple plot commands
        all_xs = list()
        all_ys = list()
        all_zs = list()
        all_us = list()
        all_triangulations = list()
        scale = 0
        for t in triangles:
            xs1, ys1, zs1, us1 = self.plot_triangle(t[0], t[1], self.bary1, self.bary2, self.bary3)

            all_triangulations.extend(self.triangulation + len(all_xs))
            all_xs.extend(xs1)
            all_ys.extend(ys1)
            all_zs.extend(zs1)
            all_us.extend(us1)
            scale = max(scale, (np.max(t[0])))

        offset = np.arange(1, len(all_triangulations) + 1, dtype=int) * 3
        ctype = np.ones(len(all_triangulations)) * VtkTriangle.tid

        unstructuredGridToVTK('plot_%04d' % (index,), np.array(all_xs), np.array(all_ys),
                              np.array(all_zs),
                              connectivity=np.concatenate(all_triangulations), offsets=offset, cell_types=ctype,
                              cellData=None, pointData={'values': np.array(all_us)})

    @staticmethod
    def plot_triangle(ctrl: np.ndarray, vertices: np.ndarray, bary1, bary2, bary3):
        """
        Returns 1d arrays of x, y, z for trisurf plotting

        :param bary3: Third barycentric coordinates
        :param bary2: Second barycentric coordinates
        :param bary1: First barycentric coordinates
        :param ctrl: control points
        :param vertices: self-explanatory
        :return:
        """
        p = ctrl.shape[0] - 1

        xs = vertices[0][0] * bary1 + vertices[1][0] * bary2 + vertices[2][0] * bary3
        ys = vertices[0][1] * bary1 + vertices[1][1] * bary2 + vertices[2][1] * bary3
        zs = vertices[0][2] * bary1 + vertices[1][2] * bary2 + vertices[2][2] * bary3

        return np.rot90(xs, -1)[np.triu_indices(p + 1)], np.rot90(ys, -1)[np.triu_indices(p + 1)], \
               np.rot90(zs, -1)[np.triu_indices(p + 1)], np.rot90(ctrl, -1)[np.triu_indices(p + 1)]

    def subdivide_2d_gallier(self, ctrl: np.ndarray, abscissa: np.ndarray):
        """
        Subdivides a triangle in an efficient manner into 4 triangle; follows the algorithm in Jean Gallier's book

        :param ctrl: control points
        :param abscissa: coordiantes corresponding to the current control points
        :return: a tuple of 4 triangles
        """

        # Step 1: Initial decast algorithm
        _, abr, arc = self.decast([.5, .5, 0], ctrl)

        # Step 2: calculate TBR
        tbr, _, _ = self.decast([0, .5, .5], abr)

        # Step 3: Calculate SRC:
        src, ars, _ = self.decast([.5, 0, .5], arc)

        # Step 4: Calculate the two remaining triangles
        trs, _, ats = self.decast([-1, 1, 1], ars)

        # Step 5: Give correct vertices data
        a_v = abscissa[0]
        b_v = abscissa[1]
        c_v = abscissa[2]
        t_v = (a_v + b_v) / 2
        r_v = (b_v + c_v) / 2
        s_v = (a_v + c_v) / 2

        return (ats, [a_v, t_v, s_v]), (trs, [t_v, r_v, s_v]), (tbr, [t_v, b_v, r_v]), (src, [s_v, r_v, c_v])

    def decast(self, lambdas: list, ctrl: np.ndarray, degree: int = None) -> (np.ndarray, np.ndarray, np.ndarray):
        """
        de Castlejau algorithm returns control nets to the three subdivided triangles.

        Triangle ABC is oriented such that A is [0,0], B, is [1, 0], C is [0, 1].
        Call the barycentric coordinate L.

        They way we store the indices allow us to have triangle LBC easily; just need to store ALC, ALB.

        :param ctrl: control point in 2d array form (NOTE: THIS IS OVERWRITTEN)
        :param lambdas: barycnetric coordinates
        :return: 4 Numpy arrays
        """
        if degree is None:
            degree = self.p

        # Storage for two other triangles
        alc = np.zeros_like(ctrl)
        abl = np.zeros_like(ctrl)

        alc[:, 0] = ctrl[:, 0]
        abl[0, :] = ctrl[0, :]

        # Iterate p times
        for r in range(degree):
            for order in range(degree - r):
                for i in range(order + 1):
                    ctrl[i, order - i] = lambdas[2] * ctrl[i, order - i] + lambdas[0] * ctrl[i + 1, order - i] + \
                                         lambdas[1] * ctrl[i, order - i + 1]
            alc[0:degree - r, r + 1] = ctrl[0:degree - r, 0]
            abl[r + 1, 0:degree - r] = ctrl[0, 0:degree - r]

        return ctrl, abl, alc

if __name__ == '__main__':
    coords = np.load('coordinates.npy')
    eldof = np.load('eldof.npy')
    elnode = np.load('elnode.npy')

    plotter = Plotting(6, coords, elnode, eldof)

    u = np.load('u_00000.npy')

    plotter.plot(u)