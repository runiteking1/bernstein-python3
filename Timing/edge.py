from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from schur_complement import SchurAction
from schur_preconditioner import SchurPreconditioner

import numpy as np

import timeit

if __name__ == '__main__':
    # One triangle
    coords = np.array([[0, 0], [1, 0], [0, 1]])
    elnode = np.array([[0, 1, 2]])
    dir_dof = np.array([])

    output = open('edge_timing.data', 'w')

    print("p\tbackslash\topt", file=output)

    for i in range(4, 24):
        mesh = BernsteinMesh(coords, elnode, dir_dof, i)
        Fem = BernsteinMatrix(mesh)
        _, M, _ = Fem.assemble_matrix()

        schur = SchurAction(M, Fem)
        P = SchurPreconditioner(schur, Fem)

        # Calculate Schur complement, since I use an action now
        a = M[0:3 * i, 0:3 * i].todense()
        c = M[3 * i:, 3 * i:].todense()
        b = M[0:3 * i, 3 * i:].todense()
        S = a - b.dot(np.linalg.inv(c).dot(b.T))

        # Solve with regular backslash
        C = np.zeros(S[3:, 3:].shape)
        C[0:i - 1, 0:i - 1] = S[3:(3 + (i - 1)), 3:(3 + (i - 1))]
        C[i - 1:2 * (i - 1), i - 1:2 * (i - 1)] = S[2 + i:2 * i + 1, 2 + i:2 * i + 1]
        C[2 * (i - 1):, 2 * (i - 1):] = S[2 * i + 1:, 2 * i + 1:]
        f = np.ones((C.shape[0],))
        t1 = timeit.timeit(lambda: np.linalg.solve(C, f), number=2000) / 2000
        # print(np.linalg.solve(C, f))

        # Now solve with the edge code
        # NOTE: BE SURE TO GO TO SCHUR_PRECONDITIONER AND CHANGE IT
        f = np.ones((schur.shape[0],))
        t2 = timeit.timeit(lambda: P.solve(f), number=2000) / 2000
        # print(P.solve(f))

        print('%d\t%.15f\t%.15f' % (i, t1, t2), file=output)

        print(i)

    output.close()
