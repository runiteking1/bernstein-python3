from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from mass_solver import MassSolver

import numpy as np

import timeit

if __name__ == '__main__':
    # One triangle
    coords = np.array([[0, 0], [1, 0], [0, 1]])
    elnode = np.array([[0, 1, 2]])
    dir_dof = np.array([])

    output = open('interior_timing.data', 'w')

    print("p\tbackslash\tmanuel", file=output)

    for i in range(4, 24):
        mesh = BernsteinMesh(coords, elnode, dir_dof, i)
        Fem = BernsteinMatrix(mesh)
        _, M, _ = Fem.assemble_matrix()

        Solver = MassSolver(M, Fem)

        # Solve with regular backslash
        C = M[3 * i:, 3 * i:].todense()
        f = np.ones((C.shape[0],))
        t1 = timeit.timeit(lambda: np.linalg.solve(C, f), number=2000) / 2000
        print(np.linalg.solve(C, f))

        # Now solve with the cinverse code
        t2 = timeit.timeit(lambda: Solver.schur.cinv.dot(f), number=2000) / 2000
        print(Solver.schur.cinv.dot(f))

        print('%d\t%.15f\t%.15f' % (i, t1, t2), file=output)

        print(i)

    output.close()
