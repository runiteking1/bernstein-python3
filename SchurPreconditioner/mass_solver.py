from scipy.sparse.linalg import LinearOperator

import BernsteinFEM
import numpy as np

from pcg_2 import pcg_2
from schur_complement import SchurAction
from schur_preconditioner import SchurPreconditioner


class MassSolver(LinearOperator):
    def __init__(self, mass: np.ndarray, fem: BernsteinFEM.BernsteinMatrix):
        """
        Setup the Schur Complement-based preconditioner

        :param mass: mass matrix
        :param fem: finite element generator
        """
        super(MassSolver, self).__init__(dtype=None, shape=mass.shape)

        self.schur = SchurAction(mass, fem)
        self.ns = self.schur.ns
        self.p = SchurPreconditioner(self.schur, fem)

        ## Counter/iteration counting for data output; delete for applications
        self.counter = 0
        self.itsum = list()
        ##

    def _matvec(self, x):
        return self.solve(x)

    def solve(self, f):
        """
        Solve using Schur complement approach

        :param f:
        :return:
        """
        f1 = f[0:self.ns]
        f2 = f[self.ns:]

        rhs = f1 - self.schur.b.dot(self.schur.cinv.dot(f2))

        x1, it = pcg_2(self.schur, rhs, M=self.p, eps=1e-8)

        ## Data plotting, delete for applications
        self.itsum.append(it)
        # print(self.itsum)
        # self.counter += 1
        ##

        x2 = self.schur.cinv.dot(f2 - self.schur.b.T.dot(x1))
        return np.concatenate((x1, x2))
