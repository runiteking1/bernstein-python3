from unittest import TestCase

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from mass_solver import MassSolver


class TestMassSolver(TestCase):
    def test_solve(self):
        import numpy as np
        p = 20

        # 1 Triangles
        # coords_tri = np.array([[0,0], [1, 0], [0, 1]])
        # elnode_tri = np.array([[0,1,2]])
        # bc_tri = np.array([])

        coords_john = np.array(
            [[1 / 4, 1 / 4], [0, 5 / 4], [1, 0], [1, 3 / 4], [1, 3 / 2], [3, 0], [5 / 2, 1 / 2], [3 / 2, 1], [2, 3 / 2],
             [3, 1 / 2]])
        elnode_john = np.array(
            [[1, 3, 4], [1, 4, 2], [4, 5, 2], [3, 8, 4], [4, 8, 5], [8, 9, 5], [6, 7, 3], [3, 7, 8], [8, 7, 9],
             [7, 10, 9],
             [6, 10, 7]])
        # bc_john = np.array([[1], [2], [3], [5], [6], [9], [10]])
        bc_john = np.array([])
        elnode_john -= 1
        # bc_john[:, 0] -= 1

        mesh = BernsteinMesh(coords_john, elnode_john, bc_john, p=p, subdiv=1)
        b = BernsteinMatrix(mesh)

        _, M, _ = b.assemble_matrix()
        rhs = M.dot(np.ones((M.shape[0])))

        Solver = MassSolver(M, b)

        np.testing.assert_almost_equal(np.ones((M.shape[0])), Solver.solve(rhs), decimal=2)  # Should be somewhat close
