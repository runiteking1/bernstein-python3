from scipy.sparse.linalg import LinearOperator
import numpy as np
from BernsteinFEM import BernsteinMatrix
from scipy.special import binom
import scipy.sparse as scsp
import CythonizedKernels.edge_c as edge_c


class SchurPreconditioner(LinearOperator):
    """
    Preconditioner for the Schur Complement based on additive Schwarz theory
    """

    def __init__(self, s: LinearOperator, fem: BernsteinMatrix):
        """
        Though not necessary, for developmental purposes, we use the full Schur Complement matrix

        :type fem: BernsteinMatrix
        :param s:
        :param fem:
        """
        super(SchurPreconditioner, self).__init__(dtype=s.dtype, shape=s.shape)

        self.vertices = len(fem.coordinates)  # Number of vertices
        self.transform2bernstein = np.zeros((self.vertices, s.shape[0]))
        self.p = fem.p
        self.edge_dofs = self.p - 1
        self.num_edges = int((s.shape[0] - self.vertices) / self.edge_dofs)
        self.vertex_scaling = np.zeros((self.vertices,))  # Scaling for vertex

        # Create correct transformations
        diags = np.zeros(s.shape[0], )

        # Vertex functions are a bit odd now, need to adjust
        # cps = self._cp(np.arange(1, self.p), self.p) # Old basis
        q = int(np.floor(self.p / 2))
        cps = self._cp(np.arange(0, q+1), q)
        for i in range(self.p - q):
            cps = self._raise(cps)
        cps = cps[1:-1]

        for k in range(fem.elements):
            # First find the determinant, and hence the diagonal elements
            values = self._diagonal(self._abs_deter(
                np.array([fem.coordinates[fem.elnode[k, 0]], fem.coordinates[fem.elnode[k, 1]],
                          fem.coordinates[fem.elnode[k, 2]]])))

            self.vertex_scaling[fem.elnode[k, :]] += 16 * self._abs_deter(
                np.array([fem.coordinates[fem.elnode[k, 0]], fem.coordinates[fem.elnode[k, 1]],
                          fem.coordinates[fem.elnode[k, 2]]]))

            # Now we put it into the list
            diags[sorted(fem.eldof[k][3:(3 + 3 * self.edge_dofs)])] += np.concatenate((values,) * 3)

            # Now we deal with the vertices; have to hardcode this... unless we introduce more data structures to fem

            c = np.zeros((fem.p + 1,) * 2)
            c[np.triu_indices(fem.p + 1)] = fem.eldof[k][fem.mesh.c]

            edge1 = np.diag(c)[-2:0:-1].astype(int)  # From ELNODE[k, 0] to ELNODE[k, 1]
            edge2 = c[0, 1:-1].astype(int)  # From ELNODE[k, 1] to ELNODE[k, 2]
            edge3 = c[1:-1, -1].astype(int)  # From ELNODE[k, 2] to ELNODE[k, 0]

            self.transform2bernstein[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
            self.transform2bernstein[fem.elnode[k, 1], fem.elnode[k, 1]] = 1
            self.transform2bernstein[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

            self.transform2bernstein[fem.elnode[k, 0], edge1] = cps
            self.transform2bernstein[fem.elnode[k, 0], edge3[::-1]] = cps

            self.transform2bernstein[fem.elnode[k, 1], edge2] = cps
            self.transform2bernstein[fem.elnode[k, 1], edge1[::-1]] = cps

            self.transform2bernstein[fem.elnode[k, 2], edge3] = cps
            self.transform2bernstein[fem.elnode[k, 2], edge2[::-1]] = cps

        # Vertex solve scaling
        self.vertex_scaling = 1 / self.vertex_scaling   # need to invert

        # Aggregate data for edge solves
        self.inverse_diagonals = np.zeros(s.shape[0])
        self.inverse_diagonals[self.vertices:] = 1 / diags[
                                                     self.vertices:]  # Not memory efficient, but easiest way to proceed
        self.vector_lengths = len(self.inverse_diagonals)

        # Do a resacle to fit more the math
        self.transform2bernstein = self.transform2bernstein.T

        # We can calculate the vertex block (possibly not needed in future) of Lobatto basis
        self.phi_block = np.zeros((self.vertices, self.vertices))
        for i in range(self.vertices):
            self.phi_block[:, i] = self.transform2bernstein.T.dot(s.dot(self.transform2bernstein[:, i]))

        # Convert back to sparse
        self.phi_block = scsp.csr_matrix(self.phi_block)  # spsolve wants a matrix
        self.transform2bernstein = scsp.csr_matrix(self.transform2bernstein)

        # Generate coefficients TODO: Generate these more efficiently
        self.algo_coefs = np.zeros((self.p - 1, self.p - 1))
        for n in range(self.p - 1):
            for i in range(n + 1):
                self.algo_coefs[n, i] = (-1) ** (n - i) * binom(n + 2, i) * binom(n + 2, n - i) / binom(n, i)

        self.cinv_coefs = np.zeros((self.p - 1, self.p - 1))
        for i in range(self.p - 2, -1, -1):
            for j in range(i + 1):
                self.cinv_coefs[i, j] = (-1) ** (i - j) * binom(i + 2, j) * binom(i + 2, i - j) / binom(i, j) * (
                        j + 1) / (i + 1) * (i - j + 1) / (i + 2)

        # Finally, create helper arrays for the Cython calls
        self.subworker = np.zeros((self.edge_dofs + 1,), dtype=np.double)
        self.worker = np.zeros((self.edge_dofs,), dtype=np.double)

    def _cp(self, j: np.ndarray, q: int) -> np.ndarray:
        """
        Coefficients of converting to Lobatto basis
        :param j:
        :return:
        """
        return (-1.0) ** j * binom(q, q - 1 - j) / q

    def _diagonal(self, det: float):
        """
        Calculates the diagonal indices (with scaling) of edge elements

        :param det: determinent from "abs_deter"
        :return:
        """
        indices = np.arange(self.p - 1)
        return det * 2 * self._mu(indices) / ((self.p + indices + 4) * (self.p - indices - 1))

    @staticmethod
    def _abs_deter(coords) -> float:
        """
        Absolute value of determinant of transformation for general triangle to reference triangle (-1, -1), (-1, 1),
        and (1, -1).

        Mainly used for edge transformation

        :param coords: Coordinates of triangle
        :return: absoluute value of determinant of Jacobian
        """
        return np.abs(.25 * (coords[2, 0] * (coords[0, 1] - coords[1, 1]) + coords[0, 0] * (coords[1, 1] - coords[2, 1])
                             + coords[1, 0] * (coords[2, 1] - coords[0, 1])))

    def _raise(self, cpoints: np.ndarray) -> np.ndarray:
        """
        Degree raises the Bernstein control points in 1D. Infers order from length

        :param cpoints: Control points of degree len(cpoints) - 1
        :return: Control points of same Bernstein spline but with 1 higher order
        """
        p = len(cpoints)
        out = np.zeros((p + 1,))

        for j in range(p):
            out[j] += (p - j) / p * cpoints[j]
            out[j + 1] += (j + 1) / p * cpoints[j]

        return out

    @staticmethod
    def _mu(i: np.ndarray) -> np.ndarray:
        """
        See paper; diagonal scaling

        :param i:
        :return: correct scaling
        """
        return 1 / ((2 * i + 5) / 32 * (i + 3) * (i + 4) / ((i + 1) * (i + 2)))

    def _matvec(self, x):
        return self.solve(x)

    def solve(self, f: np.ndarray) -> np.ndarray:
        """
        Given Px = f, returns x

        :param f: RHS to Solve
        :type f: np.ndarray
        :return: Solution to Px = f
        """
        out = np.zeros(self.vector_lengths, )

        edge_c.process(self.num_edges, self.edge_dofs, self.inverse_diagonals[self.vertices:], f[self.vertices:],
                       self.subworker, self.worker, self.cinv_coefs, self.algo_coefs, out[self.vertices:])

        #
        # IF DOING ON EDGES TIMING COMMENT OUT FOLLOWING LINES
        # out += self.transform2bernstein.dot(scsp.linalg.spsolve(self.phi_block, self.transform2bernstein.T.dot(f)))
        out += self.transform2bernstein.dot(self.vertex_scaling * (self.p ** 4) * self.transform2bernstein.T.dot(f))
        #

        # import line_profiler # Old profiler code
        # profile = line_profiler.LineProfiler(transforms.algo)
        # profile.runcall(transforms.algo, x[:self.edge_dofs], self.cinv_coefs)
        # profile.print_stats()

        return out


if __name__ == '__main__':
    # Output
    np.set_printoptions(precision=4, linewidth=300)

    S = np.eye(5)
