import numpy as np
from scipy.sparse.linalg import LinearOperator
from scipy.special import binom
import CythonizedKernels.interior_c as interior_c

from BernsteinFEM import BernsteinMatrix


class SchurAction(LinearOperator):
    def __init__(self, mass: np.ndarray, fem: BernsteinMatrix):
        """
        Setup a Schur Complement linear operator; requires mass matrix to be formed
        """
        self.cinv = CInverse(mass, fem)
        self.ns = mass.shape[0] - fem.elements * self.cinv.internal_dofs
        self.b = mass[0:self.ns, self.ns:mass.shape[0]].tocsr()
        self.a = mass[0:self.ns, 0:self.ns]
        super(SchurAction, self).__init__(mass.dtype, self.a.shape)

    def _matvec(self, x):
        return self.a.dot(x) - self.b.dot(self.cinv.dot(self.b.T.dot(x)))


class CInverse(LinearOperator):
    """
    Object to represent the inverse of internal nodes
    """

    def __init__(self, mass: np.ndarray, fem: BernsteinMatrix):
        self.p = fem.p
        self.elements = fem.elements

        # Find shapes
        self.internal_dofs = int((self.p + 2) * (self.p + 1) / 2) - 3 * (self.p - 1) - 3

        super(CInverse, self).__init__(dtype=mass.dtype, shape=(self.internal_dofs * self.elements, self.internal_dofs
                                                                * self.elements))

        # Extractor matrix for Manuel's code
        grid = np.indices((self.p - 2, self.p - 2))
        self.indices = grid[0] + grid[1] <= (self.p - 3)
        self.C = np.zeros((self.p + 1,) * 2)

        self.scaling = np.zeros((self.elements,))
        for i in range(self.elements):
            self.scaling[i] = self._abs_deter(np.array([fem.coordinates[fem.elnode[i, 0]],
                                                        fem.coordinates[fem.elnode[i, 1]],
                                                        fem.coordinates[fem.elnode[i, 2]]]))

        # Additional stuff for Cython
        self.j2b = self._jacobi22bernsteincoefficients(self.p - 2)
        self.nu = self._nucoefficients(self.p - 2)
        self.q = np.zeros((self.p - 2, self.p - 2), dtype=np.double)
        self.output = np.zeros(int((self.p - 1) * (self.p - 2) / 2), dtype=np.double)

    def _matvec(self, x):
        out = np.zeros((self.internal_dofs * self.elements,))

        interior_c.process(x, self.internal_dofs, self.elements, self.p, self.j2b, self.nu, self.q, self.scaling,
                           out)

        return out

    @staticmethod
    def _abs_deter(coords: np.ndarray) -> float:
        """
        Absolute value of determinant of transformation given coordinates.
        NOTE THAT THIS IS SCALED DIFFERENTLY FROM THE EDGE ONE DUE TO DIFFERENCE REFERENCE TRIANGLES.

        :param coords: coordinates of triangle
        :return: absolute value of determinant
        """
        t0 = -(coords[0, :] - coords[1, :])
        t1 = -(coords[0, :] - coords[2, :])
        t2 = -(coords[1, :] - coords[2, :])

        return area(t0, t1, t2) * 2

        # return np.abs((coords[2, 0] * (coords[0, 1] - coords[1, 1]) + coords[0, 0] * (coords[1, 1] - coords[2, 1]) +
        #                coords[1, 0] * (coords[2, 1] - coords[0, 1])))

    @staticmethod
    def _jacobi22bernsteincoefficients(n):
        gamma = np.zeros((n + 1, n + 1), dtype=np.double)
        alpha = 2.0
        beta = 2.0
        gamma[0, 0] = 1.0
        for r in range(0, n + 1):
            for i in range(0, r + 1):
                gamma[r, i] = (-1) ** (r + i) * binom(r + alpha, alpha + i) * (r + 1) * (r + 2) / ((r + 2 - i) *
                                                                                                   (r + 1 - i))

        return gamma

    @staticmethod
    def _nucoefficients(n):
        def nu22(nu_k, nu_n, r):
            nuknr = ((nu_n - r + 1.0) * (nu_n - r + 2.0) / 2.0) * ((-1) ** nu_k) * (
                    binom(nu_n - r, nu_k) * binom(nu_n + r + 5.0, nu_k) * 2.0) / (
                            (nu_k + 1.0) * (nu_k + 2.0) * binom(nu_n, nu_k))
            return nuknr

        nus = np.zeros((n + 1, n + 1, n + 1), dtype=np.double)
        for j in range(n + 1):
            for i in range(j + 1):
                for k in range(n + 1):
                    nus[i, j, k] = nu22(i, j, k)
        return nus

def area(t0, t1, t2) -> float:
    """
        Calculates the area of a triangle usign Heron's formula

        We need this for the 3D code

        :type t0: np.ndarray
        :type t1: np.ndarray
        :type t2: np.ndarray
        :param t0: side 0 (numpy vector)
        :param t1: side 1 (numpy vector)
        :param t2: side 2 (numpy vector)
        :return: area of triangle
        """
    t0 = np.linalg.norm(t0)
    t1 = np.linalg.norm(t1)
    t2 = np.linalg.norm(t2)
    s = (t0 + t1 + t2) / 2
    return np.sqrt(s * (s - t0) * (s - t1) * (s - t2))

if __name__ == '__main__':
    # 1 Triangles
    # coords_tri = np.array([[0, 0], [1, 0], [0, 1]])
    # elnode_tri = np.array([[0, 1, 2]])
    # bc_tri = np.array([])

    coords_tri = np.array([[-1, -1], [1, -1], [-1, 1], [1, 1]])
    elnode_tri = np.array([[0, 1, 2], [1, 2, 3]])
    bc_tri = np.array([])

    from bernstein_mesh import BernsteinMesh

    mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=5, subdiv=0)
    b = BernsteinMatrix(mesh)

    S, M, f = b.assemble_matrix()
    schur = SchurAction(M, b)
    interior = CInverse(M, b)
    # print(interior.nu)
    print(interior.dot(np.array([1.0, 2.0, 3.0, 4, 5, 6, 7, 8, 9, 10, 11, 12])))
    # print(interior.dot(np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])))
    # print(schur.dot(np.array([1, 1, 1, 1, 1, 1, 1, 1, 1])))
