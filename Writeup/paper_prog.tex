\documentclass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
 
\usepackage{amsmath}
\usepackage{listings}
\usepackage{commath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}
\usepackage{pgfplots}
\usepackage{xr}
\usepackage{subcaption}

\externaldocument[T-]{paper_theory}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}

\pgfplotsset{compat=1.12}
% \pgfplotsset{compat=1.13,
% 	table/search path = {../Eigenvalues_Timing},
% }

\begin{document}
\tableofcontents

\section{Introduction}
In a previous paper \cite{AMMPaper}, we presented a preconditioner for the mass matrix which is uniform under the order $p$. 
While the basis were chosen carefully, they are computationally hard to work with, especially the construction of minimal energy extensions. 
In the present article, we demonstrate how to efficiently implement the method using Bernstein basis finite elements. 

The structure is we present the algorithm is Section~\ref{sec:algorithm}, then we connect the algorithm to 

\section{Bernstein Polynomials}
Let $T$ be a non-degenerate triangle in $\mathbb{R}^2$ with vertices $v_1, v_2, v_3$. 
For a fixed integer $p \in \mathbb{N}^+$, we define the domain points as 
\begin{align*}
	\mathcal{D}^p(T) = \left\{ \frac{1}{p}\left(\alpha_1 v_1 + \alpha_2 v_2 + \alpha_3 v_3 \right) :  \vec{\alpha} \in \mathcal{I}^p \right\}
\end{align*}
where we have the index set $\mathcal{I}^p = \{ \vec{\alpha} \in \mathbb{Z}_+^{3}: \abs{\vec{\alpha}} = p \}$.
We use the shorthand where $\alpha_i$ denotes the $i$th component of vector $\alpha$; for simplicity, we will omit the vector notation on the multi-index $\alpha$. 

The barycentric coordinates $\lambda_i, i \in \{1,2,3\}$ of $T$ are the unique linear functions such that $\lambda_i(v_j) = \delta_{ij}$ for $i, j \in \{1,2,3\}$.
The Bernstein polynomials of degree $p$ associated with triangle $T$ is defined as 
\begin{align*}
	B^p_{{\alpha}} (x) = \frac{p!}{\alpha_{1!}\alpha_{2!}\alpha_{3!}} \lambda_1^{\alpha_1} \lambda_2^{\alpha_2} \lambda_3^{\alpha_3}
\end{align*}
for $\alpha \in \mathcal{I}^p$.
There are many mathematical and computational properties of Bernstein polynomials which we will elucidate below, but a general reference can be found in \cite{farin2002}.

Note that there is a clear 1-to-1 correspondence between a single domain point $x_{ \alpha} \in \mathcal{D}^p$ and a Bernstein polynomial $B_\alpha^p$; it is natural to think about associating a domain point to a specific Bernstein polynomial.
Define the following subsets of $\mathcal{I}^p$, $\mathcal{V} = \{ (p, 0, 0), (0, p, 0), (0, 0, p) \}, \mathcal{I} = \{\alpha \in \mathcal{I}^p_2 : \alpha_1 > 0, \alpha_2 > 0, \alpha_3>0 \}$ and $\mathcal{E} = \{\alpha \in \mathcal{I}_2^p \setminus (\mathcal{V} \cup \mathcal{I}) \}$.
Note that these are simply the multi-indices which corresponds to the vertices, interior and edge domain points. 
Furthermore, define for $i = 1,2,3$, $\mathcal{E}_i = \{ \alpha \in \mathcal{E}: \alpha \text{ lies on edge }i\}$ which are simply the domain points that lie on one edge excluding the vertices. 

By the properties of barycentric coordinates, the Bernstein polynomial associated with a vertex domain point (say $v_1$) will be 0 on $v_2, v_3$ and the edge between $v_2$ and $v_3$.
Similarly, the Bernstein polynomials associated with an edge domain point will be 0 on the other two edges. 
Lastly, the Bernstein polynomials associated with an interior domain point is only supported on the interior of the triangle. 
Due to the above properties of the Bernstein polynomials, it is natural to construct finite elements with Bernstein polynomials basis with degrees of freedom as the domain points.
The finite element triple in our case is $(T, \mathbb{P}^p, \Sigma)$ \cite{ciarlet2002finite} where $\Sigma$ is the dofs associated with evaluation of the polynomial at the domain points. 
Here we have another 1-to-1 correspondence between three different objects: domain points, Bernstein polynomials and degrees of freedom (see \cite{Ainsworth2011} for algorithmic details on construction of the finite elements matrix).

In addition to the 2D version, we will also utilize the one dimensional Bernstein polynomials on an interval $[a,b]$. 
These are defined simply as 
\begin{align*}
	B^p_n(x) = \binom{p}{n}\lambda_1^n \lambda_2^{p - n}
\end{align*}
for $0\le n \le p$ where $\lambda_1, \lambda_2$ are the barycentric coordinates on $[a,b]$. % TODO: double check that notation is consistent. 
The main property of the 1D Bernstein polynomials that we will need is the so-called degree raising formula: 
\begin{align}
	B^{p-1}_i(x) &= \frac{p-i}{p} B_i^p(x) + \frac{i+1}{p} B_{i+1}^p(x).
	\label{deg-raising}
\end{align}

In addition to the Bernstein polynomials, we will rely on the standard definitions of Jacobi polynomial. 
The notation we will use is $P_n^{(a,b)}$. 
A key identity relating the two types of polynomial together is  
\begin{align}
	P_n^{(2,2)}(x) &= \sum_{i = 0}^n \frac{\binom{n + 2 }{i} \binom{n+2}{n-i}}{(-1)^{n-i} \binom {n}{i}} B_i^n(x), \qquad -1 \le x \le 1.
	\label{bern-jac}
\end{align}

\section{Basis Overview and the Schur Complement}
In this section, we provide a brief overview of the block-Jacobi preconditioner from \cite{AMMPaper} which is the basis and the solvers.
The basis for the wirebasket depended on the minimal energy extension of functions on triangle $T$ which is defined by for $u \in \mathbb{P}^p(\partial T)$ to be
\begin{align*}
	\tilde u :=  \argmin_{\tilde u = u \text{ on } \partial T, \tilde u \in \mathbb{P}^p} \norm{\tilde u}_{L^2(T)}^2.
\end{align*}
They key properties are that minimal energy extensions are uniquely determined by $p$, and $u$ on the boundary of $T$. 
Furthermore, it is orthogonal in $L^2$ sense to all polynomials of degree $p$ or lower with support strictly inside the triangle; in the language of the Bernstein polynomial, we have that
\begin{align*}
 	(\tilde u, B_\alpha^p) = 0
 \end{align*} 
 for all $\alpha \in \mathcal{I}$.
Lastly, it makes sense to discuss the minimal energy extension as a subspace. 

Define
\begin{align*}
	f_p(x) &= \frac{(-1)^{p+1}}{(p+1)p}P_{p}'(x)(1 - x),
\end{align*}
then the vertex basis functions can be defined as $\tilde f_p(1 - 2\lambda_i)$ where $\lambda_i$ is the barycentric coordinate; we will abbreviate this as $\tilde f^p_i$. 
On the edge, we let $g^p_i \in \mathbb{P}^p(\partial T)$ be defined such that it is equal to $(1-x^2) P^{(2,2)}_i(x)$ on a particular edge, and zero on the other edges. 
The edge basis is simply $\tilde g^p_i$.
Finally, the interior functions can be any appropriate polynomials supported solely in the interior; we will use the Bernstein basis in this case. 
% \draw (0, 0) 
% 		-- (4, 0) 
% 		-- (2, 2*sqrt(3);) 
% 		-- (0,0);

\begin{figure}[ht]
	\begin{center}
	\begin{tikzpicture}
	\draw (0,0) -- (4,0) -- (2,{2*sqrt(3)}) -- cycle;
	\node [thick] at (0,0) {$\circ$};	
	\node [thick] at (4,0) {$\circ$};	
	\node [thick] at (2,{2*sqrt(3)}) {$\circ$};	
\end{tikzpicture}
\end{center}
\caption{TODO: draw a triangle with domain points for $p = 4$ with the basis added in.
}
\end{figure}

The preconditioner as described in \cite{AMMPaper} is of additive Schwarz form, and it breaks up the triangulation into individual vertices, individual edges of elements and individual interiors of elements. 
For each of these subspaces, we use an exact solver, meaning we will invert the mass matrix corresponding to the dofs of the subspaces. 
Finally, the preconditioner takes the sum of all the local solves; it is well known that additive Schwarz preconditioners take the form of a block-diagonal matrices \cite{toselli2005domain}.

\subsection{Schur Complement and the Minimal Energy Space}
	There are no easily computable closed-form expressions for either the vertex basis, nor the edge basis to the authors' knowledge. 
	To compute the mass matrix for the minimal energy space, we note that the Schur complement provides exactly the matrix we need. 
	Indeed, in \cite[\S 4.4]{toselli2005domain} Toselli and Widlund use the term ``discrete harmonic extensions'' which has an equivalent definition to the minimal energy extensions, but the authors believe that ``minimal energy extensions'' provide a more descriptive name in our context. 

	The Schur complement with respect to the interior degrees of freedom of the mass matrix constructed with the Bernstein basis gives us a basis for the space of the minimal energy extensions. 
	Here we reiterate the crucial fact that we simply have to match the basis function on the boundary, as the extensions are uniquely determined on the interior. 
	Thus for the change of basis operators we present below, the procedure is that we will work in 1D, then match the boundaries. 


\section{Preconditioner Algorithm}
	\label{sec:algorithm}
	In this section, we present the preconditioner algorithmically and efficient algorithms to compute it, and leave all proofs/derivations for later.


\subsection{Setup and Notation}
	Let $M$ be the global mass matrix constructed from the Bernstein basis such that the degrees of freedom are grouped by vertices $V$, edges $E$ and interiors $I$.
	We can express the block corresponding to the interaction between $i, j \in \{V, E, I\}$ as $M_{ij}$.
	Furthermore, we assume that the degrees of freedom for a single edge are grouped together, and the interior DOFs residing within each element are also grouped together.
	We can express the mass matrix equation in block matrix form as
	\begin{align*}
		M{x} = 
		\begin{bmatrix}
			M_{VV} & M_{VE} & M_{VI} \\
			M_{VE}^t & M_{EE} & M_{EI} \\
			M_{VI}^t & M_{EI}^t & M_{II}
		\end{bmatrix}{x} = f.
	\end{align*}
	Denote $A = [M_{VV}, M_{VE}; M_{VE}^t, M_{EE}]$, $B = [M_{VI}; M_{EI}]$ and $C = [M_{II}]$.
	We can reformulate our problem using the Schur complement $S$ of block $C$ as solving the system
	\begin{equation}
		\begin{aligned}
		Sx_1 := (A -  BC^{-1}B^t)x_1 &= f_1 - BC^{-1}f_2 \\
		x_2 &= C^{-1}(f_2 - B^tx_1)
	\end{aligned}
	\label{eqn:schur-complement}
	\end{equation}
	where $x_1, f_1$ corresponds to the boundary DOFs while $x_2, f_2$ corresponds to the interior DOFs.
	We note that $S$ will be the mass matrix corresponding to the minimal energy extensions of the Bernstein edge and vertex functions. 
	This is the same as performing static condensation on the interior degrees of freedom \cite{wilson1974static}. 

\subsection{Computing $C^{-1}$ (Manuel's)}
	\label{sec:cinv}
	The block matrix $C$ is of a block diagonal format, but since there are $O(p^2)$ internal DOFs, inverting each element's block will be order $O(p^6)$ using naive Gaussian elimination.
	We present an algorithm to invert the interior Bernstein mass matrices on a single element in $O(p^3)$.

	\begin{algorithm}[ht]
		\caption{Manuel's algorithm...}
		\label{int_inv}
		\begin{algorithmic}[1]
		\State Todo
		\end{algorithmic}
	\end{algorithm}

\subsection{Matrix Formulation}
	\label{sec:schur-complement}
	Due to the algorithms of Section~\ref{sec:cinv}, using conjugate gradient on $S$ is no longer expensive due to matrix inversion of the $C$ block.
	Further, note that with an operator to compute $C^{-1}$, we are able to compute the action $Sx$ without explicit construction of $S$.
	In fact, \cite{Ainsworth2011} demonstrates that the mass matrix action using the Bernstein basis can also be matrix free. 

	To precondition $S$, we first express it as a block matrix 
	\begin{align*}
		S = \begin{bmatrix}
			S_{VV} & S_{VE} \\
			S_{VE}^t & S_{EE}
		\end{bmatrix}.
	\end{align*}
	Our preconditioner $P$ for the Schur complement is the following block-Jacobi preconditioner with a change of basis operator: 
	\begin{align*}
		P_S =   \begin{bmatrix}
			I_{vv} & -T \\
			0 & I_{ee}
		\end{bmatrix}
		\begin{bmatrix}
			\frac{1}{p^4}D_{vv}&  0\\
			0 & D_{ee}
		\end{bmatrix}
		\begin{bmatrix}
			I_{vv} & 0 \\
			-T^t & I_{ee}
		\end{bmatrix}.
	\end{align*}
	where we have the following definitions: 
	\begin{enumerate}
		\item $I_{ee}$ and $I_{vv}$ is the identity matrix with shape of the edge-edge blocks or vertex-vertex blocks respectively.
		\item $D_{vv}$ is a diagonal matrix corresponding to the vertex functions.
		\item $T$ is a change of basis operator to a different basis. We can explicitly calculate this block. 
		\item $D_{ee}$ is a block diagonal matrix of $S_{ee}$ such that each block corresponds to the edge-edge interactions on a single edge. In other words, $D_{ee}$ will be comprised of $\abs{E}$ blocks of size $p-1$ by $p-1$ where $\abs{E}$ is the total number of distinct edges.
		We denote the $i$th block in $D_{ee}$ as $D_{e_ie_i}$.
	\end{enumerate}

	To apply our preconditioner $P_S$, we will need to compute it's inverse. 
	We have that the inverse is 
	\begin{align*}
		P_S^{-1} = \begin{bmatrix}
			I_{vv} & 0 \\
			T^t & I_{ee}
		\end{bmatrix}  
		\begin{bmatrix}
			p^4D_{vv}^{-1}&  0\\
			0 & D_{ee}^{-1}
		\end{bmatrix}
		\begin{bmatrix}
			I_{vv} & T \\
			0 & I_{ee}
		\end{bmatrix}.
	\end{align*}
	Note that since $D_{ee}$ is a block diagonal matrix, we only need to invert the blocks.
	We remark that the preconditioner can be applied mostly entirely matrix free; the only non-operator based process is the multiplication of $T, T^t$.
	The details are presented below. 

\subsubsection{Computing $T$ and $D_{vv}$}
	\label{sec:compute-t}
	Recall that our vertex functions are $\tilde f_p(1 - 2\lambda_i)$ where $\lambda_i$ is the barycentric coordinates of our triangle.
	The resulting matrix will be spectrally-equivalent to a diagonal matrix with $1/p^4$ as its entries, up to a scaling of the area (see Lemma~\ref{T-lem:vertex-norm} of \cite{AMMPaper}).
	We can compute $D_{vv}$ by looping through the elements, computing the determinant of the linear transformation which maps the reference triangle to the current element, then summing. 
	See Algorithm~\ref{algorithm:vertex-diag} for the pseudo-code; note that we represent the diagonal matrix as an array in the code. 

	The transformation matrix $T: \mathbb{R}^{\abs{V} \times \abs{E}}$ is computed using Algorithm~\ref{algorithm:precompute-vertex}.
	$T$ can be readily computed due to the minimal energy extension being uniquely defined by the boundary values; derivation is located in Section~\ref{sec:vertex-derivation}. 
	We remark that both the matrices above should be precomputed, and stored. 

	\begin{remark}
		The vertices do not have to be solved using diagonal scaling. 
		One may compute the vertex-vertex block of the Schur complement, and use that instead of $D_{vv}$.
		This offer slightly, but not significantly, better condition numbers than diagonal scaling. 
	\end{remark}


	\begin{algorithm}[H]
		\caption{Computing $D_{vv}$}
		\label{algorithm:vertex-diag}
		\begin{algorithmic}
			\For{element $k$ in triangulation $\Omega_h$}
			\State{Let $T$ be the linear transform of the reference triangle to $k$}
			\State{$D_{vv}[\text{dofs of vertices of }k] += \abs{T}$} \Comment{Determinant of matrix}
			\EndFor
		\end{algorithmic}
	\end{algorithm}
	\begin{algorithm}[H]
		\caption{Computing the values of $T$}
		\label{algorithm:precompute-vertex}
		\begin{algorithmic}[1]
			\State{$T = \text{zeros}(\text{number of vertices}, \text{number of edges})$}
			\State{$c = \text{zeros}(p - 1)$}
			\For{$i = 1$ to $p - 1$}
			\State $c[i] = \frac{(-1.0)^i}{p} \binom{p}{p-1-i}$
			\EndFor
			\For{triangle $k$ in partition $\Omega_h$}
			\For{vertex $v_i$ in triangle $k$}
			\State Let $v_{j}, v_{k}$ be the two remaining vertices of $k$
			\State $d_1 := \text{DOFs on edge from vertex $v_i$ (excluded) to } v_j (\text{included})$
			\State $d_1 := \text{DOFs on edge from vertex $v_i$ (excluded) to } v_k (\text{included})$
			\State $T[\text{dof of }v_i, d_1] = c$ \Comment{Set an array equal to another array}
			\State $T[\text{dof of }v_i, d_2] = c$
			\EndFor
			\EndFor
		\end{algorithmic}
	\end{algorithm}


\subsubsection{Computing $D_{ee}^{-1}$}
	\label{sec:edge_block}
	Lemma~\ref{T-lemma:extension} of \cite{AMMPaper} shows that the mass matrix blocks corresponding to a single edge with the minimal energy extension of $(1-x^2)P^{(2,2)}_i(x)$ as the basis is a diagonal blocks with known entries. 
	As a result, we introduce an operator to convert the Bernstein problem to the Jacobi basis (see Algorithm~\ref{gamma-inverse}) and a second operator to convert the solution back into a Bernstein polynomial (see Algorithm~\ref{gamma-transpose}).
	Paired with a diagonal scaling, the above two algorithms will be able to efficiently compute the inverse on an edge. 
	For the full pseudo-code, see Algorithm~\ref{edge_solve}.

	Note that without this result, as each block is of size ${p-1 \times p-1}$, it will take $O(p^3)$ to invert; our basis conversion algorithms is $O(p^2)$, hence we can invert $D_{ee}$ in $O(p^2)$.
	On top of that, we would have to calculate explicitly the values of the Schur complement; using an operator allows us to never construct the Schur complement matrix. 

	\begin{algorithm}[H]
		\caption{$\Gamma^{-1}$: Converts $(f, B_i^n)$ into $(f, (1-x^2)P_i^{(2,2)})$}
		\label{gamma-inverse}
		\begin{algorithmic}[1]
		 	\Require $b$, a vector of length $n-1$ 
		 	\Function{Gammainverse}{b}
			\For{$i = n-2$ to $0$}
			\If{$i \not = n - 2$}
			\For{$j = 0, \ldots, i + 1$}
			\State $b[j]= \frac{i + 2 - j}{i + 3}b[j]  +  \frac{j + 2}{i + 3}b[j+1]$ \Comment{Degree lowering}
			\EndFor
			\EndIf
			\For{$j = 0, \ldots, i + 1$}
			\State $o[i] = o[i] + \frac{\binom{i + 2}{j} \binom{i+2}{i-j}}{(-1)^{i-j} \binom{i}{j}} \frac{j+1}{i+1} \frac{i - j + 1}{i+2}b[j]$ 
			\EndFor
			\EndFor
			\State \textbf{return} $o$
			\EndFunction
		\end{algorithmic}
	\end{algorithm}

	\begin{algorithm}[H]
		\caption{$\Gamma^{-t}$: Converts the coefficients of a Jacobi polynomial to a BB-form coefficients}
		\label{gamma-transpose}
		\begin{algorithmic}[1]
			\Require $w$, a vector of length $n-1$
			\Function{Gammainvtran}{w}
			\For{$i = 0, \ldots, n - 1$}
			\If{$i \not = 0$}
			\For{$j = i+2, \ldots, 0$}
			\State $o[i] = \frac{i - j}{i} o[i] + \frac{j}{i} o[i-1]$ \Comment{Degree raise output; $o[-1] = 0$}
			\EndFor
			\EndIf
			\For{$j = 0, \ldots, i + 1$}
			\State $o[i] = o[i]+ \frac{\binom{i + 2}{j} \binom{i+2}{i-j}}{(-1)^{i-j} \binom{i}{j}} \frac{j+1}{i+1} \frac{i - j + 1}{i+2}w[j] $
			\EndFor
			\EndFor
			\State \textbf{return} $o$
			\EndFunction
		\end{algorithmic}
	\end{algorithm}

	\begin{algorithm}[H]
		\caption{Solves the edge DOFs in the Schur complement $D_{E_iE_i}x_i = f_i$}
		\label{edge_solve}
		\begin{algorithmic}[1]
			\Require $f$, a vector of length $n-1$
			\Function{EdgeSolve}{$f$}
			\State $f = $ \texttt{Gammainverse}($f$)
			\For{$i = 0, \ldots, n-1$} \Comment{Diagonal scaling}
			\State $f[i] =  \abs{L}\frac{32(i+1)(i+2)}{(2i + 5)(i + 3)(i + 4)} \frac{2}{(n + i + 4)(n - i - 1)} f[i]$
			\State \Comment{$L$ is the affine transform from the element to the standard triangle}
			\EndFor
			\State $f = $ \texttt{Gammainvtran}($f$)
			\State \textbf{return} $f$
			\EndFunction
		\end{algorithmic}
	\end{algorithm}

\subsubsection{Schur Complement Preconditioner}
	Putting together Section~\ref{sec:edge_block} and \ref{sec:compute-t}, we can write the pseudo-code for our Schur complement preconditioner. 
	As stated in Section~\ref{sec:schur-complement}, the matrix action we are trying to compute is 
		\begin{align*}
		P_S^{-1} = \begin{bmatrix}
			I_{vv} & 0 \\
			T^t & I_{ee}
		\end{bmatrix}  
		\begin{bmatrix}
			p^4D^{-1}_{vv}&  0\\
			0 & D_{ee}^{-1}
		\end{bmatrix}
		\begin{bmatrix}
			I_{vv} & T \\
			0 & I_{ee}
		\end{bmatrix}.
	\end{align*}
	The pseudo-code is described in Algorithm~\ref{algo:preconditioner-schur-complement}.

	\begin{algorithm}[H]
		\caption{Schur Complement Preconditioner's action on a vector}
		\label{algo:preconditioner-schur-complement}
		\begin{algorithmic}[1]
			\State Precompute $T$
			\Function{SchurPreconditioner}{$g$}
			\State $w = \texttt{zeros}(ns)$ \Comment{Output holder}
			\For{$i = 0, \ldots, \abs{E}$} 
			\State $w_{E_i} = \texttt{EdgeSolve}(g_{E_i})$ \Comment{Solve on each edge with loop}
			\EndFor
			\State $w_V = D^{-1}_{vv}p^4(g_V + Tg_E)$ \Comment{Add vertex block contributions}
			\State $w_E = w_E + T^tw_V$ 
			\EndFunction
		\end{algorithmic}


\subsection{Mass Matrix Preconditioner: Exact}
	\label{sec:preconditioner-algorithm}
	We can construct a preconditioner for the mass matrix using the Schur complement preconditioner. 
	Let $M$ be the global mass matrix as described before, and denote $ns$ the number of total vertex and edge degrees of freedom.
	Furthermore, let $A,B,C$ be the matrix blocks as described in the setup, and $S$ the Schur complement.

	The algorithm simply solves the reduced problem from Equation~\ref{eqn:schur-complement}, as we can iterate on the Schur complement easily and have a uniform preconditioner.  
	Note that if we were have infinite precision and iterate PCG until convergence, then we have the exact solution to the mass inverse problem.
	In practice, a tolerance of $10^{-8}$ works well as the preconditioner for the Schur complement performs extremely well. 
	See Algorithm~\ref{preconditioner-schur-algo} for the pseudo-code. 

	Note that mathematically, this is NOT the additive Schwarz preconditioner from \cite{AMMPaper}, but rather a mild perturbation. 
	The theoretical discussion is in Section TODO. 

	\begin{algorithm}[H]
		\caption{Preconditioner 1's action on a vector}
		\label{preconditioner-schur-algo}
		\begin{algorithmic}[1]
			\Function{ApproxMassInverse}{$f$}
			\State $f_1 = f[0:ns]$
			\State $f_2 = f[ns:]$
			\State $\tilde f_1 = f_1 - B^T\texttt{IntInv}(f_2)$
			\State $x_1 = \texttt{PCG}(A - B\texttt{IntInv}(B^T), \tilde f_1, \texttt{SchurPreconditioner})$ \Comment{PCG with preconditioner on linear operator} 
			\State $x_2 = \texttt{IntInv}(f_2 - B^Tx_1)$
			\State \textbf{return} $[x_1; x_2]$
			\EndFunction
		\end{algorithmic}
	\end{algorithm}

\subsection{Mass Matrix Preconditioner: Perturbed}
	The preconditioner presented above performs well if we are working with the pure mass matrix.
	In the case where we have mass matrix plus a small perturbation, we note that the interior inversion from Algorithm~\ref{int_inv} is \emph{not} exact; hence, it is unwise to iterate on the wrong Schur complement matrix. 
	In this case, we only iterate once on the Schur complement using our preconditioner; while this might seem arbitrary, mathematically this corresponds to a block-diagonal preconditioner as shown in \cite{AMMPaper} as we are doing the correct change of basis operations. 
		\begin{algorithm}[H]
			\caption{Precondition 2's action on a vector}
			\label{preconditioner-mass-algo}
			\begin{algorithmic}[1]
				\Function{ApproxMassInverse}{$f$}
				\State $f_1 = f[0:ns]$
				\State $f_2 = f[ns:]$
				\State $x_I = \texttt{InvInv}(f_2)$ \Comment{Solve interior}
				\State $\tilde f_1 = f_1 - B^tx_I$
				\State $x_B = \texttt{SchurPreconditioner}(\tilde f_1)$ \Comment{Solve wirebasket}
				\State $x_I = x_I - \texttt{IntInv}(B^tx_B)$ \Comment{Correct Interiors}
				\State \textbf{return} $[x_B; x_I]$
				\EndFunction
			\end{algorithmic}
		\end{algorithm}

\subsection{Numerical Results}
	We demonstrate the 

	TODO: double check the numbers to make sure you're using updated code.

	\label{sec:numerical-results}
	\subsubsection{Condition Numbers}
	We first demonstrate that the condition number is drastically reduced on a single triangle.
	It is known that the condition number of the Bernstein mass matrix under $p$-refinement in 2 dimension is $\binom{2p + 2}{p}$; asymptotically, this values grows like $2^{2p}p^{-1/2}$ \cite{Lyche2000}.
	The first plot shows the condition number of the Schur complement, and after it was preconditioned using Algorithm~\ref{algo:preconditioner-schur-complement}.
	\begin{center}
		\begin{tikzpicture}
	\begin{semilogyaxis}[
	title=Plot of Schur Complement Condition Numbers,
	xlabel={$p$},
	ylabel={Condition number},
	legend pos=north west
	]
	\addplot [green, mark=x] table {schur_complement_pre.data};
	\addplot [blue, mark=x] table {schur_complement_reg.data};
	\addplot [red, mark=x] table {cond.data};
	\legend{Preconditioned, Schur Complement, Mass Matrix}
	\end{semilogyaxis}
	\end{tikzpicture}
	\end{center}

	\begin{center}
		\begin{tikzpicture}
		\begin{semilogyaxis}[
		title=Plot of Condition Numbers of $M + 0.01 S$,
		xlabel={$p$},
		ylabel={Condition number},
		legend pos=north west
		]
		\addplot [green, mark=x] table {mass_perturbed_pre.data};
		\addplot [blue, mark=x] table {mass_perturbed_reg.data};
		\legend{Preconditioned, Regular}
		\end{semilogyaxis}
		\end{tikzpicture}
	\end{center}


	\subsection{Timing Data}

	\begin{center}
		\begin{tikzpicture}
			\begin{semilogyaxis}[
			title=Timing of Interior Code,
			xlabel={$p$},
			ylabel={Seconds},
			legend pos=north west
			]
			\addplot [blue, mark=x] table [x={p}, y={backslash}] {interior_timing.data};
			\addplot [red, mark=x] table [x={p}, y={manuel}]{interior_timing.data};
			\legend{Backslash, Optimal}
			\end{semilogyaxis}
		\end{tikzpicture}
	\end{center}

		\begin{center}
		\begin{tikzpicture}
			\begin{semilogyaxis}[
			title=Timing of Edge Code,
			xlabel={$p$},
			ylabel={Seconds},
			legend pos=north west
			]
			\addplot [blue, mark=x] table [x={p}, y={backslash}] {edge_timing.data};
			\addplot [red, mark=x] table [x={p}, y={opt}]{edge_timing.data};
			\legend{Backslash, Optimal}
			\end{semilogyaxis}
		\end{tikzpicture}
	\end{center}

	\begin{center}
		\begin{tikzpicture}
			\begin{semilogyaxis}[
			title=Timing of All Code,
			xlabel={$p$},
			ylabel={Seconds},
			legend pos=north west
			]
			\addplot [blue, mark=x] table [x={p}, y={backslash}] {edge_timing.data};
			\addplot [red, mark=x] table [x={p}, y={opt}]{edge_timing.data};
			\addplot [green, mark=x] table [x={p}, y={backslash}] {interior_timing.data};
			\addplot [black, mark=x] table [x={p}, y={manuel}]{interior_timing.data};
			\legend{edge, edge, int, int}
			\end{semilogyaxis}
		\end{tikzpicture}
	\end{center}

\section{Derivation of Exact Local Solvers}
	\label{sec:implementation-details}
	In this section, we detail the derivation of the algorithms/coefficients in the previous section. 
	We note that we have exact local solvers for each of our subspaces \cite{AMMPaper}.... how to explain? 


	\subsection{Vertex Functions}
	\label{sec:vertex-derivation}
	Recall that the Schur complement of the Bernstein mass matrix gives us a basis of the minimal energy extensions in terms of Bernstein polynomials, hence we only need to convert our Bernstein polynomials on the boundary to the vertex functions on the boundary. 
	WLOG assume that $i = 1$ on the reference triangle corresponds to the barycentric coordinate of $ \frac{-x-y}{2}$ which is 1 at the bottom-left vertex. 
	Let us look at the bottom edge of the triangle, 
	\begin{align*}
	 \tilde f_p(x, -1) &= f_p\left( \frac{-x + 1}{2} \right) \\
	&= \frac{(-1)^{p+1}}{(p+1)p}P_{p}'(x)(1 - x) \\
	&= \frac{(-1)^{p+1}}{(p+1)p} \frac{(p + 1)}{2}(1 - x) P_{p-1}^{(1,1)}(x) \\
		&= 	\frac{1}{p}\left(\sum_{j=0}^{p-1} \binom{p}{p-1-j}(-1)^{j} B^p_j(x) \right).
	\end{align*} 
		Note that converting to and from the vertex functions require us to know the values at the edges.
	We also note that on edge $(-1, y)$ for $y \in [-1, 1]$, that the above derivation is the same due to symmetry of the barycentric coordinate. 
	These values are the ones we see in Algorithm~\ref{algorithm:precompute-vertex}.

	Define $\vec{F} = [\tilde f_1^p, \tilde f_2^p, \tilde f_3^p]$, and similarly $\vec{B}_v$ and $\vec{B}_e$ to be the vectors of Bernstein vertex and edge functions respectively of minimal extension. 
	Then define $T: \abs{E} \to 3$ with the coefficients from above such that 
	\begin{align*}
		\vec{F} = \vec{B}_v + T\vec{B}_e.
	\end{align*}
	We can rewrite this in matrix vector form as 
	\begin{align*}
		\begin{bmatrix}
			F \\
			B_e
		\end{bmatrix}
		= \begin{bmatrix}
			I & T \\
			0 & I
		\end{bmatrix}
		\begin{bmatrix}
			B_v \\
			B_e
		\end{bmatrix}
	\end{align*}

	Then we see that 
	\begin{align*}
		\begin{bmatrix}
		S_{vv} & S_{ve} \\
		S_{ev} & S_{ee}
	\end{bmatrix}
	&= \begin{bmatrix}
		I & -T \\
		0 & I
	\end{bmatrix}
	\begin{bmatrix}
		\hat S_{vv} & \hat S_{ve} \\
		\hat S_{ev} & S_{ee}
	\end{bmatrix}
	\begin{bmatrix}
		I & 0 \\
		-T^t & I
	\end{bmatrix}
	\end{align*}
	where $S_{ee}, S_{ev}, S_{vv}$ are the standard Schur complement matrix blocks with Bernstein polynomials, while the hats denote the blocks under the basis of $\tilde f_i^p$. 
	We point out that this is the form of our preconditioner in Algorithm~\ref{algo:preconditioner-schur-complement}
	Computation of $T$ is not expensive and is dependent on the number of elements, and we require only matrix multiplies in the basis conversions.

	For the diagonal scaling, we note that we have no exact value for the norm of $\tilde f_i^p$ as we do for the edge solves. 
	Nevertheless, we refer the reader to Lemma~\ref{T-lem:vertex-norm} of \cite{AMMPaper} which upper and lower bounds the norm of $\tilde f_i^p$ to be on the order of $p^{-4}$; the scaling from the size of the elements can be seen through the $L^2$ norm in the lemma.
	This is spectrally good enough, and numerical experiments show that inverting the exact diagonals do not offer large advantages. 

	\subsection{Edge Solver}
	The edge solvers use the same strategy as the vertex solvers of matching the basis on the boundary of the triangle. 
	The crux of the edge solver relies on this extension lemma from \cite{AMMPaper}:
	\begin{lemma}[Extension Theorem]
		Let $u \in \mathbb{P}^p(\mathcal{E}_i)$ by a polynomial on edge $\mathcal{E}_i$ vanish at the endpoints of edge $\mathcal{E}_i$. 
		Then the minimal energy extension $\tilde u \in \mathbb{P}^p(T)$ such that $\tilde u = 0$ on $\partial T \setminus \mathcal{E}_i$ and $u = \tilde u$ on $\mathcal{E}_i$ have the bounds 
		\begin{align}
			\frac{2}{(p-1)(p+4)}\norm{u}_{L^2(\mathcal{E}_i)}^2 \le \norm{\tilde u}_{L^2(T)}^2 \le \frac{1}{p+1} \norm{u}^2_{L^2(\mathcal{E}_i)}.
		\end{align}
		Furthermore, expressing $u$ as a sum of Jacobi polynomials as follows
		\begin{align*}
			u(x) = (1-x^2) \sum_{i=0}^{p-2} w_i P_i^{(2,2)}(x), 
		\end{align*}
		then the minimal energy extension $\tilde u$ satisfies the equality
		\begin{align*}
			\norm{\tilde u}^2_{L^2(T)} = \sum_{i=0}^{p-2} \frac{2\mu_i w_i^2}{(p + i + 4)(p - i - 1)}
		\end{align*}
		where $\mu_i = \int_{-1}^1 (1 - x^2)^2 P_i^{(2,2)}(x)^2 \, dx$. 
		\label{lemma:extension}
	\end{lemma}
	The crucial result from the lemma, is that the minimal extension from an edge is spectrally equivalent to a diagonal matrix if we express the function in terms of the basis $(1-x^2)P_i^{(2,2)}$.
	Thus to invert, we can use a change of basis to $(1-x^2)P_i^{(2,2)}$ polynomials first, invert under a diagonal scaling, then change it back to Bernstein polynomials. 
	We will describe how to do the conversion below. 

	Let $\vec{B}$ be the vector $[B_1^n(x); \ldots; B_{n-1}^n(x)]$, $\vec{P}$ be the vector $[(1-x^2)P_0^{(2,2)}; \ldots; (1-x^2)P_{n-2}^{(2,2)}]$ of Jacobi polynomials and the linear transformation $\Gamma$ be such that $\vec{B} = \Gamma\vec{P}$.
	We wish to find a vector $f$ for a fixed polynomial $g$ 
	\begin{align*}
		(\vec{B}, \vec{B}^T)_S f = \begin{bmatrix}
			(g, B_1^n)_S \\
			\vdots \\
			(g, B_{n-1}^n)_S
		\end{bmatrix} \iff \Gamma(\vec{P}, \vec{P}^T)_S \Gamma^Tf =  \begin{bmatrix}
			(g, B_1^n)_S \\
			\vdots \\
			(g, B_{n-1}^n)_S
		\end{bmatrix} ,
	\end{align*}
	where $(\cdot, \cdot)_S$ is the inner product under the Schur complement (i.e. the inner product of the minimal energy extensions). 
	By Lemma~\ref{lemma:extension}, we see that $(\vec{P}, \vec{P}^T)_S$ is a diagonal matrix with known coefficients.
	Hence, all we need is to find efficient algorithms to compute $\Gamma^{-1}$, which maps a vector of moments against Bernstein polynomials to a vector of moments against Jacobi polynomials
	\begin{align*}
		\Gamma^{-1}: \begin{bmatrix}
			(g, B_1^n)_S \\
			\vdots \\
			(g, B_{n-1}^n)_S
		\end{bmatrix} \to 
		\begin{bmatrix}
			(g, (1-x^2)P_0^{(2,2)})_S \\
			\vdots \\
			(g,(1-x^2)P_{n-2}^{(2,2)})_S
		\end{bmatrix}, 
	\end{align*} and $\Gamma^{-T}$ which maps the coefficient of a single Jacobi polynomial to the coefficients of the equivalent BB-form polynomial
	\begin{align*}
		\Gamma^{-T}: (1-x^2)\sum_{j=0}^{n-2} w_j P_j^{(2,2)}(x) \to \sum_{j=1}^{n-1} \tilde c_j B_j^n(x).
	\end{align*}

	For $\Gamma^{-1}$, note that from Equation~\ref{bern-jac}, and degree raising formulas, we have
	\begin{align}
		(1-x^2)P_k^{(2,2)} &= 4\sum_{i = 0}^{k} \frac{\binom{k + 2}{i} \binom{k+2 }{k-i}}{(-1)^{k-i} \binom{k}{i}} \frac{i+1}{k+1} \frac{k - i + 1}{k+2} B_{i+1}^{k+2}(x).
		\label{jacobi-bernstein-compact}
	\end{align}
	Hence given $[(f, B_1^n), \ldots, (f, B_{n-1}^n)]$, we can immediately solve for $(f, (1-x^2)P_{n-2}^{(2,2)})_S$.
	We next perform a degree dropping operation using Equation~\ref{deg-raising} to find the values of $[(f, B^{n-1}_1), \ldots, (f, B_{n-2}^{n-1})]$.
	This allows us to compute the inner-product with respect to $(1-x^2)P^{(2,2)}_{n-3}$, and we iterate the above process.
	See Algorithm~\ref{gamma-inverse} for the psuedo-code. 

	A similar approach works for $\Gamma^{-T}$ work using Equation~\ref{jacobi-bernstein-compact}, but note that using it naively will result in Bernstein basis of lower orders rather than Bernstein polynomials of the same order. 
	To remedy this problem, we start with applying Equation~\ref{jacobi-bernstein-compact} on the first index $w_0$, then perform a degree raising operation on the result; this will result in order 3 Bernstein polynomials after the degree raise.
	We can then apply Equation~\ref{jacobi-bernstein-compact} on $w_1$ and add it to our previous results (as it'll output order 3 Bernstein polynomials also), then degree raise the sum to order 4 Bernstein polynomials. 
	This process is iterable up to the desired order; see Algorithm~\ref{gamma-transpose} for the pseudo-code.
	It is not hard to see that both algorithms are order $O(p^2)$. 

	The binomial coefficients in the algorithms can get expensive to calculate.
	One can pre-compute the coefficients, or we can simplify the coefficients to a single binomial coefficient
	\begin{align*}
		\frac{\binom{i + 2}{j} \binom{i+2}{i-j}}{\binom{i}{j}} \frac{j+1}{i+1} \frac{i - j + 1}{i+2} &= \frac{(i+2)!}{(i + 2 - j)!(j + 2)!} (j+1)(i - j + 1) \\
		&= \frac{j + 1}{(i - j + 2)} \binom{i + 2}{j + 2} .
	\end{align*}

	\subsection{Interior Solver}


\section{Appendix}

\subsection{Evaluation and Visualization}
Using Bernstein polynomials as the basis allows for easy function evaluation through the de Castlejau algorithm \cite{farin2002}. 
It is stable, and can compute the value at a single point in $O(p^3)$ by using linear interpolation. 
On the other hand, if one wishes to plot the solution from the finite element method, one requires significantly more points evaluation and using de Castlejau for all of them will be quite slow.

There are more efficient ways to generate the plots. 
In OpenGL, there are functions called ``evaluators'' which can plot Bernstein polynomial patches (see \texttt{glEvalMesh2} in any OpenGL reference book).
There are two drawbacks. 
The first is that OpenGL evaluators are on a triangular patch, which requires a conversion to use for rectangular patches (see \cite{yan2014conversion} for a simple algorithm).
The second drawback is there is vendor dependent constant, namely \texttt{GL\_MAX\_EVAL\_ORDER}, which sets the maximum order that OpenGL can plot, which is typically limited to $p < 8$.

A less exact but more efficient plotting solution is to simply plot the control points of the Bernstein solution. 
It is known that as one increase $p$ to represent the same polynomial, the control points will converge to the curve. 
This leads to the degree raising algorithm, where one simply converts the function to higher and higher order using degree raising equations, then plotting the control points. 
While feasible, the control points converges only linearly with $p$ to the curve. 

The better method is the subdivision algorithm, where one divides the triangle into 4 triangles representing the same polynomial. 
On a single element, we more than triple the number of control points from $\frac{(p+1)(p+2)}{2}$ to $2p^2 + 3p + 7$ control points.  
The most efficient way to implement the subdivision algorithm requires only 4 de Castlejau evaluations at specific points \cite{curves}.
Subdivision algorithm has the benefit of converging quadratically \cite{Cohen1985,Dahmen1986}, and allows of easy exporting to sophisticated visualization software.
See Figures~\ref{fig:raising} and \ref{fig:subdivision} for a visual comparison.

\begin{figure}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{sub_2.png}
  \caption{2 Subdivisions}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{sub_3.png}
  \caption{3 Subdivisions}
\end{subfigure}
\caption{Visual Convergence of Subdivision Algorithms in 1D}
\label{fig:subdivision}
\end{figure}

\begin{figure}[ht]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{raise_3.png}
  \caption{3 Degree Raises}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{raise_10.png}
  \caption{10 Degree Raises}
\end{subfigure}
\caption{Visual Convergence of Degree Raising Algorithms in 1D}
\label{fig:raising}
\end{figure}

\bibliography{source.bib} 
\bibliographystyle{plain}

\end{document}
