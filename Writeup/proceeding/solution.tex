f\documentclass{article}

\usepackage{listings}
\usepackage{commath} \let\abs\undefined
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}
\usepackage{diagbox}
\usepackage{pgfplots,tikz}
\usepgfplotslibrary{fillbetween}
\usepackage{enumerate}
\usepackage{mathtools}
\usepackage[nocompress]{cite}
\newtheorem{theorem}{Theorem}[section]
% \newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{remark}{Remark}


\newcommand{\matr}[1]{\mathbf{#1}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\sech}{sech}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\M}{\matr{M}}
\newcommand{\s}{\matr{S}}
\newcommand{\A}{\matr{A}}
\newcommand{\stiff}{\matr{L}}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\pgfplotsset{compat=1.12}
% \pgfplotsset{compat=1.13,
% 	table/search path = {../Eigenvalues_Timing},
% }

\begin{document}
Let $K = \{(x, y): 0 \le x, y, x + y \le 1 \}$ the reference triangle and $E = \{(x, 0): 0 \le x \le 1$ the reference edge. 
We first document a stronger Poincare result. 

\begin{lemma}
	Let $u \in \mathbb{P}_p([-1, 1])$ such that 
	\begin{align*}
		u(x) = \sum_{i=k}^p a_i \int_{-1}^x P_i(s) \, ds := \sum_{i=k}^p a_i \psi_i(x)
	\end{align*}
	where $P_i$ are the Legendre polynomials, then 
	\begin{align*}
		\norm{u}_{L^2}^2 \le \frac{1}{k(k+1)} \norm{u'}_{L^2}^2. 
	\end{align*}
\end{lemma}
\begin{proof}
	Noting that (From Schwab's)
	\begin{align*}
		\int_{-1}^1 \frac{1}{1 - \xi^2} \psi_i \psi_j \, d\xi = \frac{2 \delta_{ij}}{i(i + 1)(2i + 1)}, 
	\end{align*}
	we have 
	\begin{align*}
		\norm{u}^2 = \int_{-1}^1 u^2(s) \, ds &\le \int_{-1}^1 \frac{1}{1 - s^2} u^2(s) \, ds \\
		&= \int_{-1}^1 \frac{1}{1 - s^2} \left( \sum_{i=k}^p a_i \psi_i(x) \right)^2 \, ds \\
		&= \sum_{i=k}^p a_i^2 \frac{2}{i(i + 1)(2i + 1)} \\
		&\le \frac{1}{k(k +1)}  \sum_{i=k}^p a_i^2 \frac{2}{2i + 1} = \frac{1}{k(k+1)} \norm{u'}^2. 
	\end{align*}
\end{proof}
We next want to prove a similar statement about the extension. 
Let 
\begin{align*}
	F(x, y) := \frac{1}{y} \int_{x}^{x+y} u(s) \, ds
\end{align*}
for some $u\in \mathbb{P}(E)$ which satisfies the statement above.
By Poincare, it's trivial that $\norm{F}^2 \le \norm{\nabla F}^2 = \norm{F_x}^2 + \norm{F_y}^2$. 
We simply need to show something like $\norm{F}^2 \le \frac{1}{k}\norm{F_x}^2$ as then 
\begin{align*}
	\norm{F}^2 \le \norm{F}^2 + \frac{1}{k}\norm{F}^2 \le \frac{1}{k}\norm{F_x}^2 + \frac{1}{k}\norm{F_y}^2 = \frac{1}{k}\norm{\nabla F}^2.
\end{align*}
Note that 
\begin{align*}
	F_x &= \frac{u(x +y) - u(x)}{y} \\
	F_y &= -\frac{1}{y^2} \int_{x}^{x+y} u(s) \, ds + \frac{1}{y}u(x + y) \\
\end{align*}

Calculating, we have 
\begin{align*}
	\norm{F}^2 &= \int_{0}^1 \int_{0}^{1-x} \frac{1}{y^2}\left(\int_{x}^{x+y} u(s) \, ds \right)^2 \, dy \, dx \\
	\norm{F_x}^2 &= \int_{0}^1 \int_{0}^{1-x}\left(\frac{u(x +y) - u(x)}{y}\right)^2  \, dy \, dx \\
		\norm{F_x}^2 &= \int_{0}^1 \int_{0}^{1-x}\left( -\frac{1}{y^2} \int_{x}^{x+y} u(s) \, ds + \frac{1}{y}u(x + y)\right)^2  \, dy \, dx
\end{align*}

By a Hardy's inequality, we have
\begin{align*}
	\norm{F}^2 &\le 4\int_0^1 \int_x^1 u^2(s) \, ds \, dx \\
	& = 4 \int_0^1 \norm{u}_{L^2([x, 1])}^2 \, dx \\
	&\le \frac{4}{k(k + 1)} \int_0^1 \int_x^1 (u'(y))^2 \, dy \, dx \\
	&= \frac{4}{k(k + 1)} \int_0^1 \int_0^{1-x} (u'(y))^2 \, dy \, dx \\
\end{align*}
We also have $\norm{F}^2_T \le\norm{u}^2_E \le\frac{1}{k^2} \norm{u'}^2_E$, hence we really just need to show that $\norm{u'}_{L^2([0, 1])} \le \norm{F_x}_{L^2(T)}$, but it suffices to show this on a strip. 

By triangle inequality, and $y > 0$
\begin{align*}
	\norm{u'}^2 &\le C\left(\norm{u' - \frac{u(x +y) - u(x)}{y}}^2 + \norm{\frac{u(x +y) - u(x))}{y}}^2\right).
\end{align*}
Integrating over the strip $y \in (0, \delta)$ for a $\delta$ such that $\abs{u'(x) - \frac{u(x + \delta) - u(x)}{\delta}} < \varepsilon$ for all $x \in [0, 1]$ for a $\varepsilon$ to be chosen later
\begin{align*}
	\int_{0}^1 \int_0^{\delta} (u'(x))^2 \, dy \, dx = \delta \norm{u'}^2 &\le \int_0^1 \int_0^\delta \left(\frac{u(x +y) - u(x))}{y} \right)^2 \, dy\, dx \\
	&+  \int_0^1 \int_0^\delta \left(u' - \frac{u(x +y) - u(x))}{y} \right)^2 \, dy\, dx \\
	&\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(u' - \frac{u(x +y) - u(x))}{y} \right)^2 \, dy\, dx \\
		&\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \varepsilon^2 \, dy\, dx 
	% &\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(u' - \frac{1}{y}\int_{x}^{x+y} u(s) \, ds \right)^2 \, dy\, dx \\
	% &\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(\frac{1}{y}\int_{x}^{x+y} u'(x) - u(s) \, ds \right)^2 \, dy\, dx  \\
	% &\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(\frac{1}{y}\int_{x}^{x+y} u'(x) - u(s) \, ds \right)^2 \, dy\, dx
\end{align*}
Hence we have 
\begin{align*}
	\delta \norm{u'}^2 \le \norm{F_x}_S^2 + \varepsilon^2 \delta  	\implies \norm{u'}^2 \le \norm{F_x}_S^2/\delta + \varepsilon^2 
\end{align*}
I guess we sub in $\varepsilon^2 \approx \frac{1}{2}\norm{u'}^2$ or something similar to obtain $\norm{u'}^2 \le C\norm{F_x}^2_S /\delta $.
The real question now is how big is $\delta$?
This may not be the best way to approach this, let's try the Hardy's approach
\begin{align*}
	\int_{0}^1 \int_0^{\delta} (u'(x))^2 \, dy \, dx = \delta \norm{u'}^2 &\le \int_0^1 \int_0^\delta \left(\frac{u(x +y) - u(x))}{y} \right)^2 \, dy\, dx \\
	&+  \int_0^1 \int_0^\delta \left(u' - \frac{u(x +y) - u(x))}{y} \right)^2 \, dy\, dx \\
	&\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(u' - \frac{u(x +y) - u(x))}{y} \right)^2 \, dy\, dx \\
	&\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(u' - \frac{1}{y}\int_{x}^{x+y} u(s) \, ds \right)^2 \, dy\, dx \\
	&\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(\frac{1}{y}\int_{x}^{x+y} u'(x) - u(s) \, ds \right)^2 \, dy\, dx  \\
	&\le \int_S F_x^2(x,y) \, d(x,y) + \int_0^1 \int_0^\delta \left(\frac{1}{y}\int_{0}^{y} u'(x) - u(s + x) \, ds \right)^2 \, dy\, dx \\
	&\le \int_S F_x^2(x,y) \, d(x,y) + C\int_0^1 \int_0^\delta \left(u'(x) - u(s + x) \right)^2 \, ds\, dx\\
	% &\le \int_S F_x^2(x,y) \, d(x,y) + C\int_0^1 \int_x^{x +\delta} \left(u'(x) - u(y) \right)^2 \, dy\, dx
\end{align*}
But we introduce another $ \norm{u'}^2$ to the right with a that's bigger than 1 if we use triangle inequality again and I really don't know any inequality about $u' - u$... 




Consider 
	\begin{align*}
		u(x) = \sum_{i=k}^p a_i \psi_i(x)
	\end{align*} 
and let $u^L = \sum_{i=1}^{c_p} a_i \psi_i$ and $u^H = \sum_{i=c_p+1}^p a_i \psi_i$ where $c_p$ will be specified later. 
The low frequency in the $\kappa$ norm is equivalent to the $L^2$ norm as follows: 
\begin{align*}
	\kappa\norm{u_E^L}^2 \le \norm{u^L_E}_\kappa^2 &= \kappa \norm{u^L_E}^2 + \abs{u^L_E}_1^2 \\
	&\le(\kappa + c_p^4)\norm{u_E^L}^2.
\end{align*}
Hence, we choose $c_p^4 = \kappa$. 
For the high frequency, 
\begin{align*}
	\abs{u_E^H}^2_1 \le \norm{u^H_E}_\kappa^2 &= \kappa \norm{u^H_E}^2 + \abs{u^H_E}_1^2 \\
	&\le \kappa \frac{1}{\kappa^2} \abs{u^H_E}^2_1 + \abs{u^H_E}_1^2 \\
	&\le 2\abs{u_H^L}^2.
\end{align*}

Unfortunately, this doesn't work very well due to the lack of an inverse theorem which only depends on the boundary for polynomials due to the following theorem from Borwein ( Markov and Bernstein type Inequalities in $L_p$ for Classes of Polynomials with Constraints): 
\begin{theorem}
	There exists a constant $c$ ind of $n, k$ (defined below) such that 
	\begin{align*}
		\norm{f'} \le c (n(k + 1)) \norm{f}
	\end{align*}
	for every $f \in \mathbb{P}_n([-1, 1])$ of the form $f(x) = h(x) q(x)$ where $h \in \mathbb{P}_{n - k}$ has no zeros in the open unit disk, $q \in \mathbb{P}_k$ for $0 \le k \le n$. 
\end{theorem}
And apparently the theorem is sharp. 
Indeed, this is supported by the numerical evidence in the Ipython notebook. 

\begin{align*}
	\norm{\nabla f} \le n \norm{f}
\end{align*}
such that $f \in \mathbb{P}_p(K)$ with $f$ vanishing on two of the edges. 

% Let's look at the decomposition. 
% Let $u^L_E$ be the $L^2$ extension from edge $E$ and $u^H_E$ be the $H^1$-norm extension from edge $E$.
% Let $\widetilde u_E$ be the optimal extension such that $\widetilde u_E|_{\partial T} = (u^L_E + u^H_E)|_{\partial T}$.
% By minimality, we have
% \begin{align*}
% 	\norm{\widetilde u_E}^2_\kappa &= \kappa \norm{\widetilde u_E}^2 + \abs{\widetilde u_E}_1^2 \\
% 	&\le \kappa \norm{u^L_E + u^H_E}^2 + \abs{u^L_E + u^H_E}_1^2 \\
% 	&= \norm{u^L_E + u^H_E}_{H^1}^2 + (\kappa - 1)\norm{u^L_E + u^H_E}^2 \\
% 	&= (u^L_E + u^H_E, u^L_E + u^H_E)_{H^1} + (\kappa - 1)(u^L_E + u^H_E, u^L_E + u^H_E) \\
% 	&= (u^L_E + u^H_E, u^L_E + u^H_E)_{H^1} + (\kappa - 1)(u^L_E + u^H_E, u^L_E + u^H_E)
% \end{align*}

Let's look at the infinite dimensions version. 
Let the domain be $x \in (0, 2\pi)$ and let
\begin{align*}
	u(x) =  \frac{1}{\sqrt{2\pi}}\sum_{n = -\infty}^\infty a_n e^{i n x}
\end{align*}
and we want to examine the extension onto $(x, y) \in (0, 2\pi) \times (0, \infty)$ with respect to the norm 
\begin{align*}
	\norm{u}_\kappa = \kappa^2 \norm{u}^2 + \abs{u}^2_1.
\end{align*}
We assume a separation of variables, hence (assuming conjugation where appropriate)
\begin{align*}
	E(x, y) &= \frac{1}{\sqrt{2\pi}} \sum_{n=-\infty}^\infty a_n e^{inx} f_n(y) \\
	\norm{E(x, y)}^2 &= \int_0^\infty \int_0^{2\pi} \left(\frac{1}{\sqrt{2\pi}}\sum_{n=-\infty}^\infty a_n e^{inx} f_n(y)\right)^2 \\
	&= \int_0^\infty \sum_{n=-\infty}^\infty a_n^2  f_n^2(y) \, dy.
\end{align*}
and 
\begin{align*}
	\nabla E(x, y) &= \begin{bmatrix}
		\frac{1}{\sqrt{2\pi}} \sum_{n=-\infty}^\infty a_n in e^{inx} f_n(y) \\
		\frac{1}{\sqrt{2\pi}} \sum_{n=-\infty}^\infty a_n e^{inx} f_n'(y)
	\end{bmatrix} \\
	\abs{E}_{1}^2 &= \int_0^\infty \int_0^{2\pi}  \left(\frac{1}{\sqrt{2\pi}}\sum_{n=-\infty}^\infty a_n in e^{inx} f_n(y)\right)^2 +  \left(\frac{1}{\sqrt{2\pi}}\sum_{n=-\infty}^\infty a_n e^{inx} f_n'(y)\right)^2  \\
	&=  \int_0^\infty \sum_{n=-\infty}^\infty a_n^2 n^2f_n^2(y) +  \sum_{n=-\infty}^\infty a_n^2 (f_n'(y))^2 
\end{align*}
Hence we need to minimize for a fixed $n$ such that $f_n(0) = 1$: 
\begin{align*}
	(\kappa^2 + n^2) \int_0^\infty f_n^2 + \int_0^\infty (f'_n(y))^2 .
\end{align*}
In order to minimize this, we solve the Euler equation 
\begin{align*}
	(\kappa^2 + n^2) f_n(x) - f''_n(x) = 0
\end{align*}
which is solved by $f_n(x) = e^{-\sqrt{\kappa^2 + n^2} x}$ hence 
\begin{align*}
	E(x, y) &= \frac{1}{\sqrt{2\pi}} \sum_{n=-\infty}^\infty a_n e^{inx} e^{-\sqrt{\kappa^2 + n^2} y}. 
\end{align*}
The norm is 
\begin{align*}
	\norm{E}_\kappa^2 &= \kappa^2\sum_{n=-\infty}^\infty \frac{a_n^2 }{2\sqrt{\kappa^2 + n^2}} + \sum_{n=-\infty}^\infty \frac{a_n^2 n^2}{2\sqrt{\kappa^2 + n^2}} +  \int_0^\infty \sum_{n=-\infty}^\infty a_n^2 (\sqrt{\kappa^2+n^2} \left(-e^{y \left(-\sqrt{\kappa^2+n^2}\right)}\right))^2 \\
	&= \kappa^2\sum_{n=-\infty}^\infty \frac{a_n^2 }{2\sqrt{\kappa^2 + n^2}} +  \sum_{n=-\infty}^\infty \frac{a_n^2 n^2}{2\sqrt{\kappa^2 + n^2}} +  \sum_{n=-\infty}^\infty a_n^2 \frac{1}{2}\sqrt{\kappa^2 +n^2}
\end{align*}

If we do a splitting as in the meeting, we split $\kappa^2 < n^2$ which implies
\begin{align*}
	-\sqrt{2n^2} \le -\sqrt{\kappa^2 + n^2} \le -\sqrt{n^2}
\end{align*}
and the other case $\kappa^2 \ge n^2$ which implies 
\begin{align*}
	-\sqrt{2\kappa^2} \le - \sqrt{\kappa^2 + n^2} \le - \sqrt{\kappa^2}
\end{align*}
So looking at the norm
\begin{align*}
	\norm{E}_\kappa^2 &\approx \kappa^2\sum_{n=-\kappa}^\kappa \frac{a_n^2 }{\kappa} +  \sum_{n=-\kappa}^\kappa \frac{a_n^2 n^2}{\kappa} +  \sum_{n=-\kappa}^\kappa a_n^2 \kappa +  \\
	& \kappa^2\sum_{n \text{ else}} \frac{a_n^2 }{n} +  \sum_{\text{ else}}\frac{a_n^2 n^2}{n} +  \sum_{n\text{ else}}a_n^2n
\end{align*}
hence the conjectured quasioptimal extensions is 
\begin{align*}
	F(x, y) &= \frac{1}{\sqrt{2\pi}} \left( e^{-{\kappa} y}\sum_{n = -\kappa}^{{\kappa}} a_n e^{inx}  + \sum_{n \text{ else}} a_n e^{inx} e^{-n y} \right) . 
\end{align*}

% Lets do this for finite dimensional tensor-product domain now. 
% We now let 
% \begin{align*}
% 	u(x) = \sum_{i=0}^p a_i P_i(x)
% \end{align*}
% on $[-1, 1]$ and assume the extension is 
% \begin{align*}
% 	E(x, y) = \sum_{i=0}^p a_i P_i(x) f_i(y).
% \end{align*}
% We see that 
% \begin{align*}
% 	\norm{E(x, y)}_\kappa^2 &= \int_{-1}^1 \sum_{i=0}^p a_i^2 \frac{2}{2i + 1} f_i^2
% \end{align*}


Let's look at the tensor product case. 
Let $f \in \mathbb{P}_p([-1, 1])$, and write it such that 
\begin{align*}
	f = \sum_{i=0}^p w_i \Psi_i(x)
\end{align*}
where $(\Psi_i, \Psi_j)_\kappa = \delta_{ij}$.
Similar to the $L^2$ case, we wish to extend it with some basis functions with the following properties
\begin{align*}
	\varphi_i \in \mathbb{P}_i([-1, 1]), \varphi_i(-1) = 1, \varphi_i(1) = 0.
\end{align*}
We also wish for our function to be the minimal extension hence we require
\begin{align*}
	u(x, y) = \sum_{i=0}^p w_i \Psi_i(x) \varphi_{p}(y)
\end{align*}
to be $\kappa$-orthogonal to interiors. 
Unfortunately, this is useless.

% Let's look at tensor product later, but first examine the norm on the boundary. 
% Inspired by substructuring preconditioners in $h$-version, we first let 
% \begin{align*}
% 	\norm{u}_\kappa^2 = \kappa^2 \norm{u}^2 + \abs{u}_1^2.
% \end{align*}
% The trace norm is defined as
% \begin{align*}
% 	\norm{u}_{1/2, \kappa}^2 &= \kappa \norm{u}^2 + \abs{u}_{1/2}^2.
% \end{align*}

Let $\norm{\cdot}_\kappa^2 = \kappa^2 \norm{\cdot}^2 + \abs{\cdot}_1^2$, then the boundary norm is probably 
\begin{align*}
	\norm{\cdot}_{1/2\kappa}^2 = \kappa\norm{\Pi_q \cdot}^2 + \abs{(I - \Pi_q)\cdot}_{1/2}^2
\end{align*}
where $\Pi_q$ is the projection to ``lower frequencies.''
If we assume that there exists an extension operator similar to BCMP such that 
\begin{align*}
	\norm{F}_\kappa^2 \le \norm{f}_{1/2\kappa}^2,
\end{align*}
this is still not enough if we consider the limiting case of $\kappa \to \infty$ which is the mass matrix case. 
Somehow, we know that for $\kappa \to \infty$ that 
\begin{align*}
		\norm{F}_\kappa^2 \le \frac{1}{p}\norm{f}_{1/2\kappa}^2
\end{align*}
and even that is not enough to prove uniformity in condition number growth since the inverse trace estimate garners $p^2$. 
This suggests that this way is not enough to prove (at least on the triangle), maybe it'll work in tensor product. 



\end{document}
