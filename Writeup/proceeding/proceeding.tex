\documentclass{siamart171218}
\headers{Preconditioning the Mass Matrix}{M. Ainsworth, and S. Jiang}

% \usepackage[utf8]{inputenc}

\usepackage{color,soul}
\soulregister\cite7
\soulregister\cref7
\soulregister\pageref7

\usepackage{listings}
\usepackage{commath} \let\abs\undefined
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
% \usepackage{amsthm}
\usepackage{pgfplots}
\usepackage{enumerate}
\usepackage{mathtools}
\usepackage[nocompress]{cite}
% \newtheorem{theorem}{Theorem}[section]
% \newtheorem{corollary}{Corollary}[theorem]
% \newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}


\newcommand{\matr}[1]{\mathbf{#1}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\sech}{sech}
\newcommand{\bigO}{\mathcal{O}}

\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\pgfplotsset{compat=1.12}
% \pgfplotsset{compat=1.13,
% 	table/search path = {../Eigenvalues_Timing},
% }

\title{On the Effect of Different Nodal Bases on Preconditioning the Mass Matrix}
\author{Mark Ainsworth and Shuai Jiang}

\begin{document}
\maketitle

% REQUIRED
\begin{abstract}
Insert abstract sometimes. 
Numerical examples are presented illustrating the
performance of the algorithm.
\end{abstract}

% REQUIRED
\begin{keywords}
	preconditioning mass matrix, polynomial extension theorem, high order finite elements
\end{keywords}

% REQUIRED
\begin{AMS}
  65N30, 65N55, 65F08
\end{AMS}


\section{Introduction}
Key points to hit in introduction
\begin{enumerate}
	\item Mass matrices and their poor conditioning for high order approximations, and why this is a problem in time stepping
	\item Allude to a the ASM preconditioner that we previously developed, and discuss the oddity of the vertex function order 
	\item Say what we do in the paper, which is to analyze other vertex function, and show that they are have suboptimal growth
\end{enumerate}

\section{Additive Schwarz Preconditioner}\label{sec:basis-functions}
In this section, we give a brief review of the Additive Schwarz Method (ASM) preconditioner, denoted by $\matr{P}^{-1}$, for the mass matrix presented in \cite{AMMPaper}. 
We then present three ASM preconditioners which are variations of $\matr{P}^{-1}$ where each have a different nodal space, and state results regarding their performance. 

% Goals of this section
% \begin{enumerate}
% 	\item Define the basis functions, and the subsequent ASM. (We do not have to layout the matrix formulation)
% 	\item Clarify that we only need to consider the ASM on the reference triangle
% 	\item Let the reader know what 
% \end{enumerate}

\subsection{Review of the Original ASM}
Let $T$ be the reference triangle in $\mathbb{R}^2$ with vertices $v_1 = (-1, -1), v_2 = (1, -1), v_3 = (1, -1)$; denote the edges as $\gamma_i$ for $i \in \{1,2,3\}$ where $\gamma_i$ is the edge opposite vertex $v_i$. 
Let $X := \mathbb{P}_p(T) = \spn \{ x^\alpha y^\beta : 0 \le \alpha, \beta , \alpha + \beta \le p \}$ be the space of polynomials of total degree $p$ on $T$. 
Denote the standard $L^2$ inner-product on $T$ as $(\cdot, \cdot)$.

The barycentric coordinates $\lambda_i\in \mathbb{P}_1(T), i \in \{1,2,3\}$ of $T$ are functions such that $\lambda_i(v_j) = \delta_{ij}$ for $i,j \in \{1,2,3\}$.
Recall that the $n$th order Jacobi polynomials $P^{(a,b)}_n$ with weights $a, b > -1$ \cite{abramowitz1964handbook} satisfy the orthogonality property
\begin{align*}
	\int_{-1}^1 (1-x)^a(1+x)^b P_n^{(a,b)}P_m^{(a,b)} \, dx = 0 \qquad n \not = m.
\end{align*}


We first define the interior basis functions by
\begin{align*}
	\psi_{ij}(x, y) &= \frac{1 - s}{2} \frac{1 + s}{2} P_{i-1}^{(2,2)}(s) \left(\frac{1 -t}{2} \right)^{i+1} \frac{1 + t}{2} P_{j-1}^{(2i + 3, 2)}(t) \quad (x,y) \in T
\end{align*}
for $1 \le i, j, i + j \le p - 1$, where
\begin{align*}
	s = \frac{\lambda_2 - \lambda_1}{1 - \lambda_3}, \quad t = 2\lambda_3 - 1.
\end{align*}
Let $X_I = \spn\{\psi_{ij} : 1 \le i, j, i + j \le p - 1\}$, which is the space of polynomial bubble functions on $T$. 

Next define the edge basis functions on each edge $\gamma_i$ by
\begin{align*}
 	\chi_n^{(i)}(x, y) = 4 \lambda_{j}\lambda_k P_n^{(2,2)}(\lambda_j - \lambda_k) \qquad (x,y) \in T
 \end{align*} 
 where $i,j,k\in \{1,2,3\}$, $i,j,k$ are mutually distinct, and $n = 0, \ldots, p-2$.
 Define $X_{E_i} =\spn \{ \chi_n^{(i)} : 0 \le n \le p-2\}$ for $i = 1,2,3$, then the edge subspaces can can be defined as
 \begin{align*}
 	\widetilde X_{E_i} = \spn\{ u + u_I : u \in X_{E_i}, u_I \in X_I \text{ s.t. }   (u + u_I, w) = 0 \quad \forall w \in X_I\}
 \end{align*}
 for $i = 1,2,3$.
 Recall from \cite{AMMPaper} that $\widetilde X_{E_i}$ is the space of the so-called ``minimal extensions.''



Finally, we define our original vertex basis function from \cite{AMMPaper}: for $i \in \{1,2,3\}$, let 
\begin{align*}
	\varphi_i(x, y) &= \frac{(-1)^{\floor{p/2}+1}}{\floor{p/2}}  \lambda_iP_{\floor{p/2}-1}^{(1,1)}(1 - 2\lambda_i), \qquad (x, y) \in T.
\end{align*}
Let $X_V = \spn\{\varphi_i : i = 1,2,3 \}$; the vertex space can be defined as 
\begin{align*}
 	\widetilde X_{V} = \spn\{ u + u_I : u \in X_{V}, u_I \in X_I \text{ s.t. }   (u + u_I, w) = 0 \quad \forall w \in X_I\}.
\end{align*}
We have the decomposition of $X$ 
\begin{align}\label{eqn:space-decomposition}
	X = X_I \oplus \widetilde X_V \oplus \bigoplus_{i=1}^3 \widetilde X_{E_i},
\end{align}
hence for all $u \in X$, the following decomposition is unique
\begin{align}\label{eqn:decomp}
	u = u_I + u_V + \sum_{i=1}^3 u_{E_i} 
\end{align}
where $u_I \in X_I, u_V \in \widetilde X_V$ and $u_{E_i} \in \widetilde X_{E_i}$ for $i = 1,2,3$.

To complete the definition of the ASM preconditioner, we have to define bilinear forms on each subspace from \cref{eqn:space-decomposition}:
	\begin{itemize}
		\item Interior space $X_I$:
		\begin{align*}
			a_I(u, w) &:= (u, w),  \qquad u, w \in X_I.
		\end{align*}
		\item Vertex space $\widetilde X_V$:
		\begin{align*}
			a_V( u,  w) &:= \frac{1}{p^4} \sum_{i = 1}^3  u(v_i)  w(v_i), \qquad  u,  w \in  \widetilde X_{V}.
		\end{align*}
		where $v_i$ for $i \in \{1,2,3\}$ are the vertices of $T$.
		\item Edge spaces $\widetilde X_{E_i}$ for $i \in \{ 1,2,3\}$: 
		\begin{align*}
		 	a_{E_i}( u,  w) &:= \sum_{n=0}^{p-2} q_n \mu_n( u) \mu_n( w), \qquad  u, w \in  \widetilde X_{E_i}
		 \end{align*}
		 where 
		 \begin{align}\label{eqn:coefficients}
\begin{split}
		q_n&:=  \frac{2}{(p + 4 + n)(p - n + 1)}	\int_{-1}^1 (1-x^2)^2 P_{n}^{(2,2)}(x)^2 \, dx \\
	 &= \frac{64(n+1)(n+2)}{(p + 4 + n)(p - n + 1)(2n + 5)(n+3)(n+4)} 
\end{split}
		\end{align}
		 and $\mu_n$ is the weighted moment given by
		 \begin{align*}
		 	\mu_n(u) := \frac{(2n + 5)(n+3)(n+4)}{32(n+1)(n+2)}\int_{-1}^1 (1-x^2)P_n^{(2,2)}(x) u(x) \, dx
		 \end{align*}
		 where we use a linear parametrization such that $\gamma_i =[-1, 1]$.
		 % We recall that as $\chi^{(i)}_n|_{\gamma_i} = (1-x^2)P_n^{(2,2)}(x)$, we obtain orthogonality properties.
	\end{itemize}

	Given $f \in X$, the additive Schwarz method $\matr{P}^{-1}$ from \cite{AMMPaper} is defined as:\vspace{4pt}
	\begin{enumerate}[(i)]
		\item Seek $u_{I} \in X_{I}:a_{I}(u_{I}, v_I) = (f, v_I) \quad \forall v_I \in X_{I}$.
		\item Seek $u_V \in X_V : a_V( u_V,  v_V) = (f,  v_V)\quad \forall  v_V \in   X_V$.
		\item For edges $i = 1,2,3$, seek $ u_{E_i} \in \widetilde X_{E_i} : a_{e}( u_{E_i}, v_{E_i}) = (f, v_{E_i})\quad \forall v_{e} \in \widetilde X_{E_i}$.
		\item $u := u_I +   u_V +  \sum_{i=1}^3  u_{E_i}$ is our solution.
	\end{enumerate}

	The key result regarding $\matr{P}^{-1}$ is the following theorem 
	\begin{theorem}
	For all $u \in X$ with the decomposition as in \cref{eqn:decomp}, there exists constants $\gamma$ and $\Upsilon$ independent of $p$ such that 
		\begin{align*}
			\gamma (u, u) \le a_I(u_I, u_I) + a_V(u_V, u_V) + \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i})\le \Upsilon (u,u)
		\end{align*}	
		Hence, $\matr{P}^{-1}$ is a uniform preconditioner of the mass matrix. 
	\end{theorem}

	\subsection{Variations of the ASM Preconditioner}	
	In this subsection, we will define three additional vertex basis functions, and their corresponding minimal extension subspaces to construct three different ASM preconditioner which are close variations of our original preconditioner $\matr{P}^{-1}$.

	For the first space, let
	\begin{align*}
		\ddot \varphi_i(x, y) &= \frac{(-1)^{p+1}}{p}  \lambda_iP_{p-1}^{(1,1)}(1 - 2\lambda_i), \qquad (x, y) \in T
	\end{align*}
	for $i = \{1,2,3\}$.
	Observe that this is simply $\varphi_i$ with $\floor{p/2}$ replaced with $p$; we shall call these basis functions ``full order'' basis functions.
	Let $X_F = \spn\{\ddot \varphi_i : i = 1,2,3\}$, then let the corresponding subspace be 
	\begin{align*}
		\widetilde X_F = \spn\{ u + u_I : u \in X_F, u_I \in X_I \text{ s.t. }   (u + u_I, w) = 0 \quad \forall w \in X_I\}.
	\end{align*}
	The associated bilinear form is the same as the original preconditioner
	\begin{align*}
		a_F(u, w) := a_V(u, w)  \qquad u, w \in \widetilde X_F.
	\end{align*}
	The corresponding ASM preconditioner using the full order basis functions $\matr{P}_F^{-1}$ is 
	\begin{enumerate}[(i)]
		\item $u_{I} \in X_{I}:a_{I}(u_{I}, v_I) = (f, v_I) \quad \forall v_I \in X_{I}$.
		\item $\widetilde u_F \in X_F : a_F(u_F, v_F) = (f, v_F)\quad \forall  v_F \in  \widetilde X_F$.
		\item For edges $i = 1,2,3$, $u_{E_i} \in \widetilde X_{E_i} : a_{E_i}(u_{E_i},v_{E_i}) = (f, v_{E_i})\quad \forall v_{E_i} \in \widetilde X_{E_i}$.
		\item $u := u_I +   u_F +  \sum_{i=1}^3  u_{E_i}$ is our solution.
	\end{enumerate}



	For the second preconditioner, let 
	\begin{align*}
		X_B := \spn \{\lambda_i^p : i = 1,2,3\}
	\end{align*}
	which is the space of the so-called Bernstein vertex functions and will be named as such \cite{Ainsworth2011}.
	The subspace is 
	\begin{align*}
		\widetilde X_B = \spn\{ u + u_I : u \in X_{B}, u_I \in X_I \text{ s.t. }   (u + u_I, w) = 0 \quad \forall w \in X_I\}.
	\end{align*}
	The associated bilinear form is the $L^2$ inner-product
	\begin{align*}
		a_B(u, w) := (u, w) \qquad u, w \in \widetilde X_B.
	\end{align*}
	The corresponding ASM preconditioner $\matr{P}_B^{-1}$ is 
	\begin{enumerate}[(i)]
		\item $u_{I} \in X_{I}:a_{I}(u_{I}, v_I) = (f, v_I) \quad \forall v_I \in X_{I}$.
		\item $u_B\in X_B : a_B(u_B,  v_B) = (f,  v_B)\quad \forall  v_B \in  \widetilde X_B$.
		\item For edges $i = 1,2,3$, $ u_{E_i} \in \widetilde X_{E_i} : a_{E_i}( u_{E_i}, v_{E_i}) = (f, v_{E_i})\quad \forall v_{E_i} \in \widetilde X_{E_i}$.
		\item $u := u_I +   u_B +  \sum_{i=1}^3  u_{E_i}$ is our solution.
	\end{enumerate}

	Finally, for the last preconditioner, let
	\begin{align*}
		X_L := \spn \{ \lambda_i : i = 1,2,3\}.
	\end{align*}
	This is the space of the affine, vertex interpolation functions. 
	The vertex space is
	\begin{align*}
		\widetilde X_L := \spn\{ u + u_I : u \in X_{L}, u_I \in X_I \text{ s.t. }   (u + u_I, w) = 0 \quad \forall w \in X_I\}
	\end{align*}
	The associated bilinear form is 
	\begin{align*}
		a_L(u, w) := (u, w) \qquad u, w \in \widetilde X_L.
	\end{align*}
	The corresponding ASM preconditioner $\matr{P}_L^{-1}$ is 
	\begin{enumerate}[(i)]
		\item $u_{I} \in X_{I}:a_{I}(u_{I}, v_I) = (f, v_I) \quad \forall v_I \in X_{I}$.
		\item $u_L \in X_L : a_L( u_L,  v_L) = (f,  v_L)\quad \forall  v_L \in  \widetilde X_L$.
		\item For edges $i = 1,2,3$, $ u_{E_i} \in \widetilde X_{E_i} : a_{E_i}( u_{E_i}, v_{E_i}) = (f, v_{E_i})\quad \forall v_{E_i} \in \widetilde X_{E_i}$.
		\item $u := u_I +   u_L +  \sum_{i=1}^3  u_{E_i}$ is our solution.
	\end{enumerate}

	We
	\begin{theorem}\label{thm:main}
	For all $u \in X$ with the decomposition as in \cref{eqn:decomp}, there exists constants $\gamma$ and $\Upsilon$ independent of $p$ such that 
		\begin{align*}
			\gamma (u, u) &\le a_I(u_I, u_I) + a_V(u_V, u_V) + \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i})\le \Upsilon (u,u) \\
			\gamma (u, u) &\le a_I(u_I, u_I) + a_F(u_F, u_F) + \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i})\le\log p\Upsilon (u,u) \\
			\gamma (u, u) &\le a_I(u_I, u_I) + a_B(u_B, u_B) + \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i})\le p\Upsilon (u,u) \\
			\gamma (u, u) &\le a_I(u_I, u_I) + a_L(u_L, u_L) + \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i})\le ?\Upsilon (u,u).
		\end{align*}	
	\end{theorem}

	\begin{table}[tb]
		\caption{Table which lists the four different vertex basis functions}
		\label{tab:basis}
		\centering
	
		\begin{tabular}{l|ccc}
		\hline
	
		\hline
		\textbf{Name} & \textbf{Definition} & \textbf{Subspace} & \textbf{ Growth} \\
		\hline
			Original  & $\frac{(-1)^{\floor{p/2}+1}}{\floor{p/2}}  \lambda_iP_{\floor{p/2}-1}^{(1,1)}(1 - 2\lambda_i)$ & $\widetilde X_V$& $\bigO(1)$\\
			Full order  &$\frac{(-1)^{p+1}}{p}  \lambda_iP_{p-1}^{(1,1)}(1 - 2\lambda_i)$& $ \widetilde X_F$ & $\bigO(\log p)$\\
			Bernstein & $\lambda_i^p$ & $\widetilde X_B$  & $\bigO(p)$\\
			Linear & $\lambda_i$ & $\widetilde X_L$ & $\bigO(p^2)$\\
		\hline
	
		\hline
		\end{tabular}
	\end{table}

	
	
\begin{figure}
	\begin{center}
	\begin{tikzpicture}[scale=.8]
	\begin{axis}[
	  axis x line=center,
	  axis y line=center,
	  xtick={1},
	  ytick={1},
	  xlabel={$x$},
	  ylabel={$y$},
	  xlabel style={below left},
	  ylabel style={above left},
	  xmin=-1.5,
	  xmax=1.5,
	  ymin=-1.5,
	  ymax=1.5]
	\coordinate [label=left:{$v_1$}](A) at (-1,-1);	
	\coordinate [label=right:{$v_2$}](B) at (1,-1);	
	\coordinate [label=left:{$v_3$}](C) at (-1,1);	
	\draw (A) -- (B) -- (C) -- cycle;
	
	\node [thick, align=right] at (.15,0.20) {$\gamma_1$};	
	\node [thick, align=right] at (.1, -1.15) {$\gamma_3$};	
	\node [thick, align=right] at (-1.20,-.1) {$\gamma_2$};	
	\end{axis}
	% \node [thick] at (-1,1) {$v_3$};	

\end{tikzpicture}
\caption{Figure of reference triangle $T$}
\label{fig:triangle}
\end{center}
\end{figure}


\section{Numerical Examples}
In this section, we present a numerical example which illustrates the performance of the preconditioners.
We plot the condition number of the preconditioned system obtained by using $\matr{P}^{-1}$, $\matr{P}^{-1}_F$, $\matr{P}^{-1}_B$, and $\matr{P}^{-1}_B$. 
We note that condition number from $\matr{P}^{-1}$ remains constant with $p$, while $\matr{P}^{-1}_F$ exhibits TODO: Figure out what the growths really are.

	\begin{figure}[h]
	\center
			\begin{tikzpicture}
				\begin{loglogaxis}[scale=1, 
				xlabel={$p$},
				ylabel={Condition number},
				legend pos=outer north east, 
				legend style={font=\small}
				]
				\addplot [green, mark=triangle] table [x={p}, y={pre}] {mass-cond.data};
				\addplot [orange, mark=x] table [x={p}, y={pre}] {log-growth.data};
				\addplot table [x={p}, y={bernstein-pre}] {bernstein.data};
				\addplot table [x={p}, y={linear}] {linear.data};

				\legend{$\matr{ P}^{-1}$, $\matr{P}_F^{-1}$, $\matr{P}_B^{-1}$, $\matr{P}_L^{-1}$}
				\end{loglogaxis}
			\end{tikzpicture}
	\label{fig:plot-cond}
	\caption{TODO: insert appropriate caption.
	% \hl{The condition numbers of $\matr{\hat M}, \matr{\hat M}_S$, $\matr{\hat P}^{-1/2}\matr{\hat M}\matr{\hat P}^{-1/2}$ and $\matr{\hat P}^{-1/2}\matr{\ddot M}\matr{\hat P}^{-1/2}$ are plotted on a log-log axis for $p = 5, 10, \ldots, 95$. 
	% The algebraic growth of $\kappa(\matr{\hat M})$ and $\kappa(\matr{\hat M}_S)$ with $p$ are consistent with \cite{ainsworth2003conditioning}, and the boundedness of $\kappa(\matr{\hat P}^{-1/2}\matr{\hat M}\matr{\hat P}^{-1/2})$ is predicted in \cref{thm:main-result}. Finally, we note that the ``full-order'' vertex basis system $\kappa(\matr{\hat P}^{-1/2}\matr{\ddot M}\matr{\hat P}^{-1/2})$ exhibits growth.}
	}
	\end{figure}

\section{Additive Schwarz Theory}\label{sec:additive-scwharz-theory}
Analogous to the approach in \cite{AMMPaper}, we will analyze the preconditioners on the reference triangle $\hat T$ with vertices $v_1 = (-1, -1), v_2 = (1, -1), v_3 = (-1, 1)$. 

\hl{Thanks to \cref{cor:mesh-result}, the analysis of the preconditioner reduces to bounding the condition number on the reference element as in \cref{thm:main-result}. }
\hl{Consequently, for the remainder of this article we confine our attention to the reference triangle.}
% In this section, we will restrict our attention to the reference triangle.

\hl{Let $X := \mathbb{P}_p( T)$ be equipped with the standard $L^2$ inner-product denoted by $(\cdot, \cdot)$ with the respective norm denoted by $\norm{\cdot}$, and let $X_I := H^1_0( T) \cap \mathbb{P}_p( T)$ be the interior space equipped with the $L^2( T)$ inner-product.}
\hl{The orthogonal complement of the (closed) subspace $X_I$ in $X$ is denoted by $\widetilde X_B$, i.e.}
\begin{align}
	X = X_I \oplus \widetilde X_B, \qquad X_I \perp \widetilde X_B.
	\label{eqn:orthogonal-decomp}
\end{align}

\hl{We begin by exploring the structure of the space $\widetilde X_B$.}
\hl{Let $\mathbb{P}_p(\partial  T)$ denote the space of traces of $\mathbb{P}_p( T)$ on the boundary $\partial  T$ of the reference triangle:}
\begin{align}
	\mathbb{P}_p(\partial  T) &= \{u : u = v |_{\partial  T} \text{ for some } v \in \mathbb{P}_p( T) \}.
	\label{eqn:boundary-space-def}
\end{align}
\hl{The next result shows that there is a one-to-one correspondence between $\widetilde X_B$ and $\mathbb{P}_p(\partial  T)$.}
\begin{lemma}
	\label{lem:orthogonality}
\hl{	For every $u \in \mathbb{P}_p(\partial  T)$, there exists a unique $\widetilde u \in \widetilde X_B$ which satisfies $\widetilde u = u$ on $\partial  T$, and $(\widetilde u, v) = 0$ for all $v \in X_I$. }
\hl{	Furthermore, $\widetilde u$ is a minimal $L^2$ extension of $u$ in the sense that for all $w \in \mathbb{P}_p( T)$ with $w|_{\partial  T} = u$ we have $\norm{\widetilde u}\le \norm{w}$.}
\end{lemma}

\begin{proof}
\hl{	Let $u \in \mathbb{P}_p(\partial T)$ be given.}
\hl{	According to \cref{eqn:boundary-space-def}, $u$ is equal to the trace of a polynomial in $\mathbb{P}_p(T)$, which we again denote by $u$.}
\hl{	We can construct a $\widetilde u \in \widetilde X_B$ with the claimed properties as follows. }

\hl{	Let}
	\begin{align*}
		u_I \in X_I : (u_I, v_I) = -(u, v_I) \qquad \forall v_I \in X_I.
	\end{align*}
\hl{	Set $\widetilde u = u + u_I$; clearly $\widetilde u|_{\partial T} = u$ and $(\widetilde u, v_I) = 0$ for all $v_I \in X_I$; this gives existence.}
\hl{	For uniqueness, let $\tilde w \in \mathbb{P}_p(T): \widetilde w|_{\partial T} = u, (\widetilde w, v_I) = 0$ for all $v_I \in X_I$, then }
	\begin{align*}
	  	(\tilde u - \tilde w, v_I) = 0 \qquad \forall v_I \in X_I.
	  \end{align*}
\hl{	Hence $\tilde u - \tilde w = 0$ as $\tilde u - \tilde w \in X_I$.}
\hl{	The minimal $L^2$ extension property follows from the Pythagorean identity. }
\end{proof}

\hl{We say that $\widetilde u$ is the ``minimal $L^2$ extension'' or ``minimal extension'' of $u \in \mathbb{P}_p(\partial T)$.}
\hl{\cref{lem:orthogonality} shows that $\widetilde u$ is uniquely determined by the boundary values of $u$ and the degree of the space.}


\hl{We decompose the space $\widetilde X_B$ further. }
\hl{Let $\widetilde \varphi_i$ and $\widetilde \chi^{(i)}_n$ be the minimal extension, constructed as described in \cref{lem:orthogonality}, of the vertex basis function and edge basis function defined in \cref{sec:basis-functions} respectively.}
\hl{Let }
\begin{align*}
	\widetilde X_V = \spn \{ \widetilde \varphi_i : i = 1,2,3 \}
\end{align*}
\hl{and }
\begin{align*}
	\widetilde X_{E_i} = \spn \{\widetilde \chi^{(i)}_n : n = 0, \ldots, p - 2 \}, \qquad i = 1,2,3.
\end{align*}
% One can check that for all $u \in \mathbb{P}_p(\partial T)$ that there exists a $v \in \widetilde X_V \oplus \bigoplus_{i=1}^3 \widetilde X_{E_i}$ such that $v|_{\partial T} = u$ due to the edge basis functions being equal to $(1-x^2)P_{i}^{(2,2)}(x)$ on the edge.
\hl{By the construction of the basis functions on the boundary and, thanks to \cref{eqn:span-decomposition} and \cref{eqn:orthogonal-decomp}, we have }
\begin{align}
	X = X_I\oplus \widetilde X_V \oplus\bigoplus_{i=1}^3 \widetilde X_{E_i}.
	\label{eqn:decomposition}
\end{align}

\hl{Let $\vec \varphi = [\varphi_1; \varphi_2; \varphi_3]$ where $\varphi_i$ are the vertex basis functions with $\vec \psi$ defined similarly for the interior basis functions, and, using the notation of \cref{sec:basis-functions}, define}
\begin{align}
	\vec{\widetilde \varphi}
	&= \vec{\varphi} - \matr{\hat M}_{VI}\matr{\hat M}_{II}^{-1} \vec\psi.
	\label{eqn:basis-min-energy}
\end{align}
% Writing $w \in X_I$ in the form $\vec w^T \vec \psi$, 
\hl{Then for $\vec u \in \mathbb{R}^3$, we have for all $X_I \ni w = \vec w^T \vec \psi$, }
\begin{align*}
	(\vec u^T \vec{\widetilde \varphi} , w) = \left(\vec u^T \vec{\widetilde \varphi} , \vec w^T \vec \psi \right) &= \left( \vec u^T ( \vec{\varphi} - \matr{\hat M}_{VI}\matr{\hat M}_{II}^{-1} \vec\psi), \vec w^T \vec \psi  \right) \\
	&= \vec u^T \matr{\hat M}_{VI} \vec w - \vec u^T \matr{\hat M}_{VI}\matr{\hat M}_{II}^{-1}\matr{\hat M}_{II} \vec w = 0.
\end{align*}
\hl{Hence $\{\widetilde \varphi_1, \widetilde \varphi_2, \widetilde \varphi_3\} \in \widetilde X_B$, and as a consequence forms a basis for $\widetilde X_V$ (since $\widetilde \varphi_i |_{\partial T} = \varphi_i |_{\partial T}$). }
\hl{A basis for $\widetilde X_{E_i}$ with $i = 1,2,3$ can be constructed in the same fashion.}


\subsection{Proof of Theorem 3.1}
\hl{We apply the standard theory \cite{chan1994domain,smith2004domain,toselli2005domain} for the analysis of additive Schwarz methods to the scenario as described above. }
\hl{In particular, we will follow the framework as laid out in \cite[\S 2]{toselli2005domain}.}
	\begin{lemma}[Local Stability]\label{lem:local-stability}
\hl{		For a constant $C$ independent of $p$, each of our local bilinear forms are coercive in the sense that}
		\begin{align*}
			(u, u) &= a_I(u, u)& \forall u \in X_I ,\\
			(u, u) &= a_{E_i}(u, u) & \forall u \in \widetilde X_{E_i}, i = 1,2,3, \\
			(u, u) &\le 3Ca_V(u ,u) &  \forall u \in \widetilde X_{V}.
		\end{align*}
	\end{lemma}
\hl{	The next result gives an estimate for the largest eigenvalue, and is an immediate consequence of the triangle inequality and \cref{lem:local-stability}:}
	\begin{lemma}\label{lem:cauchy-ineq}
\hl{		There exists a constant $C$ independent of $p$ such that for all $u \in X$, the unique decomposition }
		\begin{align*}
			u = u_I + u_V + \sum_{i=1}^3 u_{E_i}, 
		\end{align*}
\hl{		with $u_I \in X_I, u_V \in \widetilde X_{V}, u_{E_i} \in \widetilde X_{E_i}$,  satisfies}
		\begin{align*}
			\norm{u}^2 \le C\left(a_I(u_I, u_I) + a_V(u_V, u_V) +  \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i}) \right).
		\end{align*}
		\label{lem:triangle}
	\end{lemma}
\hl{	The final ingredient is the following bound for the smallest eigenvalue of the additive Schwarz operator, whose proof is the subject of \cref{sec:proofs}:}
	\begin{theorem}[Stable Decomposition]\label{lem:stable-decomposition}
\hl{		For all $u \in X$, with the decomposition as in \cref{lem:triangle}, there exists a constant $C$ independent of $p$ such that }
		\begin{align*}
			a_I(u_I, u_I) + a_V(u_{V}, u_V) +  \sum_{i=1}^3 a_{E_i}(u_{E_i}, u_{E_i}) \le C\norm{u}^2.
		\end{align*}
	\end{theorem}
	% The proof will be deferred to a later section (see \cref{pf:decomposition}). 
	% Finally, we can state the proof \cref{thm:main-result}: 
	% \begin{theorem}
	% 	% \label{thm:main-result}
	% 	The matrix $\matr{\hat P}^{-1}\matr{\hat M}$ has uniform condition number under $p$.
	% \end{theorem}
	% \begin{proof}[Proof of \cref{thm:main-result}]
		% By Theorem 2.7 of \cite{toselli2005domain}, and the above 3 lemmas. 
	% \end{proof}
\hl{	The proof of \cref{thm:main-result} is now an immediate consequence of \cref{lem:local-stability,lem:cauchy-ineq,lem:stable-decomposition} thanks to Theorem 2.7 of \cite{toselli2005domain}.}



\section{Technical Lemmas}\label{sec:proofs}
\hl{	In this section, we present the technical lemmas that were used in the proof of \cref{thm:main-result}. }
\hl{	For notational purposes, we let $\norm{\cdot}_{\omega}$ define the $L^2$-norm over a domain $\omega$, and we shall omit the subscript in the case $\omega = T$ the reference element.}
	% it is again implied $\norm{\cdot}$ is the $L^2$ norm over the reference element $T$ unless otherwise indicated. 

\hl{		We begin with a bound relating the vertex values of a polynomial to its $L^2$ norm over the triangle.}
\hl{	The constant appearing in \cref{lemma:endpoint} is the best one possible; a related result was proved in \cite{Warburton}.}
	\begin{lemma}
		\label{lemma:endpoint}
\hl{		For $u  \in \mathbb{P}_p(T)$, we have that }
		\begin{align*}
			\max_{i \in \{1,2,3\}} \abs{u(v_i)} &\le \frac{1}{2\sqrt{2}} (p+1)(p+2) \norm{u}. 
		\end{align*}
	\end{lemma}

\hl{	Next, we prove an equality needed to bound the minimal extension of the vertex functions.}
		\begin{lemma}\label{lem:vertex-norm-1D}
\hl{		Define}
		\begin{align*}
			\xi_p(x) &= \frac{(-1)^{p+1}}{p(p+1)}P_p'(x)(1-x) = \frac{(-1)^{p+1}}{p} \frac{1-x}{2}P^{(1,1)}_{p-1}(x), \quad x \in [-1, 1]
		\end{align*}
\hl{		where $P_p$ is the Legendre polynomial.}
\hl{		Then}
				\begin{align*}
			\norm{\xi_p}_{[-1, 1]}^2 = \frac{4}{(p+1) (2 p+1)}.
		\end{align*}
	\end{lemma}
	\begin{proof}
\hl{		We note that $\xi_p(-1) = 1, \xi_p(1) = 0$, and $\xi_p(x_i) = 0$ where $x_i, i = 2, \ldots, p$ are the roots of $P_p'(x)$.}
\hl{		Hence, using the $(p+1)$ point Gauss-Lobatto quadrature gives}
		\begin{align*}
			\int_{-1}^1 \xi^2_p(x) \, dx &= w_1 + \sum_{i=2}^{p} w_i \xi^2_p(x_i) + E % = \frac{2}{p(p+1)}
		\end{align*}
\hl{		where $E$ is the error term}
		% as the Gauss-Lobatto points are the zeros of $P_p'$.
		% Now recall that the error bound for Lobatto quadrature is 
		\begin{align*}
			E = - \frac{(p + 1)p^3 2^{2p+1} [(p-1)!]^4}{(2p+1)[(2p)!]^3} \od[2p]{}{x} \xi_p^{2}(x)\bigl|_{x = \eta}, \qquad \eta \in [-1, 1].
		\end{align*}
\hl{		for some $\eta \in [-1, 1]$.}
		% We have the exact error value as the $2p$th derivative of a $2p$ order polynomial is constant. 
\hl{		Direct calculation shows that $E = -\frac{2}{(2 p+1) (p+1) p}$ which, along with the fact that $w_1 = \frac{2}{p(p+1)}$, gives the result claimed.}
	\end{proof}
\hl{	Using the function defined in \cref{lem:vertex-norm-1D}, we can bound the minimal extensions of the vertex functions.}
	\begin{lemma}
		\label{lem:vertex-norm}
\hl{		The minimal extension of the vertex basis function of degree $p$  satisfies the bound}
		\begin{align*}
			\frac{c}{p^4} \le \norm{\widetilde \varphi_i}^2 \le \frac{C}{p^4}
		\end{align*}
\hl{		where $c$ and $C$ are positive constants independent of $p$.}
	\end{lemma}

	\begin{remark}
\hl{		The $\floor{p/2}$ order on the vertex functions is crucial here to guarantee that $\mathbb{Q}_{\floor{p/2}}$ is a smaller space than $\mathbb{P}_p$.}
\hl{		Using $p$ as the order on the Legendre polynomial will result in log-like growth rather than a uniform bound on the condition number; see \cref{fig:plot-cond}. }
	\end{remark}

	\begin{lemma}\label{lemma:integral2122}
	For $i \ge 1, j > 0$, we have the following equality
		\begin{align*}
			I_{i-1, j}:=\int_{-1}^1 (1-x)^2 (1+x) P_{i-1}^{(2,1)} P_j^{(2,2)} \, dx = (-1)^{j - i + 1} \frac{16i}{(j+3)(j+4)}.
		\end{align*}
	\end{lemma}
	\begin{proof}
		For $j < i-1$, $I_{i-1, j} = 0$ by orthogonality. 
		For $j \ge i-1$, first let $c_{nm} := \int_{-1}^1 (1-x)^2(1+x)P_n^{(2,1)} P_m^{(2,1)} \, dx = \delta_{nm}\frac{16}{2n+4} \frac{n+1}{n+3}$.
		From \cite{abramowitz1964handbook}, we have that 
		\begin{align*}
		(2n+4)P_n^{(2,1)} = (n+4)P_n^{(2,2)} + (n+2)P_{n-1}^{(2,2)}, 
		\end{align*}
		hence rewriting the equality
		\begin{align*}
			P_n^{(2,2)} = a_n P_n^{(2,1)} - b_n P_n^{(2,2)}
		\end{align*}
		where $a_n = \frac{2n + 4}{n+4}$ and $b_n = \frac{n+2}{n+4}$. 
		Substituting the identity into the integral
		\begin{align*}
			\int_{-1}^1 (1-x)^2 (1+x) P_{i-1}^{(2,1)} P_j^{(2,2)} \, dx &= a_j c_{i-1,j} - b_j \int_{-1}^1 (1-x)^2(1+x)P_{i-1}^{(2,1)}P_{j-1}^{(2,2)} \, dx \\
			&= - b_j I_{i-1, j-1} \\
			& \quad\vdots \\
			&= (-1)^{j-i+1} c_{i-1, i-1}a_{i-1} \Pi_{k=i}^j b_k
		\end{align*}
		where we iterate this process until $I_{i-1, i-2} = 0$.
		Direct simplification yields the result. 

	\end{proof}
	\begin{lemma}
		The minimal extension of the full-degree basis function of degree $p$ satisfies the bound
		\begin{align*}
		 	\frac{c}{p^4}\le \norm{\widetilde{\ddot \varphi_i}}^2_{\hat T} \le C \frac{\log p}{p^4}
		 \end{align*} 
	\end{lemma}
	\begin{proof}
		Without loss of generality, consider $\ddot \varphi_i$ which corresponds to the vertex at $(-1, -1)$ and we drop the $i$ indexing. 
		The lower bound is a direct application of Lemma 6.1 of \cite{AMMPaper}.

		We assume that $p$ is even, and let $q = p/2$ for the sake of simplicity. 
		We have that 
		\begin{align*}
			\norm{\widetilde{\ddot \varphi}}^2_T \le 2\norm{\widetilde\varphi}^2_{\hat T} + 2\norm{\widetilde{\ddot \varphi} - \widetilde\varphi}^2_{\hat T}
		\end{align*}
		where $\varphi$ is the original vertex functions corresponding to $(-1, 1)$. 
		Lemma 6.3 of \cite{AMMPaper} states that $\norm{\widetilde\varphi}^2_{\hat T} \le \frac{C}{p^4}$; all that remains is to estimate the second term. 

		To estimate $\norm{\widetilde{\ddot \varphi} - \widetilde\varphi}^2_{\hat T}$, we resort to Lemma 6.4 of \cite{AMMPaper} which gives an exact quantity for the norm of a minimal extension of an edge function. 
		To utilize Lemma 6.4, we need to express find $w_j, j = 0, \ldots, p-2$ such that 
		\begin{align*}
			\ddot \varphi - \varphi|_{\gamma} = \sum_{j=0}^{p - 2} w_j (1-x^2) P_j^{(2,2)}(x)
		\end{align*}
		where edge $\gamma = \{ (x, -1) : -1 \le x \le 1 \}$.
		Let $c_p = \frac{(-1)^{p+1}}{2p}$, we note that 
		\begin{align*}
			\ddot \varphi - \varphi|_{\gamma} =(1-x) \left(c_p P_{p-1}^{(1,1)}- c_qP_{q-1}^{(1,1)}\right)
		\end{align*}
		Due to orthogonality, we need to calculate $j = 0, \ldots, p-2$
		\begin{align*}
			w_j := \frac{\int_{-1}^1(1-x) \left(c_p P_{p-1}^{(1,1)} - c_qP_{q-1}^{(1,1)} \right)(1-x^2)P_j^{(2,2)} \, dx}{\norm{(1-x^2)P_j^{(2,2)}}^2_{[-1,1]}}.
		\end{align*}
		From \cref{eqn:coefficients}, we recall $\norm{(1-x^2)P_j^{(2,2)}}^2_{[-1,1]} = \frac{32(j+1)(j+2)}{(2j+5)(j+3)(j+4)}$, hence all that remains is to calculate the integral quantity. 

		To evaluate the integral, we note that the first part
		\begin{align*}
			c_p \int_{-1}^1  (1-x^2) P_{p-1}^{(1,1)}(1-x) P_j^{(2,2)} \, dx = 0 \qquad j = 0, \ldots, p-3
		\end{align*}
		due to orthogonality of $P_{p-1}^{(1,1)}$.
		We will calculate the value for $j = p-2$ below. 

		For the second portion, we again note by orthogonality that it is zero for $j = 0, \ldots, q-3$.
		From \cite{abramowitz1964handbook}, we have that
		\begin{align*}
			(2n+3)P_n^{(1,1)} = &(n+3)P_n^{(2,1)} - (n+1)P_{n-1}^{(2,1)} \\
			\implies P^{(1,1)}_{q-1} = &\frac{q+2}{2q+1} P_{q-1}^{(2,1)} - \frac{q}{2q+1} P_{q-2}^{(2,1)}
		\end{align*}
		Hence, for $j \ge q - 1$ 
		\begin{align*}
			\int_{-1}^1(1-x) c_qP_{q-1}^{(1,1)}(1-x^2)P_j^{(2,2)} \, dx &= \\
			c_q \int_{-1}^1 (1-x)^2(1+x)& \left(\frac{q+2}{2q+1} P_{q-1}^{(2,1)} - \frac{q}{2q+1} P_{q-2}^{(2,1)} \right)P_j^{(2,2)} \, dx \\
			&= c_q \left(\frac{q+2}{2q+1} I_{q-1,j} - \frac{q}{2q+1} I_{q-2, j} \right) \\
			&= c_q (-1)^{j-q+1}\frac{16 q }{(j+3) (j+4)}.
		\end{align*}
		For $j = q-2$, note that $I_{q-1, q-2} = 0$, hence 
		\begin{align*}
			\int_{-1}^1(1-x) c_qP_{q-1}^{(1,1)}(1-x^2)P_{q-2}^{(2,2)} \, dx &= c_q \left(- \frac{q}{2q+1} I_{q-2, q-2} \right) \\
			&= -c_q \frac{16(q-1)q}{(q+1)(q+2)(2q+1)}.
		\end{align*}

		Hence, we have that 
		\begin{table}[H]
			\caption{Value of $w_j$ at the indices from $j = 0, \ldots, p-2$.}
			\label{tab:tablename}
			\centering
		
			\begin{tabular}{l|c}
			\hline
		
			\hline
			\textbf{Range of $w_j$} & \textbf{Value}  \\
			\hline
				$j = 0, \ldots, q-3$ & $0$ \\
				$j = q-2, \ldots, p-3$ & $\frac{(-1)^j (2 j+5)}{4 (j+1) (j+2)}$ \\ % TODO: double check
				$j = p-2$ & $(-c_p \frac{16(p-1)p}{(p+1)(p+2)(2p+1)} +c_q \frac{16(q-1)q}{(q+1)(q+2)(2q+1)})/\norm{(1-x^2)P_{p-2}^{(2,2)}}^2$ \\ % TODO: simplify 
			\hline
		
			\hline
			\end{tabular}
		\end{table}
		

		Turning back to Lemma 6.3 of \cite{AMMPaper}, we see that 
		\begin{align*}
		 	\norm{\widetilde{\ddot \varphi} - \widetilde \varphi}^2_T &= \sum_{j=0}^{p-2} \frac{64(j+3)(j+4) w_j^2}{(2j + 5)(j+1)(j+2)(p+j+4)(p-j-1)}\\
		 	% &\le \bigO(p^{-4}) +C\sum_{j=q-2}^{p-3} \frac{w_j^2}{jp(p-j-1)}  \\
		 	&\le \bigO(p^{-4}) +C\sum_{j=q-2}^{p-3} \frac{1}{j^3p(p-j-1)}  \\
		 	&\le \bigO(p^{-4}) +\frac{C}{p^4}\int_{p/2-2}^{p-3} \frac{1}{p-j} \le \bigO\left(\frac{\log(p)}{p^{4}}\right) %TODO: double check integral 
		 \end{align*} 

	\end{proof}

\hl{	The next result gives an explicit expression for the norm of a minimal extension of an edge function:}
	\begin{lemma}
\hl{		Let $u \in \mathbb{P}_p(\gamma)$ be a polynomial on edge $\gamma \subset \partial T$, which vanishes at the endpoints, be written in the form}
		\begin{align*}
			u(x) = (1-x^2) \sum_{i=0}^{p-2} w_i P_i^{(2,2)}(x), 
		\end{align*}
\hl{		where $x\in [-1, 1]$ is a parametrization of $\gamma$. }
\hl{		Then the norm of the the minimal energy extension $\widetilde u \in \mathbb{P}_p(T)$, satisfying $\widetilde u = 0$ on $\partial  T \setminus \gamma$ and $u = \widetilde u$ on $\gamma$, is given by}
		\begin{align}
			\norm{\widetilde u}^2 = \sum_{i=0}^{p-2} \frac{2\mu_i w_i^2}{(p + i + 4)(p - i - 1)}
			\label{eqn:norm-equiv-edge}
		\end{align}
\hl{		where $\mu_i = \int_{-1}^1 (1 - x^2)^2 P_i^{(2,2)}(x)^2 \, dx =  \frac{32}{2i + 5} \frac{(i+1)(i+2)}{(i+3)(i+4)}$. }
		\label{lemma:extension}
	\end{lemma}




\hl{	Finally, we are in a position to give the proof of \cref{lem:stable-decomposition}:}
	\begin{proof}\label{pf:decomposition}
\hl{		The first step is to construct a suitable decomposition for $u \in X$.}
\hl{		Let}
		\begin{align*}
			u_{V} = \sum_{i=1}^3 u(v_i) \widetilde \varphi_i \in X_V
		\end{align*}
\hl{		be the interpolant to $u$ at the vertices using the minimal $L^2$ vertex functions. }

\hl{		Consequently $(u -u_{V})|_{\partial T} \in \mathbb{P}_p(\partial T)$ vanishes at the element vertices, and can therefore be written in the form }
		\begin{align*}
			u - u_V |_{\partial T} = U_1 + U_2 + U_3
		\end{align*}
\hl{		where $U_i \in \mathbb{P}_p(\partial T)$ is supported on edge $\gamma_i$.}
\hl{		We then let}
		\begin{align*}
			u_{E_i} \in X_{E_i}
		\end{align*}
\hl{		be the minimal $L^2$ extension of $U_i$ into the triangle. }
\hl{		It follows that}
		\begin{align*}
			u - u_{V}- \sum_{i=1}^3 u_{E_i} = u_I \in X_I
		\end{align*}
\hl{		Thus $u = u_V + \sum_{i=1}^3 u_{E_i} + u_I$ is a decomposition of $u$. }
\hl{		It remains to show the decomposition is uniformly bounded.}

		% We first note that by \cref{lem:vertex-norm} and \cref{lemma:extension} that $a_V(u_V, u_V)$ is spectrally equivalent to $\norm{u_V}^2$, and that $a_{E_i}(u_{E_i}, u_{E_i}) = \norm{u_{E_i}}^2$; hence, we will work with the standard $L^2$ inner-product on the reference element.
		% Since $u_{V} + \sum_{i=1}^3 u_{E_i} \in \widetilde X_B$, \cref{lem:orthogonality} gives us $\left(u_I,  u_{V} + \sum_{i=1}^3 u_{E_i}\right) = 0$, hence
		% \begin{align*}
		% 	a_I(u_I, u_I) = \norm{u_I}^2 \le \norm{u_I}^2 + \norm{ u_{V} + \sum_{i=1}^3 u_{E_i}}^2 = \norm{u}^2.
		% \end{align*}
		% It suffices to show that there exists a constant $C$ independent of $p$ such that 
		% \begin{align}
		% 	\norm{u_{V}}^2 +  \sum_{i=1}^3 \norm{u_{E_i} }^2 \le C\norm{u}^2.
		% 	\label{eqn:stable-decomp-schur}
		% \end{align}

\hl{		Firstly, by \cref{lemma:endpoint}:}
		\begin{align}\label{eqn:avleu}
			a_V(u_V, u_V) &= \frac{1}{p^4}\sum_{i=1}^3 u(v_i)^2 \le \frac{3}{p^4} \max_{i \in \{ 1,2,3\}} u^2(v_i) \le 3C \norm{u}^2.
		\end{align}
		% \begin{align*}
		% 	\norm{u_{V}}^2 = \norm{\sum_{i=1}^3 u(v_i) \widetilde \varphi_i}^2 \le 3 \max_{i \in \{1,2,3\}} \abs{u(v_i)}^2  Cp^{-4} \le C \norm{u}^2.
		% \end{align*}

\hl{		For the edge contributions, we use \cref{lem:edge-bound} to bound}
		\begin{align*}
			a_{E_i}(u_{E_i}, u_{E_i}) = \norm{u_{E_i}}^2  % \norm{\left( u - \sum_{i=1}^3 u_{V_i}\right)\bigl|_{E_1}}^2 \\
			\le C\norm{ u - u_{V}}^2 
			\le 2C\left(\norm{u}^2 +\norm{u_{V}}^2\right),
		\end{align*}
\hl{		and then use the estimate $\norm{u_V}^2 \le Ca_V(u_V, u_V)$ from \cref{lem:local-stability} and \cref{eqn:avleu}, to deduce $\norm{u_V}^2 \le \norm{u}^2$ and hence $a_{E_i}(u_{E_i}, u_{E_i}) \le C \norm{u}^2$.}

\hl{		Finally, as $u_{V} + \sum_{i=1}^3 u_{E_i} \in \widetilde X_B$, \cref{lem:orthogonality} gives us $\left(u_I,  u_{V} + \sum_{i=1}^3 u_{E_i}\right) = 0$, hence}
		\begin{align*}
			a_I(u_I, u_I) = \norm{u_I}^2 \le \norm{u_I}^2 + \norm{ u_{V} + \sum_{i=1}^3 u_{E_i}}^2 = \norm{u}^2,
		\end{align*}
\hl{		and our result follows.}
	\end{proof}

\bibliography{source.bib} 
\bibliographystyle{plain}

\end{document}
