\documentclass{article}

\usepackage{listings}
\usepackage{commath} \let\abs\undefined
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
% \usepackage{amsthm}
\usepackage{diagbox}
\usepackage{pgfplots,tikz}
\usepgfplotslibrary{fillbetween}
\usepackage{enumerate}
\usepackage{mathtools}
\usepackage[nocompress]{cite}
% \newtheorem{theorem}{Theorem}[section]
% \newtheorem{corollary}{Corollary}[theorem]
% \newtheorem{lemma}[theorem]{Lemma}
\newtheorem{remark}{Remark}


\newcommand{\matr}[1]{\mathbf{#1}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\sech}{sech}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\M}{\matr{M}}
\newcommand{\s}{\matr{S}}
\newcommand{\A}{\matr{A}}
\newcommand{\stiff}{\matr{L}}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\pgfplotsset{compat=1.12}
% \pgfplotsset{compat=1.13,
% 	table/search path = {../Eigenvalues_Timing},
% }

\begin{document}
	Let $0 \le \kappa \le 1/2$, and 
	\begin{align*}
		\norm{u}_{\kappa}^2 &= (1 - \kappa)\norm{\nabla u}^2 + \kappa \norm{u}^2 
	\end{align*}
	where $\norm{\cdot}^2$ is the $L^2$ norm. 
	We note that $\norm{u}_\kappa^2 \le \norm{u}_{H^1}^2$ trivially, but 
	\begin{align*}
		\norm{u}_{H^1}^2 \le \frac{1}{\kappa}\norm{u}_{\kappa}^2 
	\end{align*}

	Let $X = \mathbb{P}_p(K)$, and consider $X = X_I \oplus \widetilde X$ such that $X_I \perp \widetilde X$ with respect to the $\kappa$ norm. 
	Let $\widetilde X_V = \spn\{(I - \Pi_\kappa) \lambda_i \} $ where $\Pi_\kappa$ is the orthogonal projection (relative to $\kappa$ norm) into $X_I$. 
	Similarly, let $\widetilde X_{E_i} = \spn\{(I - \Pi_\kappa) u_{E_i} : X \ni u_{E_i} \text{ vanishes on the remaining edges} \}$ (this is just the standard edge space). 

	The decomposition is then $X \ni u = u_I +\widetilde u_V + \sum_{i=1}^3 \widetilde u_{E_i}$ where $u_I \in X_I$, $\widetilde u_V \in \widetilde X_V$ and $\widetilde u_{E_i} \in \widetilde X_{E_i}$. 
	By orthogonality, we have that $\norm{u}_\kappa = \norm{u_I}_\kappa + \norm{\widetilde u_V + \sum_{i=1}^3 \widetilde u_{E_i}}_\kappa$. 
	
	We first examine $\widetilde u_{V} = \sum_{i=1}^3 u(v_i) (I - \Pi_\kappa) \lambda_i$
	We note that for any $\widetilde u \in \widetilde X$ that the minimal norm property hold, thus: 
	\begin{align*}
		\norm{\widetilde u_{V}}^2 &= \min_{v = \widetilde u_{V}\text{ on } \partial \hat  T, v \in  X} (1-\kappa)\norm{\nabla v}^2 +\kappa \norm{v}^2.
	\end{align*}
	We choose $v =\sum_{i=1}^3 u(v_i)\lambda_i $, hence for $\alpha \in \mathbb{R}$, 
	\begin{align*}
				\norm{\widetilde u_{V}}^2 &\le (1-\kappa)\norm{\nabla \left(\sum_{i=1}^3 u(v_i)\lambda_i \right)}^2 +\kappa \norm{\sum_{i=1}^3 u(v_i)\lambda_i}^2 \\
				&\le (1-\kappa)\norm{\nabla \left(\sum_{i=1}^3 u(v_i)\lambda_i + \alpha\right)}^2 +\kappa \norm{\sum_{i=1}^3 u(v_i)\lambda_i}^2 \\
				&\le (1-\kappa)\norm{\sum_{i=1}^3 (u(v_i) + \alpha)\nabla \lambda_i}^2 +C\kappa \norm{u}_{\infty}^2 \\
				&\le C(1-\kappa)\norm{u + \alpha}_{\infty}^2\norm{\nabla\lambda_i}^2 +C \kappa (1 + \log p) \norm{u}_{H^1}^2 \\
				&\le C(1-\kappa)(1 + \log p)\norm{u + \alpha}_{H^1}^2 +C(1 + \log p) \norm{u}_{\kappa}^2.
	\end{align*}
	Finally, choosing $\alpha = -\frac{1}{\abs{K}} \int_K u$ the average value of $u$ allows one to use Poincaire/Fredrichs, meaning that 
	\begin{align*}
		\norm{\widetilde u_{V}}^2 &\le C(1-\kappa)(1 + \log p)\norm{u + \alpha}_{H^1}^2 +C(1 + \log p) \norm{u}_{\kappa}^2 \\
		&\le C(1-\kappa)(1 + \log p)\norm{\nabla u }^2 +C(1 + \log p) \norm{u}_{\kappa}^2 \\
		&\le C(1 + \log p)\norm{u }^2_\kappa +C(1 + \log p) \norm{u}_{\kappa}^2 \le \norm{u}_\kappa^2. 
	\end{align*}

	It remains to show that the edge decomposition is stable. 
	Let $\widetilde u_1 = u - u_I - \widetilde u_V$. 
	Trivially, we have that 
	\begin{align*}
		\norm{\widetilde u_1}_\kappa &\le (1 + \log^{1/2}p) \norm{u}_\kappa.
	\end{align*}

	From BCMP, let $\mathbb{Z}_p = \{u \in \mathbb{P}_p([-1, 1]): u(-1) = u(1) = 0\}$ and $v \in \mathbb{Z}_p$ then 
	\begin{align*}
		\norm{u}_{H_{00}^{1/2}([-1, 1])} \le C( 1+ \log p) \norm{u}_{H^{1/2}([-1, 1])}.
	\end{align*}
	Furthermore, 
	\begin{align*}
		\norm{u}_{H_{00}^{1/2}([-1, 1])}^2 \le C( 1+ \log p) \norm{u}_{\infty}^2 + \abs{u}_{H^{1/2}([-1, 1])}^2.
	\end{align*}

	Let $u_{E_i} \in X$ such that $\abs{u_{E_i}}_{H^1} \le \abs{u}_{H^1}$ where $u_{E_i} = \widetilde u_{E_i}$ on the boundary. 
	By BCMP, we know that for every $u \in X$, there exists a decomposition such that 
	\begin{align*}
		\abs{u_I}_{H^1}^2 + \abs{u_V}_{H^1}^2 + \sum_{i=1}^3\abs{u_{E_i}}_{H^1}^2 \le (1 + \log^2p)\abs{u}_{H^1}^2.
	\end{align*}
	is satisfied.

	%Mark's argument
	As $\widetilde u_V = u_V$ on the boundary, we have $\widetilde u_{E_i} = u_{E_i}$ and so by the min argument
	\begin{align*}
		\norm{\widetilde u_{E_i}}_\kappa^2 &\le \norm{u_{E_i}}^2_\kappa
	\end{align*}

	\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}

	Let's look at the edge splitting for the mass dominated case.
	We know the naive way will result in $p^4$ scaling which is not what we want. 
	Let $\widetilde u_1 = u - u_I - \sum_{k=1}^3 \widetilde u_{V_i}$, we note that 
	\begin{align*}
		\norm{\widetilde u_1}^2_{L^2} &\le \norm{u}_{L^2}^2 + \norm{u_I}^2_{L^2} + \sum_{i=1}^3\norm{u_{V_i}}_{L^2}^2 \\
		&\le 2 \norm{u}_\kappa^2 + 2 \norm{u_I}_\kappa^2+ p^4 \Upsilon_p \norm{u}_{L^2}^2.
	\end{align*}
	TODO: double check above for the $u_V$ case. 

	\begin{align*}
		\norm{\widetilde u_{E_i}}^2_\kappa &\le \norm{u_{E_i}^{MS}}_\kappa^2 \\
		&= (1 - \kappa)\norm{u_{E_i}^{MS}}_{H^1}^2 + (2\kappa - 1) \norm{u_{E_i}^{MS}}_{L^2}^2 \\
		&\le (1 - \kappa) \norm{\widetilde u_{E_i}}_{H^{1/2}_{00}(E_i)}^2 + (2\kappa - 1)\norm{\widetilde u_{E_i}}_{L^2(E_i)}^2 \\ 
		&= (1 - \kappa) \norm{\widetilde u_1}_{H^{1/2}_{00}(E_i)}^2 + (2\kappa - 1)\norm{\widetilde u_1}_{L^2(E_i)}^2 \\
		&\le (1 - \kappa)(1 + \log p) \norm{\widetilde u_1}_{\infty}^2 + (2\kappa - 1)p^2\norm{\widetilde u_1}_{L^2(K)}^2 \\
		&\le (1 + \log^2 p) \norm{u}_{\kappa}^2 + (2\kappa - 1)p^6 \Upsilon \norm{u}_{\kappa}^2.
	\end{align*}

	Let us look at the high/low frequency splitting. 

 	Consider the reference triangle as $K = \{x, y : 0 \le x, y, x + y \le 1\}$ and let $u(x) = \sum_{i=0}^p a_i \psi_i(x)$ where $\psi_i(x) \in \mathbb{P}_i$ is defined later.
 	Let $u_L(x) = \sum_{i=0}^{c_p} a_i \psi_i(x)$ and $u_H(x) = \sum_{i=c_p + 1}^p a_i \psi_i(x)$.
 	Now, let 
 	\begin{align*}
 		\norm{u}_\kappa^2 &= \kappa \norm{u}^2 + \abs{u}_1^2. 
 	\end{align*}
 	We want to examine $\kappa \ge 1$. 

 	Looking at the low frequency (or otherwise the $L^2$ part), 
 	\begin{align*}
 		\norm{u_L^E}^2_\kappa &= \kappa \norm{u_L^E}^2 + \abs{u_L^E}_1^2 \\
 		&\le \kappa \norm{u_L^E}^2 + c_p^4 \norm{u_L^E}^2 \\
 		&\le (\kappa + c^4_p)\norm{u_L^E}^2 \le 2\kappa\norm{u_L^E}^2.
 	\end{align*}
 	where we chose $c^4_p = \kappa$. 

 	For the Poincaire, we need 

\end{document}
