\documentclass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}

\usepackage{amsmath}
\usepackage{listings}
\usepackage{commath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\newcommand{\F}{\mathcal{F}}
\newcommand{\E}{\mathcal{E}}

\begin{document}
\section{1D}
Let $u = \sum_{k=0}^p c_k B^p_k(x)$, and let $\widetilde u = \sum_{k=0}^p w_k c_k B^p_k(x)$ where $w_k \in [0, 1]$ such that 
\begin{enumerate}
	\item $\widetilde u(-1) = u(-1)$
	\item $\widetilde u(1) = 0$
	\item $\norm{\widetilde u}_\kappa \le C \norm{u}_\kappa$.
\end{enumerate}
The first two condition implies that $w_0 = 1$ and $w_p = 0$. 
Observe that 
\begin{align*}
	\norm{\widetilde u} &= \begin{bmatrix}
		w_0 c_0 &
		\hdots &
		w_{p} c_p
	\end{bmatrix}M \begin{bmatrix}
		w_0 c_0 \\
		\vdots \\
		w_{p} c_p
	\end{bmatrix} \\
	&= \vec c^T\Lambda M \Lambda \vec c
\end{align*}
where $\Lambda = \text{diag}(w_0, \ldots, w_p)$.
One thing to prove is that 
\begin{align*}
	0 < CM - \Lambda M \Lambda
\end{align*}
but this seems to be true from numerical experiments; we will deal with this later but this would imply that $\norm{\widetilde u} \le \norm{u}$.

Now, we have that 
\begin{align*}
	(B^p_k)' &= p(B^{p-1}_{k-1} - B^{p-1}_{k})
\end{align*}
hence 
\begin{align*}
	\nabla u &= p\sum_{k=0}^{p} c_k (B^{p-1}_{k-1} - B^{p-1}_{k}) \\
	&= -p\sum_{k=0}^{p - 1} (c_{k+1} - c_k)B^{p-1}_k
\end{align*}
and 
\begin{align*}
	\nabla \widetilde u &= -p\sum_{k=0}^{p - 1} (w_{k+1}c_{k+1} - w_{k}c_k)B^{p-1}_k
\end{align*}
Adding/subtracting gives $w_{k+1}c_{k+1} - w_{k}c_k = (w_{k+1} - w_k)c_{k+1}  + w_{k}(c_{k+1} - c_k)$, hence 
\begin{align*}
	\nabla \widetilde u &=  -p\sum_{k=0}^{p - 1} ((w_{k+1} - w_k)c_{k+1}  + w_{k}(c_{k+1} - c_k))B^{p-1}_k\\
	&=  -p\sum_{k=0}^{p - 1} ((w_{k+1} - w_k)c_{k+1})B^{p-1}_k  - p \sum_{k=0}^{p-1} (w_{k}(c_{k+1} - c_k))B^{p-1}_k.
\end{align*}
Using triangle inequality, 
\begin{align*}
	\norm{\nabla \widetilde u} &\le \norm{p\sum_{k=0}^{p - 1} ((w_{k+1} - w_k)c_{k+1})B^{p-1}_k} + \norm{p \sum_{k=0}^{p-1} (w_{k}(c_{k+1} - c_k))B^{p-1}_k} \\
	&\le\norm{\sum_{k=0}^{p - 1} (\nabla_h w_k)c_{k+1}B^{p-1}_k}  +  (\nabla\vec u)\Lambda_{p-1}M_{p-1}\Lambda_{p-1} (\nabla \vec u)
\end{align*}
where we set $\nabla_h w_k = \frac{w_{k+1} - w_k}{h} = p(w_{k+1} - w_k)$.
For the second term, we note that $(\nabla\vec u)M_{p-1} (\nabla \vec u) = \norm{\nabla u}$ hence the quantity is bounded. 
For the first term, we need to minimize $ (\nabla_h w_k)$ given the boundary condition, but this is done with the linear interpolation.

\subsection{Quadratic form}
Let's try proving the one-d scaling thing.
Let $u = \sum_{k=0}^p c_k B^p_k$ and $\tilde u = \sum_{k=0}^p \frac{k}{p} c_k B^p_k$.
We want to show that $\norm{\tilde u} \le C\norm{u}$ where $C$ is a constant independent of $p$ (and is around 1.3 for that matter).

Recall that 
\begin{align*}
	(\lambda_0 + \lambda_1)B^p_k &= \binom{p}{k} \lambda_0^{k+1} \lambda_1^{p-k} + \binom{p}{k} \lambda_0^{k} \lambda_1^{p-k + 1} \\
	&= \frac{\binom{p}k}{\binom{p + 1}{k+1}} B^{p + 1}_{k + 1} + \frac{\binom{p}{k}}{\binom{p + 1}{k}} B^{p+1}_k \\
	&= \frac{k + 1}{p + 1}B^{p + 1}_{k + 1} + \frac{p - k + 1}{p + 1} B^{p+1}_k,
\end{align*}
so applying the above to $\widetilde u$, we have 
\begin{align*}
	\tilde u &= \sum_{k=0}^p \frac{k}{p} c_k B^p_k \\
	&= \sum_{k=0}^{p}\frac{k}{p} c_k \left( \frac{k + 1}{p + 1}B^{p + 1}_{k + 1} + \frac{p - k + 1}{p + 1} B^{p+1}_k \right) \\
	&= \sum_{k=0}^{p+1} \left(\frac{k}{p}\frac{p - k + 1}{p + 1}c_k + \frac{k-1}{p}\frac{k}{p + 1} c_{k-1}\right) B^{p+1}_k.
\end{align*}
Let's compare this to $x u$
\begin{align*}
	x u = \lambda_0 u &= \lambda_0 \sum_{k=0}^{p} c_k \binom{p}{k}\lambda_0^k \lambda_1^{p-k} \\
	&= \sum_{k=0}^p c_k \frac{\binom{p}{k}}{\binom{p + 1}{k+1}} B^{p+1}_{k+1}\\
	&= \sum_{k=0}^p c_k \frac{k + 1}{p + 1} B^{p+1}_{k+1}
\end{align*}



\section{2D}
Let $u = \sum_{\abs{\alpha} = p} c_\alpha B^p_\alpha(x)$ which vanishes at vertices, and let $\widetilde u = \sum_{\abs{\alpha} = p} w_\alpha c_\alpha B^p_\alpha(x)$ where $w_\alpha \in [0, 1]$ such that 
\begin{enumerate}
	\item $\widetilde u|_\gamma = u|_\gamma$
	\item $\widetilde u|_{\partial T \setminus \gamma} = 0$
	\item $\norm{\widetilde u}_\kappa \le C \norm{u}_\kappa$.
\end{enumerate}
The $L^2$ norm is conditional on the 2D equivalent of the statement 
\begin{align*}
	0 < CM - \Lambda M \Lambda.
\end{align*}
Here, I'm a bit confused as to what this means if it's true for general $\Lambda$ for the vertex extensions. (TODO: discuss below).

So assuming the $L^2$ part is satisfied, we go to the $H^1$.
Numerical evidence indicates this is not so trivial and there is actually growth here for the mass part. 
How do we deal with this....



\end{document}