\documentclass{siamart171218}
\headers{Preconditioning the Mass Matrix}{M. Ainsworth, and S. Jiang}

% \usepackage[utf8]{inputenc}
\usepackage{color,soul}
\soulregister\cite7
\soulregister\cref7
\soulregister\pageref7



\usepackage{listings}
\usepackage{commath} \let\abs\undefined
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
% \usepackage{amsthm}
\usepackage{diagbox}
\usepackage{pgfplots,tikz}
\usepgfplotslibrary{fillbetween}
\usepackage{enumerate}
\usepackage{mathtools}
\usepackage[nocompress]{cite}
% \newtheorem{theorem}{Theorem}[section]
% \newtheorem{corollary}{Corollary}[theorem]
% \newtheorem{lemma}[theorem]{Lemma}
\newtheorem{remark}{Remark}


\newcommand{\matr}[1]{\mathbf{#1}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\sech}{sech}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\M}{\matr{M}}
\newcommand{\s}{\matr{S}}
\newcommand{\A}{\matr{A}}
\newcommand{\stiff}{\matr{L}}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\pgfplotsset{compat=1.12}
% \pgfplotsset{compat=1.13,
% 	table/search path = {../Eigenvalues_Timing},
% }

\title{The influence and choice of the coarse space in Substructuring Preconditioners for Matrices in $p$-version FEM on Triangles\thanks{Submitted to the editors DATE.
\funding{This work was supported by the Department of Defense (DoD) through the National Defense Science \& Engineering Graduate Fellowship (NDSEG) Program.}}}
\author{Mark Ainsworth\thanks{Department of Applied Mathematics, Brown University, Providence, RI. (\email{mark\_ainsworth@brown.edu}, \email{shuai\_jiang@brown.edu})}
\and Shuai Jiang\footnotemark[2]}


\begin{document}
\maketitle

% REQUIRED
\begin{abstract}
Insert abstract. 
\end{abstract}

% REQUIRED
\begin{keywords}
	preconditioning, high order finite elements
\end{keywords}

% REQUIRED
\begin{AMS}
  65N30, 65N55, 65F08
\end{AMS}


\section{Introduction}
High order finite element methods ($p$-FEM) have many attractive theoretical properties, in particular exponential convergence in large classes of problems; for an overview, we refer the reader to \cite{Schwab,Szabo,Demkowicz} and the references therein.
While the theory is sound, there are still some computational issues in practice.
In the present work, we are concerned with the ill-conditioning, for even moderate orders $p$, of the mass and stiffness matrices for commonly used bases. 
For example, the condition number of the mass matrix arising from using a hierarchical basis grows algebraically with the polynomial degree $\bigO(p^{4})$ on a triangle, with the stiffness matrix growing at a similar rate  \cite{ainsworth2003conditioning,Maitre,Olsen}. 

Ill-conditioning poses a serious obstacle to iterative linear solvers, especially for problems where large numbers of mass and stiffness matrices (or a combination thereof) have to be inverted such as transient problems, and it can also cause numerical roundoff errors; one remedy to the problem of ill-conditioning is to utilize effective preconditioners. 
% such as transient problems or hierarchical modeling of thin plates

In \cite{babuvska1991efficient}, Babu\v ska et al. (BCMP) developed an additive Schwarz method (ASM) preconditioner for the stiffness matrix with condition number growing as $\bigO(1 + \log^2 p)$ in two dimension.
Using the same substructuring techniques \cite{chan1994domain,toselli2005domain}, the present authors developed a preconditioner in \cite{AMMPaper} for the mass matrix which has condition number independent of $p$.
The authors showed in \cite{implementation} that the mass matrix preconditioner can be implemented in an efficient, matrix-free manner using the Bernstein basis.
In section 2, we briefly discuss the mathematical foundation on which both these preconditioners are built: ASM preconditioners using substructuring techniques.
While the above two preconditioners essentially solve the issue of preconditioning the pure mass and pure stiffness matrices, there are still some lingering questions. 

One such puzzle which remained unanswered in \cite{AMMPaper,implementation} was a comprehensive explanation of the choice of basis for the coarse space. 
Substructuring preconditioners for $p$-FEM stiffness matrix typically use a coarse space composed of affine functions \cite{babuvska1991efficient,schoberl2007additive,pavarino1993additive}.  
On the other hand, the mass matrix preconditioner from \cite{AMMPaper} relies on the nontraditional function
\begin{align*}
	\varphi(x) &= \frac{(-1)^{\floor{p/2}+1}}{\floor{p/2}} \left(\frac{1-x}{2} \right)P_{\floor{p/2}-1}^{(1,1)}(x), \qquad x \in [-1, 1],
\end{align*}
where $P_{\floor{p/2}-1}^{(1,1)}$ is the Jacobi polynomial with weights $1, 1$ and order $\floor{p/2}-1$, to define the nodal space. 
In sections 3 and 4, we utilize the extension theorems developed in \cite{AMMPaper} to quantify the dependence of the condition number on the coarse space and why the exotic nodal space is \emph{crucial} for the preconditioner to be uniform in polynomial order.
Several other coarse spaces such as the hat functions and Bernstein polynomials are also considered in the context of the mass matrix preconditioner.

The existence now of a preconditioner for both the mass and stiffness matrix begs the question whether the operator arising from the following, more general problem, can be effectively preconditioned
\begin{align}\label{eqn:operator}
	 -\Delta u + \mu^2u &= f, \qquad  \mu \ge 0.
\end{align}
The corresponding matrix operator is of the form $\stiff + \mu^2\M$ where $\stiff, \M$ is the mass and stiffness matrix respectively. 
Such operators arises naturally in many applications; in section 5, we consider examples including implicit time-stepping and hierarchic modeling of thin plates. 


In the case where $\mu \gg 1$, the mass matrix becomes dominant and a preconditioner for the pure mass matrix is effective for \cref{eqn:operator} as shown in \cite{AMMPaper,implementation}. % shows that the pure mass matrix preconditioner is effective. % or, equally well, as the mesh is refined $h \to 0$
Conversely as $\mu \to 0$, the stiffness matrix dominates the condition number, in which case \cite{babuvska1991efficient} gives an effective preconditioner. 
Some problems, for example the operators arising in hierarchic modeling, give rise to coupled systems which contain both the pure stiffness matrix case and the mass matrix dominated case. 
Unfortunately, results in section 4 show that neither the pure mass matrix nor the pure stiffness matrix preconditioners is effective for such problems.
In order to tackle such problems, one requires a preconditioner which remains effective for the full range of $\mu \in [0, \infty)$. 
% This is even problematic for problems on a fixed mesh; for example the operators arising in hierarchic modeling ranges from the pure stiffness matrix to \cref{eqn:operator} with extremely large $\mu$. 


This issue is addressed in section 4, by combining the results in \cite{babuvska1991efficient} and \cite{AMMPaper}: an ASM preconditioner is developed which leads to a condition number that grows at most $C(1 + \log^2 p)$ where $C$ is a positive constant \emph{independent of} $\mu$ and the mesh size $h$.
It transpires all that is needed is to augment the preconditioner \cite{babuvska1991efficient} with a Jacobi smoothener on the coarse grid degrees of freedom.
 % secondary coarse space was all that was required to obtain robustness in $\mu$.
This means that the additional cost compared to \cite{babuvska1991efficient} is minimal (subject to a possible change of basis).
% In addition, we also quantify the dependence of the condition number of the new preconditioner on the smoothener, and show this numerically in section 4. 

% The remainder of the paper is organized as follows: 
The performance of the preconditioner is illustrated for some representative examples in Section 5. 
We conclude with section 6 and 7 which contain the technical lemmas.


\bibliography{source.bib} 
\bibliographystyle{siam}


\end{document}
