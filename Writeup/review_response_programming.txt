Reviewer #1 

- p12, line 194: this is not entirely accurate. "Boundary layer preconditioners for finite-element discretizations of singularly perturbed reaction-diffusion problems" proves epsilon independence of a preconditioner on Shishkin meshes (though not robustness w.r.t. p). 
	- The suggested paper by the reviewer would actually fall under the case of a different mesh, as it uses a Shishkin mesh and not one from Schwab/Melenk. We have added a phrase into the sentence to make this part clear. 

- p12, Figure 4: The definition of h in the presented meshes is unclear, as the presented mesh seems to only depend on p, epsilon.  Please clarify the definition of h or remove it from the discussions (see next comment). 
	- As the mesh does *not* have any dependence on h, we removed the h from the discussion for clarity. 


- p13, eq 10: this bound indicates that condition numbers are independent of h, p, and epsilon.  This is surprising, since mass matrix preconditioning is not robust when applied to the stiffness matrix.  I would expect the bound to hold for epsilon sufficiently small with respect to h/p.  If so, it would be helpful to note if epsilon must be related to h/p.  If this is not true, it would be helpful to clarify this. 
	- It can be seen from eq 10 that the condition number is dependent on p^2. While it is true that the mass matrix preconditioning applied to stiffness matrix is not robust for isotropic elements, the crux is that we are working with anisotropic elements with a specific aspect ratio. While epsilon is arbitrary, the result from Melenk is that the mesh will have to have a single layer of size epsilon*p. 

- p26-27: on the choices of the vertex and edge spaces, it would be helpful to provide intuition (especially for the edge bilinear form) or motivation (e.g. these specific choices enable lower bounds, etc). 
	- This section is a review of [5], and is justified in that paper. We simply want to implement it. 

- p31: the discussion of the interior preconditioner is highly technical. It would help to provide intuition or illustration behind the main steps if possible.
	- We added a small schematic and changed the wording to hopefully make the steps a bit more clear. 

- p31, line 25: "is given" -> "is given from Step 1". 
	- Done

- p33: step 2 is not labeled within Algorithms 4 or 5 
	- Added to the comments to reflect the steps, and also made the comments a bit more fine to reflect the algorithm 


- p45, line 17: is O(p^3) observed for larger p?  If not, is there an intuitive explanation for why O(p^2) is observed? 
	- O(p^3) is observed for larger p, we added a sentence pointing the reader to a plot from a previous paper. There is really no intuitive explanation, but simply the product of modern CPUs and their architecture. 

- p20, Table 1: please specify that O(1) subdivisions are assumed.  
	- Added a phrase in the caption in the table to say so. 

- A conclusion summarizing the main results would be helpful. 
	- Added

- Bernstein algorithms achieve a more significant reduction of O(p^6) to O(p^4) in cost in 3D. It would be helpful to specify for which steps this holds (e.g. post processing, residual computations, etc) and which steps do not (e.g. mat-vecs within the mass preconditioner, aspects of the preconditioner which have not been extended to 3D).
	- Discussed in the added conclusion section 

Review #2: 

1.) The conclusion drawn -- that *all* the major steps can be made O(p^3) -- is perhaps a bit too grand.  In particular, mass matrices and their singular perturbations (whether by time-stepping or small \epsilon) don't help tackle regular steady-state elliptic problems.  Confessing this doesn't take away from the real accomplishment in this paper.
	- Addressed in the newly added conclusion 

2.) Is the subdivision result for visualizing new, or does this follow from the standard techniques in, say, Lai & Schumaker?  If it is not new, this is a spot that could be shortened.
	- While the first part is not new, the quantities of interest is. Furthermore, our description of the gradient is more suited for FEM than the one in section 2.6, 7.27 of Lai & Schumaker. Furthermore, in order to explain the gradient portion, we would need all the previous techniques like de Casteljau algo and subdivision. 

3.) It seems that the survey of the Schwarz algorithm could be condensed, saying, "Here are the steps we showed previously that are needed".  Although it is not particular to the mass matrix, there is work of Pavarino (1990s up to 2000s) and Schoeberl (2008) on additive Schwarz for p-methods.  Perhaps citations would be in order?
	- We think that section 4.2 is about as short as possible; it is mathematically a bit hard to put down on paper due to the odd vertex space and the inner-product on the edges. As for the citations, the historical works were discussed in our previous paper; this section is less so a survey of methods, and more a review of the mass preconditioner we plan to implement. 


4.) It seems odd that the Schur complement algorithm is embedded in the numerical results section.  To my taste, it should go as a subsection in section 4 so that one section has all the algorithms and the next all the examples.
	- We agree, and have moved it as suggested. 

5.) Conditioning! Just because the resulting condition number of the overall system is good does not mean all the internal steps are stable.  For example, the (cited) work of Kirby on the element-level mass matrix shows that the roundoff error grows on the mass matrix inversion as the degree increases.  His algorithm is completely different and doesn't rely on orthogonal polynomials.  Farouki (JCAM 2000) shows that the Bernstein-Legendre conversion in 1d is less bad than other types of conversions, so perhaps the 2d equivalent used here for the interior degrees of freedom has a similar property?  At any rate, giving some empirical evidence as to the accuracy actually achieved in addition to the iteration counts would be helpful, as would citing relevant basis conversion papers.
	- We added a plot of the residuals; I think this shows that this gives enough evidence that the preconditioner is achieving the accuracy. 

6.) Do the authors care to comment on the applicability of this method to tetrahedra?  Is it, in principle, extensible, or is there something special about 2d?
	- Added a conclusion section where we discuss the extension to 3D

7.) Length.  It seems that the paper could benefit from some compression, perhaps by condensing some of the visualization discussion and abbreviating the Schwarz overview.
