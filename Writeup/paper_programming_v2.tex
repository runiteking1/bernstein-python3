\documentclass{article}
% \headers{Preconditioning the Mass Matrix}{M. Ainsworth, and S. Jiang}

\usepackage[utf8]{inputenc}

% \usepackage{amsmath}
\usepackage{listings}
\usepackage{commath} \let\abs\undefined
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{amsthm}
\usepackage{pgfplots}
\usepackage{enumerate}
\usepackage{mathtools}
\usepackage[nocompress]{cite}
\usepackage{cleveref}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}


\newcommand{\matr}[1]{\mathbf{#1}}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclareMathOperator{\spn}{span}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\sech}{sech}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\newcommand{\G}{\matr{\Gamma}}
\newcommand{\M}{\matr{M}}
\newcommand{\barM}{\matr{\bar M}}

\pgfplotsset{compat=1.12}
% \pgfplotsset{compat=1.13,
% 	table/search path = {../Eigenvalues_Timing},
% }

\title{Efficient Algorithms for Preconditioning the Mass Matrix}

\begin{document}
\maketitle

% REQUIRED
\begin{abstract}
We present stuff
\end{abstract}

\tableofcontents

\section{Introduction}


\section{Model Problems and Finite Element Formulations}
\subsection{Model Problems}
Let us consider the following two difficult problems:
\subsubsection*{Brusselator} 
		Let $u(x, y, t)$ and $v(x, y, t)$ be defined in a region $[0, 1]^2 \in \mathbb{R}^2$ with dynamics given by
	\begin{equation}
	\label{eqn:brussel} % TODO: Change constants to lower case
	  \begin{aligned}
		\pd{u}{t} &= 1 + u^2v - 4.4u + 0.002 \Delta u, & (x, y) \in \Omega, t > 0,\\
		\pd{v}{t} &= 3.4u - u^2v + 0.002 \Delta v, & (x, y) \in \Omega, t > 0, 
	\end{aligned}
	\end{equation}
		subject to a zero Neumann boundary conditions.
	The initial conditions are 
	\begin{align*}
		u(x, y, 0) &= 0.5 + y , & (x, y) \in \Omega \cup \partial \Omega, \\
		v(x, y, 0) &= 1 + 5x, & (x, y) \in \Omega \cup \partial \Omega.
	\end{align*}
	This is the so called `Brusselator' system which is an idealized model of autocatalytic chemical reactions \cite[p. 248]{hairer_2011}.

	Let $w, \mu$ be test functions, then the variational form is 
	\begin{align*}
		\pd{}{t}(u, w) &= (1, w) + (u^2v, w) - 4.4 (u, w) - 0.002(\nabla u, \nabla w) \\
		\pd{}{t}(v, \mu) &= 3.4(u, \mu) - (u^2v, \mu) - 0.002 (\nabla v, \nabla \mu).
	\end{align*}
	We will use an implicit Crank-Nicolson-esque scheme as follows: 
	\begin{align*}
		\frac{\matr{M} \vec u^{n+1} - \matr{M}\vec u^n}{\Delta t} &= (1, w) + (u_n^2v_n, w) - 4.4\matr{M}u^{n+1} - \frac{0.002}{2}(\matr{S}\vec u^{n+1} + \matr{S}\vec u^n) \\
		\frac{\matr{M}v^{n+1} - \matr{M}v^n}{\Delta t} &= 3.4\matr{M}u^{n+1} - (u_n^2v_n, z) - \frac{0.002}{2}(\matr{S}\vec v^{n+1} + \matr{S}\vec v^n)
	\end{align*}
	where $\matr{M}, \matr{S}$ is the mass and stiffness matrix respectively. 
	The inversions required to time step this scheme are of the form $\matr{M} + \varepsilon \matr{S}$, which is a small perturbation of the mass matrix.


\subsubsection*{Sine-Gordon}
		Let $u(x, y, t)$  be defined in a region $[-7, 7]^2 \in \mathbb{R}^2$ with dynamics given by
	\begin{equation}
	\label{eqn:sinegordon}
	  \begin{aligned}
		\pd[2]{u}{t} &= \Delta u - \sin(u), & (x, y) \in \Omega, t > 0.\\
	\end{aligned}
	\end{equation}
	% Boundary conditions and initial conditions will be prescribed later.
	Initial conditions are prescribed as 
	\begin{align*}
		u(x, 0) &=  4\arctan\left(\exp[x + 1 - 2\text{sech}(y + 7) - 2\text{sech}(y - 7)] \right) \\
		u_t(x, 0) &= 0.
	\end{align*}
	The boundary condition is a zero Neumann condition. 
	This is the so called Sine-Gordon equation which is studied due to the existence of solitons in its solution \cite{drazin1989solitons}; the initial conditions is of two perturbed dents of a static line soliton approaching each other with constant velocity \cite{Bratsos2007}.

	Let $v$ be a test function, then the variational form of the Sine-Gordon is 
	\begin{align*}
		\pd[2]{}{t}(u, v) &= -(\nabla u, \nabla v) - (\sin u, v).
	\end{align*}
	The time-stepping scheme we will use is a Nystr{\"o}m method \cite[p. 285]{hairer_2011}, which is a modified Runge-Kutta 4th order scheme for wave-type equations.
	In each sub-step, we will need to solve the mass matrix exactly; for example, the first substep is to solve $\matr{M}\vec u^{n+1}_1 = -\matr{S}\vec u^n - (\sin \vec u^n, \vec v)$.
	\\

	\subsection{$p$-version Finite Element Methods}
	Due to the Laplacian factor, the solutions to both problems are smooth, hence the $p$-version finite elements method will obtain exponential convergence \cite{9780198503903}.
	Unfortunately, the standard implementation of $p$-version FEM using hierarchical basis will face several challenges for the model problems described above: 
	\begin{enumerate}
		\item The nonlinearities such as the $(u^2v, w)$ term in the Brusselator or the $(\sin(u), v)$ term in Sine-Gordon requires an expensive quadrature rule at each time step. 
		There are roughly $\binom{p+2}{p}$ entries per element, hence with a quadrature rule cost of $\mathcal{O}(p^2)$ per entry, the naive evaluation will cost at least $\mathcal{O}(p^3)$.
		\item The construction of the mass and stiffness matrices will have a cost of $\mathcal{O}(p^6)$ with a naive quadrature method \cite{Ainsworth2011}.
		\item Post-processing of the solutions is not a trivial matter; for example, plotting the solution at the time-steps will require the evaluation of large numbers of Jacobi polynomials. 
		Similarly, analysis of the gradients will cause headaches. 
		\item Finally, the mass matrices constructed have condition numbers which grows algebraically (e.g. $\mathcal{O}(p^8)$) as $p$ increases~\cite{Olsen,Maitre,ainsworth2003conditioning}; this poses a significant problem as one has to invert a mass matrix (or a perturbation of it) for each time step as techniques from $h$-version finite elements like mass-lumping does not work here. 
	\end{enumerate}
	Using the Bernstein-Bezier basis instead, we can deal with the first three problems presented above in optimal complexity \cite{Ainsworth2011}, but would greatly exacerbate the poor condition number of the mass matrix into exponential growth $\mathcal{O}(2^{2p} p^{-1/2})$ \cite{Lyche2000}.

\subsection{Review of Bernstein Polynomials}
	Let $T$ be a non-degenerate triangle in $\mathbb{R}^2$ with vertices $v_1, v_2, v_3$.
	For a fixed integer $p \ge 3$, we define the domain points as 
	\begin{align*}
		\mathcal{D}^p(T) = \left\{ \frac{1}{p}\left(\alpha_1 v_1 + \alpha_2 v_2 + \alpha_3 v_3 \right) :  \vec{\alpha} \in \mathcal{I}^p \right\}
	\end{align*}
	where we have the index set $\mathcal{I}^p = \{ \vec{\alpha} \in \mathbb{Z}_+^{3}: \abs{\vec{\alpha}} = p \}$.
	% TODO: Need to denote vertices, edges and interior domain points. 
	We use the shorthand where $\alpha_i$ denotes the $i$th component of vector $\alpha$; for simplicity, we will omit the vector notation on the multi-index $\alpha$. 

	The barycentric coordinates $\lambda_i \in \mathbb{P}_1(T), i \in \{1,2,3\}$ of $T$ are polynomials such that $\lambda_i(v_j) = \delta_{ij}$ for $i, j \in \{1,2,3\}$.
	The Bernstein polynomials of degree $p$ associated with triangle $T$ is defined as 
	\begin{align*}
		B^p_{{\alpha}} (\vec x) = \frac{p!}{\alpha_{1!}\alpha_{2!}\alpha_{3!}} \lambda_1^{\alpha_1} \lambda_2^{\alpha_2} \lambda_3^{\alpha_3}
	\end{align*}
	for $\alpha \in \mathcal{I}^p$.
	If the degree is clear, we will omit the degree and simply write the polynomial as $B_\alpha$.
	There are many mathematical and computational properties of Bernstein polynomials which we will elucidate when needed, but a general reference can be found in \cite{farin2002}.

	In addition to the 2D version, we will also utilize the one dimensional Bernstein polynomials on an interval $[a,b]$. 
	These are defined simply as 
	\begin{align*}
		B^p_n(x) = \binom{p}{n}\lambda_1^n \lambda_2^{p - n}
	\end{align*}
	for $0\le n \le p$ where $\lambda_1, \lambda_2$ are the barycentric coordinates on $[a,b]$; we will use Greek letters for 2D Bernstein polynomials, and the alphabet for 1D Bernstein polynomials. 
	The main property of the 1D Bernstein polynomials that we will need is the so-called degree raising formula: 
	\begin{align}
		B^{p-1}_i(x) &= \frac{p-i}{p} B_i^p(x) + \frac{i+1}{p} B_{i+1}^p(x).
		\label{deg-raising}
	\end{align}
	This allows us to express a polynomial expanded using Bernstein polynomials of degree $p$ with one which is expanded using Bernstein polynomials of degree $p+1$:
	\begin{align*}
		p(x) = \sum_{i=0}^p c_i^p B^p_i(x) \rightarrow p(x) = \sum_{i=0}^{p+1} c^{p+1}_i B^{p+1}_i(x).
	\end{align*}
	As this operation is crucial, we will introduce a subroutine which performs this operation here: 
	\begin{algorithm}[H]
		\caption{Degree Raising Operator}
		\label{algo:raise}
		\begin{algorithmic}[1]
			\Require $\vec c$ corresponding to the coefficients of BB-polynomial of degree $p$
			\Function{DegreeRaise}{$\vec c$}
			\State $\vec o = \text{zeros}(p+2)$ \Comment{Coefficients for degree $p+1$}
			\For{$i = 0, \ldots, p$}
			\State $o[i] += (p + 1 - i) c[i] / (p+1)$
			\State $o[i + 1] += (i + 1) c[i] / (p+1)$
			\EndFor
			\State \textbf{return} $\vec o$
			\EndFunction
		\end{algorithmic}
	\end{algorithm}
	An equally useful operation is the degree-lowering operation, where (if possible) we replace a Bernstein polynomials of degree $p+1$ with one which is expanded using Bernstein polynomials of degree $p$.
	\begin{algorithm}[H]
		\caption{Degree Lowering Operator}
		\label{algo:lower}
		\begin{algorithmic}[1]
			\Require $\vec c$ corresponding to the coefficients of BB-polynomial of degree $p + 1$
			\Function{DegreeLower}{$\vec c$}
			\State $\vec o = \text{zeros}(p+1)$ \Comment{Coefficients for degree $p$}
			\For{$i = 0, \ldots, p$}
			\State $o[i] = ((p + 1 - i) c[i] + (i + 1) c[i + 1]) / (p+1)$
			\EndFor
			\State \textbf{return} $\vec o$
			\EndFunction
		\end{algorithmic}
	\end{algorithm}
	The degree lowering operation will also be helpful when we are working with inner-products; for example, for a function $g$, we can figure out $(g, B^3_1)$ given $(g, B^4_1)$ and $(g, B^4_2)$.


	In addition to the Bernstein polynomials, we will rely on the standard definitions of Jacobi polynomial. 
	The notation we will use is $P_n^{(a,b)}$ where $n$ is the order and $a, b > -1$ are the weights. 
	A key identity relating the two types of polynomial together is  
	\begin{align}
		P_n^{(a,b)}(x) &= \sum_{i = 0}^n \frac{\binom{n + a }{i} \binom{n+b}{n-i}}{(-1)^{n-i} \binom {n}{i}} B_i^n(x), \qquad -1 \le x \le 1.
		\label{bern-jac}
	\end{align}



\section{Analytical Derivation}
	\subsection{Review of the Mass Preconditioner}
	We will restrict our attention first to the reference triangle, and present a short review of the preconditioner presented in \cite{AMMPaper}.
	Let $\matr{\bar M}$ be the mass matrix on the reference triangle constructed using the basis and blocked as presented in section 1 and 2 of \cite{AMMPaper}. 
	The bar notation will signify the vector or matrix corresponds to the basis in \cite{AMMPaper}.
	The preconditioner of $\matr{\bar M}$ can be written in algorithm form as: 
	\begin{algorithm}[H]
		\caption{Preconditioner for Mass Matrix}
		\label{alg:algorithm-old-basis}
		\begin{algorithmic}[1]
		\Require $\matr{\bar M}$ global mass matrix, $\bar f$ residual vector 
		\Function{}{}
		\State $\bar x_I := \matr{\bar M}_{II}^{-1}\bar f_I$ \Comment{Interior solve}
		\State $\bar x_E := \matr{D}_{EE}^{-1}\left(\bar f_E - \matr{\bar M}_{EI}\bar x_I\right)$ \Comment{Edges solve}
		\State $\bar x_V := \matr{D}_{VV}^{-1}\left(\bar f_V - \matr{\bar M}_{VI}\bar x_I\right)$ \Comment{Vertices solve}
		\State $\bar x_I := \bar x_I -\matr{\bar M}_{II}^{-1} \matr{\bar M}_{IV}\bar x_V -  \matr{\bar M}_{II}^{-1} \matr{\bar M}_{IE}\bar x_E$ \Comment{Interior correction}
		\State \textbf{return} $\bar x : = \bar x_I + \bar x_E + \bar x_V$ 
		\EndFunction
		\end{algorithmic}
	\end{algorithm}
	We claim that 
	\begin{algorithm}[H]
		\caption{Preconditioner for Bernstein Mass Matrix}
		\label{alg:algorithm-bernstein-basis}
		\begin{algorithmic}[1]
		\Require $\matr{ M}$ global mass matrix, $\vec f$ residual vector 
		\Function{}{}
		\State $\vec x_I := \matr{ M}_{II}^{-1} \vec f_I$ \Comment{Interior solve}
		\State $\vec x_E := \G_{EE}^{-T}\matr{D}_{EE}^{-1}\G_{EE}^{-1}\left(\vec f_E - \matr{ M}_{EI}\vec x_I\right)$ \Comment{Edges solve}
		\State $\vec x_V := \matr{D}_{VV}^{-1}\left((\vec f_V - \matr{M}_{VI} \matr{M}_{II} \vec f_I) - \G_{VE}\G_{EE}^{-1} \left(\vec f_E - \matr{ M}_{EI} \matr{M}_{II}^{-1}\vec f_I\right)\right)$ \Comment{Vertices solve}
		\State $\vec x_E := \vec x_E - \G_{EE}^{-T}\G_{VE}^T \vec x_V$
		\State $\vec x_I := \vec x_I -\matr{ M}_{II}^{-1} \matr{ M}_{IV}\vec x_V -  \matr{ M}_{II}^{-1} \matr{ M}_{IE}\vec x_E$ \Comment{Interior correction}
		\State \textbf{return} $ x : =  x_I +  x_E +  x_V$ 
		\EndFunction
		\end{algorithmic}
	\end{algorithm}


	Let us consider a function $f$ such that the residual with respect to $\varphi$ basis leads to the following form, 
	\begin{align*}
		\bar f &= \begin{bmatrix}
			\bar f_V \\
			0 \\
			0 
		\end{bmatrix}.
	\end{align*}
	We can find the corresponding residual with respect to the Bernstein polynomials
	\begin{align*}
		\vec f &= \begin{bmatrix}
			\bar f_V \\ 0 \\ 0
		\end{bmatrix}.
	\end{align*}

	It is easy to see that 
	\begin{align*}
		\matr{\bar P}^{-1}\bar f = \begin{bmatrix}
			\matr{D}_{VV}^{-1} \bar f_V \\
			0 \\
			-\barM_{II}^{-1} \barM_{IV} \matr{D}_{VV}^{-1} \bar f_V
		\end{bmatrix}
	\end{align*}
	while 
	\begin{align*}
		\matr{P}^{-1}\vec f = \begin{bmatrix}
			\matr{D}_{VV}^{-1} \vec f_V \\
			- \G_{EE}^{-T} \G_{VE}^T \matr{D}_{VV}^{-1} \vec f_V \\
			- \M_{II}^{-1} \M_{IV}  \matr{D}_{VV}^{-1} \vec f_V + \M_{II}^{-1} \M_{IE}\G_{EE}^{-T} \G_{VE}^T \matr{D}_{VV}^{-1} \vec f_V 
		\end{bmatrix}
	\end{align*}
	Looking at the individual components, we first look at the part corresponding to the vertex functions.
	\begin{align*}
	 	(\matr{D}_{VV}^{-1} \bar f_V )^T \varphi_V &= (\matr{D}_{VV}^{-1} \bar f_V )^T (B_V - \G_{VE} \G_{EE}^{-1} B_E + (\G_{VE}\G_{EE}^{-1}\G_{EI}\G_{II}^{-1}-\G_{VI}\G_{II}^{-1}) B_I) \\
	 	&= \begin{bmatrix}
	 		\matr{D}_{VV}^{-1} \bar f_V \\
	 		-\G_{EE}^T \G_{VE}^T \matr{D}_{VV}^{-1} \bar f_V\\
	 		(\G_{II}^{-T} \G_{EI}^T \G_{EE}^{-T} \G_{VE}^T - \G_{II}^{-T} \G_{VI}^{T}) \matr{D}_{VV}^{-1} \bar f_V
	 	\end{bmatrix}
	 \end{align*} 
	We see that the vertex and edge coefficients corresponds to $\matr{P}^{-1}\vec f$. 
	All that remains is to confirm the interior coefficients match. 
	We first note that 
	\begin{align*}
		(-\barM_{II}^{-1} \barM_{IV} \matr{D}_{VV}^{-1} \bar f_V )^T \varphi_I &= (-\barM_{II}^{-1} \barM_{IV} \matr{D}_{VV}^{-1} \bar f_V )^T \G_{II}^{-1} B_I \\
		&= \begin{bmatrix}
			0 \\
			0 \\
			(-\matr{M}_{II}^{-1}\left(\matr{M}_{IV} - \matr{M}_{IE} \G_{EE}^{-T}\G_{VE}^{T}\right) + \G_{II}^{-T}\left(\G_{EI}^T\G_{EE}^{-T}\G_{VE}^{T} -\G_{VI}^T \right)) \matr{D}_{VV}^{-1} \bar f_V
		\end{bmatrix}
	\end{align*}
	Hence, the two functions are equal. 

	Next, let us consider a function such that the residual with respect to $\varphi$ basis has the following form 
	\begin{align*}
		\bar f = \begin{bmatrix}
			0 \\
			\bar f_E \\
			0 
		\end{bmatrix}
	\end{align*}
	which will lead to the residual under the Bernstein basis of 
	\begin{align*}
		\vec f = \begin{bmatrix}
			\G_{VE} \bar f_E \\
			\G_{EE} \bar f_E \\
			0 
		\end{bmatrix}
	\end{align*}
	It is easy to see that 
	\begin{align*}
		\matr{\bar P}^{-1}\bar f = \begin{bmatrix}
			0 \\
			\matr{D}_{EE}^{-1} \bar f_E \\
			-\barM_{II}^{-1} \barM_{IE} \matr{D}_{EE}^{-1} \bar f_E
		\end{bmatrix}
	\end{align*}
	Converting this to a Bernstein basis function, we first have that
	\begin{align*}
		(\matr{D}_{EE}^{-1} \bar f_E)^T \varphi_E &= (\matr{D}_{EE}^{-1} \bar f_E)^T (\G_{EE}^{-1} B_E - \G_{EE}^{-1} \G_{EI} \G_{II}^{-1} B_I)  \\
		&= \begin{bmatrix}
			0 \\
			\G_{EE}^{-T} \matr{D}_{EE}^{-1} \bar f_E \\
			-\G_{II}^{-T} \G_{EI}^T \G_{EE}^{-T}\matr{D}_{EE}^{-1} \bar f_E
		\end{bmatrix}
	\end{align*}
	and 
	\begin{align*}
		(-\barM_{II}^{-1} \barM_{IE} \matr{D}_{EE}^{-1} \bar f_E)^T \varphi_I &= \begin{bmatrix}
			0 \\
			0 \\
			-\G_{II}^{-T} (\G_{II}^{T} \matr{M}_{II}^{-1}\matr{M}_{IE}\G_{EE}^{-T} -  \G_{EI}^T \G_{EE}^{-T}) \matr{D}_{EE}^{-1} \bar f_E
		\end{bmatrix}
	\end{align*}
	We can also calculate 
	\begin{align*}
		\matr{P}^{-1} \vec f &= \begin{bmatrix}
			0 \\
			\G_{EE}^{-T} \matr{D}_{EE}^{-1} \bar f_E \\
			-\M_{II}^{-1} \M_{IE} \G_{EE}^{-T} \matr{D}_{EE}^{-1} \bar f_E
		\end{bmatrix}
	\end{align*}
	hence we again have the same functions. 

	Finally, there is the case of 
	\begin{align*}
		\bar f = \begin{bmatrix}
			0 \\
			0 \\
			\bar f_I
		\end{bmatrix}
	\end{align*}
	which corresponds to the Bernstein basis residual of 
	\begin{align*}
		\vec f &= \begin{bmatrix}
			\G_{VI} \bar f_I \\
			\G_{EI} \bar f_I \\
			\G_{II} \bar f_I 
		\end{bmatrix}
	\end{align*}
	We first note that 
	\begin{align*}
		\matr{\bar P}^{-1} \bar f &= \begin{bmatrix}
			-\matr{D}_{VV}^{-1} \barM_{VI} \barM_{II}^{-1} \bar f_I \\
			-\matr{D}_{EE}^{-1} \barM_{EI} \barM_{II}^{-1} \bar f_I \\
			\barM_{II}^{-1}(\bar f_I + \barM_{IV}\matr{D}_{VV}^{-1} \barM_{VI} \barM_{II}^{-1} \bar f_I + \barM_{IE} \matr{D}_{EE}^{-1} \barM_{EI} \barM_{II}^{-1} \bar f_I)
		\end{bmatrix}
	\end{align*}
	Let's slowly convert this to a Bernstein format. 
	First, doing the vertex portion, 
	\begin{align*}
		(-\matr{D}_{VV}^{-1} \barM_{VI} \barM_{II}^{-1} \bar f_I)^T \varphi_V &= (-\matr{D}_{VV}^{-1} \barM_{VI} \barM_{II}^{-1} \bar f_I)^T(B_V - \G_{VE} \G_{EE}^{-1} B_E + \matr{K} B_I) 
	\end{align*}


	We first note that 
	\begin{align*}
		\matr{M}_{II}^{-1} &= \G_{II}^{-T} \matr{\bar M}_{II}^{-1} \G_{II}^{-1} \\
		\matr{M}_{IE} &= \G_{II} \barM_{IE} \G_{EE}^T + \G_{II} \barM_{II} \G_{EI}^T \\
		\matr{M}_{IV} &= \G_{II} \barM_{IV} + \G_{II} \barM_{IE} \G_{VE}^T + \G_{II} \barM_{II} \G_{VI}^T
	\end{align*}



	Now let us consider $f$ such that only the residuals with $\varphi_E$ survives. 
	\begin{align*}
		\bar f &= \begin{bmatrix}
			0 \\
			\bar f_E \\
			0
		\end{bmatrix}.
	\end{align*}
	Noting that $\vec f_E = \G_{EE} \bar f_{E} + \G_{EI} \bar f_I$ and that $\vec f_V = \bar f_{V} + \G_{VE} \bar f_E + \G_{VI} \bar f_I$.
	We see that 
	\begin{align*}
		\vec f &= \begin{bmatrix}
			\G_{VE}\bar f_E \\
			\G_{EE} \bar f_E \\
			0 
		\end{bmatrix}
	\end{align*}
	We see that the function which 
	\begin{align*}
		\matr{\bar P}^{-1} \bar f &= \begin{bmatrix}
			0 \\
			\matr{D}_{EE}^{-1} \bar f_E \\
			- \barM_{II}^{-1} \barM_{IE} \matr{D}_{EE}^{-1} \bar f_E
		\end{bmatrix} 
	\end{align*}
	while 
	\begin{align*}
		\matr{P}^{-1} \vec f &= \matr{P}^{-1} \begin{bmatrix}
			0 \\
			\G_{EE} \bar f_E \\
			0 
		\end{bmatrix} + \matr{P}^{-1} \begin{bmatrix}
			\G_{VE} \bar f_E \\
			0 \\
			0 
		\end{bmatrix} \\
		&= \begin{bmatrix}
			-\matr{D}_{VV}^{-1} \G_{VE} \bar f_E\\
			\G_{EE}^{-T}\matr{D}_{EE}^{-1}\bar f \\
			-\M_{II}^{-1} \M_{IE} \G_{EE}^{-T}\matr{D}_{EE}^{-1}\bar f
		\end{bmatrix}  
		+ \begin{bmatrix}
			\matr{D}_{VV}^{-1} \left( \G_{VE} \bar f_E \right)  \\
			- \G_{EE}^{-T}\G_{VE}^T\matr{D}_{VV}^{-1} \left( \G_{VE} \bar f_E - \G_{VE} \bar f_E \right) \\
			0 
		\end{bmatrix}
	\end{align*}


	\vspace{10pt}
	\hrule
	\vspace{10pt}


	There exists matrices $\G$ such that 
	\begin{align*}
	 	\begin{bmatrix}
	 		\vec B_{V} \\
	 		\vec B_{E} \\
	 		\vec B_{I}
	 	\end{bmatrix} 
	 	= \begin{bmatrix}
	 		\matr{I}_3 & \G_{VE} & \G_{VI} \\
	 		0 & \G_{EE} & \G_{EI} \\
	 		0 & 0 & \G_{II}
	 	\end{bmatrix}
	 	\begin{bmatrix}
	 		\vec\varphi_V \\
	 		\vec\varphi_E \\
	 		\vec\varphi_I
	 	\end{bmatrix}.
	 \end{align*}
	 We can also find the inverse transformation 
	 \begin{align*}
	 		 	\begin{bmatrix}
	 		\vec\varphi_V \\
	 		\vec\varphi_E \\
	 		\vec\varphi_I
	 	\end{bmatrix} = \begin{bmatrix}
	 		\matr{I}_3 & -\G_{VE}\G_{EE}^{-1} & \G_{VE}\G_{EE}^{-1}\G_{EI}\G_{II}^{-1}-\G_{VI}\G_{II}^{-1} \\
	 		0 & \G_{EE}^{-1} & -\G_{EE}^{-1}\G_{EI}\G_{II}^{-1} \\
	 		0 & 0 & \G_{II}^{-1}
	 	\end{bmatrix}
	 	\begin{bmatrix}
	 		\vec B_{V} \\
	 		\vec B_{E} \\
	 		\vec B_{I}
	 	\end{bmatrix}.
	 \end{align*}
	 With this in mind, we can also find the relationship between the mass matrices constructed using the Bernstein basis and the ones constructed using the basis in \cite{AMMPaper}.
	 For example, 
	 \begin{align*}
	 	\matr{M}_{II} &= (\vec B_I, \vec B^T_I) = \G_{II} (\vec \varphi_I, \vec \varphi^T_I) \G_{II}^T = \G_{II} \matr{\bar M}_{II} \G_{II}^T.
	 \end{align*}

	 We will step through the \cref{alg:algorithm-old-basis} line by line to 

	 \subsubsection*{Interior Solve}
	 (This is easy and short to explain, and it really depends on how Manuel wants to explain it)

	 \subsubsection*{Edge Solve}
	 We have that 
	 \begin{align*}
	 	\vec f_E &= \G_{EE} \bar f_{E} + \G_{EI} \bar f_I \\
	 	\matr{M}_{EI} &=  \G_{EE} \matr{\bar M}_{EI} \G_{II}^T + \G_{EI} \matr{\bar M}_{II} \G_{II}^T \\
	 	\matr{M}_{II}^{-1} &= \G_{II}^{-T} \matr{\bar M}_{II}^{-1} \G_{II}^{-1} \\
	 	\vec f_I &= \G_{II} \bar f_I,
	 \end{align*}
	 hence, we can show that
	 \begin{align*}
	 	\matr{D}_{EE}^{-1}\G_{EE}^{-1}\left(\vec f_E - \matr{ M}_{EI} \matr{M}_{II}^{-1}\vec f_I\right) =\matr{D}_{EE}^{-1}\left(\bar f_E - \matr{\bar M}_{EI}\matr{\bar M}_{II}^{-1}\bar f_I\right)
	 \end{align*}
	 which allows us to compute $\bar x_E$.
	 We note that $\bar x_E$ corresponds to a function under the original basis, thus
	 \begin{align*}
	 	\bar x_E^T \vec \varphi_E &= \bar x_E^T \left(\G_{EE}^{-1} \vec B_E - \G_{EE}^{-1}\G_{EI}\G_{II}^{-1}\vec B_I \right) \\
	 	&= \vec x_E^T \vec B_E + \vec x_I^T \vec B_I
	 \end{align*}
	 From here, we can convert the $\bar x_E$ to an edge Bernstein basis by $\vec x_E = \G_{EE}^{-T} \bar x_E$.

	 \subsubsection*{Vertex Solve}
	 Using the identities from above, and noting that  
	 \begin{align*}
	 	\vec f_V &= \bar f_{V} + \G_{VE} \bar f_E + \G_{VI} \bar f_I\\
	 	\matr{M}_{VI} &= \matr{\bar M}_{VI}\G_{II}^T + \G_{VE} \matr{\bar M}_{EI} \G_{II}^T + \G_{VI} \matr{\bar M}_{II} \G_{II}^T,
	 \end{align*}
	 we can verify that 
	 \begin{align*}
	 	\matr{D}_{VV}^{-1}\left((\vec f_V - \matr{M}_{VI} \matr{M}_{II} \vec f_I) - \G_{VE}\G_{EE}^{-1} \left(\vec f_E - \matr{ M}_{EI} \matr{M}_{II}^{-1}\vec f_I\right)\right) = \matr{D}_{VV}^{-1}\left(\bar f_V - \matr{\bar M}_{VI}\matr{\bar M}_{II}^{-1}\bar f_I\right)
	 \end{align*}
	 which means that we can compute $\bar x_V$. 
	 Using a similar technique as above, we have that 
	 \begin{align*}
	 	\bar x_V^T \vec \varphi_V &= \bar x_V^T \left(\vec B_V - \G_{VE}\G_{EE}^{-1}\vec B_E - \left(\G_{VI}\G_{II}^{-1}\vec B_I - \G_{VE}\G_{EE}^{-1}\G_{EI}\G_{II}^{-1}\vec B_I \right)\right).
	 \end{align*}
	 This is where the edge correction comes from, so we are good. 

	 \subsection*{Interior Corrections}
	 \begin{align*}
	 		\matr{\bar M}_{II}^{-1} &= \G_{II}^{T} \matr{M}_{II}^{-1} \G_{II} \\
	 		\matr{\bar M}_{IE} &= \G^{-1}_{II} \left(\matr{M}_{IE}\G_{EE}^{-T} - \matr{M}_{II} \G_{II}^{-T} \G_{EI}^T \G_{EE}^{-T} \right) \\
	 		\matr{\bar M}_{IV} &= \G^{-1}_{II} \left(\matr{M}_{IV} - \matr{M}_{IE} \G_{EE}^{-T}\G_{VE}^{T} + \matr{M}_{II} \left(\G_{II}^{-T}\G_{EI}^T\G_{EE}^{-T}\G_{VE}^{T} - \G_{II}^{-T}\G_{VI}^T \right)\right) \\
	 		\matr{\bar M}_{II}^{-1}\matr{\bar M}_{IE} &= \G_{II}^{T} \matr{M}_{II}^{-1}\matr{M}_{IE}\G_{EE}^{-T} -  \G_{EI}^T \G_{EE}^{-T} \\
	 		\matr{\bar M}_{II}^{-1}\matr{\bar M}_{IV} &=  \G_{II}^{T} \matr{M}_{II}^{-1}\left(\matr{M}_{IV} - \matr{M}_{IE} \G_{EE}^{-T}\G_{VE}^{T}\right) + \left(\G_{EI}^T\G_{EE}^{-T}\G_{VE}^{T} -\G_{VI}^T \right)
	 \end{align*}
	 Hence, now we can calculate $\bar x_V, \bar x_E$ and $\bar x_I$, but all that remains is to express these functions as Bernstein polynomials. 

	 \begin{align*}
	 	\matr{\bar M}_{II}^{-1}\matr{\bar M}_{IE}\bar x_E &= \left(\G_{II}^{T} \matr{M}_{II}^{-1}\matr{M}_{IE}\G_{EE}^{-T} -  \G_{EI}^T \G_{EE}^{-T} \right)\left( \G_{EE}^T \vec x_E  + \G_{VE}^T \vec x_V\right)\\
	 	&=  \left(\G_{II}^{T} \matr{M}_{II}^{-1}\matr{M}_{IE} -  \G_{EI}^T  \right) \vec x_E + \left(\G_{II}^{T} \matr{M}_{II}^{-1}\matr{M}_{IE}\G_{EE}^{-T} \G_{VE}^T - \G_{EI}^T \G_{EE}^{-T} \G_{VE}^T \right) \vec x_V
	 \end{align*}

	 \begin{align*}
	 	\matr{\bar M}_{II}^{-1}\matr{\bar M}_{IV}\bar x_V &=  \G_{II}^{T} \matr{M}_{II}^{-1}\left(\matr{M}_{IV} - \matr{M}_{IE} \G_{EE}^{-T}\G_{VE}^{T}\right) \vec x_V + \left(\G_{EI}^T\G_{EE}^{-T}\G_{VE}^{T} -\G_{VI}^T \right)\vec x_V
	 \end{align*}

	 Taking the sum, and canceling the terms, we have 
	 \begin{align*}
	 	\matr{\bar M}_{II}^{-1}\matr{\bar M}_{IE}\bar x_E  + \matr{\bar M}_{II}^{-1}\matr{\bar M}_{IV}\bar x_V&= (\G_{II}^{T} \matr{M}_{II}^{-1}\matr{M}_{IE} \vec x_E + \G_{II}^T \M_{II}^{-1} \M_{IV}\vec x_V ) - \G_{EI}^T \vec x_E - \G_{VI}^T \vec x_V
	 \end{align*}


	 % Our algorithm gives
	 \begin{align*}
	 	\vec x_V &= \bar x_V \\
	 	\vec x_E &= \G_{EE}^{-T} \bar x_E - \G_{EE}^{-T}\G_{VE}^T\bar x_V.
	 \end{align*}
	 % We have 
	 % \begin{align*}
	 % 	\M_{II}^{-1}\M_{IE}\vec x_E &= \left(\G_{II}^{-T} \barM_{II}^{-1} \barM_{IE} \G_{EE}^T + \G_{II}^{-T}\G_{EI}^T\right)(\G_{EE}^{-T} \bar x_E - \G_{EE}^{-T}\G_{VE}^T\bar x_V) \\
	 % 	\M_{II}^{-1}\M_{IV}\vec x_V &= \G_{II}^{-T} \barM_{II}^{-1} (\barM_{IV} + \barM_{IE}\G_{VE}^T + \barM_{II}\G_{VI}^T)\bar x_V
	 % \end{align*}

	 \begin{align*}
	 	\matr{Q}^{-1} &= \begin{bmatrix}
	 		\matr{I} & 0 & -(\M_{VI}\G_{II}^{-T} - \G_{VE}\G_{EE}^{-1}\M_{EI}\G_{II}^{-T} + \matr{K} \matr{M}_{II}\G_{II}^{-T} ) \G_{II}^T\M_{II}^{-1} \G_{II}\\
	 		0 & \matr{I} & -\G_{EE}^{-1}\M_{EI} \M_{II} \G_{II} + \G_{EE}^{-1}\G_{EI} \\
	 		0 & 0 & \matr{I}
	 	\end{bmatrix} \\
	 	&= \begin{bmatrix}
	 		\matr{I} & 0 & -(\M_{VI} - \G_{VE}\G_{EE}^{-1}\M_{EI}) \M_{II}^{-1}\G_{II} - \matr{K} \G_{II} \\
	 		0 & \matr{I} & -\G_{EE}^{-1}\M_{EI} \M_{II} \G_{II} + \G_{EE}^{-1}\G_{EI} \\
	 		0 & 0 & \matr{I}
	 	\end{bmatrix}
	 \end{align*}
	\begin{align*}
	 	\begin{bmatrix}
	 		\vec B_{V} \\
	 		\vec B_{E} \\
	 		\vec B_{I}
	 	\end{bmatrix} 
	 	= \begin{bmatrix}
	 		\matr{I}_3 & \G_{VE} & \G_{VI} \\
	 		0 & \G_{EE} & \G_{EI} \\
	 		0 & 0 & \G_{II}
	 	\end{bmatrix}
	 	\begin{bmatrix}
	 		\vec\varphi_V \\
	 		\vec\varphi_E \\
	 		\vec\varphi_I
	 	\end{bmatrix}.\\
	 		 		 	\begin{bmatrix}
	 		\vec\varphi_V \\
	 		\vec\varphi_E \\
	 		\vec\varphi_I
	 	\end{bmatrix} = \begin{bmatrix}
	 		\matr{I}_3 & -\G_{VE}\G_{EE}^{-1} & \G_{VE}\G_{EE}^{-1}\G_{EI}\G_{II}^{-1}-\G_{VI}\G_{II}^{-1} \\
	 		0 & \G_{EE}^{-1} & -\G_{EE}^{-1}\G_{EI}\G_{II}^{-1} \\
	 		0 & 0 & \G_{II}^{-1}
	 	\end{bmatrix}
	 	\begin{bmatrix}
	 		\vec B_{V} \\
	 		\vec B_{E} \\
	 		\vec B_{I}
	 	\end{bmatrix}.
	 \end{align*}

	 \begin{align*}
	 	\matr{Q} \G^{-1} &= \begin{bmatrix}
	 		\matr{I} & 0 & -(\M_{VI} - \G_{VE}\G_{EE}^{-1}\M_{EI}) \M_{II}^{-1}\G_{II} - \matr{K} \G_{II} \\
	 		0 & \matr{I} & -\G_{EE}^{-1}\M_{EI} \M_{II} \G_{II} + \G_{EE}^{-1}\G_{EI} \\
	 		0 & 0 & \matr{I}
	 	\end{bmatrix} \begin{bmatrix}
	 		\matr{I}_3 & -\G_{VE}\G_{EE}^{-1} & \G_{VE}\G_{EE}^{-1}\G_{EI}\G_{II}^{-1}-\G_{VI}\G_{II}^{-1} \\
	 		0 & \G_{EE}^{-1} & -\G_{EE}^{-1}\G_{EI}\G_{II}^{-1} \\
	 		0 & 0 & \G_{II}^{-1}
	 	\end{bmatrix} \\
	 	&= \begin{bmatrix}
	 		\matr{I} & -\G_{VE}\G_{EE}^{-1} & -(\M_{VI} - \G_{VE}\G_{EE}^{-1}\M_{EI}) \M_{II}^{-1}\\
	 		0 & \G_{EE}^{-1} & -\G_{EE}^{-1}\M_{EI} \M_{II}  \\
	 		0 & 0 & \G_{II}^{-1}
	 	\end{bmatrix}
	 \end{align*}
	
\section{Interior solve}



\bibliography{source.bib} 
\bibliographystyle{plain}

\end{document}
