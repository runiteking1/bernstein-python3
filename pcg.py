import numpy as np


# @profile
def pcg(A, b, x0=None, eps=1e-8, P=None, maxit=4000):
    """
    Preconditioned Conjugate Gradient, no checks! Be careful.

    Returns vector, iteration count, and residuals
    """
    d = A.shape[0]

    # set initial guess if it's there
    if x0 is None:
        x = np.zeros((d,))
    else:
        x = x0

    # set pre-conditioner if it's not there
    g = A.dot(x) - b
    if P is None:
        h = g
    else:
        if 'solve' in dir(P):
            h = P.solve(g)
        else:
            h = P.dot(g)

    d = -h
    resid = np.linalg.norm(g)
    # print(resid)
    delta0 = np.dot(g.T, h)
    if resid < eps:
        return x, 0, [delta0]

    iteration = 1
    residuals = [resid]
    while iteration < maxit:
        # print it
        h = A.dot(d)
        tau = delta0 / np.dot(d.T, h)
        x = x + tau * d
        g = g + tau * h

        resid = np.linalg.norm(g)
        # print(tau)
        # print(resid)
        residuals.append(resid)
        if resid < eps:
            return x, iteration, residuals

        if P is None:
            h = g
        else:
            if 'solve' in dir(P):
                # print 'Solving loop'
                h = P.solve(g)
            else:
                h = P.dot(g)

        delta1 = np.dot(g.T, h)

        beta = delta1 / delta0
        delta0 = delta1
        d = -h + beta * d

        iteration += 1

    return x, iteration, residuals


if __name__ == "__main__":
    # Test with a small array
    M = np.array([[2, -1, 0], [-1, 2, -1], [0, -1, 2]])
    f = np.ones((3,))  # Note must be vector

    [sol, it, residual] = pcg(M, f)  # Unpack
    print(it, residual)
    print(np.dot(M, sol))  # Should be [1 1 1]
