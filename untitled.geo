//+
Point(1) = {0, 0, 0, 1.0};
//+
Point(2) = {1, 0, 0, 1.0};
//+
Point(3) = {0, 1, 0, 1.0};
//+
Point(4) = {0, 0, 1, 1.0};
//+
Line(1) = {1, 4};
//+
Line(2) = {4, 2};
//+
Line(3) = {2, 3};
//+
Line(4) = {3, 1};
//+
Line(5) = {1, 2};
//+
Line(6) = {4, 3};
//+
Point(5) = {0, .5, 0, 1.0};
//+
Point(6) = {0, .5, .5, 1.0};
//+
Point(7) = {.5, .5, 0, 1.0};
//+
Line(7) = {5, 6};
//+
Line(8) = {6, 7};
//+
Line(9) = {7, 5};
//+
Point(8) = {.5, 0, 0, 1.0};
//+
Point(9) = {0, 0, .5, 1.0};
//+
Point(10) = {.5, 0, .5, 1.0};
//+
Line(10) = {9, 6};
//+
Line(11) = {6, 10};
//+
Line(12) = {10, 9};
//+
Line(13) = {9, 8};
//+
Line(14) = {8, 10};
//+
Line(15) = {5, 9};
//+
Line(16) = {10, 7};
//+
Line(17) = {7, 8};
//+
Line(18) = {5, 8};
//+
Line(19) = {9, 7};
//+
Line(20) = {6, 8};
//+
Line(21) = {5, 10};
