import numpy as np
from scipy.sparse.linalg import LinearOperator


def pcg_2(A: LinearOperator, b: np.ndarray, eps: float = 1e-8, M: LinearOperator = None, maxit: int = 2000) \
        -> np.ndarray:
    """
    Preconditioned Conjugate Gradient, optimized for the preconditioner (i.e. no guess, and no residual return)
    """
    d = A.shape[0]

    # set initial guess if it's there
    x = np.zeros((d,))

    g = -b
    h = M.solve(g)
    d = -h

    resid = np.linalg.norm(g)
    delta0 = np.dot(g.T, h)

    if resid < eps:
        return x, 1

    it = 1
    while it < maxit:
        h = A.dot(d)
        tau = delta0 / np.dot(d.T, h)
        x = x + tau * d
        g = g + tau * h

        h = M.solve(g)

        delta1 = np.dot(g.T, h)
        resid = np.linalg.norm(g)
        if resid < eps:
            return x, it + 1
        beta = delta1 / delta0
        delta0 = delta1
        d = -h + beta * d

        it += 1

    return x, it + 1
