import numpy as np
import scipy
from scipy.sparse.linalg import LinearOperator

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from mass_preconditioner import MassPreconditioner
from mass_solver import MassSolver

if __name__ == '__main__':
    # 1 Triangles
    coords_tri = np.array([[-1, -1], [-1, 1], [1, -1]])
    elnode_tri = np.array([[0, 1, 2]])
    bc_tri = np.array([])
    delta_t = .01

    np.set_printoptions(linewidth=200, precision=5)
    cond = list()
    print('p\tbern\tpre')
    for order in range(3, 21):
        print(order, end = '\t')
        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, order, subdiv=0)
        fem = BernsteinMatrix(mesh)
        stiffness, mass, _ = fem.assemble_matrix()

        precond = MassPreconditioner(mass, fem)

        def mult_precond(v: np.ndarray) -> np.ndarray:
            return precond.solve(mass.dot(v) + delta_t * stiffness.dot(v))

        def mult_true(v: np.ndarray) -> np.ndarray:
            return mass.dot(v) + delta_t * stiffness.dot(v)

        PinvS = LinearOperator(dtype=mass.dtype, shape=mass.shape, matvec=mult_true)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
                                     maxiter=mass.shape[0] * 50, ncv=mass.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=mass.shape[0] * 100,
                                     tol=1E-9, ncv=mass.shape[0] * 30)
        small = min(np.real(w))
        print(large / small, end = '\t')
        cond.append(large / small)

        PinvS = LinearOperator(dtype=mass.dtype, shape=mass.shape, matvec=mult_precond)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
                                     maxiter=mass.shape[0] * 50, ncv=mass.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=mass.shape[0] * 100,
                                     tol=1E-9, ncv=mass.shape[0] * 30)
        small = min(np.real(w))
        print(large / small)
        cond.append(large / small)

    # fil = open('mass_perturbed_pre.data', 'w')
    # print("p\tCond", file=fil)
    # counter = 3
    # for item in cond:
    #     print("%d\t%s" % (counter, item), file=fil)
    #     counter += 1
    # fil.close()
