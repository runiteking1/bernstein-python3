import numpy as np
from sympy import *
import math as m


def index_set(n, d):
    """
    Generates I^n_{d-1}
    :param n: sum of tuple
    :param d: dimension of the set
    :return: a generator of tuples
    """
    if d == 1:
        yield (n,)
    elif d == 0:
        yield ()
    else:
        for i in range(n + 1):  # Loop through all possible in the sum
            for j in index_set(n - i, d - 1):  # Recursively call
                yield (i,) + j


def dof_to_multi(i):
    """
    Converts a degree of freedom to a corresponding multinomial. Convention is vertices are enumerations of
    (p, 0, 0), and subsequent multinomials are more or less the interpolation.

    Note: no overflow/underflow check

    :param i: degree of freedom (int)
    :return: 3-tuple multinomial
    """
    if i == 0:
        return p, 0, 0
    elif i == 1:
        return 0, p, 0
    elif i == 2:
        return 0, 0, p
    elif i <= p + 1:
        return 0, p - (i - 2), (i - 2)
    elif i <= 2 * p:
        return i - (p + 1), 0, 2 * p + 1 - i
    elif i <= 3 * p - 1:
        return 3 * p - i, i - 2 * p, 0
    else:
        return tuple(np.array((multi[i - (3 * p)])) + np.array([1, 1, 1]))


def Bernstein2d(x, y, i, j, k):
    n = i + j + k
    # Barycentric Coorinates
    l1 = (y + 1) / 2
    l2 = (x + 1) / 2
    l3 = (-y - x) / 2

    B = m.factorial(n) / (m.factorial(i) * m.factorial(j) * m.factorial(k)) * (l1 ** i) * (l2 ** j) * (l3 ** k)

    return B


def lobatto(x, p):
    return (-1) ** p * (1 - x) * diff(legendre(p - 1, z), z).subs(z, x) / ((p) * (p - 1))


if __name__ == '__main__':
    init_printing(use_unicode=False, wrap_line=False, no_global=True)

    x, y, z = symbols('x y z')
    f = open('mathematica_output.m', 'w')

    for p in range(4, 5, 2):
        multi = list(index_set(p - 3, 3))
        print("Creating %d" % (p,))
        lookup = dict()
        for i in range(int((p + 1) * (p + 2) / 2)):
            lookup[i] = dof_to_multi(i)

        M = zeros(int((p + 1) * (p + 2) / 2))

        # Make matrix
        for i in range(3, int((p + 1) * (p + 2) / 2)):
            for j in range(3, int((p + 1) * (p + 2) / 2)):
                M[i, j] = binomial(lookup[i][0] + lookup[j][0], lookup[i][0]) * binomial(lookup[i][1] + lookup[j][1],
                                                                                         lookup[i][1]) * binomial(
                    lookup[i][2] + lookup[j][2], lookup[i][2])
        # print M
        print("Bernstein Matrix Made; Editing for different vertex function")
        M = M * 2 / (binomial(2 * p, p) * binomial(2 * p + 2, 2))
        ns = int((p + 2) * (p + 1) / 2) - 3 * (p - 1) - 3

        # Create the 3 vertex function
        vertices = list()
        vertices.append(lobatto(x, int(p/2) + 1) * lobatto(y, int(p/2) + 1) - lobatto(-x, int(p/2) + 1) * lobatto(-y, int(p/2) + 1))
        vertices.append(lobatto(-x - y - 1, int(p/2) + 1) * lobatto(x, int(p/2) + 1) - lobatto(x+y+1, int(p/2) + 1) * lobatto(-x, int(p/2) + 1))
        vertices.append(lobatto(y, int(p / 2) + 1) * lobatto(-x-y-1, int(p / 2) + 1) - lobatto(-y, int(p / 2) + 1) * lobatto(x+y+1, int(p / 2) + 1))
        # vertices.append(lobatto(x, int(p / 2) + 1) * lobatto(y, int(p / 2) + 1))
        # vertices.append(lobatto(-x, int(p / 2) + 1) * lobatto(y, int(p / 2) + 1))
        # vertices.append(lobatto(x, int(p / 2) + 1) * lobatto(-y, int(p / 2) + 1))

        # Modify the vertex functions and figure out the inner-product
        for i in range(3):
            for j in range(i, 3):
                M[i, j] = integrate(integrate(vertices[i] * vertices[j], (y, -1, -x)), (x, -1, 1))
                M[j, i] = M[i, j]

        for i in range(3):
            for j in range(3, int((p + 1) * (p + 2) / 2)):
                # print i, j
                M[i, j] = integrate(integrate(vertices[i] * Bernstein2d(x, y, *dof_to_multi(j)), (y, -1, -x)),
                                    (x, -1, 1))
                M[j, i] = M[i, j]

        t = repr(M)
        print("bugged!!!")
        print("p = {}; ns = (p + 2) * (p + 1) / 2 - 3 * (p - 1) - 3;".format(p), file=f)
        print("M = " + t[7:-1].replace('[', '{').replace(']', '}') + ";", file=f)
        print("a = M[[1;;3 * (p - 1) + 3, 1;;3 * (p - 1) + 3]];", file=f)
        print("b = M[[1;;3 * (p-1) + 3, {} - ns + 1;;{}]];".format(M.shape[0], M.shape[0]), file=f)
        print("c = M[[{0} - ns + 1;;{0}, {0} - ns + 1;;{0}]];".format(M.shape[0]), file=f)
        print('Print["loaded"]', file=f)
        print("s = a - b.Inverse[c].Transpose[b]", file=f)
        print('Print["schur calculated"]', file=f)
        print("block = ConstantArray[0, {{ {0}, {0} }}]".format(M.shape[0] - ns), file=f)
        # print("block[[1;;3, 1;;3]] = IdentityMatrix[3] * 4/p^4", file=f)
        print("block[[1;;3, 1;;3]] = s[[1;;3, 1;;3]];", file=f)
        print("block[[4;;p+2 , 4;;p+2]] =s[[4;;p+2 , 4;;p+2]] ", file=f)
        print("block[[p+3;;2p+1 , p+3;;2p+1]] = s[[p+3;;2p+1 , p+3;;2p+1]] ", file=f)
        print("block[[2p+2;;3p , 2p+2;;3p]] = s[[2p+2;;3p , 2p+2;;3p]] ", file=f)

        print("precond = Inverse[block].s;", file=f)
        print("largest = Max[Eigenvalues[precond, 1]]; smallest = Min[Eigenvalues[precond, -1]]", file=f)
        print("Print[N[largest]/N[smallest]];", file = f)

    f.close()
