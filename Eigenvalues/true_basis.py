import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from typing import Callable
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from tqdm import tqdm


# Barycentric coorrdinates
def l1(x):
    return (-x[0] - x[1]) / 2


def l2(x):
    return (1 + x[0]) / 2


def l3(x):
    return (1 + x[1]) / 2


def capphi(x, m, q):
    """Magic capphi"""
    return (-1) ** (q) / (q + 1) * jac(q, m, 1, x)


def q(x, l_1, l_2, p):
    i = int(np.floor(p / 2))
    return capphi(2 * l_1(x) / (1 - l_2(x)) - 1, 2, i) * (1 - l_2(x)) ** i * capphi(2 * l_2(x) - 1, 2 * i + 3,
                                                                                    p - i - 1)


# Vertex functions
def phi1(x, p: int):
    # return -2**(-1-i)*(1-x[1])**i * (x[0]+x[1]) * jac(i, 2, 0, 2*(1 + x[0])/(1 - x[1]) - 1) * jac(j, 2*i + 3, 0, x[1])
    # return (-1) ** (p + 1) / p * l1(x) * jac(p - 1, 1, 1, 1 - 2 * l1(x))
    return .5 * l1(x) * (q(x, l2, l3, p) + q(x, l3, l2, p))


def phi2(x, p: int):
    # return (-1) ** (p + 1) / p * l2(x) * jac(p - 1, 1, 1, 1 - 2 * l2(x))
    return .5 * l2(x) * (q(x, l1, l3, p) + q(x, l3, l1, p))

def phi3(x, p: int):
    # return (-1) ** (p + 1) / p * l3(x) * jac(p - 1, 1, 1, 1 - 2 * l3(x))
    return .5 * l3(x) * (q(x, l1, l2, p) + q(x, l2, l1, p))

def phi1_quad(x, p: int):
    # return -2**(-1-i)*(1-x[1])**i * (x[0]+x[1]) * jac(i, 2, 0, 2*(1 + x[0])/(1 - x[1]) - 1) * jac(j, 2*i + 3, 0, x[1])
    return (-1) ** (p + 1) / p * l1(x) * jac(p - 1, 2, 1, 1 - 2 * l1(x))


def phi2_quad(x, p: int):
    return (-1) ** (p + 1) / p * l2(x) * jac(p - 1, 2, 1, 1 - 2 * l2(x))
    # return .5 * l2(x) * (q(x, l1, l3, p) + q(x, l3, l1, p))

def phi3_quad(x, p: int):
    return (-1) ** (p + 1) / p * l3(x) * jac(p - 1, 2, 1, 1 - 2 * l3(x))
    # return .5 * l3(x) * (q(x, l1, l2, p) + q(x, l2, l1, p))



# Interior functions
def psi(x, i: int, j: int):
    return -2 ** (-2 - i) * (1 + x[0]) * (1 - x[1]) ** (-1 + i) * (1 + x[1]) * (x[0] + x[1]) * \
           jac(i - 1, 2, 2, (1 + 2 * x[0] + x[1]) / (1 - x[1])) * jac(-1 + j, 3 + 2 * i, 2, x[1])


# Edge functions
def chi1(x, n):
    return (1 + x[0]) * (1 + x[1]) * jac(n, 2, 2, x[0])


def chi2(x, n):
    return -(1 + x[1]) * (x[0] + x[1]) * jac(n, 2, 2, -x[1])


def chi3(x, n):
    return (1 + x[0]) * (x[0] + x[1]) * jac(n, 2, 2, x[0])


def quadtriangle(f, w00, w10, x00, x10):
    # # Generate points first (assumes w00 and w10 has same length)
    # points = np.zeros((2, len(w00) ** 2))
    # for i in range(len(w00)):
    #     points[:, i*len(w00): (i + 1) * len(w00)] = [(1 + x00[i]) * (1 - x10) / 2 - 1, x10]
    # return np.dot(w00, np.reshape(f(points), (len(w00), len(w00))).dot(w10/2))

    total = 0
    for i in range(len(w00)):
        total += w00[i] * np.dot(w10 / 2, f([(1 + x00[i]) * (1 - x10) / 2 - 1, x10]))
    return total


def plotting(f: Callable):
    subdiv = 30
    xlis = list()
    ylis = list()
    zlis = list()
    for i in range(subdiv):
        y = -1 + 2 * i / (subdiv - 1)
        for j in range(subdiv - i):
            x = -1 + 2 * j / (subdiv - 1)
            xlis.append(x)
            ylis.append(y)
            try:
                zlis.append(f([x, y]))
            except ZeroDivisionError:
                zlis.append(0)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot_trisurf(xlis, ylis, zlis, antialiased=True)

    plt.show()


if __name__ == '__main__':
    np.set_printoptions(precision=6, linewidth=200)

    # Create barycentric lists
    bary = [l1, l2, l3]
    start = 4
    stop = 75
    print("Ranges are", start, stop)
    # print("This is Good")
    for p in range(start, stop, 5):
    # for p in range(4, 15):
        print(p, end=' ')
        x00, w00 = rj(p + 1, 0, 0)
        x10, w10 = rj(p + 1, 1, 0)

        # print(quadtriangle(lambda x: phi(4, x, bary[0])**2, w00, w10, x00, x10))
        # print(dblquad(lambda x, y:  phi(4, [x,y], bary[0])**2, -1, 1, lambda x: -1, lambda x: -x)[0])

        M = np.zeros((int((p + 1) * (p + 2) / 2), int((p + 1) * (p + 2) / 2)))
        ns = int((p + 2) * (p + 1) / 2) - 3 * (p - 1) - 3
        # Create list of lambda functions
        func_dofs = list()
        # func_dofs.append(lambda x: phi1(x, np.floor(p / 2)))  # Very odd (blog about this) see solution on edges
        # func_dofs.append(lambda x: phi2(x, np.floor(p / 2)))
        # func_dofs.append(lambda x: phi3(x, np.floor(p / 2)))

        # Linear
        # func_dofs.append(lambda x: l1(x))  # Very odd (blog about this) see solution on edges
        # func_dofs.append(lambda x: l2(x))
        # func_dofs.append(lambda x: l3(x))

        # Bernstien
        # func_dofs.append(lambda x: l1(x)**p)  # Very odd (blog about this) see solution on edges
        # func_dofs.append(lambda x: l2(x)**p)
        # func_dofs.append(lambda x: l3(x)**p)

        # Full (use 2 as the numerator in vertex solve for good show)
        # func_dofs.append(lambda x: phi1(x, p))  # Very odd (blog about this) see solution on edges
        # func_dofs.append(lambda x: phi2(x, p))
        # func_dofs.append(lambda x: phi3(x, p))

        # Quad basis
        func_dofs.append(lambda x: phi1_quad(x, p))  # Very odd (blog about this) see solution on edges
        func_dofs.append(lambda x: phi2_quad(x, p))
        func_dofs.append(lambda x: phi3_quad(x, p))

        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi1(x, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi2(x, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi3(x, i))

        for i in range(1, p - 1):
            for j in range(1, p - i):
                func_dofs.append(lambda x, i=i, j=j: psi(x, i, j))

        # for i in range(len(func_dofs)):
        #     plotting(func_dofs[i])

        # Construct mass matrix
        # print("Creating Boundary")
        for i in (range(M.shape[0] - ns)):
            for j in range(i, M.shape[0] - ns):
                M[i, j] = quadtriangle(lambda x: func_dofs[i](x) * func_dofs[j](x), w00, w10, x00, x10)
                M[j, i] = M[i, j]

        # print("Boundary done, creating sides")
        for i in (range(M.shape[0] - ns, M.shape[0])):
            for j in range(M.shape[0] - ns):
                M[i, j] = quadtriangle(lambda x: func_dofs[i](x) * func_dofs[j](x), w00, w10, x00, x10)
                M[j, i] = M[i, j]

        # print("Side done, calculating interior")
        for i in range(M.shape[0] - ns, M.shape[0]):
            M[i, i] = quadtriangle(lambda x: func_dofs[i](x) * func_dofs[i](x), w00, w10, x00, x10)

        A = M[0:3 * p, 0:3 * p]
        B = M[0:3 * p, 3 * p:]
        C = 1 / np.diag(M[3 * p:, 3 * p:])
        S = A - B.dot(np.diag(C).dot(B.T))

        # print(S)

        # print(S[0:3, 0:3])
        # print(S[0,0])
        # eigs = np.linalg.eigvals(M)
        # print(max(eigs) / min(eigs), end=" ")

        # eigs = np.linalg.eigvals(np.diag(1/np.diag(M)).dot(M))
        # print(max(eigs) / min(eigs), end=" ")

        # Create preconditioner
        P = np.zeros_like(S)
        # P[0:3, 0:3] = np.eye(3) * 16 / (p ** 4)
        # P[0:3, 0:3] = np.eye(3) * np.log(p) * 2 / (p ** 4)
        P[0:3, 0:3] = S[0:3, 0:3]

        # print(np.diag(P[0:3, :3]))
        # print(np.diag(S[3:3 + (p - 1), 3:3 + (p - 1)]))

        # P[0:3, 0:3] = np.diag(np.diag(S[0:3, 0:3]))
        # P[3:, 3:] = S[3:, 3:]
        P[3:3 + (p - 1), 3:3 + (p - 1)] = S[3:3 + (p - 1), 3:3 + (p - 1)]
        P[3 + (p - 1):3 + 2 * (p - 1), 3 + (p - 1):3 + 2 * (p - 1)] = S[3 + (p - 1):3 + 2 * (p - 1),
                                                                      3 + (p - 1):3 + 2 * (p - 1)]
        P[3 + 2 * (p - 1):3 + 3 * (p - 1), 3 + 2 * (p - 1):3 + 3 * (p - 1)] = S[3 + 2 * (p - 1):3 + 3 * (p - 1),
                                                                              3 + 2 * (p - 1):3 + 3 * (p - 1)]

        # eigs = np.linalg.eigvals(np.diag(1 / np.diag(P)).dot(S))
        # eigs = np.linalg.eigvals(np.linalg.inv(P).dot(S))
        eigs = np.real(np.linalg.eigvals(np.linalg.solve(P, S)))

        ## Vertices check
        # P_eye = np.identity(3)
        # P_eye = P_eye * 4 / (p ** 4)
        # eigs = np.linalg.eigvals(np.diag(1 / np.diag(P_eye)).dot(S[0:3, 0:3]))

        ## Edge check
        # P_edge = np.zeros((3*p - 3, 3*p - 3))
        # P_edge[0:(p - 1), 0:(p - 1)] = S[3:3 + (p - 1), 3:3 + (p - 1)]
        # P_edge[(p - 1):2 * (p - 1),  (p - 1):2 * (p - 1)] = S[3 + (p - 1):3 + 2 * (p - 1),
        #                                                               3 + (p - 1):3 + 2 * (p - 1)]
        # P_edge[2 * (p - 1):3 * (p - 1),  2 * (p - 1): 3 * (p - 1)] = S[3 + 2 * (p - 1):3 + 3 * (p - 1),
        #                                                                       3 + 2 * (p - 1):3 + 3 * (p - 1)]
        # eigs = np.linalg.eigvals(np.diag(1 / np.diag(P_edge)).dot(S[3:3*p, 3:3*p]))
        #
        # print(max(eigs) / min(eigs),  S[0,0] * (p**3), S[0,0]*(p**3 * np.log(p)), S[0, 0] * (p**3 * np.log(p) * np.log(p)))
        print(max(eigs) / min(eigs), max(eigs), min(eigs))
