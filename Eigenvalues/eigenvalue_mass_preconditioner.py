import numpy as np
import scipy
from scipy.sparse.linalg import LinearOperator

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from mass_preconditioner import MassPreconditioner

if __name__ == '__main__':
    # 1 Triangles
    coords_tri = np.array([[-1, -1], [-1, 1], [1, -1]])
    elnode_tri = np.array([[0, 1, 2]])
    bc_tri = np.array([])
    # coords_tri = np.array([[-1, -1], [-1, -.99], [1, -1]])
    # elnode_tri = np.array([[0, 1, 2]])
    # bc_tri = np.array([])

    print('p\tbern\tpre')
    np.set_printoptions(linewidth=200, precision=6)
    for order in range(3, 4, 1):
        print(order, end='\t')
        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, order, subdiv=0)
        fem = BernsteinMatrix(mesh)
        _, mass, _ = fem.assemble_matrix()
        precond = MassPreconditioner(mass, fem)

        print(np.real(max(np.linalg.eigvals(mass.todense()))/min(np.linalg.eigvals(mass.todense()))), end = '\t')
        def mult(v: np.ndarray) -> np.ndarray:
            return precond.solve(.1*mass.dot(v))
            # return mass.dot(v)
        print(mult(np.ones(mass.shape[0])))
        # PinvS = LinearOperator(dtype=mass.dtype, shape=mass.shape, matvec=mult)
        #
        # w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
        #                              maxiter=mass.shape[0] * 60, ncv=mass.shape[0] * 6)
        # large = max(np.real(w))
        # print(large, end = ' ')
        # w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=mass.shape[0] * 200,
        #                              tol=1E-9, ncv=mass.shape[0] * 40)
        # small = min(np.real(w))
        # print(small, end = ' ')
        # print(large / small)
