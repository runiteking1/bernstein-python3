import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from typing import Callable
from matplotlib import pyplot as plt


# Helper Functions
def xi(x, k):
    return (-1) ** (k + 1) / k * (1 - x) / 2 * jac(k - 1, 1, 1, x)


def phi(x, m, q):
    return (-1) ** q / (q + 1) * jac(q, m, 1, x)


# Nodal basis on interval
def f(x, p):
    phat = int(np.floor(p / 2))
    return (1 - x) / 2 * (phi(x, 2, phat) + ((1 - x) / 2) ** (phat) * phi(x, 2 * phat + 3, p - phat - 1)) / 2


def min1d(x, p):
    return (1 - x) / 2 * phi(x, 2, p - 1)


# Interior basis on interval
def g(x, i):
    return (1 - x) * (1 + x) * jac(i, 2, 2, x)


# Nodal Functions
def phi1(x, p: int):
    return f(x[0], p) * f(x[1], p) * f(x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(x[0], phat) * xi(x[1], phat)


def phi2(x, p: int):
    return f(x[0], p) * f(x[1], p) * f(-x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(x[0], phat) * xi(-x[1], phat)


def phi3(x, p: int):
    return f(x[0], p) * f(-x[1], p) * f(x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(-x[0], phat) * xi(x[1], phat)


def phi4(x, p: int):
    return f(x[0], p) * f(-x[1], p) * f(-x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(-x[0], phat) * xi(-x[1], phat)


def phi5(x, p: int):
    return f(-x[0], p) * f(x[1], p) * f(x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(-x[0], phat) * xi(-x[1], phat)


def phi6(x, p: int):
    return f(-x[0], p) * f(x[1], p) * f(-x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(-x[0], phat) * xi(-x[1], phat)


def phi7(x, p: int):
    return f(-x[0], p) * f(-x[1], p) * f(x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(-x[0], phat) * xi(-x[1], phat)


def phi8(x, p: int):
    return f(-x[0], p) * f(-x[1], p) * f(-x[2], p)
    # phat = int(np.floor(p/2))
    # return xi(-x[0], phat) * xi(-x[1], phat)


# Interior functions
def psi(x, i: int, j: int, k: int):
    return g(x[0], i) * g(x[1], j) * g(x[2], k)


# Edge functions
def chi1(x, p: int, n):
    return f(x[0], p) * f(x[1], p) * g(x[2], n)


def chi2(x, p: int, n):
    return f(x[0], p) * f(-x[1], p) * g(x[2], n)


def chi3(x, p: int, n):
    return f(-x[0], p) * f(x[1], p) * g(x[2], n)


def chi4(x, p: int, n):
    return f(-x[0], p) * f(-x[1], p) * g(x[2], n)


def chi5(x, p: int, n):
    return f(x[0], p) * f(x[2], p) * g(x[1], n)


def chi6(x, p: int, n):
    return f(x[0], p) * f(-x[2], p) * g(x[1], n)


def chi7(x, p: int, n):
    return f(-x[0], p) * f(x[2], p) * g(x[1], n)


def chi8(x, p: int, n):
    return f(-x[0], p) * f(-x[2], p) * g(x[1], n)


def chi9(x, p: int, n):
    return f(x[1], p) * f(x[2], p) * g(x[0], n)


def chi10(x, p: int, n):
    return f(x[1], p) * f(-x[2], p) * g(x[0], n)


def chi11(x, p: int, n):
    return f(-x[1], p) * f(x[2], p) * g(x[0], n)


def chi12(x, p: int, n):
    return f(-x[1], p) * f(-x[2], p) * g(x[0], n)


# Face functions
def face1(x, p: int, i: int, j: int):
    return f(x[0], p) * g(x[1], i) * g(x[2], j)


def face2(x, p: int, i: int, j: int):
    return f(-x[0], p) * g(x[1], i) * g(x[2], j)


def face3(x, p: int, i: int, j: int):
    return f(x[1], p) * g(x[0], i) * g(x[2], j)


def face4(x, p: int, i: int, j: int):
    return f(-x[1], p) * g(x[0], i) * g(x[2], j)


def face5(x, p: int, i: int, j: int):
    return f(x[2], p) * g(x[0], i) * g(x[1], j)


def face6(x, p: int, i: int, j: int):
    return f(-x[2], p) * g(x[0], i) * g(x[1], j)


def hex_quadrature(f, w00, x00):
    # Generate points first (assumes w00 and w10 has same length)
    total = 0
    for i in range(len(w00)):
        for j in range(len(w00)):
            total += w00[i] * w00[j] * np.dot(w00, f([x00[i], x00[j], x00]))

    return total


def plotting(f: Callable):
    from mpl_toolkits.mplot3d import Axes3D

    subdiv = 30

    x = np.linspace(-1, 1, num=subdiv)
    y = np.linspace(-1, 1, num=subdiv)
    xx, yy = np.meshgrid(x, y)

    all_coords = np.zeros((2, len(xx) ** 2))
    all_coords[0, :] = xx.flatten()
    all_coords[1, :] = yy.flatten()
    zlis = f(all_coords)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot_trisurf(all_coords[0, :], all_coords[1, :], zlis, antialiased=True)

    plt.show()


if __name__ == '__main__':
    print("THIS DOENS'T WORK LMAO")
    np.set_printoptions(precision=4, linewidth=200)

    # Create barycentric lists
    start = 4
    stop = 12
    print("Ranges are", start, stop)
    print("This is Good")

    from scipy.integrate import dblquad

    # for p in range(start, stop, 5):
    for p in range(start, stop, 1):
        print(p, end=' ')
        x00, w00 = rj(p + 1, 0, 0)

        M = np.zeros(((p + 1) * (p + 1) * (p + 1), (p + 1) * (p + 1) * (p + 1)))
        ns = (p - 1) * (p - 1)  # Number of internal nodes

        # Create list of lambda functions
        func_dofs = list()
        func_dofs.append(lambda x: phi1(x, p))  # Very odd (blog about this) see solution on edges
        func_dofs.append(lambda x: phi2(x, p))
        func_dofs.append(lambda x: phi3(x, p))
        func_dofs.append(lambda x: phi4(x, p))
        func_dofs.append(lambda x: phi5(x, p))
        func_dofs.append(lambda x: phi6(x, p))
        func_dofs.append(lambda x: phi7(x, p))
        func_dofs.append(lambda x: phi8(x, p))

        # plotting(func_dofs[0])
        # Linear: TODO: try with xi basis functions

        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi1(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi2(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi3(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi4(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi5(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi6(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi7(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi8(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi9(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi10(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi11(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi12(x, p, i))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: face1(x, p, i, j))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: face2(x, p, i, j))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: face3(x, p, i, j))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: face4(x, p, i, j))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: face5(x, p, i, j))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: face6(x, p, i, j))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                for k in range(0, p - 1):
                    func_dofs.append(lambda x, i=i, j=j: psi(x, i, j, k))

        # Construct mass matrix
        # print("Creating Boundary")
        for i in (range(M.shape[0] - ns)):
            for j in range(i, M.shape[0] - ns):
                M[i, j] = hex_quadrature(lambda x: func_dofs[i](x) * func_dofs[j](x), w00, x00)
                M[j, i] = M[i, j]
        # print(quad_quadrature(lambda x: func_dofs[0](x) * func_dofs[4](x), w00, x00))
        # print("Boundary done, creating sides")
        for i in (range(M.shape[0] - ns, M.shape[0])):
            for j in range(M.shape[0] - ns):
                M[i, j] = quad_quadrature(lambda x: func_dofs[i](x) * func_dofs[j](x), w00, x00)
                M[j, i] = M[i, j]

        # print("Side done, calculating interior")
        for i in range(M.shape[0] - ns, M.shape[0]):
            M[i, i] = quad_quadrature(lambda x: func_dofs[i](x) * func_dofs[i](x), w00, x00)
        # plt.spy(M, precision=1e-8)
        # plt.show()
        A = M[0:4 * p, 0:4 * p]
        B = M[0:4 * p, 4 * p:]
        C = 1 / np.diag(M[4 * p:, 4 * p:])
        S = A - B.dot(np.diag(C).dot(B.T))

        # Create preconditioner
        P = np.zeros_like(S)
        P[0:4, 0:4] = np.eye(4) * 16 / (p ** 4)
        # P[0:4, 0:4] = S[0:4, 0:4]
        # print(S[0:4, 0:4])
        # print(np.diag(S[4:4 + (p - 1), 4:4 + (p-1)]))

        P[4:4 + (p - 1), 4:4 + (p - 1)] = S[4:4 + (p - 1), 4:4 + (p - 1)]
        P[4 + (p - 1):4 + 2 * (p - 1), 4 + (p - 1):4 + 2 * (p - 1)] = S[4 + (p - 1):4 + 2 * (p - 1),
                                                                      4 + (p - 1):4 + 2 * (p - 1)]
        P[4 + 2 * (p - 1):4 + 3 * (p - 1), 4 + 2 * (p - 1):4 + 3 * (p - 1)] = S[4 + 2 * (p - 1):4 + 3 * (p - 1),
                                                                              4 + 2 * (p - 1):4 + 3 * (p - 1)]
        P[4 + 3 * (p - 1):4 + 4 * (p - 1), 4 + 3 * (p - 1):4 + 4 * (p - 1)] = S[4 + 3 * (p - 1):4 + 4 * (p - 1),
                                                                              4 + 3 * (p - 1):4 + 4 * (p - 1)]

        # print(S[4 + (p-1):4 + 2 * (p-1), 4 + (p-1): 4 + 2 * (p-1)])
        eigs = np.real(np.linalg.eigvals(np.linalg.solve(P, S)))

        print(max(eigs) / min(eigs), max(eigs), min(eigs))
