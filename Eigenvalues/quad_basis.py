import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from typing import Callable
from matplotlib import pyplot as plt


# Helper Functions
def xi(x, k):
    return (-1) ** (k + 1) / k * (1 - x) / 2 * jac(k-1, 1, 1, x)


def phi(x, m, q):
    return (-1) ** q / (q + 1) * jac(q, m, 1, x)


def f(x, p):
    phat = int(np.floor(p/2))
    # TODO: make sure this is correct
    return (1 - x)/2 * (phi(x, 2, phat) + ((1 - x)/2) ** (phat) * phi(x, 2 * phat + 3, p - phat - 1)) / 2

    # i = int(np.floor(p/2))
    # j = int(np.floor(i/2))
    # # TODO: make sure this is correct
    # return (1 - x)/6 * (phi(x, 2, i) + ((1-x)/2) ** (i + 1) * phi(x, 2*i + 3, j) + ((1 - x)/2) ** (i + j + 1) * phi(x, 2*i + 2*j + 4, p - 1 - i - j))


    # xi phat nodal basis
    # phat = int(np.floor(p/2))
    # return xi(x, phat)

    # optimal nodal
    # return (1 - x) / 2 * phi(x, 2, p - 1)
    # return (1-x)/2
def min1d(x, p):
    return (1 - x)/2 * phi(x, 2, p - 1)


def g(x, i):
    return (1 - x) * (1 + x) * jac(i, 2, 2, x)


# Nodal Functions
def phi1(x, p: int):
    # Bilinear
    # return (1 - x[0])/2 * (1 - x[1])/2

    # return f(x[0], p) * f(x[1], p)
    # phat = int(np.floor(p/2))
    return f(x[0], p) * f(x[1], p)


def phi2(x, p: int):
    # Bilinear
    # return (1 - x[0]) / 2 * (1 + x[1]) / 2

    # return f(x[0], p) * f(-x[1], p)
    # phat = int(np.floor(p/2))
    return f(x[0], p) * f(-x[1], p)


def phi3(x, p: int):
    # Bilinear
    # return (1 + x[0]) / 2 * (1 - x[1]) / 2

    # return f(-x[0], p) * f(x[1], p)
    # phat = int(np.floor(p/2))
    return f(-x[0], p) * f(x[1], p)


def phi4(x, p: int):
    # Bilinear
    # return (1 + x[0]) / 2 * (1 + x[1]) / 2

    # return f(-x[0], p) * f(-x[1], p)
    # phat = int(np.floor(p/2))
    return f(-x[0], p) * f(-x[1], p)


# Interior functions
def psi(x, i: int, j: int):
    return g(x[0], i) * g(x[1], j)


# Edge functions
def chi1(x, p: int, n):
    # return xi(x[0], p) * g(x[1], n)
    # return min1d(x[0], p) * g(x[1], n)

    # phat = int(np.floor(p/2))
    return f(x[0], p) * g(x[1], n)


def chi2(x, p: int, n):
    # return f(-x[0], p) * g(x[1], n)
    # return min1d(-x[0], p) * g(x[1], n)
    # phat = int(np.floor(p/2))
    return f(-x[0], p) * g(x[1], n)


def chi3(x, p: int, n):
    # return f(x[1], p) * g(x[0], n)
    # return min1d(x[1], p) * g(x[0], n)
    # phat = int(np.floor(p/2))
    return f(x[1], p) * g(x[0], n)


def chi4(x, p: int, n):
    # return f(-x[1], p) * g(x[0], n)
    # return min1d(-x[1], p) * g(x[0], n)
    # phat = int(np.floor(p/2))
    return f(-x[1], p) * g(x[0], n)


def quad_quadrature(f, w00, x00):
    # Generate points first (assumes w00 and w10 has same length)
    total = 0
    for i in range(len(w00)):
        total += w00[i] * np.dot(w00, f([x00[i], x00]))
    return total

def plotting(f: Callable):
    from mpl_toolkits.mplot3d import Axes3D

    subdiv = 30

    x = np.linspace(-1, 1, num=subdiv)
    y = np.linspace(-1, 1, num=subdiv)
    xx, yy = np.meshgrid(x, y)

    all_coords = np.zeros((2, len(xx)**2))
    all_coords[0, :] = xx.flatten()
    all_coords[1, :] = yy.flatten()
    zlis = f(all_coords)

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.plot_trisurf(all_coords[0, :], all_coords[1, :], zlis, antialiased=True)

    plt.show()


if __name__ == '__main__':
    np.set_printoptions(precision=5, linewidth=200)

    # Create barycentric lists
    start = 4
    stop = 5
    print("Ranges are", start, stop)
    print("This is Good")
    # print(f(.1, 4))
    from scipy.integrate import dblquad
    # for p in range(start, stop, 5):
    for p in range(start, stop, 2):
        print(p, end=' ')
        x00, w00 = rj(p + 1, 0, 0)

        M = np.zeros(((p + 1) * (p + 1), (p + 1) * (p + 1)))
        ns = (p - 1) * (p - 1)          # Number of internal nodes

        # Create list of lambda functions
        func_dofs = list()
        func_dofs.append(lambda x: phi1(x, p))  # Very odd (blog about this) see solution on edges
        func_dofs.append(lambda x: phi2(x, p))
        func_dofs.append(lambda x: phi3(x, p))
        func_dofs.append(lambda x: phi4(x, p))
        # plotting(func_dofs[0])
        # Linear: TODO: try with xi basis functions

        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi1(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi2(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi3(x, p, i))
        for i in range(p - 1):
            func_dofs.append(lambda x, i=i: chi4(x, p, i))

        for i in range(0, p - 1):
            for j in range(0, p - 1):
                func_dofs.append(lambda x, i=i, j=j: psi(x, i, j))

        # Construct mass matrix
        # print("Creating Boundary")
        for i in (range(M.shape[0] - ns)):
            for j in range(i, M.shape[0] - ns):
                M[i, j] = quad_quadrature(lambda x: func_dofs[i](x) * func_dofs[j](x), w00, x00)
                M[j, i] = M[i, j]
        # print(quad_quadrature(lambda x: func_dofs[0](x) * func_dofs[4](x), w00, x00))
        # print("Boundary done, creating sides")
        for i in (range(M.shape[0] - ns, M.shape[0])):
            for j in range(M.shape[0] - ns):
                M[i, j] = quad_quadrature(lambda x: func_dofs[i](x) * func_dofs[j](x), w00, x00)
                M[j, i] = M[i, j]

        # print("Side done, calculating interior")
        for i in range(M.shape[0] - ns, M.shape[0]):
            M[i, i] = quad_quadrature(lambda x: func_dofs[i](x) * func_dofs[i](x), w00, x00)
        # plt.spy(M, precision=1e-8)
        # plt.show()
        A = M[0:4 * p, 0:4 * p]
        B = M[0:4 * p, 4 * p:]
        C = 1 / np.diag(M[4 * p:, 4 * p:])
        S = A - B.dot(np.diag(C).dot(B.T))

        # Create preconditioner
        # P = np.zeros_like(S)
        P = np.diag(np.diag(S))
        # P[0:4, 0:4] = np.eye(4) * 16 / (p ** 4)
        # P[0:4, 0:4] = S[0:4, 0:4]
        # print(S[0:4, 0:4])
        # print(np.diag(S[4:4 + (p - 1), 4:4 + (p-1)]))
        #
        # P[4:4 + (p - 1), 4:4 + (p - 1)] = S[4:4 + (p - 1), 4:4 + (p - 1)]
        # P[4 + (p - 1):4 + 2 * (p - 1), 4 + (p - 1):4 + 2 * (p - 1)] = S[4 + (p - 1):4 + 2 * (p - 1),
        #                                                               4 + (p - 1):4 + 2 * (p - 1)]
        # P[4 + 2 * (p - 1):4 + 3 * (p - 1), 4 + 2 * (p - 1):4 + 3 * (p - 1)] = S[4 + 2 * (p - 1):4 + 3 * (p - 1),
        #                                                                       4 + 2 * (p - 1):4 + 3 * (p - 1)]
        # P[4 + 3 * (p - 1):4 + 4 * (p - 1), 4 + 3 * (p - 1):4 + 4 * (p - 1)] = S[4 + 3 * (p - 1):4 + 4 * (p - 1),
        #                                                                       4 + 3 * (p - 1):4 + 4 * (p - 1)]

        # print(S[4 + (p-1):4 + 2 * (p-1), 4 + (p-1): 4 + 2 * (p-1)])
        eigs = np.real(np.linalg.eigvals(np.linalg.solve(P, S)))
        # eigs = np.real(np.linalg.eigvals(np.linalg.solve(np.diag(np.diag(M)), M)))
        print(max(eigs) / min(eigs), max(eigs), min(eigs))
