import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj, binom, eval_chebyt as cheb, eval_chebyu as chebu
from typing import Callable
import matplotlib.pyplot as plt
# from basis import l1, grad_l1, l2, grad_l2, l3, grad_l3
from basis import sum_quads


# Barycentric coorrdinates
def l1(x):
    return (-x[0] - x[1]) / 2


def grad_l1(x):
    return (-.5, -.5)


def l2(x):
    return (1 + x[0]) / 2


def grad_l2(x):
    return (.5, 0)


def l3(x):
    return (1 + x[1]) / 2


def grad_l3(x):
    return (0, .5)


# Vertex functions
def phi1(x, p: int):
    return (-1) ** (p + 1) / p * l1(x) * jac(p - 1, 1, 1, 1 - 2 * l1(x))


def grad_phi1(x, p: int):
    return (
        (-1) ** p * (
                (2 + p) * (x[0] + x[1]) * jac(p - 2, 2, 2, 1 - 2 * l1(x)) + 2 * jac(p - 1, 1, 1, 1 - 2 * l1(x))) / (
                4 * p),
        (-1) ** p * (
                (2 + p) * (x[0] + x[1]) * jac(p - 2, 2, 2, 1 - 2 * l1(x)) + 2 * jac(p - 1, 1, 1, 1 - 2 * l1(x))) / (
                4 * p))


def phi2(x, p: int):
    return (-1) ** (p + 1) / p * l2(x) * jac(p - 1, 1, 1, 1 - 2 * l2(x))


def grad_phi2(x, p: int):
    return ((-1) ** p * ((2 + p) * (x[0] + 1) * jac(p - 2, 2, 2, -x[0]) - 2 * jac(p - 1, 1, 1, - x[0])) / (4 * p),
            0)


def phi3(x, p: int):
    return (-1) ** (p + 1) / p * l3(x) * jac(p - 1, 1, 1, 1 - 2 * l3(x))


def grad_phi3(x, p: int):
    return (0,
            (-1) ** p * ((2 + p) * (1 + x[1]) * jac(p - 2, 2, 2, - x[1]) - 2 * jac(p - 1, 1, 1, - x[1])) / (4 * p))


# Interior functions
def psi(a, i: int, j: int):
    x, y = a
    return 2 ** (-3 - i) * (1 + x) * (2 - (2 * (1 + x)) / (1 - y)) * (1 - y) ** i * (1 + y) * jac(-1 + i, 1, 1,
                                                                                                  -1 + (2 * (1 + x)) / (
                                                                                                          1 - y)) * jac(
        -1 + j, 1 + 2 * i, 1, y)
    # return -2 ** (-2 - i) * (1 + x[0]) * (1 - x[1]) ** (-1 + i) * (1 + x[1]) * (x[0] + x[1]) * \
    #        jac(i - 1, 2, 2, (1 + 2 * x[0] + x[1]) / (1 - x[1])) * jac(-1 + j, 3 + 2 * i, 2, x[1])


def grad_psi(x, i: int, j: int):
    a, b = x

    return (2 ** (-2 - i) * (1 - b) ** (-2 + i) * (1 + b) * (
            -((2 + i) * (1 + a) * (a + b) * jac(-2 + i, 2, 2, (1 + 2 * a + b) / (1 - b))) +
            (-1 + b) * (1 + 2 * a + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b))) * jac(-1 + j, 1 + 2 * i, 1, b),
            -(2 ** (-3 - i) * (1 + a) * (1 - b) ** (-3 + i) * (
                    (2 + 2 * i + j) * (-1 + b) ** 2 * (1 + b) * (a + b) * jac(-1 + i, 1, 1,
                                                                              (1 + 2 * a + b) / (1 - b)) * jac(-2 + j,
                                                                                                               2 + 2 * i,
                                                                                                               2, b) +
                    2 * (2 + i) * (1 + a) * (1 + b) * (a + b) * jac(-2 + i, 2, 2, (1 + 2 * a + b) / (1 - b)) * jac(
                -1 + j, 1 + 2 * i, 1, b) -
                    2 * (1 + a) * (-1 + b) * (1 + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b)) * jac(-1 + j,
                                                                                                          1 + 2 * i, 1,
                                                                                                          b) +
                    2 * (-1 + b) ** 2 * (a + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b)) * jac(-1 + j, 1 + 2 * i,
                                                                                                     1, b) +
                    2 * i * (-1 + b) * (1 + b) * (a + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b)) * jac(-1 + j,
                                                                                                              1 + 2 * i,
                                                                                                              1, b))))

    # return ((2 ** (-2 - i) * (1 - b) ** (-2 + i) * (1 + b) * (
    #         (1 + a) * (a + b) * (-4 - i) * jac(-2 + i, 3, 3, (1 + 2 * a + b) / (1 - b)) +
    #         (-1 + b) * (1 + 2 * a + b) * jac(-1 + i, 2, 2, (1 + 2 * a + b) / (1 - b))) * jac(-1 + j, 3 + 2 * i, 2, b),
    #          -(2 ** (-3 - i) * (1 + a) * (1 - b) ** (-3 + i) * (
    #                  2 * (1 + a) * (1 + b) * (a + b) * (4 + i) * jac(-2 + i, 3, 3, (1 + 2 * a + b) / (1 - b)) * jac(
    #              -1 + j, 3 + 2 * i, 2, b) +
    #                  (-1 + b) * jac(-1 + i, 2, 2, (1 + 2 * a + b) / (1 - b)) * (
    #                          (a + b) * (-1 + b ** 2) * (5 + 2 * i + j) * jac(-2 + j, 4 + 2 * i, 3, b) +
    #                          2 * (-1 + a * (-2 + i + b * i) + b * (-2 + b + i + b * i)) * jac(-1 + j, 3 + 2 * i, 2,
    #                                                                                           b))))))


# Edge functions
def chi3(x, p, n):
    # Bern
    # n = n + 1
    # return binom(p, n) * l2(x) ** (p - n) * l3(x) ** (n)

    # Scaled Bern
    # n = n + 1
    # return l2(x) ** (p - n) * l3(x) ** (n)

    # Hier
    return (1 + x[0]) * (1 + x[1]) * jac(n, 2, 2, x[0])


def grad_chi3(x, p, n):
    a, b = x

    # Bern
    # n = n + 1
    # return (((1 + a) ** (-1 - n + p) * (1 + b) ** n * (-n + p) * binom(p, n)) / 2 ** p,
    #         ((1 + a) ** (-n + p) * (1 + b) ** (-1 + n) * n * binom(p, n)) / 2 ** p)

    # Scaled Bern
    # n = n + 1
    # return (((1 + a) ** (-1 - n + p) * (1 + b) ** n * (-n + p) ) / 2 ** p,
    #         ((1 + a) ** (-n + p) * (1 + b) ** (-1 + n) * n ) / 2 ** p)

    # Heir
    return (
        ((1 + b) * ((1 + a) * (5 + n) * jac(-1 + n, 3, 3, a) + 2 * jac(n, 2, 2, a))) / 2., (1 + a) * jac(n, 2, 2, a))


def chi2(x, p, n):
    # Bern
    # n = n+1
    # return binom(p, n) * l1(x) ** (p - n) * l3(x) ** n

    # Scaled Bern
    # n = n+1
    # return l1(x) ** (p - n) * l3(x) ** n

    # Heir
    return -(1 + x[1]) * (x[0] + x[1]) * jac(n, 2, 2, -x[1])


def grad_chi2(x, p, n):
    a, b = x
    # Bern
    # n = n+1
    # return (-(((-a - b) ** (-1 - n + p) * (1 + b) ** n * (-n + p) * binom(p, n)) / 2 ** p),
    #         -(((-a - b) ** (-1 - n + p) * (1 + b) ** (-1 + n) * ((-1 + a) * n + (1 + b) * p) * binom(p, n)) / 2 ** p))

    # Scaled Bern
    # n = n+1
    # return (-(((-a - b) ** (-1 - n + p) * (1 + b) ** n * (-n + p) ) / 2 ** p),
    #         -(((-a - b) ** (-1 - n + p) * (1 + b) ** (-1 + n) * ((-1 + a) * n + (1 + b) * p)) / 2 ** p))

    # Hier
    # return (-((1 + b) * jac(n, 2, 2, -b)),
    #      ((1 + b) * (a + b) * (5 + n) * jac(-1 + n, 3, 3, -b)) / 2. - (1 + a + 2 * b) * jac(n, 2, 2, -b))
    return (-((1 + b) * jac(n, 2, 2, -b)),
            ((1 + b) * (a + b) * (5 + n) * jac(-1 + n, 3, 3, -b)) / 2. - (1 + b) * jac(n, 2, 2, -b) - (a + b) * jac(n,
                                                                                                                    2,
                                                                                                                    2,
                                                                                                                    -b))


def chi1(x, p, n):
    # Bern
    # n = n+1
    # return binom(p, n) * l1(x) ** (p - n) * l2(x) ** n

    # Scaled bern
    # n = n+1
    # return l1(x) ** (p - n) * l2(x) ** n

    # Heir
    return -(1 + x[0]) * (x[0] + x[1]) * jac(n, 2, 2, x[0])


def grad_chi1(x, p, n):
    a, b = x

    # Bern
    # n = n + 1
    # return (-(((1 + a) ** (-1 + n) * (-a - b) ** (-1 - n + p) * ((-1 + b) * n + (1 + a) * p) * binom(p, n)) / 2 ** p),
    #         -(((1 + a) ** n * (-a - b) ** (-1 - n + p) * (-n + p) * binom(p, n)) / 2 ** p))

    # Scaled Bern
    # n = n + 1
    # return (-(((1 + a) ** (-1 + n) * (-a - b) ** (-1 - n + p) * ((-1 + b) * n + (1 + a) * p) ) / 2 ** p),
    #         -(((1 + a) ** n * (-a - b) ** (-1 - n + p) * (-n + p) ) / 2 ** p))

    # Heir
    return (
        -((1 + a) * (a + b) * (5 + n) * jac(-1 + n, 3, 3, a)) / 2. - (1 + a) * jac(n, 2, 2, a) - (a + b) * jac(n, 2, 2,
                                                                                                               a),
        -((1 + a) * jac(n, 2, 2, a)))
    # return (-((1 + a)*(a + b)*(5 + n)*jac(-1 + n,3,3,a))/2. - (1 + 2*a + b)*jac(n,2,2,a),-((1 + a)*jac(n,2,2,a)))

def calculate_schur_compelemnt(A, p):
    a = A[0:3 * (p - 1), 0:3 * (p - 1)]
    b = A[0:3 * p - 3, 3 * p - 3:]
    c = A[3 * p - 3:, 3 * p - 3:]
    return a - b @ np.linalg.solve(c, b.T)


if __name__ == '__main__':
    np.set_printoptions(precision=4, linewidth=200)

    kappa = 0.75

    print("Kappa: ", kappa)
    # Create barycentric lists
    start = 5
    stop = 55
    values = np.arange(5, stop, 5)
    # print("Ranges are", start, stop)
    for p in values:
        print(p, end=' ')

        # Extract quadrature points for triangle quadrature; see Sherwin book
        x00, w00 = rj(p + 1, 0, 0)
        x10, w10 = rj(p + 1, 1, 0)

        # Create mass stiffness matrix (without the vertex components)
        M = np.zeros((int((p + 1) * (p + 2) / 2) - 3, int((p + 1) * (p + 2) / 2) - 3))
        S = np.zeros((int((p + 1) * (p + 2) / 2) - 3, int((p + 1) * (p + 2) / 2) - 3))

        # Create list of lambda functions
        func_dofs_mass = list()
        func_dofs_stiff = list()

        for i in range(p - 1):
            func_dofs_mass.append(lambda x, i=i: chi1(x, p, i))
            func_dofs_stiff.append(lambda x, i=i: grad_chi1(x, p, i))
        for i in range(p - 1):
            func_dofs_mass.append(lambda x, i=i: chi2(x, p, i))
            func_dofs_stiff.append(lambda x, i=i: grad_chi2(x, p, i))
        for i in range(p - 1):
            func_dofs_mass.append(lambda x, i=i: chi3(x, p, i))
            func_dofs_stiff.append(lambda x, i=i: grad_chi3(x, p, i))

        for i in range(1, p - 1):
            for j in range(1, p - i):
                func_dofs_mass.append(lambda x, i=i, j=j: psi(x, i, j))
                func_dofs_stiff.append(lambda x, i=i, j=j: grad_psi(x, i, j))

        # Make large arrays which stores quadrature points of functions
        mass_points = np.zeros((len(func_dofs_mass), len(w00), len(w10)))
        stiff_points_1 = np.zeros((len(func_dofs_mass), len(w00), len(w10)))
        stiff_points_2 = np.zeros((len(func_dofs_mass), len(w00), len(w10)))
        for func in range(len(func_dofs_mass)):
            for i in range(len(w00)):
                mass_points[func, i, :] = func_dofs_mass[func]([(1 + x00[i]) * (1 - x10) / 2 - 1, x10])
                stiff_points_1[func, i, :], stiff_points_2[func, i, :] = func_dofs_stiff[func](
                    [(1 + x00[i]) * (1 - x10) / 2 - 1, x10])

        for i in (range(M.shape[0])):
            for j in range(i, M.shape[0]):
                M[i, j] = sum_quads(mass_points[i], mass_points[j], w00, w10)
                M[j, i] = M[i, j]

                S[i, j] = sum_quads(stiff_points_1[i], stiff_points_1[j], w00, w10) + sum_quads(stiff_points_2[i],
                                                                                                stiff_points_2[j], w00,
                                                                                                w10)
                S[j, i] = S[i, j]

        A = kappa * M + (1 - kappa) * S
        # A = kappa * M + (1 - kappa) * (M + S)
        # A = S
        # A = 10 * S + .001 * M

        # Calculate Schur complement
        # s1 = calculate_schur_compelemnt(M, p)
        # s0 = calculate_schur_compelemnt(M + S, p)
        # s_kappa = calculate_schur_compelemnt(A, p)

        # print(min(np.sort(np.linalg.eigvalsh(2*s_kappa - (kappa) * s1 - (1 - kappa) * s0))))
        # print(min(np.sort(np.linalg.eigvalsh(1.8 * ((kappa) * s1 + (1 - kappa) * s0) - s_kappa))))
        # print(min(np.linalg.eigvalsh(M + S)))


        schur = calculate_schur_compelemnt(A, p)
        edge_blocks = np.zeros_like(schur)
        edge_blocks[0:p-1, 0:p-1] = schur[0:p-1, 0:p-1]
        edge_blocks[p-1:2*(p-1), p-1:2*(p-1)] = schur[p-1:2*(p-1), p-1:2*(p-1)]
        edge_blocks[2*(p-1):, 2*(p-1):] = schur[2*(p-1):, 2*(p-1):]

        # eigs = np.real(np.linalg.eigvals(np.linalg.solve(test, schur)))
        # eigs = np.real(np.linalg.eigvals(schur))
        eigs = np.real(np.linalg.eigvals(np.linalg.solve(edge_blocks, schur)))
        # print(eigs)
        # print(max(eigs) / min(eigs), max(eigs), min(eigs))
        print(max(eigs) / min(eigs))
