import numpy as np
from sympy import *


# 25 12.053397814308513
# 30 13.701715431257133
# 35 15.723069770337577

def index_set(n, d):
    """
    Generates I^n_{d-1}
    :param n: sum of tuple
    :param d: dimension of the set
    :return: a generator of tuples
    """
    if d == 1:
        yield (n,)
    elif d == 0:
        yield ()
    else:
        for alpha in range(n + 1):  # Loop through all possible in the sum
            for gamma in index_set(n - alpha, d - 1):  # Recursively call
                yield (alpha,) + gamma


def dof_to_multi(alpha):
    """
    Converts a degree of freedom to a corresponding multinomial. Convention is vertices are enumerations of
    (p, 0, 0), and subsequent multinomials are more or less the interpolation.

    Note: no overflow/underflow check

    :param alpha: degree of freedom (int)
    :return: 3-tuple multinomial
    """
    if alpha == 0:
        return p, 0, 0
    elif alpha == 1:
        return 0, p, 0
    elif alpha == 2:
        return 0, 0, p
    elif alpha <= p + 1:
        return 0, p - (alpha - 2), (alpha - 2)
    elif alpha <= 2 * p:
        return alpha - (p + 1), 0, 2 * p + 1 - alpha
    elif alpha <= 3 * p - 1:
        return 3 * p - alpha, alpha - 2 * p, 0
    else:
        return tuple(np.array((multi[alpha - (3 * p)])) + np.array([1, 1, 1]))


if __name__ == '__main__':
    init_printing(use_unicode=False, wrap_line=False, no_global=True)

    x, y, z = symbols('x y z')
    f = open('mathematica_output.m', 'w')

    for p in range(4, 5, 1):
        multi = list(index_set(p - 3, 3))
        print("Creating %d" % (p,))
        lookup = dict()
        for i in range(int((p + 1) * (p + 2) / 2)):
            lookup[i] = dof_to_multi(i)

        M = zeros(int((p + 1) * (p + 2) / 2))

        # Make matrix
        for i in range(0, int((p + 1) * (p + 2) / 2)):
            for j in range(0, int((p + 1) * (p + 2) / 2)):
                M[i, j] = binomial(lookup[i][0] + lookup[j][0], lookup[i][0]) * binomial(lookup[i][1] + lookup[j][1],
                                                                                         lookup[i][1]) * \
                          binomial(lookup[i][2] + lookup[j][2], lookup[i][2])

        print("Bernstein Matrix Made")
        M = M * 2 / (binomial(2 * p, p) * binomial(2 * p + 2, 2))
        ns = int((p + 2) * (p + 1) / 2) - 3 * (p - 1) - 3

        # Generate T
        T = zeros(3, 3 * (p - 1))


        def cp(j):
            return (-1) ** j * binomial(p, p - 1 - j) / p


        cps = Matrix([cp(j) for j in np.arange(1, p)]).T
        cps_reverse = Matrix([cp(j) for j in np.arange(p - 1, 0, -1)]).T

        T[0, 2 * (p - 1):] = cps
        T[0, (p - 1):2 * (p - 1)] = cps_reverse
        T[1, 0:(p - 1)] = cps
        T[1, 2 * (p - 1):] = cps_reverse
        T[2, 0:(p - 1)] = cps_reverse
        T[2, (p - 1):2 * (p - 1)] = cps

        R = Matrix(BlockMatrix([[Identity(3), -T], [ZeroMatrix(3*(p-1), 3), Identity(3*(p-1))]]))
        ###

        t = repr(M)
        print("p = {}; ns = (p + 2) * (p + 1) / 2 - 3 * (p - 1) - 3;".format(p), file=f)
        print("M = " + t[7:-1].replace('[', '{').replace(']', '}') + ";", file=f)
        t = repr(R)
        print("R = " + t[7:-1].replace('[', '{').replace(']', '}') + ";", file=f)
        print("a = M[[1;;3 * (p - 1) + 3, 1;;3 * (p - 1) + 3]];", file=f)
        print("b = M[[1;;3 * (p-1) + 3, {} - ns + 1;;{}]];".format(M.shape[0], M.shape[0]), file=f)
        print("c = M[[{0} - ns + 1;;{0}, {0} - ns + 1;;{0}]];".format(M.shape[0]), file=f)
        # print('Print["loaded"]', file=f)
        print("s = a - b.Inverse[c].Transpose[b]", file=f)
        # print('Print["schur calculated"]', file=f)
        print("block = ConstantArray[0, {{ {0}, {0} }}]".format(M.shape[0] - ns), file=f)
        print("block[[1;;3, 1;;3]] = IdentityMatrix[3] * 4/p^4", file=f)
        print("block[[4;;p+2 , 4;;p+2]] =s[[4;;p+2 , 4;;p+2]] ", file=f)
        print("block[[p+3;;2p+1 , p+3;;2p+1]] = s[[p+3;;2p+1 , p+3;;2p+1]] ", file=f)
        print("block[[2p+2;;3p , 2p+2;;3p]] = s[[2p+2;;3p , 2p+2;;3p]] ", file=f)

        print("blocked = R.block.Transpose[R];", file = f)

        print("precond = Inverse[blocked].s;", file=f)
        print("largest = Max[Eigenvalues[precond, 1]]; smallest = Min[Eigenvalues[precond, -1]]", file=f)
        print("Print[N[largest]/N[smallest]];", file = f)
        # print("eigs = Eigenvalues[Inverse[blocked].s]; Print[N[Max[eigs]]/N[Min[eigs]]];" , file = f)
    f.close()
