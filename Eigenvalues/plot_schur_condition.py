import numpy as np
import scipy
from scipy.sparse.linalg import LinearOperator

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from schur_complement import SchurAction
from schur_preconditioner import SchurPreconditioner

if __name__ == '__main__':
    # 1 Triangles
    coords_tri = np.array([[0, 0], [1, 0], [0, 1]])
    elnode_tri = np.array([[0, 1, 2]])
    bc_tri = np.array([])

    np.set_printoptions(linewidth=200, precision=5)

    # A bit unefficient, but just outputting the condition numbers of the pure schur complement
    reg_cond = list()
    for order in range(3, 21):
        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, order)
        fem = BernsteinMatrix(mesh)
        _, mass, _ = fem.assemble_matrix()

        schur = SchurAction(mass, fem)

        def mult(v: np.ndarray) -> np.ndarray:
            return schur.dot(v)

        PinvS = LinearOperator(dtype=schur.dtype, shape=schur.shape, matvec=mult)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
                                     maxiter=schur.shape[0] * 50, ncv=schur.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=schur.shape[0] * 100,
                                     tol=1E-9, ncv=schur.shape[0] * 30)
        small = min(np.real(w))
        reg_cond.append(large / small)

    fil = open('schur_complement_reg.data', 'w')
    print("p\tCond", file=fil)
    counter = 3
    for item in reg_cond:
        print("%d\t%s" %(counter, item), file=fil)
        counter += 1
    fil.close()

    # And now do the preconditioned version
    pre_cond = list()
    for order in range(3, 21):
        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, order)
        fem = BernsteinMatrix(mesh)
        _, mass, _ = fem.assemble_matrix()

        schur = SchurAction(mass, fem)
        p = SchurPreconditioner(schur, fem)

        def mult(v: np.ndarray) -> np.ndarray:
            return p.solve(schur.dot(v))

        PinvS = LinearOperator(dtype=schur.dtype, shape=schur.shape, matvec=mult)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
                                     maxiter=schur.shape[0] * 50, ncv=schur.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=schur.shape[0] * 100,
                                     tol=1E-9, ncv=schur.shape[0] * 30)
        small = min(np.real(w))
        pre_cond.append(large / small)

    fil = open('schur_complement_pre.data', 'w')
    print("p\tCond", file=fil)
    counter = 3
    for item in pre_cond:
        print("%d\t%s" %(counter, item), file=fil)
        counter += 1
    fil.close()
