import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj
from scipy.integrate import quad
from scipy.sparse import csr_matrix


# Helper Functions
def xi(x, k):
    return (-1) ** (k + 1) / k * (1 - x) / 2 * jac(k - 1, 1, 1, x)


def phi(x, m, q):
    return (-1) ** q / (q + 1) * jac(q, m, 1, x)


# Nodal basis on interval
def f(x, p):
    # phat = int(np.floor(p / 2))
    # return (1 - x) / 2 * (phi(x, 2, phat) + ((1 - x) / 2) ** (phat) * phi(x, 2 * phat + 3, p - phat - 1)) / 2

    i = int(np.floor(p / 2))
    j = int(np.floor(i / 2))
    return (1 - x) / 2 * (
            phi(x, 2, i) + ((1 - x) / 2) ** (i + 1) * phi(x, 2 * i + 3, j) + ((1 - x) / 2) ** (i + j + 1) *
            phi(x, 2 * i + 2 * j + 4, p - 1 - i - j)) / 3


# Interior basis on interval
def g(x, i):
    return (1 - x) * (1 + x) * jac(i, 2, 2, x)


if __name__ == '__main__':
    for p in range(4, 50, 4):

        # Create list of dofs
        func_dofs = list()

        # Add the nodal basis functions
        for i in range(2):
            for j in range(2):
                for k in range(2):
                    func_dofs.append((i, j, k))

        # Now add the edge basis functions
        for i in range(2):
            for j in range(2):
                for k in range(p - 1):
                    func_dofs.append((i, j, k + 2))

        for i in range(2):
            for j in range(2):
                for k in range(p - 1):
                    func_dofs.append((i, k + 2, j))

        for i in range(2):
            for j in range(2):
                for k in range(p - 1):
                    func_dofs.append((k + 2, i, j))

        # Add face functions
        for i in range(2):
            for j in range(p - 1):
                for k in range(p - 1):
                    func_dofs.append((i, j + 2, k + 2))

        for i in range(2):
            for j in range(p - 1):
                for k in range(p - 1):
                    func_dofs.append((j + 2, i, k + 2))

        for i in range(2):
            for j in range(p - 1):
                for k in range(p - 1):
                    func_dofs.append((j + 2, k + 2, i))

        # Finally add the interior basis
        for i in range(p - 1):
            for j in range(p - 1):
                for k in range(p - 1):
                    func_dofs.append((i + 2, j + 2, k + 2))

        # Now, we create the 1D mass matrix for easy look up
        interval_functions = list()
        interval_functions.append(lambda x: f(x, p))
        interval_functions.append(lambda x: f(-x, p))
        for i in range(p - 1):
            interval_functions.append(lambda x, i=i: g(x, i))

        interval_mass = np.zeros((p + 1, p + 1))
        for i in range(p + 1):
            for j in range(p + 1):
                interval_mass[i, j] = quad(lambda x: interval_functions[i](x) * interval_functions[j](x), -1, 1)[0]

        # Construct mass matrix on hex
        M = np.zeros(((p + 1) * (p + 1) * (p + 1), (p + 1) * (p + 1) * (p + 1)))
        # M = csr_matrix(((p + 1) * (p + 1) * (p + 1), (p + 1) * (p + 1) * (p + 1)))

        ns = 8 + 12 * (p - 1) + 6 * (p - 1) * (p - 1)
        for index_i in range(ns):
            for index_j in range(index_i, ns):
                i = func_dofs[index_i]
                j = func_dofs[index_j]
                # print(index_i, index_j, interval_mass[i[0], j[0]] * interval_mass[i[1], j[1]] * interval_mass[i[2], j[2]])
                M[index_i, index_j] = interval_mass[i[0], j[0]] * interval_mass[i[1], j[1]] * interval_mass[i[2], j[2]]
                M[index_j, index_i] = M[index_i, index_j]

        for index_i in range(ns, len(func_dofs)):
            for index_j in range(ns):
                i = func_dofs[index_i]
                j = func_dofs[index_j]
                # print(index_i, index_j, interval_mass[i[0], j[0]] * interval_mass[i[1], j[1]] * interval_mass[i[2], j[2]])
                M[index_i, index_j] = interval_mass[i[0], j[0]] * interval_mass[i[1], j[1]] * interval_mass[i[2], j[2]]
                M[index_j, index_i] = M[index_i, index_j]

        for index_i in range(ns, len(func_dofs)):
            i = func_dofs[index_i]
            j = func_dofs[index_i]
            # print(index_i, index_j, interval_mass[i[0], j[0]] * interval_mass[i[1], j[1]] * interval_mass[i[2], j[2]])
            M[index_i, index_i] = interval_mass[i[0], j[0]] * interval_mass[i[1], j[1]] * interval_mass[i[2], j[2]]

        A = M[0:ns, 0:ns]
        B = M[0:ns, ns:]
        C = 1 / np.diag(M[ns:, ns:])
        # C = 1 / np.diag(M[ns:, ns:].todense())
        S = A - B.dot(np.diag(C).dot(B.T))

        # Create preconditioner
        P = np.zeros_like(S)
        # P[0:4, 0:4] = np.eye(4) * 16 / (p ** 4)

        # Make the block jacobi preconditioner
        # for i in range(8):
        #     P[i, i] = S[i, i]
        # # P[0:8, 0:8] = S[0:8, 0:8]
        #
        # ind = 8
        # for i in range(12):
        #     P[ind:ind + (p - 1), ind:ind + (p - 1)] = S[ind:ind + (p - 1), ind:ind + (p - 1)]
        #     ind += p - 1
        #
        # num_dofs_face = (p - 1) ** 2
        # for j in range(6):
        #     print(S[ind:ind + num_dofs_face, ind:ind + num_dofs_face])
        #     P[ind:ind + num_dofs_face, ind:ind + num_dofs_face] = S[ind:ind + num_dofs_face, ind:ind + num_dofs_face]
        #     ind += num_dofs_face
        P = np.diag(np.diag(S))
        # print(S[0:4, 0:4])
        # # print(np.diag(S[4:4 + (p - 1), 4:4 + (p-1)]))
        #
        # P[4:4 + (p - 1), 4:4 + (p - 1)] = S[4:4 + (p - 1), 4:4 + (p - 1)]
        # P[4 + (p - 1):4 + 2 * (p - 1), 4 + (p - 1):4 + 2 * (p - 1)] = S[4 + (p - 1):4 + 2 * (p - 1),
        #                                                               4 + (p - 1):4 + 2 * (p - 1)]
        # P[4 + 2 * (p - 1):4 + 3 * (p - 1), 4 + 2 * (p - 1):4 + 3 * (p - 1)] = S[4 + 2 * (p - 1):4 + 3 * (p - 1),
        #                                                                       4 + 2 * (p - 1):4 + 3 * (p - 1)]
        # P[4 + 3 * (p - 1):4 + 4 * (p - 1), 4 + 3 * (p - 1):4 + 4 * (p - 1)] = S[4 + 3 * (p - 1):4 + 4 * (p - 1),
        #                                                                       4 + 3 * (p - 1):4 + 4 * (p - 1)]
        #
        # # print(S[4 + (p-1):4 + 2 * (p-1), 4 + (p-1): 4 + 2 * (p-1)])
        eigs = np.real(np.linalg.eigvals(np.linalg.solve(P, S)))
        #
        print(p, max(eigs) / min(eigs), max(eigs), min(eigs))
