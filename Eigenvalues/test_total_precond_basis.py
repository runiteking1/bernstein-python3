from unittest import TestCase
from basis_total_precond import *
import numpy as np

class TestGrads(TestCase):
    def test_phi1_grad(self):
        np.testing.assert_almost_equal(grad_phi1([-.1, -.1], 5), (0.2175, 0.2175), decimal=5)
    def test_phi2_grad(self):
        np.testing.assert_almost_equal(grad_phi2([-.1, -.1], 5), (0.206656, 0), decimal=5)
    def test_phi3_grad(self):
        np.testing.assert_almost_equal(grad_phi3([-.1, -.1], 5), (0, 0.206656), decimal=5)
    def test_psi_grad(self):
        np.testing.assert_almost_equal(grad_psi([-.1, -.1], 2, 3),(-0.00427781, 0.225813), decimal=5)
    def test_chi3_grad(self):
        np.testing.assert_almost_equal(grad_chi1([-.1, -.1], 5), (3.62365, -.547876), decimal=5)

    def test_chi2_grad(self):
        np.testing.assert_almost_equal(grad_chi2([-.1, -.1], 5), (-0.547876, -1.35313), decimal=5)

    def test_chi1_grad(self):
        np.testing.assert_almost_equal(grad_chi3([-.1, -.1], 5), (1.35313, 0.547876), decimal=5)