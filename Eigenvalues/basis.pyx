#cython: boundscheck=False, wraparound=False, cdivision=True
# import numpy as np
# def l1(double[::1] x):
#     return (-x[0] - x[1]) / 2
#
# def grad_l1(double[::1] x):
#     return (-.5, -.5)
#
# def l2(double[::1] x):
#     return (1 + x[0]) / 2
#
# def grad_l2(double[::1] x):
#     return (.5, 0)
#
# def l3(double[::1] x):
#     return (1 + x[1]) / 2
#
# def grad_l3(double[::1] x):
#     return (0, .5)

def sum_quads(double[:, ::1] func1, double[:, ::1] func2, double[::1] w00, double[::1] w10):
    cdef double total = 0, inner_total = 0
    for i in range(len(w00)):

        inner_total = 0
        for j in range(len(w10)):
            inner_total += w10[j]/2 * func1[i, j] * func2[i, j]

        total += w00[i] * inner_total

    return total
