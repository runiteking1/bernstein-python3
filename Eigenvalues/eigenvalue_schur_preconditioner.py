import numpy as np
import scipy
from scipy.sparse.linalg import LinearOperator

from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from schur_complement import SchurAction
from schur_preconditioner import SchurPreconditioner

if __name__ == '__main__':
    # 1 Triangles
    # coords_tri = np.array([[0, -1], [-1, 1], [1, -1]])
    coords_tri = np.array([[0, 0], [1, 0], [0, 1]])

    elnode_tri = np.array([[0, 1, 2]])
    bc_tri = np.array([])

    np.set_printoptions(linewidth=200, precision=5)
    for order in range(3, 21):
        print(order, end = '\t')
        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, order, subdiv=0)

        fem = BernsteinMatrix(mesh)
        _, mass, _ = fem.assemble_matrix()

        schur = SchurAction(mass, fem)
        p = SchurPreconditioner(schur, fem)

        def mult(v: np.ndarray) -> np.ndarray:
            return p.solve(schur.dot(v))

        PinvS = LinearOperator(dtype=schur.dtype, shape=schur.shape, matvec=mult)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
                                     maxiter=schur.shape[0] * 50, ncv=schur.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=schur.shape[0] * 100,
                                     tol=1E-9, ncv=schur.shape[0] * 30)
        small = min(np.real(w))
        print(large / small, end='\t')

        def mult(v: np.ndarray) -> np.ndarray:
            return schur.dot(v)

        PinvS = LinearOperator(dtype=schur.dtype, shape=schur.shape, matvec=mult)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-7,
                                     maxiter=schur.shape[0] * 50, ncv=schur.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=schur.shape[0] * 100,
                                     tol=1E-9, ncv=schur.shape[0] * 30)
        small = min(np.real(w))
        print(large / small)

