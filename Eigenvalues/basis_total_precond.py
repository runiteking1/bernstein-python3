import numpy as np
from scipy.special import eval_jacobi as jac, roots_jacobi as rj, binom, eval_chebyt as cheb, eval_chebyu as chebu
from typing import Callable
import matplotlib.pyplot as plt
# from basis import l1, grad_l1, l2, grad_l2, l3, grad_l3
from basis import sum_quads


# Barycentric coorrdinates
def l1(x):
    return (-x[0] - x[1]) / 2


def grad_l1(x):
    return (-.5, -.5)


def l2(x):
    return (1 + x[0]) / 2


def grad_l2(x):
    return (.5, 0)


def l3(x):
    return (1 + x[1]) / 2


def grad_l3(x):
    return (0, .5)


# Vertex functions
def phi1(x, p: int):
    return (-1) ** (p + 1) / p * l1(x) * jac(p - 1, 1, 1, 1 - 2 * l1(x))


def grad_phi1(x, p: int):
    return (
        (-1) ** p * (
                (2 + p) * (x[0] + x[1]) * jac(p - 2, 2, 2, 1 - 2 * l1(x)) + 2 * jac(p - 1, 1, 1, 1 - 2 * l1(x))) / (
                4 * p),
        (-1) ** p * (
                (2 + p) * (x[0] + x[1]) * jac(p - 2, 2, 2, 1 - 2 * l1(x)) + 2 * jac(p - 1, 1, 1, 1 - 2 * l1(x))) / (
                4 * p))


def phi2(x, p: int):
    return (-1) ** (p + 1) / p * l2(x) * jac(p - 1, 1, 1, 1 - 2 * l2(x))


def grad_phi2(x, p: int):
    return ((-1) ** p * ((2 + p) * (x[0] + 1) * jac(p - 2, 2, 2, -x[0]) - 2 * jac(p - 1, 1, 1, - x[0])) / (4 * p),
            0)


def phi3(x, p: int):
    return (-1) ** (p + 1) / p * l3(x) * jac(p - 1, 1, 1, 1 - 2 * l3(x))


def grad_phi3(x, p: int):
    return (0,
            (-1) ** p * ((2 + p) * (1 + x[1]) * jac(p - 2, 2, 2, - x[1]) - 2 * jac(p - 1, 1, 1, - x[1])) / (4 * p))


# Chebyshev vertex functions
def phi_dagger(x, p: int):
    """
    Interesting chebyshev basis

    :param x:
    :param p:
    :return:
    """
    odd_factor = np.sum(1 / (np.arange(1, p + 1, 2) + 1))
    even_factor = np.sum(1 / (np.arange(0, p + 1, 2) + 1))

    odd_part = 0
    even_part = 0
    for i in range(1, p + 1, 2):
        odd_part += 1 / (i + 1) * cheb(i, x)
    for i in range(0, p + 1, 2):
        even_part += 1 / (i + 1) * cheb(i, x)

    return .5 * (even_part / even_factor - odd_part / odd_factor)


def deriv_phi_dagger(x, p):
    """
    Derivative of the phi_dagger; needed for the

    :param x:
    :param p:
    :return:
    """
    odd_factor = np.sum(1 / (np.arange(1, p + 1, 2) + 1))
    even_factor = np.sum(1 / (np.arange(0, p + 1, 2) + 1))

    odd_part = 0
    even_part = 0
    for i in range(1, p + 1, 2):
        odd_part += 1 / (i + 1) * i * chebu(i - 1, x)
    for i in range(0, p + 1, 2):
        even_part += 1 / (i + 1) * i * chebu(i - 1, x)

    return .5 * (even_part / even_factor - odd_part / odd_factor)


def phidagger1(x, p):
    return phi_dagger(1 - 2 * l1(x), p)


def grad_phidagger1(x, p):
    return (deriv_phi_dagger(1 - 2 * l1(x), p), deriv_phi_dagger(1 - 2 * l1(x), p))


def phidagger2(x, p):
    return phi_dagger(1 - 2 * l2(x), p)


def grad_phidagger2(x, p):
    return (-deriv_phi_dagger(1 - 2 * l2(x), p), 0)


def phidagger3(x, p):
    return phi_dagger(1 - 2 * l3(x), p)


def grad_phidagger3(x, p):
    return (0, -deriv_phi_dagger(1 - 2 * l3(x), p))


def bern1(x, p):
    return l1(x)**p

def grad_bern1(x, p):
    return (-p * l1(x)**(p-1)/2, -p * l1(x)**(p-1)/2)

def bern2(x, p):
    return l2(x)**p

def grad_bern2(x, p):
    return (p * l2(x)**(p-1)/2, 0)

def bern3(x, p):
    return l3(x)**p

def grad_bern3(x, p):
    return (0, p * l3(x)**(p-1)/2)

# Interior functions
def psi(a, i: int, j: int):
    x, y = a
    return 2 ** (-3 - i) * (1 + x) * (2 - (2 * (1 + x)) / (1 - y)) * (1 - y) ** i * (1 + y) * jac(-1 + i, 1, 1,
                                                                                                  -1 + (2 * (1 + x)) / (
                                                                                                          1 - y)) * jac(
        -1 + j, 1 + 2 * i, 1, y)
    # return -2 ** (-2 - i) * (1 + x[0]) * (1 - x[1]) ** (-1 + i) * (1 + x[1]) * (x[0] + x[1]) * \
    #        jac(i - 1, 2, 2, (1 + 2 * x[0] + x[1]) / (1 - x[1])) * jac(-1 + j, 3 + 2 * i, 2, x[1])


def grad_psi(x, i: int, j: int):
    a, b = x

    return (2 ** (-2 - i) * (1 - b) ** (-2 + i) * (1 + b) * (
            -((2 + i) * (1 + a) * (a + b) * jac(-2 + i, 2, 2, (1 + 2 * a + b) / (1 - b))) +
            (-1 + b) * (1 + 2 * a + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b))) * jac(-1 + j, 1 + 2 * i, 1, b),
            -(2 ** (-3 - i) * (1 + a) * (1 - b) ** (-3 + i) * (
                    (2 + 2 * i + j) * (-1 + b) ** 2 * (1 + b) * (a + b) * jac(-1 + i, 1, 1,
                                                                              (1 + 2 * a + b) / (1 - b)) * jac(-2 + j,
                                                                                                               2 + 2 * i,
                                                                                                               2, b) +
                    2 * (2 + i) * (1 + a) * (1 + b) * (a + b) * jac(-2 + i, 2, 2, (1 + 2 * a + b) / (1 - b)) * jac(
                -1 + j, 1 + 2 * i, 1, b) -
                    2 * (1 + a) * (-1 + b) * (1 + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b)) * jac(-1 + j,
                                                                                                          1 + 2 * i, 1,
                                                                                                          b) +
                    2 * (-1 + b) ** 2 * (a + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b)) * jac(-1 + j, 1 + 2 * i,
                                                                                                     1, b) +
                    2 * i * (-1 + b) * (1 + b) * (a + b) * jac(-1 + i, 1, 1, (1 + 2 * a + b) / (1 - b)) * jac(-1 + j,
                                                                                                              1 + 2 * i,
                                                                                                              1, b))))

    # return ((2 ** (-2 - i) * (1 - b) ** (-2 + i) * (1 + b) * (
    #         (1 + a) * (a + b) * (-4 - i) * jac(-2 + i, 3, 3, (1 + 2 * a + b) / (1 - b)) +
    #         (-1 + b) * (1 + 2 * a + b) * jac(-1 + i, 2, 2, (1 + 2 * a + b) / (1 - b))) * jac(-1 + j, 3 + 2 * i, 2, b),
    #          -(2 ** (-3 - i) * (1 + a) * (1 - b) ** (-3 + i) * (
    #                  2 * (1 + a) * (1 + b) * (a + b) * (4 + i) * jac(-2 + i, 3, 3, (1 + 2 * a + b) / (1 - b)) * jac(
    #              -1 + j, 3 + 2 * i, 2, b) +
    #                  (-1 + b) * jac(-1 + i, 2, 2, (1 + 2 * a + b) / (1 - b)) * (
    #                          (a + b) * (-1 + b ** 2) * (5 + 2 * i + j) * jac(-2 + j, 4 + 2 * i, 3, b) +
    #                          2 * (-1 + a * (-2 + i + b * i) + b * (-2 + b + i + b * i)) * jac(-1 + j, 3 + 2 * i, 2,
    #                                                                                           b))))))


# Edge functions
def chi3(x, p, n):
    # Bern
    # n = n + 1
    # return binom(p, n) * l2(x) ** (p - n) * l3(x) ** (n)

    # Scaled Bern
    # n = n + 1
    # return l2(x) ** (p - n) * l3(x) ** (n)

    # Hier
    return (1 + x[0]) * (1 + x[1]) * jac(n, 2, 2, x[0])


def grad_chi3(x, p, n):
    a, b = x

    # Bern
    # n = n + 1
    # return (((1 + a) ** (-1 - n + p) * (1 + b) ** n * (-n + p) * binom(p, n)) / 2 ** p,
    #         ((1 + a) ** (-n + p) * (1 + b) ** (-1 + n) * n * binom(p, n)) / 2 ** p)

    # Scaled Bern
    # n = n + 1
    # return (((1 + a) ** (-1 - n + p) * (1 + b) ** n * (-n + p) ) / 2 ** p,
    #         ((1 + a) ** (-n + p) * (1 + b) ** (-1 + n) * n ) / 2 ** p)

    # Heir
    return (
        ((1 + b) * ((1 + a) * (5 + n) * jac(-1 + n, 3, 3, a) + 2 * jac(n, 2, 2, a))) / 2., (1 + a) * jac(n, 2, 2, a))


def chi2(x, p, n):
    # Bern
    # n = n+1
    # return binom(p, n) * l1(x) ** (p - n) * l3(x) ** n

    # Scaled Bern
    # n = n+1
    # return l1(x) ** (p - n) * l3(x) ** n

    # Heir
    return -(1 + x[1]) * (x[0] + x[1]) * jac(n, 2, 2, -x[1])


def grad_chi2(x, p, n):
    a, b = x
    # Bern
    # n = n+1
    # return (-(((-a - b) ** (-1 - n + p) * (1 + b) ** n * (-n + p) * binom(p, n)) / 2 ** p),
    #         -(((-a - b) ** (-1 - n + p) * (1 + b) ** (-1 + n) * ((-1 + a) * n + (1 + b) * p) * binom(p, n)) / 2 ** p))

    # Scaled Bern
    # n = n+1
    # return (-(((-a - b) ** (-1 - n + p) * (1 + b) ** n * (-n + p) ) / 2 ** p),
    #         -(((-a - b) ** (-1 - n + p) * (1 + b) ** (-1 + n) * ((-1 + a) * n + (1 + b) * p)) / 2 ** p))

    # Hier
    # return (-((1 + b) * jac(n, 2, 2, -b)),
    #      ((1 + b) * (a + b) * (5 + n) * jac(-1 + n, 3, 3, -b)) / 2. - (1 + a + 2 * b) * jac(n, 2, 2, -b))
    return (-((1 + b) * jac(n, 2, 2, -b)),
            ((1 + b) * (a + b) * (5 + n) * jac(-1 + n, 3, 3, -b)) / 2. - (1 + b) * jac(n, 2, 2, -b) - (a + b) * jac(n,
                                                                                                                    2,
                                                                                                                    2,
                                                                                                                    -b))


def chi1(x, p, n):
    # Bern
    # n = n+1
    # return binom(p, n) * l1(x) ** (p - n) * l2(x) ** n

    # Scaled bern
    # n = n+1
    # return l1(x) ** (p - n) * l2(x) ** n

    # Heir
    return -(1 + x[0]) * (x[0] + x[1]) * jac(n, 2, 2, x[0])


def grad_chi1(x, p, n):
    a, b = x

    # Bern
    # n = n + 1
    # return (-(((1 + a) ** (-1 + n) * (-a - b) ** (-1 - n + p) * ((-1 + b) * n + (1 + a) * p) * binom(p, n)) / 2 ** p),
    #         -(((1 + a) ** n * (-a - b) ** (-1 - n + p) * (-n + p) * binom(p, n)) / 2 ** p))

    # Scaled Bern
    # n = n + 1
    # return (-(((1 + a) ** (-1 + n) * (-a - b) ** (-1 - n + p) * ((-1 + b) * n + (1 + a) * p) ) / 2 ** p),
    #         -(((1 + a) ** n * (-a - b) ** (-1 - n + p) * (-n + p) ) / 2 ** p))

    # Heir
    return (
        -((1 + a) * (a + b) * (5 + n) * jac(-1 + n, 3, 3, a)) / 2. - (1 + a) * jac(n, 2, 2, a) - (a + b) * jac(n, 2, 2,
                                                                                                               a),
        -((1 + a) * jac(n, 2, 2, a)))
    # return (-((1 + a)*(a + b)*(5 + n)*jac(-1 + n,3,3,a))/2. - (1 + 2*a + b)*jac(n,2,2,a),-((1 + a)*jac(n,2,2,a)))


def quad(func, x00, w00):
    return np.dot(func(x00), w00)


def find_edge_coefs_bottom(p, edge_func_list, x00, w00):
    # First need to create the mass matrix
    # Then need to create the rhs
    mass = np.zeros((p - 1, p - 1))
    forward_rhs = np.zeros((p - 1,))
    backward_rhs = np.zeros((p - 1,))

    for i in range(len(edge_func_list)):
        # forward_rhs[i] = quad(lambda x: (phi1([x, -1], np.floor(p / 2)) - (1 - x) / 2) * edge_func_list[i]([x, -1]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda x: (phi2([x, -1], np.floor(p / 2)) - (1 + x) / 2) * edge_func_list[i]([x, -1]),
        #                        x00, w00)

        forward_rhs[i] = quad(lambda x: (phi1([x, -1], (p )) - (1 - x) / 2) * edge_func_list[i]([x, -1]),
                              x00, w00)
        backward_rhs[i] = quad(lambda x: (phi2([x, -1], (p)) - (1 + x) / 2) * edge_func_list[i]([x, -1]),
                               x00, w00)

        # forward_rhs[i] = quad(lambda x: (bern1([x, -1], p) - (1 - x) / 2) * edge_func_list[i]([x, -1]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda x: (bern2([x, -1], p ) - (1 + x) / 2) * edge_func_list[i]([x, -1]),
        #                        x00, w00)

        # forward_rhs[i] = quad(lambda x: (phidagger1([x, -1], p) - (1 - x) / 2) * edge_func_list[i]([x, -1]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda x: (phidagger2([x, -1], p) - (1 + x) / 2) * edge_func_list[i]([x, -1]),
        #                        x00, w00)

        for j in range(len(edge_func_list)):
            mass[i, j] = quad(lambda x: edge_func_list[j]([x, -1]) * edge_func_list[i]([x, -1]), x00, w00)
            mass[j, i] = mass[i, j]

    return np.linalg.solve(mass, forward_rhs), np.linalg.solve(mass, backward_rhs)


def find_edge_coefs_left(p, edge_func_list, x00, w00):
    # First need to create the mass matrix
    # Then need to create the rhs
    mass = np.zeros((p - 1, p - 1))
    forward_rhs = np.zeros((p - 1,))
    backward_rhs = np.zeros((p - 1,))

    for i in range(len(edge_func_list)):
        # forward_rhs[i] = quad(lambda y: (phi1([-1, y], np.floor(p / 2)) - (1 - y) / 2) * edge_func_list[i]([-1, y]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda y: (phi3([-1, y], np.floor(p / 2)) - (1 + y) / 2) * edge_func_list[i]([-1, y]),
        #                        x00, w00)
        forward_rhs[i] = quad(lambda y: (phi1([-1, y], (p )) - (1 - y) / 2) * edge_func_list[i]([-1, y]),
                              x00, w00)
        backward_rhs[i] = quad(lambda y: (phi3([-1, y], (p )) - (1 + y) / 2) * edge_func_list[i]([-1, y]),
                               x00, w00)
        # forward_rhs[i] = quad(lambda y: (bern1([-1, y], p) - (1 - y) / 2) * edge_func_list[i]([-1, y]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda y: (bern3([-1, y], p) - (1 + y) / 2) * edge_func_list[i]([-1, y]),
        #                        x00, w00)

        # forward_rhs[i] = quad(lambda y: (phidagger1([-1, y], p) - (1 - y) / 2) * edge_func_list[i]([-1, y]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda y: (phidagger3([-1, y], p) - (1 + y) / 2) * edge_func_list[i]([-1, y]),
        #                        x00, w00)
        #
        for j in range(len(edge_func_list)):
            mass[i, j] = quad(lambda y: edge_func_list[j]([-1, y]) * edge_func_list[i]([-1, y]), x00, w00)
            mass[j, i] = mass[i, j]

    return np.linalg.solve(mass, forward_rhs), np.linalg.solve(mass, backward_rhs)


def find_edge_coefs_tright(p, edge_func_list, x00, w00):
    # First need to create the mass matrix
    # Then need to create the rhs
    mass = np.zeros((p - 1, p - 1))
    forward_rhs = np.zeros((p - 1,))
    backward_rhs = np.zeros((p - 1,))

    for i in range(len(edge_func_list)):
        # forward_rhs[i] = quad(lambda x: (phi2([x, -x], np.floor(p / 2)) - l2([x, -x])) * edge_func_list[i]([x, -x]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda x: (phi3([x, -x], np.floor(p / 2)) - l3([x, -x])) * edge_func_list[i]([x, -x]),
        #                        x00, w00)
        forward_rhs[i] = quad(lambda x: (phi2([x, -x], p ) - l2([x, -x])) * edge_func_list[i]([x, -x]),
                              x00, w00)
        backward_rhs[i] = quad(lambda x: (phi3([x, -x], (p)) - l3([x, -x])) * edge_func_list[i]([x, -x]),
                               x00, w00)
        # forward_rhs[i] = quad(lambda x: (bern2([x, -x], p ) - l2([x, -x])) * edge_func_list[i]([x, -x]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda x: (bern3([x, -x], p) - l3([x, -x])) * edge_func_list[i]([x, -x]),
        #                        x00, w00)

        # forward_rhs[i] = quad(lambda x: (phidagger2([x, -x], p) - l2([x, -x])) * edge_func_list[i]([x, -x]),
        #                       x00, w00)
        # backward_rhs[i] = quad(lambda x: (phidagger3([x, -x], p) - l3([x, -x])) * edge_func_list[i]([x, -x]),
        #                        x00, w00)

        for j in range(len(edge_func_list)):
            mass[i, j] = quad(lambda x: edge_func_list[j]([x, -x]) * edge_func_list[i]([x, -x]), x00, w00)
            mass[j, i] = mass[i, j]

    return np.linalg.solve(mass, forward_rhs), np.linalg.solve(mass, backward_rhs)


if __name__ == '__main__':
    np.set_printoptions(precision=4, linewidth=200)

    param_vertex = 'star'  # Choices are star, L and dagger, choose L for the good preconditioner and change the find_edge_coefs functions above

    kappa = 0.5

    print("Kappa: ", kappa, " param vertex ", param_vertex)
    # Create barycentric lists
    start = 35
    stop =  81
    values = np.append(np.arange(5, 31, 5), np.arange(35, 111, 5))
    # print("Ranges are", start, stop)
    print(param_vertex)
    for p in values:
        print(p, end=' ')

        # Extract quadrature points for triangle quadrature; see Sherwin book
        x00, w00 = rj(p + 2, 0, 0)
        x10, w10 = rj(p + 2, 1, 0)

        # Create mass stiffness matrix
        M = np.zeros((int((p + 1) * (p + 2) / 2), int((p + 1) * (p + 2) / 2)))
        S = np.zeros((int((p + 1) * (p + 2) / 2), int((p + 1) * (p + 2) / 2)))

        # Create list of lambda functions
        func_dofs_mass = list()
        func_dofs_stiff = list()

        if param_vertex == 'star':
            # Mass
            func_dofs_mass.append(
                lambda x: phi1(x, np.floor(p / 2)))  # Very odd (blog about this) see solution on edges
            func_dofs_mass.append(lambda x: phi2(x, np.floor(p / 2)))
            func_dofs_mass.append(lambda x: phi3(x, np.floor(p / 2)))

            # Stiffness
            func_dofs_stiff.append(
                lambda x: grad_phi1(x, np.floor(p / 2)))  # Very odd (blog about this) see solution on edges
            func_dofs_stiff.append(lambda x: grad_phi2(x, np.floor(p / 2)))
            func_dofs_stiff.append(lambda x: grad_phi3(x, np.floor(p / 2)))

        elif param_vertex == 'L':
            func_dofs_mass.append(lambda x: l1(x))  # Very odd (blog about this) see solution on edges
            func_dofs_mass.append(lambda x: l2(x))
            func_dofs_mass.append(lambda x: l3(x))

            func_dofs_stiff.append(lambda x: grad_l1(x))  # Very odd (blog about this) see solution on edges
            func_dofs_stiff.append(lambda x: grad_l2(x))
            func_dofs_stiff.append(lambda x: grad_l3(x))

        elif param_vertex == 'dagger':
            func_dofs_mass.append(lambda x: phidagger1(x, p))  # Very odd (blog about this) see solution on edges
            func_dofs_mass.append(lambda x: phidagger2(x, p))
            func_dofs_mass.append(lambda x: phidagger3(x, p))

            func_dofs_stiff.append(lambda x: grad_phidagger1(x, p))  # Very odd (blog about this) see solution on edges
            func_dofs_stiff.append(lambda x: grad_phidagger2(x, p))
            func_dofs_stiff.append(lambda x: grad_phidagger3(x, p))

        else:
            raise Exception('Ahhhhhh')

        for i in range(p - 1):
            func_dofs_mass.append(lambda x, i=i: chi1(x, p, i))
            func_dofs_stiff.append(lambda x, i=i: grad_chi1(x, p, i))
        for i in range(p - 1):
            func_dofs_mass.append(lambda x, i=i: chi2(x, p, i))
            func_dofs_stiff.append(lambda x, i=i: grad_chi2(x, p, i))
        for i in range(p - 1):
            func_dofs_mass.append(lambda x, i=i: chi3(x, p, i))
            func_dofs_stiff.append(lambda x, i=i: grad_chi3(x, p, i))

        for i in range(1, p - 1):
            for j in range(1, p - i):
                func_dofs_mass.append(lambda x, i=i, j=j: psi(x, i, j))
                func_dofs_stiff.append(lambda x, i=i, j=j: grad_psi(x, i, j))

        # Make the change of variables matrix
        cps, back_cps = find_edge_coefs_bottom(p, func_dofs_mass[3:(3 + p - 1)], x00, w00)
        left_cps, left_back_cps = find_edge_coefs_left(p, func_dofs_mass[3 + p - 1:(3 + 2 * (p - 1))], x00, w00)
        right_cps, right_back_cps = find_edge_coefs_tright(p, func_dofs_mass[3 + 2 * (p - 1):(3 + 3 * (p - 1))], x00,
                                                           w00)

        # print(cps)
        gamma = np.zeros((3, 3 * (p - 1)))
        gamma[0, 0:p - 1] = cps
        gamma[0, p - 1:2 * (p - 1)] = left_cps  # back_cps

        gamma[1, 0:p - 1] = back_cps
        gamma[1, 2 * (p - 1):] = right_cps

        gamma[2, p - 1:2 * (p - 1)] = left_back_cps
        gamma[2, 2 * (p - 1):] = right_back_cps

        transform = np.block([[np.eye(3), gamma], [np.zeros((3 * (p - 1), 3)), np.eye(3 * (p - 1))]])

        # Make large arrays which stores quadrature points of functions
        mass_points = np.zeros((len(func_dofs_mass), len(w00), len(w10)))
        stiff_points_1 = np.zeros((len(func_dofs_mass), len(w00), len(w10)))
        stiff_points_2 = np.zeros((len(func_dofs_mass), len(w00), len(w10)))
        for func in range(len(func_dofs_mass)):
            for i in range(len(w00)):
                mass_points[func, i, :] = func_dofs_mass[func]([(1 + x00[i]) * (1 - x10) / 2 - 1, x10])
                stiff_points_1[func, i, :], stiff_points_2[func, i, :] = func_dofs_stiff[func](
                    [(1 + x00[i]) * (1 - x10) / 2 - 1, x10])

        for i in (range(M.shape[0])):
            for j in range(i, M.shape[0]):
                M[i, j] = sum_quads(mass_points[i], mass_points[j], w00, w10)
                M[j, i] = M[i, j]

                S[i, j] = sum_quads(stiff_points_1[i], stiff_points_1[j], w00, w10) + sum_quads(stiff_points_2[i],
                                                                                                stiff_points_2[j], w00,
                                                                                                w10)
                S[j, i] = S[i, j]
        # print(S)
        # Take the sum of the two
        A = (1-kappa)*S + kappa*M
        # A = S
        # A = 10 * S + .001 * M

        # Calculate Schur complement
        a = A[0:3 * p, 0:3 * p]
        b = A[0:3 * p, 3 * p:]
        c = A[3 * p:, 3 * p:]
        schur = a - b @ np.linalg.solve(c, b.T)
        # print(np.diag(schur[0:3, 0:3]))
        # print(transform @ schur @ transform.T)
        # print(schur)
        # d = np.block([[np.linalg.inv(np.diag(np.diag((transform @ schur @ transform.T)[0:3, 0:3]))), np.zeros((3, 3 * (p - 1)))],
        #               [np.zeros((3 * (p - 1), 3)), np.eye(3 * (p - 1))]])
        d = np.block(
            [[np.linalg.inv(np.diag(np.diag((transform @ schur @ transform.T)[0:3, 0:3]))), np.zeros((3, 3 * (p - 1)))],
             [np.zeros((3 * (p - 1), 3)), np.zeros((3 * (p - 1), 3 * (p - 1)))]])
        # print(np.diag((transform @ schur @ transform.T)[0:3, 0:3]))
        # d = np.block([[np.linalg.inv((transform @ schur @ transform.T)[0:3, 0:3]), np.zeros((3, 3 * (p - 1)))],
        #               [np.zeros((3 * (p - 1), 3)), np.eye(3 * (p - 1))]])

        # print(transform @ schur @ transform.T)
        # Create preconditioner
        linear_and_edges = np.zeros_like(schur)
        linear_and_edges[0:3, 0:3] = np.linalg.inv(schur[0:3, 0:3])
        linear_and_edges[3:3 + (p - 1), 3:3 + (p - 1)] = np.linalg.inv(schur[3:3 + (p - 1), 3:3 + (p - 1)])
        linear_and_edges[3 + (p - 1):3 + 2 * (p - 1), 3 + (p - 1):3 + 2 * (p - 1)] = np.linalg.inv(
            schur[3 + (p - 1):3 + 2 * (p - 1),
            3 + (p - 1):3 + 2 * (p - 1)])
        linear_and_edges[3 + 2 * (p - 1):3 + 3 * (p - 1), 3 + 2 * (p - 1):3 + 3 * (p - 1)] = np.linalg.inv(
            schur[3 + 2 * (p - 1):3 + 3 * (p - 1),
            3 + 2 * (p - 1):3 + 3 * (p - 1)])
        # linear_and_edges[3:, 3:] = np.linalg.inv(schur[3:, 3:])
        
        # Coarse space test
        # coarse_inv = np.ones(3) @ linear_and_edges[0:3, 0:3] @ np.ones(3)
        # coarse_trans = np.block([[np.ones((1,3)), np.zeros((1, 3*(p-1)))], [np.zeros((3*(p-1), 3)), np.eye(3*(p-1))]])
        # coarse = np.zeros((1 + 3*(p-1), 1 + 3*(p-1)))
        # coarse[1:1 + (p - 1), 1:1 + (p - 1)] = np.linalg.inv(schur[3:3 + (p - 1), 3:3 + (p - 1)])
        # coarse[1 + (p - 1):1 + 2 * (p - 1), 1 + (p - 1):1 + 2 * (p - 1)] = np.linalg.inv(schur[3 + (p - 1):3 + 2 * (p - 1),
        #                                                                             3 + (p - 1):3 + 2 * (p - 1)])
        # coarse[1 + 2 * (p - 1):1 + 3 * (p - 1), 1 + 2 * (p - 1):1 + 3 * (p - 1)] = np.linalg.inv(
        #     schur[3 + 2 * (p - 1):3 + 3 * (p - 1),
        #     3 + 2 * (p - 1):3 + 3 * (p - 1)])
        # linear_and_edges = coarse_trans.T @ coarse @ coarse_trans

        test = linear_and_edges + transform.T @ d @ transform
        # test = linear_and_edges
        # print((transform.T @ d @ transform)[0:3, 0:3])
        # print((d[0:3, 0:3]))
        # test = linear_and_edges

        if param_vertex == 'star':
            test = linear_and_edges
        if param_vertex == 'dagger':
            test = linear_and_edges

        # print(schur)

        # eigs = np.real(np.linalg.eigvals(np.linalg.solve(test, schur)))
        # eigs = np.real(np.linalg.eigvals(schur))
        eigs = np.real(np.linalg.eigvals(test @ schur))
        # print(eigs)
        # print(max(eigs) / min(eigs), max(eigs), min(eigs))
        print(max(eigs) / min(eigs))
