import numpy as np


def decast(lambdas: np.ndarray, ctrl: np.ndarray):
    """
    de Castlejau algorithm returns control nets to the three subdivided triangles.

    Triangle ABC is oriented such that A is [0,0], B, is [1, 0], C is [0, 1].
    Call the barycentric coordinate L.

    They way we store the indices allow us to have triangle LBC easily; just need to store ALC, ALB.

    :param ctrl: control point in 2d array form
    :param lambdas: barycnetric coordinates
    :return: 4 Numpy arrays
    """

    p = ctrl.shape[0] - 1

    # Storage for two other triangles
    alc = np.zeros_like(ctrl)
    abl = np.zeros_like(ctrl)

    alc[:, 0] = ctrl[:, 0]
    abl[0, :] = ctrl[0, :]

    # Iterate p times
    for r in range(p):
        for order in range(p - r):
            for i in range(order + 1):
                ctrl[i, order - i] = lambdas[2] * ctrl[i, order - i] + lambdas[0] * ctrl[i + 1, order - i] + \
                                     lambdas[1] * ctrl[i, order - i + 1]

        alc[0:p - r, r + 1] = ctrl[0:p - r, 0]
        abl[r + 1, 0:p - r] = ctrl[0, 0:p - r]

    return (ctrl, abl, alc)


def subdivide_2d(ctrl: np.ndarray, abscissa: np.ndarray):
    """


    :param ctrl: control points
    :param abscissa: coordiantes corresponding to the current control points (not hard to calculate...)
    :return:
    """

    # Step 1: Initial decast algorithm
    _, abr, arc = decast([.5, .5, 0], ctrl)

    # Step 2: calculate TBR
    tbr, _, _ = decast([0, .5, .5], abr)

    # Step 3: Calculate SRC:
    src, ars, _ = decast([.5, 0, .5], arc)

    # Step 4: Calculate the two remaining triangles
    trs, _, ats = decast([-1, 1, 1], ars)

    # Step 5: Give correct vertices data
    a = abscissa[0]
    b = abscissa[1]
    c = abscissa[2]
    t = (a + b) / 2
    r = (b + c) / 2
    s = (a + c) / 2

    return (ats, [a, t, s]), (trs, [t, r, s]), (tbr, [t, b, r]), (src, [s, r, c])


def plot_triangle(ctrl: np.ndarray, vertices: np.ndarray, bary1, bary2, bary3):
    """
    Returns 1d arrays of x, y, z for trisurf plotting

    :param ctrl: control points
    :param vertices:
    :return:
    """
    p = ctrl.shape[0] - 1

    xs = vertices[0][0] * bary1 + vertices[1][0] * bary2 + vertices[2][0] * bary3
    ys = vertices[0][1] * bary1 + vertices[1][1] * bary2 + vertices[2][1] * bary3
    return np.rot90(xs, -1)[np.triu_indices(p + 1)], np.rot90(ys, -1)[np.triu_indices(p + 1)], np.rot90(ctrl, -1)[
        np.triu_indices(p + 1)]


def return_triangulation(p):
    out = list()

    # Create a 2D matrix for helping (only need to run this once, so it shouldn't be bad)
    helper = np.zeros((p+1, p+1), dtype=int)
    helper[np.triu_indices(p+1)] = range(int((p+1)*(p+2)/2))

    # Now create top triangles
    for i in range(p):
        for j in range(p - i):
            out.append((helper[i, j + i], helper[i, j+i+1], helper[i+1, j+i+1]))

    # Now the more awkward bottom triangles
    for i in range(p-1):
        for j in range(p - 1 - i):
            out.append((helper[i, j + i + 1], helper[i + 1, j + i + 1], helper[i + 1, j + i + 2]))

    return out

def generate_bary(p: int):
    """
    Generates the 3 barycentric matrices
    :param p:
    :return:
    """
    # Generate some barycentric coordinates
    lambda_1 = np.zeros((p + 1, p + 1))
    lambda_2 = np.zeros((p + 1, p + 1))
    lambda_3 = np.zeros((p + 1, p + 1))

    for i in range(p + 1):
        # Because I'm lazy, and numpy is actually stupid for not having off diagonals
        for j in range(p - i + 1):
            lambda_1[j, p - i - j] = i / p
        lambda_2[0:p - i + 1, i] = i / p
        lambda_3[i, 0: p - i + 1] = i / p

    return lambda_1, lambda_2, lambda_3


if __name__ == '__main__':
    # Sample control points (double check if vertices return expected)
    # cpoints = np.array([[1, -2, 3, 4], [2, -1, 1, 0], [3, 2, 0, 0], [-1, 0, 0, 0]], dtype=np.float)
    cpoints = np.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]], dtype=np.float)

    triangulation = return_triangulation(3)

    v = np.array([[.5, .5], [1, 0], [0, 0]])
    v2 = np.array([[.5, .5], [0, 1], [0, 0]])
    bary1, bary2, bary3 = generate_bary(3)

    triangles = subdivide_2d(cpoints, v)

    final = list()
    for t in triangles:
        final.extend(subdivide_2d(t[0], t[1]))

    mplot = False
    if mplot:
        from mpl_toolkits.mplot3d import Axes3D
        import matplotlib.pyplot as plt

        # sns.set()
        fig = plt.figure()
        ax = Axes3D(fig)

        # ax.plot_trisurf(xs, ys, zs)
        for t in final:
            xs1, ys1, zs1 = plot_triangle(t[0], t[1], bary1, bary2, bary3)
            p = ax.plot_trisurf(xs1, ys1, zs1, edgecolor='none', cmap='hot')

            # plt.tricontour(xs1, ys1, zs1)

        # ax.clim(.5, 1.5)
        # ax.set_zlim3d(0.5, 1.5)
        # ax.set_xlim(0, 1)
        # ax.set_ylim(0, 1)
        # ax.set_xlabel("x axis")
        # ax.set_ylabel("y axis")
        ax.view_init(45, -20)


        plt.show()
    else:
        from mayavi import mlab

        for t in final:
            xs1, ys1, zs1 = plot_triangle(t[0], t[1], bary1, bary2, bary3)
            p = mlab.triangular_mesh(xs1, ys1, zs1, triangulation, colormap='hot', vmin=0, vmax=1)

        mlab.show()
