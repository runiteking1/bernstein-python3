import numpy as np
from matplotlib import pyplot as plt


def degree_raise(cpoints: np.ndarray) -> np.ndarray:
    """
    Degree raises the Bernstein control points in 1D. Infers order from length

    :param cpoints: Control points of degree len(cpoints) - 1
    :return: Control points of same Bernstein spline but with 1 higher order
    """
    p = len(cpoints)
    out = np.zeros((p + 1,))

    for j in range(p):
        out[j] += (p - j) / p * cpoints[j]
        out[j + 1] += (j + 1) / p * cpoints[j]

    return out


if __name__ == '__main__':
    # Set a test array as the Bernstein control points
    control_points = np.array([1, 2, -1])

    # Degree raising algorithm basically
    for i in range(10):
        control_points = degree_raise(control_points)

    # Our original function
    x = np.linspace(0, 1)
    ori = 1 + 2 * x - 4 * x * x

    # Results: fairly slow; try something else
    plt.plot(x, ori)
    plt.plot(np.linspace(0, 1, len(control_points)), control_points, 'o-')
    plt.show()
