import numpy as np
from matplotlib import pyplot as plt


def subdivide(ctrl: np.ndarray, p: int) -> np.ndarray:
    """
    Subdivides the interval [0, 1] by half, and stores it into one array

    :param ctrl: control points in order
    :param p: order of problem
    :return: array of twice the size of ctrl with control points (note the overlap in the middle
    """
    out = np.zeros((len(ctrl) * 2,), dtype=np.float)

    num_intervals = int(len(ctrl)/(p + 1))   # Number of total intervals

    # Index of
    index_out_1 = 0
    index_out_2 = p + 1
    index_ctrl = 0

    for k in range(num_intervals):  # For each interval, need to subdivide

        # de Casteljau algorithm, with some extra indexing
        for i in range(p):
            out[index_out_1 + i] = ctrl[index_ctrl]
            for j in range(p - i):
                ctrl[index_ctrl + j] = ctrl[index_ctrl + j] * .5 + .5 * ctrl[index_ctrl + j+1]

        out[index_out_2 - 1] = ctrl[index_ctrl]
        out[index_out_2:index_out_2 + p + 1] = ctrl[index_ctrl:index_ctrl + p + 1]

        index_out_1 += 2 * p + 2
        index_out_2 += 2 * p + 2
        index_ctrl += p + 1

    return out

def create_x(times: int, p: int) -> np.ndarray:
    """
    Return correct x values
    :param times: number subdivision
    :param p: order
    :return: correct stuff to plot x values with
    """
    out = list()
    for i in range(2**times):
        out.append(np.linspace(i/(2**times), (i+1)/(2**times), num = p + 1))

    return np.concatenate(out)

if __name__ == '__main__':
    # Set a test array as the Bernstein control points
    control_points = np.array([1, 2, -1], dtype=np.float)

    # Subdivide a few times
    sub = subdivide(control_points, 2)
    sub = subdivide(sub, 2)

    xs = create_x(2, 2)

    # Our original function
    x = np.linspace(0, 1)
    ori = 1 + 2 * x - 4 * x * x

    # Results: fairly slow; try something else
    plt.plot(x, ori)
    plt.plot(xs, sub, 'o-')
    plt.show()
