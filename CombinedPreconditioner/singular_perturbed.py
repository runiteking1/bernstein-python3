import numpy as np
from scipy.sparse.linalg import spsolve
from tqdm import tqdm

from combined_preconditioner import Preconditioner
from pcg import pcg
from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh


class BoundaryLayer(BernsteinMatrix):
    def __init__(self, mesh: BernsteinMesh):
        """
        Creates the necessary components and non-linear parts for the Brusselator system

        :param mesh:
        """
        super(BoundaryLayer, self).__init__(mesh)

    @staticmethod
    def f(x: np.ndarray) -> float:
        return 1
        # return (-1 + x[0])*x[0] * (-1 + x[1])*x[1]


def wrapper(p: int, subdivide: int, epsilon_squared, verbose=False, name=None, plot=False):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param epsilon_squared: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """
    epsilon = np.sqrt(epsilon_squared)
    r = p
    coords = np.array([[0, 0],
                       [r*epsilon, 0],
                       [r*epsilon, r*epsilon],
                       [0, r*epsilon],
                       [1 - r*epsilon, 0],
                       [1, 0],
                       [1, r*epsilon],
                       [1 - r*epsilon, r*epsilon],
                       [1 - r*epsilon, 1 - r*epsilon],
                       [1, 1 - r*epsilon],
                       [1, 1],
                       [1 - r*epsilon, 1],
                       [0, 1 - r*epsilon],
                       [r*epsilon, 1 - r*epsilon],
                       [r*epsilon, 1],
                       [0, 1],
                       [0.5, 0.5]])
    elnode = np.array([[0, 1, 2],
                       [0, 2, 3],
                       [1, 4, 7],
                       [1, 7, 2],
                       [4, 5, 7],
                       [5, 6, 7],
                       [7, 6, 9],
                       [7, 9, 8],
                       [8, 9, 10],
                       [8, 10, 11],
                       [13, 8, 11],
                       [13, 11, 14],
                       [12, 13, 15],
                       [13, 14, 15],
                       [3, 2, 13],
                       [3, 13, 12],
                       [2, 7, 16],
                       [2, 16, 13],
                       [7, 8, 16],
                       [16, 8, 13]])
    bc = np.array([[0],[1],[4],[5],[6],[9],[10],[11],[14],[15],[12],[3]])

    if verbose:
        print("p = {}, epsilon^2 {}, subdivide of {}".format(p, epsilon_squared, subdivide))

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = BoundaryLayer(mesh)
    fem.mplot = False

    S, M, f = fem.assemble_matrix()
    if verbose:
        print("Generated Matrix")

    # Setup preconditioner
    Solver = Preconditioner(M + epsilon_squared*S, fem)

    from scipy.sparse.linalg import LinearOperator
    import scipy

    def mult(v: np.ndarray) -> np.ndarray:
        return Solver.solve((M + epsilon_squared * S).dot(v))

    PinvS = LinearOperator(dtype=M.dtype, shape=M.shape, matvec=mult)
    w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-9,
                                 maxiter=M.shape[0] * 60, ncv=M.shape[0] * 6)
    large = max(np.real(w))
    # print(large, end = ' ')
    w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=1, maxiter=M.shape[0] * 200,
                                 tol=1E-10, ncv=M.shape[0] * 40)
    small = min(np.real(w))
    # print(small, end = ' ')
    # print(large, small, large / small)
    print("{}\t{}".format(p, large/small))

    # Solver.solve(np.random.random(M.shape[0],))
    # print((M)[0, :])
    # u, it, resid = pcg(M + epsilon_squared * S, f, maxit=8000)
    # print(fem.mesh.dirchlet_bc[0])
    # print(u[fem.mesh.dirchlet_bc[0]])
    #
    # print(np.linalg.norm(Solver.solve(f) - u))
    # print(resid[0])
    # print(it)
    # print(resid[-1])

    if plot:
        u, it, resid = pcg(M + epsilon_squared * S, f, P=Solver, maxit=8000, eps=1e-10)
        print(it)
        print(resid[-1])

        # fem.contour_plot(u)
        # fem.contour_plot(v)
        fem.plot(u, subdivision=4, grad=True)
if __name__ == '__main__':
    # Define constants
    eps_squared = 1e-2
    print(eps_squared)
    wrapper(3, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(4, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(5, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(6, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(7, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(8, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(9, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(10, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(11, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(12, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(13, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(14, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(15, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(16, 0, epsilon_squared=eps_squared, verbose=False, plot=True)
    # wrapper(17, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(18, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(19, 0, epsilon_squared=eps_squared, verbose=False, plot=False)
    # wrapper(20, 0, epsilon_squared=eps_squared, verbose=False, plot=False)