from unittest import TestCase
from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
from combined_preconditioner import Preconditioner

class TestPreconditioner(TestCase):
    def test_solve_one_mass(self):
        import numpy as np
        p = 4

        coords_tri = np.array([[0,0], [1, 0], [0, 1]])
        elnode_tri = np.array([[0,1,2]])
        # bc_tri = np.array([[0],[1],[2]])
        bc_tri = np.array([])

        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=p)
        b = BernsteinMatrix(mesh)

        S, M, _ = b.assemble_matrix()
        f = np.ones((M.shape[0]))
        rhs = M @ f
        Solver = Preconditioner(M, b)

        # np.testing.assert_allclose(Solver.solve(rhs), [1.06666667,1.06666667,1.06666667,  5.25, -5.02222222,  5.25,5.25,-5.02222222,5.25,5.25,-5.02222222, 5.25, - 0.17777778,- 0.17777778, -0.17777778])

    def test_solve_stiff(self):
        import numpy as np
        p = 5

        coords_tri = np.array([[0,0], [1, 0], [0, 1]])
        elnode_tri = np.array([[0,1,2]])
        # bc_tri = np.array([[0],[1],[2]])
        bc_tri = np.array([])

        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=p)
        b = BernsteinMatrix(mesh)

        S, M, _ = b.assemble_matrix()
        f = np.ones((M.shape[0]))
        rhs = M @ f
        Solver = Preconditioner(M, b)
        # np.testing.assert_allclose(Solver.solve(rhs), [1.06666667,1.06666667,1.06666667,  5.25, -5.02222222,  5.25,5.25,-5.02222222,5.25,5.25,-5.02222222, 5.25, - 0.17777778,- 0.17777778, -0.17777778])