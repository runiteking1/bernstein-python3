from scipy.sparse.linalg import LinearOperator, spsolve
from scipy.sparse import csr_matrix, block_diag, lil_matrix, diags
import numpy as np
from scipy.special import binom
from BernsteinFEM import BernsteinMatrix

class Preconditioner(LinearOperator):
    def __init__(self, matrix: np.ndarray, fem: BernsteinMatrix):
        """
        :param matrix: takes in the matrix of the form M + gamma*S
        """
        super(Preconditioner, self).__init__(dtype=matrix.dtype, shape=matrix.shape)

        self.vertices = len(fem.coordinates)        # Number of vertices
        self.p = fem.p
        self.edge_dofs = self.p - 1                 # Number of dofs on edge
        self.elements = fem.elements                # Number of elements
        self.dirchlet_bc = np.array(fem.mesh.dirchlet_bc)

        # Create the interior matrix
        self.internal_dofs = int((self.p + 2) * (self.p + 1) / 2) - 3 * (self.p - 1) - 3
        self.interior = csr_matrix((self.internal_dofs * self.elements, self.internal_dofs * self.elements))
        self.boundary_length = matrix.shape[0] - self.interior.shape[0]
        self.interior = matrix[self.boundary_length:, self.boundary_length:].tocsc()

        # Now we can create the Schur complement
        self.b = matrix[0:self.boundary_length, self.boundary_length:].tocsr()
        self.schur = matrix[0:self.boundary_length, 0:self.boundary_length] - \
                     self.b @ spsolve(self.interior, self.b.T)

        # print('Created Schur')
        # Now we create the block diagonals for the edges
        total_edges = int((self.boundary_length - self.vertices) / self.edge_dofs)
        block_list = list()
        for e in range(total_edges): # Loop through each edge to aggregate the block
            block_list.append(self.schur[self.vertices + self.edge_dofs * e:self.vertices + self.edge_dofs * (e+1),
                              self.vertices + self.edge_dofs * e:self.vertices + self.edge_dofs * (e+1)])

        self.edges = block_diag(block_list).tocsc()

        # Now we have the giant block for the vertices
        self.transform2good = lil_matrix((self.vertices, self.boundary_length))
        self.transform2linear = lil_matrix((self.vertices, self.boundary_length))

        q = int(np.floor(self.p / 2))
        # q = int(self.p/3)
        # q = self.p
        cps = self._cp(np.arange(0, q+1), q)
        for i in range(self.p - q):
            cps = self._raise(cps)
        cps = cps[1:-1]

        # Create transformations for hat functions
        linear_cps = np.array([1, 0])
        for i in range(self.p - 1):
            linear_cps = self._raise(linear_cps)
        linear_cps = linear_cps[1: -1]

        for k in range(fem.elements):
            # Now we deal with the vertices; have to hardcode this... unless we introduce more data structures to fem
            c = np.zeros((fem.p + 1,) * 2)
            c[np.triu_indices(fem.p + 1)] = fem.eldof[k][fem.mesh.c]

            edge1 = np.diag(c)[-2:0:-1].astype(int)  # From ELNODE[k, 0] to ELNODE[k, 1]
            edge2 = c[0, 1:-1].astype(int)  # From ELNODE[k, 1] to ELNODE[k, 2]
            edge3 = c[1:-1, -1].astype(int)  # From ELNODE[k, 2] to ELNODE[k, 0]

            # self.transform2bernstein[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

            if True: # fem.elnode[k, 0] not in self.dirchlet_bc and fem.elnode[k, 1] not in self.dirchlet_bc:
                self.transform2good[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
                self.transform2good[fem.elnode[k, 1], fem.elnode[k, 1]] = 1

                self.transform2linear[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
                self.transform2linear[fem.elnode[k, 1], fem.elnode[k, 1]] = 1

                if fem.elnode[k, 0] not in self.dirchlet_bc:
                    self.transform2good[fem.elnode[k, 0], edge1] = cps
                    self.transform2linear[fem.elnode[k, 0], edge1] = linear_cps

                if fem.elnode[k, 1] not in self.dirchlet_bc:
                    self.transform2good[fem.elnode[k, 1], edge1[::-1]] = cps
                    self.transform2linear[fem.elnode[k, 1], edge1[::-1]] = linear_cps

            if True: # fem.elnode[k, 1] not in self.dirchlet_bc and fem.elnode[k, 2] not in self.dirchlet_bc:
                self.transform2good[fem.elnode[k, 1], fem.elnode[k, 1]] = 1
                self.transform2good[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

                self.transform2linear[fem.elnode[k, 1], fem.elnode[k, 1]] = 1
                self.transform2linear[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

                if fem.elnode[k, 1] not in self.dirchlet_bc:
                    self.transform2good[fem.elnode[k, 1], edge2] = cps
                    self.transform2linear[fem.elnode[k, 1], edge2] = linear_cps

                if fem.elnode[k, 2] not in self.dirchlet_bc:
                    self.transform2linear[fem.elnode[k, 2], edge2[::-1]] = linear_cps
                    self.transform2good[fem.elnode[k, 2], edge2[::-1]] = cps

            if True: # fem.elnode[k, 2] not in self.dirchlet_bc and fem.elnode[k, 0] not in self.dirchlet_bc:
                self.transform2good[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
                self.transform2good[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

                self.transform2linear[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
                self.transform2linear[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

                if fem.elnode[k, 0] not in self.dirchlet_bc:
                    self.transform2good[fem.elnode[k, 0], edge3[::-1]] = cps
                    self.transform2linear[fem.elnode[k, 0], edge3[::-1]] = linear_cps

                if fem.elnode[k, 2] not in self.dirchlet_bc:
                    self.transform2linear[fem.elnode[k, 2], edge3] = linear_cps
                    self.transform2good[fem.elnode[k, 2], edge3] = cps

        # Diagonal good matrix
        # self.vertex_block_good = self.transform2good @ self.schur @ self.transform2good.T
        self.vertex_block_good = diags(csr_matrix(self.transform2good @ self.schur @ self.transform2good.T).diagonal(), 0, format='csr') # Correct mathematical thing with diagonal
        # self.vertex_block_good = self.transform2good @ self.schur @ self.transform2good.T
        # print(self.vertex_block_good[0, 0], end = "\t")
        # print(self.schur.todense())
        # print(self.transform2good.todense())
        # print(self.vertex_block_good.todense())
        # print(self.transform2linear)
        self.vertex_block = self.transform2linear @ self.schur @ self.transform2linear.T
        # self.vertex_block = csr_matrix(np.diag(np.diag(self.transform2linear @ self.schur.todense() @ self.transform2linear.T)))
        # print(self.vertex_block[0, 0], end = "\n")

        self.transform2good = csr_matrix(self.transform2good)
        self.transform2linear = csr_matrix(self.transform2linear)
        
    @staticmethod
    def _abs_deter(coords) -> float:
        # Absolute value of determinant of transformation given coordinates for EDGES!
        return np.abs(.25 * (coords[2, 0] * (coords[0, 1] - coords[1, 1]) + coords[0, 0] * (coords[1, 1] - coords[2, 1])
                             + coords[1, 0] * (coords[2, 1] - coords[0, 1])))

    def _cp(self, j: np.ndarray, q: int) -> np.ndarray:
        """
        Coefficients of converting to Lobatto basis
        :param j:
        :return:
        """
        return (-1.0) ** j * binom(q, q - 1 - j) / q

    def _raise(self, cpoints: np.ndarray) -> np.ndarray:
        """
        Degree raises the Bernstein control points in 1D. Infers order from length

        :param cpoints: Control points of degree len(cpoints) - 1
        :return: Control points of same Bernstein spline but with 1 higher order
        """
        p = len(cpoints)
        out = np.zeros((p + 1,))

        for j in range(p):
            out[j] += (p - j) / p * cpoints[j]
            out[j + 1] += (j + 1) / p * cpoints[j]

        return out

    def solve(self, f: np.ndarray) -> np.ndarray:
        """
        Given Px = f, returns x

        :param f: RHS to solve
        :return: preconditions f
        """
        out = np.zeros_like(f)

        # Invert interior functions first
        out[self.boundary_length:] = spsolve(self.interior, f[self.boundary_length:])

        # Calculate right hand side
        rhs = f[0:self.boundary_length] - self.b.dot(out[self.boundary_length:])
        out[self.vertices:self.boundary_length] = spsolve(self.edges, rhs[self.vertices:])

        # Smoothing step for mass matrix stype
        out[0:self.boundary_length] += self.transform2good.T @ spsolve(self.vertex_block_good,
                                                                        self.transform2good @ rhs[0:self.boundary_length])
        # print("sdf")
        # print(self.vertex_block.todense())

        # Now we have to do vertices for stiffness matrix
        out[0:self.boundary_length] += self.transform2linear.T @ spsolve(self.vertex_block,
                                                                         self.transform2linear @ rhs[0:self.boundary_length])

        # Simple averaging on domain
        # coarse_lhs = np.ones(self.vertices) @ self.vertex_block @ np.ones(self.vertices)
        # coarse_rhs = np.ones(self.vertices) @ self.transform2linear @ rhs[0:self.boundary_length]
        # out[0:self.boundary_length] += self.transform2good.T @ \
        #                                np.ones(self.vertices) * coarse_rhs/coarse_lhs

        # Take care of boundary condition
        # try:
        #     out[self.dirchlet_bc] = f[self.dirchlet_bc]
        # except:
        #     print("Somethign weird happened")
        #     pass

        # Back solve
        out[self.boundary_length:] -= spsolve(self.interior, self.b.T @ out[0:self.boundary_length])

        return out

    def _matvec(self, x):
        return self.solve(x)

if __name__ == '__main__':
    import numpy as np
    import scipy
    from BernsteinFEM import BernsteinMatrix
    from bernstein_mesh import BernsteinMesh

    # np.set_printoptions(precision=2)
    # order = range(3, 25)
    order = [5, 10, 15]
    for p in range(4, 9):
        print(p, end='\t')
        coords_tri = 0.001*np.array([[-1, -1], [1, -1], [-1, 1]])
        elnode_tri = np.array([[0, 1, 2]])
        # bc_tri = np.array([[0],[1],[2]])
        bc_tri = np.array([])

        coords_tri = np.array([[-100, -100], [0, 0], [100, 100], [100, -100], [-100, 100]])
        elnode_tri = np.array([[1, 2, 5], [1, 4, 2], [2, 4, 3], [3, 5, 2]])
        # bc_tri = np.array([[0], [2], [3], [4]])
        # bc_tri = np.array([])
        elnode_tri -= 1

        # Two elements with bad ratios
        # coords_tri = np.array([[-1, -1], [1, -1], [-1, 1], [2.5, 2.5]])
        # elnode_tri = np.array([[0, 1, 2], [1, 2, 3]])
        # bc_tri = np.array([[0],[1],[2]])
        # bc_tri = np.array([])

        mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=p, subdiv=2)
        b = BernsteinMatrix(mesh)

        S, M, _ = b.assemble_matrix()
        ones = np.ones(M.shape[0])

        def operator(mu = 0.001):
            return 1*M + 1*S
            # return (M + mu*S)/(1 + mu)
        # print(operator() @ np.ones(M.shape[0]))

        Solver = Preconditioner(operator(), b)
        # print(Solver.dot(np.ones(M.shape[0])))
        def mult_precond(v: np.ndarray) -> np.ndarray:
            return Solver.solve(operator()@v)

        PinvS = LinearOperator(dtype=M.dtype, shape=M.shape, matvec=mult_precond)

        w = scipy.sparse.linalg.eigs(PinvS, which='LM', return_eigenvectors=False, k=1, tol=1E-8,
                                 maxiter=M.shape[0] * 50, ncv=M.shape[0] * 5)
        large = max(np.real(w))

        w = scipy.sparse.linalg.eigs(PinvS, which='SM', return_eigenvectors=False, k=2, maxiter=M.shape[0] * 100,
                                 tol=1E-9, ncv=M.shape[0] * 30)
        # print(w)
        small = max(np.real(w))

        print("{:1.2f}".format((large / small)), end='\n')
