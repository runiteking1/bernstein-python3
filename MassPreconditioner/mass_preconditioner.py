from scipy.sparse.linalg import LinearOperator
import numpy as np
from BernsteinFEM import BernsteinMatrix
from scipy.special import binom
import scipy.sparse as scsp

from schur_complement import CInverse
import CythonizedKernels.edge_c as edge_c

from tqdm import tqdm


class MassPreconditioner(LinearOperator):
    """
    Preconditioner for the the Mass Matrix using strict additive Schwarz theory
    """

    def __init__(self, m: np.ndarray, fem: BernsteinMatrix):
        super(MassPreconditioner, self).__init__(dtype=m.dtype, shape=m.shape)

        self.vertices = len(fem.coordinates)  # Number of vertices
        self.p = fem.p
        self.edge_dofs = self.p - 1

        # Method to invert the block diagonals of interior
        self.cinv = CInverse(m, fem)
        self.ns = m.shape[0] - fem.elements * self.cinv.internal_dofs
        self.b = m[0:self.ns, self.ns:m.shape[0]].tocsr()

        # We need to also set the Dirchlet boundary conditions
        self.dirchlet_bc = fem.mesh.dirchlet_bc

        # Now create boundary preconditioner
        self.boundary_length = m.shape[0] - self.cinv.shape[0]
        self.num_edges = int((self.boundary_length - self.vertices) / self.edge_dofs)
        self.transform2bernstein = scsp.lil_matrix((self.vertices, self.boundary_length))
        self.vertex_scaling = np.zeros((self.vertices,))  # Scaling for vertex
        diags = np.zeros(self.boundary_length, )

        # Vertex functions are a bit odd now, need to adjust
        # cps = self._cp(np.arange(1, self.p), self.p) # Old basis
        q = int(np.floor(self.p / 2))
        cps = self._cp(np.arange(0, q + 1), q)
        for i in range(self.p - q):
            cps = self._raise(cps)
        cps = cps[1:-1]

        for k in range(fem.elements):
            # First find the determinant, and hence the diagonal elements
            values = self._diagonal(self._abs_deter(
                np.array([fem.coordinates[fem.elnode[k, 0]], fem.coordinates[fem.elnode[k, 1]],
                          fem.coordinates[fem.elnode[k, 2]]])))

            # print(np.array([fem.coordinates[fem.elnode[k, 0]], fem.coordinates[fem.elnode[k, 1]],
            #                 fem.coordinates[fem.elnode[k, 2]]]))
            # print(self._abs_deter(np.array([fem.coordinates[fem.elnode[k, 0]], fem.coordinates[fem.elnode[k, 1]],
            #                                 fem.coordinates[fem.elnode[k, 2]]])))

            # Now we put it into the list
            diags[sorted(fem.eldof[k][3:(3 + 3 * self.edge_dofs)])] += np.concatenate((values,) * 3)

            # Vertex scaling area
            self.vertex_scaling[fem.elnode[k, :]] += 16 * self._abs_deter(
                np.array([fem.coordinates[fem.elnode[k, 0]], fem.coordinates[fem.elnode[k, 1]],
                          fem.coordinates[fem.elnode[k, 2]]]))

            # Now we deal with the vertices; have to hardcode this... unless we introduce more data structures to fem
            c = np.zeros((fem.p + 1,) * 2)
            c[np.triu_indices(fem.p + 1)] = fem.eldof[k][fem.mesh.c]

            edge1 = np.diag(c)[-2:0:-1].astype(int)  # From ELNODE[k, 0] to ELNODE[k, 1]
            edge2 = c[0, 1:-1].astype(int)  # From ELNODE[k, 1] to ELNODE[k, 2]
            edge3 = c[1:-1, -1].astype(int)  # From ELNODE[k, 2] to ELNODE[k, 0]

            # self.transform2bernstein[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

            if fem.elnode[k, 0] not in self.dirchlet_bc and fem.elnode[k, 1] not in self.dirchlet_bc:
                self.transform2bernstein[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
                self.transform2bernstein[fem.elnode[k, 1], fem.elnode[k, 1]] = 1

                self.transform2bernstein[fem.elnode[k, 0], edge1] = cps
                self.transform2bernstein[fem.elnode[k, 1], edge1[::-1]] = cps

            if fem.elnode[k, 1] not in self.dirchlet_bc and fem.elnode[k, 2] not in self.dirchlet_bc:
                self.transform2bernstein[fem.elnode[k, 1], fem.elnode[k, 1]] = 1
                self.transform2bernstein[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

                self.transform2bernstein[fem.elnode[k, 1], edge2] = cps
                self.transform2bernstein[fem.elnode[k, 2], edge2[::-1]] = cps

            if fem.elnode[k, 2] not in self.dirchlet_bc and fem.elnode[k, 0] not in self.dirchlet_bc:
                self.transform2bernstein[fem.elnode[k, 0], fem.elnode[k, 0]] = 1
                self.transform2bernstein[fem.elnode[k, 2], fem.elnode[k, 2]] = 1

                self.transform2bernstein[fem.elnode[k, 0], edge3[::-1]] = cps
                self.transform2bernstein[fem.elnode[k, 2], edge3] = cps

        # Vertex solve scaling
        self.vertex_scaling = 1 / self.vertex_scaling  # need to invert

        # Aggregate data for edge solves
        self.inverse_diagonals = np.zeros(self.boundary_length)
        self.inverse_diagonals[self.vertices:] = 1 / diags[
                                                     self.vertices:]  # Not memory efficient, but easiest way to proceed
        self.vector_lengths = len(self.inverse_diagonals)

        # Do a resacle to fit more the math
        self.transform2bernstein = self.transform2bernstein.T

        # Convert back to sparse
        self.transform2bernstein = scsp.csr_matrix(self.transform2bernstein)

        # Generate coefficients
        self.algo_coefs = np.zeros((self.p - 1, self.p - 1))
        for n in range(self.p - 1):
            for i in range(n + 1):
                self.algo_coefs[n, i] = (-1) ** (n - i) * binom(n + 2, i + 2) * (n + 1) * (n + 2) / (
                        (n - i + 1) * (n - i + 2))

        self.cinv_coefs = np.zeros((self.p - 1, self.p - 1))
        for i in range(self.p - 2, -1, -1):
            for j in range(i + 1):
                self.cinv_coefs[i, j] = (-1) ** (i - j) * (j + 1) * binom(i + 2, j + 2) / (i - j + 2)

        # Finally, create helper arrays for the Cython calls
        self.subworker = np.zeros((self.edge_dofs + 1,), dtype=np.double)
        self.worker = np.zeros((self.edge_dofs,), dtype=np.double)

        ## Testing code
        # Construct vertex block
        # s = m[:self.ns, :self.ns] - self.b.dot(np.linalg.solve(m[self.ns:, self.ns:].todense(), self.b.T.todense()))
        # print(self.transform2bernstein.todense().T.dot(s[0:self.ns, 0:self.ns].dot(self.transform2bernstein.todense())))

    def _diagonal(self, det: float):
        """
        Calculates the diagonal indices (with scaling) of edge elements

        :param det: determinent from "abs_deter"
        :return:
        """
        indices = np.arange(self.p - 1)
        return det * 2 * self._mu(indices) / ((self.p + indices + 4) * (self.p - indices - 1))

    @staticmethod
    def _abs_deter(coords) -> float:
        # Absolute value of determinant of transformation given coordinates for EDGES!
        # return np.abs(.25 * (coords[2, 0] * (coords[0, 1] - coords[1, 1]) + coords[0, 0] * (coords[1, 1] - coords[2, 1])
        #                      + coords[1, 0] * (coords[2, 1] - coords[0, 1])))
        t0 = -(coords[0, :] - coords[1, :])
        t1 = -(coords[0, :] - coords[2, :])
        t2 = -(coords[1, :] - coords[2, :])

        return area(t0, t1, t2) / 2

    @staticmethod
    def _mu(i: np.ndarray) -> np.ndarray:
        """
        See paper
        :param i:
        :return:
        """
        return 32 * (i + 1) * (i + 2) / ((2 * i + 5) * (i + 3) * (i + 4))

    def _cp(self, j: np.ndarray, q: int) -> np.ndarray:
        """
        Coefficients of converting to Lobatto basis
        :param j:
        :return:
        """
        return (-1.0) ** j * binom(q, q - 1 - j) / q

    def _raise(self, cpoints: np.ndarray) -> np.ndarray:
        """
        Degree raises the Bernstein control points in 1D. Infers order from length

        :param cpoints: Control points of degree len(cpoints) - 1
        :return: Control points of same Bernstein spline but with 1 higher order
        """
        p = len(cpoints)
        out = np.zeros((p + 1,))

        for j in range(p):
            out[j] += (p - j) / p * cpoints[j]
            out[j + 1] += (j + 1) / p * cpoints[j]

        return out

    def _matvec(self, x):
        return self.solve(x)

    def solve(self, f: np.ndarray) -> np.ndarray:
        """
        Given Px = f, returns x

        :param f: RHS to Solve
        :type f: np.ndarray
        :return: Solution to Px = f
        """
        out = np.zeros(self.shape[0], )

        # Solve interior
        out[self.boundary_length:] = self.cinv.dot(f[self.boundary_length:])
        # Calculate RHS for \tilde X solves
        rhs = f[0:self.boundary_length] - self.b.dot(out[self.boundary_length:])
        edge_c.process(self.num_edges, self.edge_dofs, self.inverse_diagonals[self.vertices:],
                       rhs[self.vertices:],
                       self.subworker, self.worker, self.cinv_coefs, self.algo_coefs,
                       out[self.vertices:self.boundary_length])
        # Solve vertices
        out[0:self.boundary_length] += \
            self.transform2bernstein.dot(
                self.vertex_scaling * (self.p ** 4) * self.transform2bernstein.T.dot(rhs[0:self.boundary_length]))
        # print(self.transform2bernstein.todense())
        # print(out)
        # out[0:self.vertices] += self.vertex_scaling * (self.p ** 3) * rhs[0:self.vertices];
        # try:
        out[self.dirchlet_bc] = f[self.dirchlet_bc]
        # except IndexError:
        #     # Very odd cases where sometimes Python does not cooperate...
        #     for i in range(len(self.dirchlet_bc)):
        #         out[self.dirchlet_bc[i]] = f[self.dirchlet_bc[i]]

        # Now backsolve after the change of basis
        # print(out)
        # print(self.cinv(self.b.T.dot(out[0:self.boundary_length])))
        out[self.boundary_length:] -= self.cinv(self.b.T.dot(out[0:self.boundary_length]))

        return out


def area(t0, t1, t2) -> float:
    """
        Calculates the area of a triangle usign Heron's formula

        We need this for the 3D code

        :type t0: np.ndarray
        :type t1: np.ndarray
        :type t2: np.ndarray
        :param t0: side 0 (numpy vector)
        :param t1: side 1 (numpy vector)
        :param t2: side 2 (numpy vector)
        :return: area of triangle
        """
    t0 = np.linalg.norm(t0)
    t1 = np.linalg.norm(t1)
    t2 = np.linalg.norm(t2)
    s = (t0 + t1 + t2) / 2
    return np.sqrt(s * (s - t0) * (s - t1) * (s - t2))

if __name__ == '__main__':
    # 2 Triangles
    # coords_tri = np.array([[-1, -1], [1, -1], [-1, 1], [1, 1]])
    # elnode_tri = np.array([[0, 1, 2], [1, 2, 3]])
    coords_tri = np.array([[-1, -1], [1, -1], [-1, 1]])
    elnode_tri = np.array([[0, 1, 2]])

    bc_tri = np.array([])

    from bernstein_mesh import BernsteinMesh
    mesh = BernsteinMesh(coords_tri, elnode_tri, bc_tri, p=4, subdiv=0)
    b = BernsteinMatrix(mesh)

    np.set_printoptions(precision=4, linewidth=500, threshold=np.inf)
    S, M, f = b.assemble_matrix()
    precond = MassPreconditioner(M, b)

    x = np.zeros_like(f)
    for i in range(f.shape[0]):
        x[i] = i
    print(precond.dot(x))
    # print(f.T)
    # print(b.massmult(f))
    # B = M[0:M.shape[0] - int(b.elements * (b.p-2) * (b.p - 1)/2), M.shape[0] - int(b.elements * (b.p-2) * (b.p - 1)/2):]
    # print(M.dot(f))
    # print(B.dot(f[M.shape[0] - int(b.elements * (b.p-2) * (b.p - 1)/2):]))
    # print(f)
    # u = np.linalg.solve(M.todense(), f)
    # print((b.stiffmult(f)))
    # print(S.dot(f))
    # print(max(np.real(np.linalg.eigvals(M.todense())))/min(np.real(np.linalg.eigvals(M.todense()))))
    # alphas = np.linalg.solve(S.todense(), f)
    # print(alphas)
    # print("Energy:", np.dot(f.T, alphas))
    # b.plot(alphas, subdivision=3, grad=True)
