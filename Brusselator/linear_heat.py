import numpy as np
from scipy.sparse.linalg import spsolve
from tqdm import tqdm

from mass_preconditioner import MassPreconditioner
from pcg import pcg
from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
# from evaluate_bernstein import evaluate
# from force_element import moment
import CythonizedKernels.nonlinear_c as nonlinear_c


class LinearHeat(BernsteinMatrix):
    def __init__(self, mesh: BernsteinMesh):
        """
        Creates the necessary components and non-linear parts for the Brusselator system

        :param mesh:
        """
        super(LinearHeat, self).__init__(mesh)

    @staticmethod
    def init_u(x: np.ndarray) -> float:
        return np.exp(-(x[0]*x[0] + x[1] * x[1]))


def wrapper(p: int, subdivide: int, delta_t=0.01, verbose=False, name=None, plot=False, animate=False):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """

    # Setup on output file
    if name is None:
        name = "data/data_%d_%d_%.1e.dat" % (p, subdivide, delta_t)

    if verbose:
        print("p = {}, time step of {}, subdivide of {}".format(p, delta_t, subdivide))

    output = open(name, 'w')

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = LinearHeat(mesh)
    fem.mplot = False

    S, M, f = fem.assemble_matrix()
    if verbose:
        print("Generated Matrix")

    # Setup preconditioner
    Solver = MassPreconditioner(M, fem)

    # Set initial conditions
    u_initial_rhs = fem.l2_projection(fem.init_u)
    u = spsolve(M, u_initial_rhs)

    if verbose:
        print("Initial Conditions set")

    all_its = list()
    for step in tqdm(range(int(np.rint(1 / delta_t)))):
        # First solve u
        u, it, resid = pcg(M + delta_t / 2 * S, M.dot(u) + delta_t * (- 1 / 2 * S.dot(u)), P=Solver)

        # Only record values for v since we're only animating v
        if verbose:
            print(step, it, resid, file=output)
            all_its.append(it)

        if animate:
            fem.plot(u, save=True, index=step)

    if verbose:
        import statistics
        print("Min, Median, Max: [{}, {}, {}]".format(min(all_its), int(statistics.median(all_its)), max(all_its)))

    if plot:
        fem.plot(u, subdivision=1)


if __name__ == '__main__':
    coords = np.array([[-1, -1], [0, 0], [1, 1], [1, -1], [-1, 1]])
    elnode = np.array([[0, 3, 1], [0, 4, 1], [2, 3, 1], [2, 4, 1]])
    bc = np.array([])

    # wrapper(4, 1, delta_t=.1, verbose=True, animate=False, plot=True)
    # wrapper(8, 1, delta_t= 1/160, verbose=True, animate=False, plot=True)
    # wrapper(12, 1, delta_t=0.00123457, verbose=True, animate=False, plot=True)
    # wrapper(16, 1, delta_t=0.000390625, verbose=True, animate=False, plot=True)
    # wrapper(20, 1, delta_t=.1/6250, verbose=True, animate=False, plot=True)

    # wrapper(4, 1, delta_t=.1, verbose=True, animate=False, plot=True)
    # wrapper(8, 1, delta_t= 1/40, verbose=True, animate=False, plot=True)
    # wrapper(12, 1, delta_t=1/90, verbose=True, animate=False, plot=True)
    # wrapper(16, 1, delta_t=1/160, verbose=True, animate=False, plot=True)
    # wrapper(20, 1, delta_t=1/250, verbose=True, animate=False, plot=True)


    #
    # wrapper(4, 1, delta_t=.005, verbose=True, animate=False, plot=True)
    # wrapper(8, 1, delta_t=.005, verbose=True, animate=False, plot=True)
    # wrapper(12, 1, delta_t=.005, verbose=True, animate=False, plot=True)
    # wrapper(16, 1, delta_t=.005, verbose=True, animate=False, plot=True)
    # wrapper(20, 1, delta_t=.005, verbose=True, animate=False, plot=True)
    #
    # wrapper(4, 1, delta_t=.01, verbose=True, animate=False, plot=True)
    # wrapper(8, 1, delta_t=.01, verbose=True, animate=False, plot=True)
    # wrapper(12, 1, delta_t=.01, verbose=True, animate=False, plot=True)
    # wrapper(16, 1, delta_t=.01, verbose=True, animate=False, plot=True)
    # wrapper(20, 1, delta_t=.01, verbose=True, animate=False, plot=True)
    #
    # wrapper(4, 1, delta_t=.02, verbose=True, animate=False, plot=True)
    # wrapper(8, 1, delta_t=.02, verbose=True, animate=False, plot=True)
    # wrapper(12, 1, delta_t=.02, verbose=True, animate=False, plot=True)
    # wrapper(16, 1, delta_t=.02, verbose=True, animate=False, plot=True)
    # wrapper(20, 1, delta_t=.02, verbose=True, animate=False, plot=True)