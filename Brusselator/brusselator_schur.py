import numpy as np
from scipy.sparse.linalg import spsolve
from tqdm import tqdm

from pcg import pcg
from BernsteinFEM import BernsteinMatrix
from bernstein_mesh import BernsteinMesh
# from evaluate_bernstein import evaluate
# from force_element import moment
import CythonizedKernels.nonlinear_c as nonlinear_c
from mass_solver import MassSolver


class BrusselatorSystem(BernsteinMatrix):
    def __init__(self, mesh: BernsteinMesh):
        """
        Creates the necessary components and non-linear parts for the Brusselator system

        :param mesh:
        """
        super(BrusselatorSystem, self).__init__(mesh)

    def nonlinear(self, u: np.ndarray, v: np.ndarray) -> np.ndarray:
        """
        Nonlinear u^2v generator
        :param u: u
        :param v: v
        :return: u^2v in Bernstein coordinates
        """
        out = np.zeros((self.eldof.max() + 1,))

        c = np.zeros((self.p + 1,) * 2)
        d = np.zeros((self.p + 1,) * 2)
        for k in range(self.elements):
            # Find side lengths for later
            t0 = -(self.coordinates[int(self.elnode[k, 0])] - self.coordinates[int(self.elnode[k, 1])])
            t1 = -(self.coordinates[int(self.elnode[k, 1])] - self.coordinates[int(self.elnode[k, 2])])
            t2 = -(self.coordinates[int(self.elnode[k, 2])] - self.coordinates[int(self.elnode[k, 0])])
            area = self.area(t0, t1, t2)

            # Put u and v into a form that I can evaluate function
            c[np.triu_indices(self.p + 1)] = u[self.eldof[k]][self.mesh.c]
            c = np.fliplr(c)
            d[np.triu_indices(self.p + 1)] = v[self.eldof[k]][self.mesh.c]
            d = np.fliplr(d)
            c = np.ascontiguousarray(c)
            d = np.ascontiguousarray(d)

            U = nonlinear_c.evaluate(c, self.q, self.p, 2, self.xis)
            V = nonlinear_c.evaluate(d, self.q, self.p, 2, self.xis)

            F = (U ** 2) * V

            outk = 2 * area * nonlinear_c.moment(F, self.q, self.p, 2, self.xis, self.omegas)

            for i in range(self.mesh.dofs_per_element):
                out[self.eldof[k, i]] += outk[self.mesh.lookup_dof(i)[:-1]]

        return out

    @staticmethod
    def init_u(x: np.ndarray) -> float:
        return .5 + x[1]

    @staticmethod
    def init_v(x: np.ndarray) -> float:
        return 1 + 5 * x[0]


def wrapper(p, subdivide, delta_t=0.05, verbose=False, name=None, plot=False, animate=False):
    """
    Wrapper function to help with running Brusselator code

    :param animate: boolean to save a plot each time iteration for plotting
    :param plot: boolean to plot
    :param name: file output name with data
    :param delta_t: Time step parameter
    :param verbose: verbage switch
    :param p: order
    :param subdivide: how many times to subdivide square
    """
    # Setup on output file
    if name is None:
        name = "data/data_%d_%d_%.1e.dat" % (p, subdivide, delta_t)

    output = open(name, 'w')

    mesh = BernsteinMesh(coords, elnode, bc, p=p, subdiv=subdivide)
    fem = BrusselatorSystem(mesh)

    S, M, f = fem.assemble_matrix()

    if verbose:
        print("Generated Matrix")

    # Setup preconditioner
    Solver = MassSolver(M, fem)

    # Set initial conditions
    u_initial_rhs = fem.l2_projection(fem.init_u)
    v_initial_rhs = fem.l2_projection(fem.init_v)
    u = spsolve(M, u_initial_rhs)
    v = spsolve(M, v_initial_rhs)
    bw = b * f  # Vector for (b, w) in variational form (constant)

    if verbose:
        print("Initial Conditions set")

    for step in tqdm(range(int(np.rint(1 / delta_t)))):
        # First do nonlinear
        u2v = fem.nonlinear(u, v)

        # First solve u
        u, it, resid = pcg(M * (1 + delta_t * (a + 1)) + delta_t * alpha / 2 * S,
                           M.dot(u) + delta_t * (bw + u2v - alpha / 2 * S.dot(u)), P=Solver, x0=u)

        # Now solve v
        v, it, resid = pcg(M + delta_t * alpha / 2 * S,
                           M.dot(v) + delta_t * (a * M.dot(u) - u2v - alpha / 2 * S.dot(u)), P=Solver, x0=v)

        # Only record values for v since we're only animating v
        print(step, it, resid, file=output)

        if animate:
            fem.contour_plot(v, save=True, index=step)

    if plot:
        fem.contour_plot(v)
        fem.contour_plot(u)


if __name__ == '__main__':
    # Define constants
    a = 3.4
    b = 1
    alpha = 0.002

    coords = np.array([[0, 0], [0.5, 0.5], [1, 1], [1, 0], [0, 1]])
    elnode = np.array([[0, 3, 1], [0, 1, 4], [2, 1, 3], [2, 4, 1]])
    bc = np.array([])

    print("This is the Schur Complement preconditioner file; not the correct one!")
    wrapper(12, 2, verbose=False, animate=False, plot=True)
